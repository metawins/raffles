// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
import { network, run } from "hardhat";
import { getVRFConfig, IVRFConfig } from "../constants/constants";
import { keccak256, toBytes } from "viem";
import hre from "hardhat";
import { GetContractReturnType, GetTransactionReturnType, PublicClient } from "@nomicfoundation/hardhat-viem/types";


const OPERATOR_WALLET = "0x13503b622abc0bd30a7e9687057df6e8c42fb928";
const OPERATOR_HASH = keccak256(toBytes("OPERATOR"));

let constructorArguments: any[];
async function verify(instance: GetContractReturnType) {

    await run(`verify:verify`, {
        address: instance.address,
        constructorArguments: constructorArguments
    });
}

async function postdeployment({ contract, deploymentTransaction }: { contract: GetContractReturnType, deploymentTransaction: GetTransactionReturnType }, name: string, client : PublicClient) {

    console.log("Deploying of " + name + " on chain " + network.name);
    await client.waitForTransactionReceipt({hash: deploymentTransaction.hash, onReplaced: replacement => console.log("replaced " + replacement), confirmations: 3});
    
    const grantRoleHash = await contract.write.grantRole([OPERATOR_HASH, OPERATOR_WALLET], { gasMultiplier: 4});
    await client.waitForTransactionReceipt({hash: grantRoleHash, onReplaced: replacement => console.log("replaced " + replacement), confirmations: 1});

    await verify(contract);

    console.log(name + ` deployed to ${contract.address}` + " in chain " + network.name);    
}

async function main() {

    const params = getVRFConfig(network.config.chainId || 11155111)

    const publicClient = await hre.viem.getPublicClient();
    
    constructorArguments = [params.vrfCoordinator, params.keyHash, params.subId, params.numberOfConfirmations, params.gasLimit, params.numWords];

    if (params === undefined) {
        console.error(`No VRF configuration found for chain ID ${network.config.chainId || 1}`);
        return;
    }

    console.log("starting deployment using params :" + JSON.stringify(params));

    const erc20Comp = await hre.viem.sendDeploymentTransaction("ERC20Comp" as string, constructorArguments);
    await postdeployment(erc20Comp, "ERC20Comp", publicClient);

    const nftSingleNoGatingSevPrices = await hre.viem.sendDeploymentTransaction("NFTComp" as string, constructorArguments);
    await postdeployment(nftSingleNoGatingSevPrices, "NFTComp", publicClient);

    const nftGatingComp = await hre.viem.sendDeploymentTransaction("NFTGatingComp" as string, constructorArguments);
    await postdeployment(nftGatingComp, "NFTGatingComp", publicClient);

    const ETHComp = await hre.viem.sendDeploymentTransaction("ETHComp" as string, constructorArguments);
    await postdeployment(ETHComp, "ETHComp", publicClient);

    const h2hComp = await hre.viem.sendDeploymentTransaction("H2HComp" as string, constructorArguments);
    await postdeployment(h2hComp, "H2HComp", publicClient);

    const multiNFTGatedERC20Comp = await hre.viem.sendDeploymentTransaction("MultiNFTGatedERC20Comp" as string, constructorArguments);
    await postdeployment(multiNFTGatedERC20Comp, "MultiNFTGatedERC20Comp", publicClient);

    const multiNFTGatedETHComp = await hre.viem.sendDeploymentTransaction("MultiNFTGatedETHComp" as string, constructorArguments);
    await postdeployment(multiNFTGatedETHComp, "MultiNFTGatedETHComp", publicClient);

    const multiNFTGatedETHFixedComp = await hre.viem.sendDeploymentTransaction("MultiNFTGatedETH" as string, constructorArguments);
    await postdeployment(multiNFTGatedETHFixedComp, "MultiNFTGatedETHFixedComp", publicClient);

    const multiNFTGatedComp = await hre.viem.sendDeploymentTransaction("MultiNFTGatedComp" as string, constructorArguments);
    await postdeployment(multiNFTGatedComp, "MultiNFTGatedComp", publicClient);

}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
    console.error(error);
    process.exitCode = 1;
});