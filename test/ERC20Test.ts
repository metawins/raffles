import hre from "hardhat";
import { expect } from "chai";
import { loadFixture } from "@nomicfoundation/hardhat-toolbox-viem/network-helpers"; 
import { getVRFConfig, getConstructorArgs, IVRFConfig, zeroAddress } from "../constants/constants";
import { decodeEventLog, keccak256, toBytes, createTestClient, http, publicActions, parseUnits } from "viem";
import { GetContractReturnType, WalletClient } from "@nomicfoundation/hardhat-viem/types";
import { hardhat } from "viem/chains"

////////// SEPOLIA VRF CONSTANTS //////////////
const params: IVRFConfig = getVRFConfig(11155111); // SEPOLIA_CHAIN_ID
const constructorArguments = getConstructorArgs(params);
////////// SEPOLIA VRF CONSTANTS //////////////

  enum STATUS {
    CREATED, // the operator creates the raffle
    ACCEPTED, // the seller stakes the nft for the raffle
    EARLY_CASHOUT, // the seller wants to cashout early
    CANCELLED, // the operator cancels the raffle and transfer the remaining funds after 30 days passes
    CLOSING_REQUESTED, // the operator requests to close the raffle and start the VRF
    ENDED, // the raffle is finished, and NFT and funds were transferred
    CANCEL_REQUESTED, // operator asks to cancel the raffle. Players has 30 days to ask for a refund
    WINNER_DETERMINED // the vrf sets a winner
  }

type IEntryInfo = {
  requireWhitelisting: boolean;
  status: STATUS;
  walletsCap: number;
  entriesLength: number;
  amountRaised: number;
}

type IEntriesList = {
  player: string;
  currentEntriesLength: number;
}

type IFundingList = {
  desiredFundsInWeis: number;
  fundingRaisedInWeis: number;
}

type RaffleStruct = {
  prizeNumber: number;
  platformPercentage: number;
  prizeAddress: string;
  winner: string;
  randomNumber: number;
}

const testClient = createTestClient({
  chain: hardhat,
  mode: 'hardhat',
  transport: http(),
})
  .extend(publicActions)

const ONLY_DIRECTLY = 0;

/// Deployment function to setup the initial state ///

const deploy = async () => {
  const basicNFT = await hre.viem.deployContract("Basic721");
  const basicToken = await hre.viem.deployContract("BasicERC20");
  const erc20Comp = await hre.viem.deployContract("MockERC20Comp" as string, constructorArguments);

  return [basicNFT, basicToken, erc20Comp];
};

let [basicNFT, basicToken, erc20Comp] : GetContractReturnType[] = [];
let [owner, player1, player2, player3, player4, player5, player6, seller, operator] : WalletClient[] = [];
let idRaffle: number, receipt;
let entryInfoStruct: IEntryInfo;

async function getRaffleId() {
  const filter = await testClient.createContractEventFilter({
    abi: erc20Comp.abi,
    address: erc20Comp.address,
    eventName: "RaffleCreated",
  });
  const logs = await testClient.getFilterLogs({filter});
  const topic = logs[0].topics[1] as '0x${string}';
  //Decode the event log to get the raffle id from the event
  const id = decodeEventLog({ abi: erc20Comp.abi, eventName: 'RaffleCreated', topics: [topic], strict: false });
  idRaffle = id.args ? id.args[0] : 0; // get the raffle id from the first argument of the topic
  return idRaffle;
}

async function getLastEntry(raffleId: number) {
  //check player3 owns the last entry, by getting the entries length from the raffle
  entryInfoStruct = await erc20Comp.read.rafflesEntryInfo([raffleId]) as IEntryInfo;
  const entriesLength = entryInfoStruct.entriesLength;
  const lastEntry = await erc20Comp.read.entriesList([raffleId, entriesLength - 1]) as IEntriesList;

  return lastEntry;
}

describe("ERC20", function () {

  this.beforeAll(async function () {
    [basicNFT, basicToken, erc20Comp] = await loadFixture(deploy);

    [
      owner,
      player1,
      player2,
      player3,
      player4,
      player5,
      player6,
      seller,
      operator,
    ] = await hre.viem.getWalletClients();
  });

  it("Should mint tokens and configure", async function () {
    
    await basicNFT.write.mint([seller.account?.address], { from : owner.account?.address });
    await basicNFT.write.mint([seller.account?.address], { from : owner.account?.address });
    await basicNFT.write.mint([seller.account?.address], { from : owner.account?.address });
    await basicNFT.write.mint([seller.account?.address], { from : owner.account?.address });
    await basicNFT.write.mint([seller.account?.address], { from : owner.account?.address });
    await basicNFT.write.mint([seller.account?.address], { from : owner.account?.address });
    await basicNFT.write.mint([seller.account?.address], { from : owner.account?.address });

  });

  it("Should add operator role directly", async function () {
    const operatorHash = keccak256(toBytes("OPERATOR"));
    await erc20Comp.write.grantRole([operatorHash, operator.account?.address], { from : owner.account?.address });

    expect(await erc20Comp.read.hasRole([operatorHash, operator.account?.address])).to.be.true;
  });

  it("should create a raffle", async function () {
    const result = await erc20Comp.write.createRaffle([1000, basicToken.address, 100, 2000,
      [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, ONLY_DIRECTLY], { from : operator.account?.address });
    
    const transactionReceipt = await testClient.getTransactionReceipt({hash: result});
    expect(transactionReceipt.logs[0].topics[1]).to.equal('0');
  });

  //Only operator should be able to stake
  it("should stake asset in a raffle", async function () {
    await basicToken.write.approve([erc20Comp.address, 100], { from : seller.account?.address });
    await erc20Comp.write.stake([0], { from : seller.account?.address });

    //get the raffle entry information
    entryInfoStruct = await erc20Comp.read.rafflesEntryInfo([0]) as IEntryInfo;
    //Check against the status of the raffle, should be in started
    expect(entryInfoStruct.status).to.be.equal(1);

  });

  it("should successflly allow players to buy an entry on the raffle multiple times", async function () {
    
    //Buy an entry with the first package
    await erc20Comp.write.buyEntry([0, 1], { value: 1000, from : player1.account?.address });

    //Check the entry is in the raffle
    entryInfoStruct = await erc20Comp.read.rafflesEntryInfo([0]) as IEntryInfo;
    expect(entryInfoStruct.entriesLength).to.be.equal(1);
    expect(entryInfoStruct.amountRaised).to.be.equal(1000);
    //Check the player owns the entry, by checking the entries list with the raffle id and position
    const entry = await erc20Comp.read.entriesList([0, 0]) as IEntriesList;
    expect(entry.player).to.be.equal(player1.account?.address);

    //Buy an entry with the second package
    await erc20Comp.write.buyEntry([0, 2], { value: 800, from : player1.account?.address });
    expect(entryInfoStruct.entriesLength).to.be.equal(6);
    expect(entryInfoStruct.amountRaised).to.be.equal(1800);
    //Check the player owns the entry, by checking the entries list with the raffle id and position
    const entry2 = await erc20Comp.read.entriesList([0, 5]) as IEntriesList;
    expect(entry2.player).to.be.equal(player1.account?.address);

    //Buy an entry with the third package with a different player
    await erc20Comp.write.buyEntry([0, 3], { value: 700, from : player2.account?.address });
    expect(entryInfoStruct.entriesLength).to.be.equal(31);
    expect(entryInfoStruct.amountRaised).to.be.equal(2500);
    //Check the player owns the entry, by checking the entries list with the raffle id and position
    const entry3 = await erc20Comp.read.entriesList([0, 30]) as IEntriesList;
    expect(entry3.player).to.be.equal(player2.account?.address);

    // Buy multiple entries with player 3
    await erc20Comp.write.buyEntry([0, 1], { value: 1000, from : player3.account?.address });
    await erc20Comp.write.buyEntry([0, 2], { value: 800, from : player3.account?.address });
    await erc20Comp.write.buyEntry([0, 3], { value: 700, from : player3.account?.address });

    //Check the player owns the entry, by checking the entries list with the raffle id and position
    const entry4 = await erc20Comp.read.entriesList([0, 33]) as IEntriesList;
    expect(entry4.player).to.be.equal(player3.account?.address);
    const entry5 = await erc20Comp.read.entriesList([0, 34]) as IEntriesList;
    expect(entry5.player).to.be.equal(player3.account?.address);

    //check player3 owns the last entry, by getting the entries length from the raffle
    const lastEntry = await getLastEntry(0);
    expect(lastEntry.player).to.be.equal(player3.account?.address);

  });

  it("should give entries for free", async function () {

    const entryAmountBefore: number = (await getLastEntry(0)).currentEntriesLength;
    await erc20Comp.write.giveBatchEntriesForFree([0, [player4.account.address]], { from : operator.account?.address });

    //Check the entry is in the raffle
    let lastEntry = await getLastEntry(0);
    expect(lastEntry.player).to.be.equal(player4.account?.address);
    expect(lastEntry.currentEntriesLength).to.be.equal(entryAmountBefore + 1);

    await erc20Comp.write.giveBatchEntriesForFree([0, [player5.account.address, player6.account.address]], { from : operator.account?.address });
    lastEntry = await getLastEntry(0);
    expect(lastEntry.player).to.be.equal(player6.account?.address);
    expect(lastEntry.currentEntriesLength).to.be.equal(entryAmountBefore + 3);
    
  });

  it("should show accurately show the minimum amount of wei required to draw comp", async function () {
    const fundingList = await erc20Comp.read.fundingList([0]) as IFundingList;
    expect(fundingList.desiredFundsInWeis).to.be.equal("1000");
  });

  it("should close the raffle", async function () {
    const balanceSellerBefore = await testClient.getBalance({address: seller.account?.address});
    await erc20Comp.write.mockSetWinner([0, 2], { from : operator.account?.address });

    const raffleInfo: RaffleStruct = await erc20Comp.read.raffles([0]) as RaffleStruct;

    expect(raffleInfo.winner).to.be.equal(player2.account?.address);

    // check the winner received the prize (100 tokens of basicERC20)
    const balanceWinnerAfter = await basicToken.read.balanceOf([player2.account?.address]);
    expect(balanceWinnerAfter).to.be.equal(100);
    // check the seller received the money (75% of the raffle pool)
    const balanceSellerAfter = await testClient.getBalance({ address: owner.account?.address });
    expect(balanceSellerAfter).to.be.equal(balanceSellerBefore + 3000n); // 75% of 4000 = 3000
  });


  it("should change the destination address", async function () {
    await erc20Comp.write.setDestinationAddress(['0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3'], { from : operator.account?.address });
  });

  it("should transfer funds when the winner is from a Multi entry", async function () {
    await erc20Comp.write.createRaffle([parseUnits('2', 18), basicToken.address, 6, parseUnits('1', 18), [{ id: 1, numEntries: 1, price: parseUnits('1', 18) }, { id: 2, numEntries: 5, price: parseUnits('2', 18) }], 2500, ONLY_DIRECTLY], { from : operator.account?.address });
  
    idRaffle = await getRaffleId();

    await basicToken.write.approve([erc20Comp.address, 6], { from : seller.account?.address });
    await erc20Comp.write.stake([idRaffle], { from : seller.account?.address });

    await erc20Comp.write.buyEntry([idRaffle, 2, { value: parseUnits('2', 18) }], { from: player1.account?.address });
    await erc20Comp.write.giveBatchEntriesForFree([idRaffle, [player3.account?.address, player5.account.address]], { from : operator.account?.address });
    const destinationWallet = await erc20Comp.read.destinationWallet();
    const balanceDestinationBefore = await testClient.getBalance( { address: destinationWallet as `0x${string}`});
    const balanceSellerBefore = await testClient.getBalance({ address: player4.account?.address } ) as bigint;

    // the winner is from a free entry
    await erc20Comp.write.mockSetWinner([idRaffle, 2], { from : operator.account?.address});
    
    // check the status is "ended"
    const raffle = await erc20Comp.read.getRafflesEntryInfo([idRaffle]) as IEntryInfo;
    expect(raffle.status).to.be.equal(5);

    // check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    const balanceDestinationAfter = await testClient.getBalance({ address: destinationWallet as `0x${string}`});
    expect(balanceDestinationAfter).to.equal(balanceDestinationBefore + BigInt('500000000000000000')); // 25% of raised amount
    const balanceSellerAfter = await testClient.getBalance({ address: player4.account?.address as `0x${string}`});
    // check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    expect(balanceSellerAfter).to.be.equal(balanceSellerBefore + BigInt('1500000000000000000')); // 75% of raised amount - gas fees
  });


  it("should cancel a raffle", async function () {
    await erc20Comp.write.createRaffle([parseUnits('5', 18), basicToken.address, 7, parseUnits('2', 18), [{ id: 1, numEntries: 1, price: parseUnits('1', 18) }, { id: 2, numEntries: 5, price: parseUnits('2', 18) }], 2500, ONLY_DIRECTLY], { from : operator.account?.address});
    
    idRaffle = await getRaffleId();
    await basicToken.write.approve([erc20Comp.address, 7], { from : seller.account?.address });
    await erc20Comp.write.stake([idRaffle], { from : seller.account?.address });
    await erc20Comp.write.buyEntry([idRaffle, 1, { value: parseUnits('1', 18) }], {from : player1.account?.address});

    const balanceSellerBefore = await basicToken.read.balanceOf([seller.account?.address]) as bigint;

    await erc20Comp.write.cancelRaffle([idRaffle]);

    // The seller received back the prize.
    const sellerBalanceAfter = await basicToken.read.balanceOf([seller.account?.address]) as bigint;
    expect(sellerBalanceAfter).to.be.equal(balanceSellerBefore + 7n);

    // check the raffle is canceled
    const raffle = await erc20Comp.read.getRafflesEntryInfo([idRaffle]) as IEntryInfo;
    expect(raffle.status).to.be.equal(6); // cancellation requested   
  });

  it("should ask a raffle for cancellation step 1", async function () {
    await erc20Comp.write.createRaffle([0, basicToken.address, 5, 0, [{ id: 1, numEntries: 1, price: parseUnits('1', 18) }, { id: 2, numEntries: 5, price: parseUnits('2', 18) }], 2500, ONLY_DIRECTLY], { from : operator.account?.address});
    
    idRaffle = await getRaffleId();

    await basicToken.write.approve([erc20Comp.address, 5], { from: seller.account?.address });
    await erc20Comp.write.stake([idRaffle], {from: seller.account?.address});

    await erc20Comp.write.buyEntry([idRaffle, 1, { value: parseUnits('1', 18) }], { from : player2.account?.address });
    await erc20Comp.write.buyEntry([idRaffle, 2, { value: parseUnits('2', 18) }], { from : player4.account?.address });
    await erc20Comp.write.buyEntry([idRaffle, 1, { value: parseUnits('1', 18) }], { from : player1.account?.address });

    await erc20Comp.write.giveBatchEntriesForFree([idRaffle, [player3.account.address, player5.account.address]], { from : operator.account?.address });

    const raffle = await erc20Comp.read.getRafflesEntryInfo([idRaffle]) as IEntryInfo;

    expect(raffle.amountRaised).to.be.equal(parseUnits('4', 18))
  });

  it("should fail transferring remaining funds if the raffle is not in cancellation_requested status", async function () {
    await expect(erc20Comp.write.transferRemainingFunds([idRaffle], { from: operator.account?.address}))
      .to.revertedWith("Wrong status");
  });

  it("should ask a raffle for cancellation step 2", async function () {
    await erc20Comp.write.cancelRaffle([idRaffle], { from : operator.account?.address })
    const raffle = await erc20Comp.read.getRafflesEntryInfo([idRaffle]) as IEntryInfo;

    expect(raffle.status).to.be.equal(6)
  });

  it("should reduce the amount raised for the raffle after the refund", async function () {
    const raffle = await erc20Comp.read.getRafflesEntryInfo([idRaffle]) as IEntryInfo;
    expect(raffle.amountRaised).to.be.equal(parseUnits('4', 18))
  });

  it("should fail transferring remaining funds a non operator", async function () {
    await expect(erc20Comp.write.transferRemainingFunds([idRaffle], { from: player1.account?.address}))
      .to.be.reverted;
  });

  it("should transfer remaining funds", async function () {
    const result = await erc20Comp.write.transferRemainingFunds([idRaffle], { from: operator.account?.address});
    expect(result).to.emit('RemainingFundsTransferred').withArgs(idRaffle, parseUnits('4', 18));
  });
  it("Gas cost when buying 100 entries (no required nft)", async function () {
    await erc20Comp.write.createRaffle([parseUnits('3', 18), basicToken.address, 1, parseUnits('1', 18), [{ id: 1, numEntries: 1, price: parseUnits('1', 18) }, { id: 2, numEntries: 10, price: parseUnits('2', 18) }, { id: 3, numEntries: 100, price: parseUnits('3', 18) }], 2500, ONLY_DIRECTLY], { from : operator.account?.address });
    
    idRaffle = await getRaffleId();

    await basicToken.write.approve([erc20Comp.address, 1], { from: seller.account?.address });
    await erc20Comp.write.stake([idRaffle], { from: seller.account?.address });

    await erc20Comp.write.buyEntry([idRaffle, 2, { value: parseUnits('2', 18) }], { from : player1.account?.address });
    

    // Buy 1 entry
    await erc20Comp.write.buyEntry([idRaffle, 1, { value: parseUnits('1', 18) }], { from : player2.account?.address });

    //  Buy 100 entries
    await erc20Comp.write.buyEntry([idRaffle, 3, { value: parseUnits('3', 18) }], { from : player3.account?.address });

    //  Buy 100 entries
    await erc20Comp.write.buyEntry([idRaffle, 3, { value: parseUnits('3', 18) }], { from : player4.account?.address });

    //  Buy 100 entries
    await erc20Comp.write.buyEntry([[idRaffle, 3, { value: parseUnits('3', 18) }], { from : player5.account?.address }], { from : player5.account?.address } );

    await erc20Comp.write.mockSetWinner([idRaffle, 11], { from: operator.account?.address})
    const raffle = await erc20Comp.read.raffles([idRaffle]) as RaffleStruct;
    expect(raffle.winner).to.be.equal(player3.account?.address);
  });

  it("Gas cost when calling a lot of times getEntry (no required nft)", async function () {
    await erc20Comp.write.createRaffle([parseUnits('0.03', 18), basicToken.address, 1, parseUnits('0.01', 18), [{ id: 1, numEntries: 1, price: parseUnits('0.01', 18) }, { id: 2, numEntries: 10, price: parseUnits('0.02', 18) }, { id: 3, numEntries: 100, price: parseUnits('0.03', 18) }], 2500, ONLY_DIRECTLY], { from : operator.account?.address });
    
    idRaffle = await getRaffleId();

    await basicToken.write.approve([erc20Comp.address, 1], { from: seller.account?.address });
    await erc20Comp.write.stake([idRaffle], { from : seller.account?.address });

    await erc20Comp.write.buyEntry([idRaffle, 1, { value: parseUnits('0.01', 18) }], { from : player2.account?.address });

    // player1 buys 10 entries
    await erc20Comp.write.buyEntry([idRaffle, 2, { value: parseUnits('0.02', 18) }], { from: player1.account?.address });

    //  player3 buys 100 entries
    await erc20Comp.write.buyEntry([idRaffle, 3, { value: parseUnits('0.03', 18) }], { from : player3.account?.address });

    //  player4 buys 100 entries
    await erc20Comp.write.buyEntry([idRaffle, 3, { value: parseUnits('0.03', 18) }], { from: player4.account?.address });
    

    expect(await erc20Comp.write.buyEntry([idRaffle, 8, { value: parseUnits('0', 18) }], { from: player5.account?.address })
      .to.be.revertedWithCustomError(erc20Comp,"EntryNotAllowed", "Id not in raffleId"));

    //  player5 buys 100 entries
    await erc20Comp.write.buyEntry([idRaffle, 3, { value: parseUnits('0.03', 18) }], { from: player5.account?.address });
    

    await erc20Comp.write.mockSetWinner([idRaffle, 210], { from: operator.account?.address})
    const raffle = await erc20Comp.read.raffles([idRaffle]) as RaffleStruct;
    expect(raffle.winner).to.be.equal(player4.account.address);

  });


  it("should allow a raffle with mixed entries at price 0 and with prices > 0", async function () {
    await erc20Comp.write.createRaffle([0, basicToken.address, 1, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: parseUnits('0.005', 18) }], 0, ONLY_DIRECTLY], { from : operator.account?.address });
    
    idRaffle = await getRaffleId();

    await basicToken.write.approve([erc20Comp.address, 1], { from: seller.account?.address });
    await erc20Comp.write.stake([idRaffle], { from : seller.account?.address });

    // Player 2 and player 4 get 1 entry for free
    await erc20Comp.write.buyEntry([idRaffle, 1, { value: parseUnits('0', 18) }], { from : player2.account?.address });
    await erc20Comp.write.buyEntry([idRaffle, 1, { value: parseUnits('0', 18) }], { from: player4.account?.address });
    // player 3 and player 5 get free entries via the operator
    await erc20Comp.write.giveBatchEntriesForFree([idRaffle, [player3.account.address, player5.account.address]], { from : operator.account?.address });
    // player 1 buys 10 entries for 0.005
    await erc20Comp.write.buyEntry([idRaffle, 2, { value: parseUnits('0.005', 18) }], { from: player1.account?.address });

    // The winner will be player2
    await erc20Comp.write.mockSetWinner([idRaffle, 0], { from: operator.account?.address});

    let winner = await erc20Comp.read.getWinnerAddressFromRandom([idRaffle, 1]);
    
    expect(winner).to.be.equal(player2.account.address);
  });

  it("should fail if player wants to buy using an older priceId", async function () {
    // the id used for the prices are 11 and 12
    await erc20Comp.write.createRaffle([0, basicToken.address, 10, 0, [{ id: 10, numEntries: 1, price: 0 }, { id: 11, numEntries: 10, price: parseUnits('0.005', 18) }], 0, 1], { from : operator.account?.address });
    
    idRaffle = await getRaffleId();
    await basicToken.write.approve([erc20Comp.address, 10], { from: seller.account?.address });
    await erc20Comp.write.stake([idRaffle], { from : seller.account?.address });

    // Player 2 and player 4 get 1 entry for free
    await erc20Comp.write.buyEntry([idRaffle, 10, { value: parseUnits('0', 18) }], { from : player2.account?.address });
    await erc20Comp.write.buyEntry([idRaffle, 10, { value: parseUnits('0', 18) }], { from: player4.account.address });
    // player 3 and player 5 get free entries via the operator
    await erc20Comp.write.giveBatchEntriesForFree([idRaffle, [player3.account.address, player5.account.address]], { from: operator.account?.address});
    // player 1 buys 10 entries for 0.005
    await erc20Comp.write.buyEntry([idRaffle, 11, { value: parseUnits('0.005', 18) }], { from: player1.account?.address });
    // Fails when using and id on the price struct that did not belong to this raffle
    await expect(erc20Comp.write.buyEntry([idRaffle, 1, { value: 700 }], { from: player1.account?.address }))
      .to.be.revertedWithCustomError(erc20Comp,"EntryNotAllowed", "Id not in raffleId");
  });
});
