const { expect } = require("chai");
const { ethers } = require("hardhat");

/// /// SEPOLIA //////////////

const SEPOLIA_VRF_COORDINATOR = "0x2bce784e69d2Ff36c71edcB9F88358dB0DfB55b4";
const SEPOLIA_LINK_TOKEN = "0x326C977E6efc84E512bB9C30f76E30c160eD06FB";
const SEPOLIA_KEYHASH =
  "0x0476f9a745b61ea5c0ab224d3a6e4c99f0b02fce4da01143a4f70aa80ae76e8a";
const ONLY_DIRECTLY = 0;

let token;
let manager;
let owner, player1, player2, player3, player4, player5, player6;
let seller, operator;
let idRaffle;
const zeroAddress = ethers.ZeroAddress;

async function getRaffleId() {
  filter = manager.filters.RaffleCreated;
  events = await manager.queryFilter(filter, -1)
  idRaffle = events[0].topics[1];
  return idRaffle;
}

async function getTokenId() {
  filter = token.filters.TokenCreated;
  events = await token.queryFilter(filter, -1)
  tokenId = events[0].topics[1];
  return tokenId;
}

describe("NFT no gating", function () {

  it("Should mint tokens and configure", async function () {
    [
      owner,
      player1,
      player2,
      player3,
      player4,
      player5,
      player6,
      player7,
      seller,
      operator,
    ] = await ethers.getSigners();

    const Basic721 = await ethers.getContractFactory("Basic721");
    token = await Basic721.deploy();
    await token.waitForDeployment();

    const NFTComp = await ethers.getContractFactory(
      "MockNFTComp"
    );
    manager = await NFTComp.deploy(
      SEPOLIA_VRF_COORDINATOR,
      SEPOLIA_LINK_TOKEN,
      SEPOLIA_KEYHASH,
      false
    );
    await manager.waitForDeployment();

    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
  });

  it("Should add operator role to raffles", async function () {

    const operatorHash = ethers.id("OPERATOR");
    await manager.grantRole(operatorHash, operator.getAddress(), {
      from: owner.getAddress(),
    });
  });

  it("The contract balance should be cero ", async function () {
    const balance = await ethers.provider.getBalance(manager.getAddress());
    expect(balance).to.equal(0);
  });

  it("should create a raffle", async function () {
    const operatorHash = ethers.id("OPERATOR");
    await manager.grantRole(operatorHash, operator.getAddress(), {
      from: owner.getAddress(),
    });
    const result = await manager.connect(operator).createRaffle(
      1000,
      token.getAddress(),
      0,
      2000,
      [
        {
          id: 1,
          numEntries: 1,
          price: ethers.parseUnits("0.05", "ether"),
        },
        { id: 2, numEntries: 5, price: 800 },
        {
          id: 3,
          numEntries: 25,
          price: ethers.parseUnits("0.3", "ether"),
        },
      ],
      2500,
      0
    );
    await expect(result).to.emit(manager, "RaffleCreated").withArgs(0, await token.getAddress(), 0);
  });

  it("should stake asset in a raffle", async function () {
    await token.connect(seller).approve(manager.getAddress(), 0);
    const result = await manager.connect(seller).stakeNFT(0);
    await expect(result).to.emit(manager, "RaffleStarted").withArgs(0, await seller.getAddress());
    const currentOwner = await token.ownerOf(0);
    expect(currentOwner).to.equal(await manager.getAddress());
    expect(currentOwner).not.to.equal(await seller.getAddress());
  });

  it("should buy a first entry on the raffle", async function () {
    const tx = await manager.connect(player2).buyEntry(0, 1, { value: ethers.parseUnits('0.05', 'ether') });
    const receipt = await tx.wait();
    console.log("Gas used buying 1st time: " + receipt.cumulativeGasUsed);
  });

  it("should buy an entry on the raffle", async function () {
    const tx = await manager.connect(player1).buyEntry(0, 1, { value: ethers.parseUnits('0.05', 'ether') });
    const receipt = await tx.wait();
    console.log("Gas used buying package 1: " + receipt.cumulativeGasUsed);
  });

  it("should buy an entry on the raffle on package 3", async function () {
    const tx = await manager.connect(player3).buyEntry(0, 3, { value: ethers.parseUnits('0.3', 'ether') });
    const receipt = await tx.wait();
    console.log("Gas used buying package 3: " + receipt.cumulativeGasUsed);
  });

  it("should give an entry for free", async function () {
    const tx = await manager.connect(operator).giveBatchEntriesForFree(0, [player4.getAddress()]);
    const receipt = await tx.wait();
    console.log("Gas used adding for free: " + receipt.cumulativeGasUsed);
  });

  it("should give a batch of free entries free", async function () {
    let entries = await manager.getEntriesBought(0);

    const tx = await manager.connect(operator).giveBatchEntriesForFree(0, [player5.getAddress(), player6.getAddress()]);

    entries = await manager.getEntriesBought(0);
    const receipt = await tx.wait();
    console.log("Gas used adding for free: " + receipt.cumulativeGasUsed);
  });

  it("should show the raffle info", async function () {
    const result = await manager.fundingList(0);
    expect(result.desiredFundsInWeis).to.equal("1000");
  });

  it("should close the raffle", async function () {
    const balanceBefore = await ethers.provider.getBalance(seller.getAddress());
    await manager.connect(operator).mockSetWinner(0, 0);

    // check the winner received the NFT
    const currentOwner = await token.ownerOf(0);
    expect(currentOwner).to.equal(await player2.getAddress());

    // check the seller received the money (75% of the raffle pool)
    const balanceAfter = await ethers.provider.getBalance(seller.getAddress());
    expect(balanceAfter).to.equal(balanceBefore + (ethers.parseUnits('0.3', 'ether')));
  });

  it("should fail buying an entry after draw", async function () {
    await expect(manager.connect(player1).buyEntry(0, 1, { value: ethers.parseUnits('0.05', 'ether') })).to.be.revertedWithCustomError(manager, "EntryNotAllowed", "Not in ACCEPTED");
  });

  it("should fail creating a raffle a non-operator", async function () {
    await expect(manager.connect(player2).createRaffle(1, token.getAddress(), 0, 2000, [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, ONLY_DIRECTLY,))
      .to.be.reverted;
  });

  it("should fail staking asset in a not created raffle", async function () {
    await expect(manager.connect(seller).stakeNFT(1)).to.be.reverted;
  });

  it("should fail staking asset in a raffle not in the right status", async function () {
    await expect(manager.connect(seller).stakeNFT(0)).to.be.revertedWith("Raffle not CREATED");
  });

  it("should create a new raffle", async function () {
    const result = await manager.connect(operator).createRaffle(1000, token.getAddress(), 1, 2000000000, [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 25, price: 70000000000 }], 2500, ONLY_DIRECTLY);
    await expect(result).to.emit(manager, "RaffleCreated").withArgs(1, await token.getAddress(), 1);
  });

  it("should fail staking asset in a raffle by a non-owner", async function () {
    await expect(manager.connect(player4).stakeNFT(1)).to.be.revertedWith("NFT is not owned by caller");
  });

  it("should stake asset in a raffle", async function () {
    await token.connect(seller).approve(manager.getAddress(), 1);
    const result = await manager.connect(seller).stakeNFT(1);
    await expect(result).to.emit(manager, "RaffleStarted").withArgs(1, await seller.getAddress());
    const currentOwner = await token.ownerOf(1);
    expect(currentOwner).to.equal(await manager.getAddress());
    expect(currentOwner).not.to.equal(await seller.getAddress());
  });

  it("should fail buying an entry paying below price", async function () {
    await expect(manager.connect(player1).buyEntry(1, 1, { value: 100 })).to.be.revertedWithCustomError(manager, "EntryNotAllowed", "msg.value not the price");
  });

  it("should fail buying a wrong amount of entries", async function () {
    await expect(manager.connect(player1).buyEntry(1, 6, { value: 800 })).to.be.reverted;
  });


  it("should allow buy several entries", async function () {
    const balanceBefore = await ethers.provider.getBalance(await manager.getAddress());
    await manager.connect(player1).buyEntry(1, 2, { value: 8000000000 });
    const balanceAfter = await ethers.provider.getBalance(await manager.getAddress());

    // check the contract received the money
    expect(balanceAfter).to.equal(balanceBefore + 8000000000n);
  });

  it("should change the destination address", async function () {
    await manager.setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3');
  });

  it("should fail changing the destination address by a non owner", async function () {
    await expect(manager.connect(operator).setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3')).to.be.reverted
  });

  it("should add free entries", async function () {
    const result = await manager.connect(operator).createRaffle(ethers.parseUnits('1', 'ether'), token.getAddress(), 6, ethers.parseUnits('1', 'ether'),
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
    const receipt = await result.wait();
    idRaffle = await getRaffleId();

    await token.connect(seller).approve(manager.getAddress(), 6);
    await manager.connect(seller).stakeNFT(idRaffle);

    await manager.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player4.getAddress(), player5.getAddress()]);
  });

  it("should transfer funds when the winner is from a free entry", async function () {
    const destinationWallet = await manager.destinationWallet();
    const trackerBefore = await ethers.provider.getBalance(destinationWallet);
    const sellerBefore = await ethers.provider.getBalance(seller.getAddress());
    // the winner is from a free entry
    await manager.connect(operator).mockSetWinner(idRaffle, 2);

    const trackerAfter = await ethers.provider.getBalance(destinationWallet);
    // check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    await expect(trackerAfter - trackerBefore).to.equal('250000000000000000')
    const sellerAfter = await ethers.provider.getBalance(seller.getAddress());
    // check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    expect(sellerAfter - sellerBefore).to.equal('750000000000000000'); // 75% of raised amount - gas fees
    // check the new owner of the token is the player (from the free entry) and not the previous owner (player2)
    const currentOwner = await token.ownerOf(6);
    expect(currentOwner).to.equal(await player4.getAddress());
    expect(currentOwner).not.to.equal(await seller.getAddress());
  });

  it("should transfer funds when the winner is from a Multi entry", async function () {
    let result = await manager.connect(operator).createRaffle(ethers.parseUnits('2', 'ether'), token.getAddress(), 6, ethers.parseUnits('1', 'ether'),
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
    const receipt = await result.wait();
    idRaffle = await getRaffleId();
    await token.connect(player4).approve(await manager.getAddress(), 6);
    await manager.connect(player4).stakeNFT(idRaffle);

    await manager.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('2', 'ether') });
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
    const destinationWallet = await manager.destinationWallet();
    const trackerBefore = await ethers.provider.getBalance(destinationWallet);
    const trackerSellerBefore = await ethers.provider.getBalance(player4.getAddress());

    // the winner is from a free entry
    result = await manager.connect(operator).mockSetWinner(idRaffle, 2);
    // check the event is emitted
    await expect(result).to.emit(manager, "RaffleEnded").withArgs(idRaffle, () => true, () => true, () => true);
    // check the status is "ended"
    const raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.equal(5);

    const trackerAfter = await ethers.provider.getBalance(destinationWallet);
    //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    expect(trackerAfter - (trackerBefore)).to.equal('500000000000000000'); // 25% of raised amount
    const trackerSellerAfter = await ethers.provider.getBalance(player4.getAddress());
    //check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    expect(trackerSellerAfter - (trackerSellerBefore)).to.equal('1500000000000000000'); // 75% of raised amount - gas fees
    // check the new owner of the token is the player (from the free entry) and not the previous owner (player2)
    const currentOwner = await token.ownerOf(6);
    expect(currentOwner).to.equal(await player1.getAddress());
    expect(currentOwner).not.to.equal(await player4.getAddress());
  });

  it("should fail adding free entries if the status is not accepted", async function () {
    // check adding free entries to a closed raffle
    await expect(manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()])).to.be.revertedWith("Raffle is not in accepted");
    // check adding free entries to a raffle that has not been accepted yet
    const result = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), token.getAddress(), 3, ethers.parseUnits('1', 'ether'),
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY,);
    const receipt = await result.wait();
    idRaffle = await getRaffleId();
    await expect(result).to.emit(manager, "RaffleCreated").withArgs(idRaffle, () => true, () => true);

    await expect(manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()])).to.be.revertedWith("Raffle is not in accepted");
  });

  it("should fail canceling a raffle already closed", async function () {
    // await expectRevert(manager.cancelRaffle(idRaffle - 1, { from: operator }), "Wrong status");
    await expect(manager.connect(operator).cancelRaffle(idRaffle - 1)).to.be.revertedWith("Wrong status");
  });

  it("should fail canceling a raffle a non-operator", async function () {
    await expect(manager.connect(player1).cancelRaffle(idRaffle)).to.be.reverted;
  });

  it("should cancel a raffle", async function () {
    const result = await manager.connect(operator).createRaffle(ethers.parseUnits('5', 'ether'), token.getAddress(), 7, ethers.parseUnits('2', 'ether'),
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
    const receipt = await result.wait();
    idRaffle = await getRaffleId();
    await token.connect(owner).mint(seller.getAddress());
    await token.connect(seller).approve(manager.getAddress(), 7);
    await manager.connect(seller).stakeNFT(idRaffle);
    await manager.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });

    let currentOwner = await token.ownerOf(7);
    expect(currentOwner).to.equal(await manager.getAddress());

    await manager.connect(operator).cancelRaffle(idRaffle);
    // check the seller has the nft back
    currentOwner = await token.ownerOf(7);
    expect(currentOwner).to.equal(await seller.getAddress());

    // check the raffle is canceled
    const raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.equal(6); // cancellation requested
  });

  it("should fail canceling a raffle already asked to cancel", async function () {
    await expect(manager.connect(player1).cancelRaffle(idRaffle)).to.be.reverted;
  });

  it("should show the entries size", async function () {
    let result = await token.connect(owner).mint(seller.getAddress());
    let receipt = await result.wait();
    const tokenId = getTokenId();

    result = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), token.getAddress(), tokenId, ethers.parseUnits('1', 'ether'),
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
    receipt = await result.wait();
    idRaffle = await getRaffleId();

    await token.connect(seller).approve(manager.getAddress(), tokenId);
    await manager.connect(seller).stakeNFT(idRaffle);

    await manager.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('2', 'ether') });
    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.entriesLength).to.equal(5);

    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.entriesLength).to.equal(7);

  });

  it("should allow a free raffle", async function () {
    const result = await token.connect(owner).mint(seller.getAddress());
    const tokenId = getTokenId();

    const resultCreation = await manager.connect(operator).createRaffle(
      0, await token.getAddress(), tokenId, 0,
      [{ id: 1, numEntries: 1, price: 0 }], 0, ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();

    await token.connect(seller).approve(manager.getAddress(), tokenId);
    await manager.connect(seller).stakeNFT(idRaffle);

    await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0', 'ether') });
    await manager.connect(player4).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0', 'ether') });
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    let currentOwner = await token.ownerOf(tokenId);
    //  console.log("current owner " + currentOwner)
    // the first call did generate a winner not in MW. So now lets do another call to setWinner
    const tx = await manager.connect(operator).mockSetWinner(idRaffle, 0);
    receipt = await tx.wait();
    console.log("Gas used setting winner: " + receipt.cumulativeGasUsed);

    // check the winner received the NFT
    currentOwner = await token.ownerOf(tokenId);
    const winner = await manager.getWinnerAddressFromRandom(idRaffle, 1);
    expect(currentOwner).to.equal(winner);
  });

  
    it("Gas cost when calling a lot of times getEntry (no required nft)", async function () {
      await token.connect(owner).mint(await seller.getAddress());
       tokenId = await getTokenId();
  
      const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), token.getAddress(), tokenId, ethers.parseUnits('0.01', 'ether'),
        [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, ONLY_DIRECTLY);
      receipt = await resultCreation.wait();
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
     await manager.connect(seller).stakeNFT(idRaffle);
  
      const r1 = await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
      receipt = await r1.wait();
      console.log("Gas used buying 1 entry no restriction: " + receipt.cumulativeGasUsed);
      // player1 buys 10 entries
      result = await manager.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.02', 'ether') });
      receipt = await result.wait();
      console.log("Gas used buying 10 entries no restriction: " + receipt.cumulativeGasUsed);
  
      //  player3 buys 100 entries
      const r2 = await manager.connect(player3).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
      receipt = await r2.wait();
    //  console.log("Gas used buying 100 entries no restriction: " + receipt.cumulativeGasUsed);
  
      //  player4 buys 100 entries
      const r3 = await manager.connect(player4).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
      receipt = await r3.wait();
   //   console.log("Gas used buying 100 entries no restriction: " + receipt.cumulativeGasUsed);
  
      await expect(manager.connect(player6).buyEntry(idRaffle, 8, { value: 0 })).to.be.revertedWithCustomError(manager,"EntryNotAllowed", "Id not in raffleId");
      //  player5 buys 100 entries
      const r4 = await manager.connect(player5).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
      receipt = await r4.wait();
    //  console.log("Gas used buying 100 entries no restriction: " + receipt.cumulativeGasUsed);
  
      await manager.connect(operator).mockSetWinner(idRaffle, 210)
      const raffle = await manager.raffles(idRaffle);
      expect(raffle.winner).to.equal(await player4.getAddress());
  
      console.log("Gas used new setwinner: " + receipt.cumulativeGasUsed);
     
    });
  
  
    it("should manage canceled entries", async function () {
      const result = await token.connect(owner).mint(seller.getAddress());
      const tokenId = getTokenId();
      const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), token.getAddress(), tokenId, ethers.parseUnits('0.01', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, ONLY_DIRECTLY);
      receipt = await resultCreation.wait();
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      // player1 allowed
      const r1 = await manager.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
      //  player3 buys 100 entries
      const r2 = await manager.connect(player2).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
      const r3 = await manager.connect(player3).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
  
      const rCancelEntry = await manager.connect(operator).cancelEntry(idRaffle, [2], player3.getAddress());
      receipt = await rCancelEntry.wait();
      console.log("Gas used cancelEntry: " + receipt.cumulativeGasUsed);
      // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2
      await manager.connect(operator).mockSetWinner(idRaffle, 1);
      const raffle = await manager.raffles(idRaffle);
      expect(raffle.winner).to.equal(await player2.getAddress());
    });
  
    it("should manage blacklisted players as time passes", async function () {
      let result = await token.connect(owner).mint(seller.getAddress());
      const tokenId = getTokenId();
  
      let ownerNFT = await token.ownerOf(tokenId);
      expect(ownerNFT).to.equal(await seller.getAddress());
  
      const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), token.getAddress(), tokenId, ethers.parseUnits('0.01', 'ether'),
        [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, ONLY_DIRECTLY);
      receipt = await resultCreation.wait();
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
      // player1 allowed
      await manager.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
      //  player3 buys 100 entries
      await manager.connect(player2).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
      // player1 can buy again
      await manager.connect(player1).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
      // player 4 buys 100 entries
      await manager.connect(player4).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
      // player 5 buys 100 entries
      await manager.connect(player5).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
  
      await expect(manager.connect(operator).cancelEntry(idRaffle, [2], player3.getAddress())).to.be.revertedWith("Entry did not belong to player");
  
      // test cancelEntry batch
      result = await manager.connect(operator).cancelEntry(idRaffle, [0, 2], player1.getAddress());
      await expect(result).to.emit(manager, "EntryCancelled").withArgs(idRaffle, 101, await player1.getAddress());
  
      const entriesData = await manager.getEntriesBought(idRaffle);
      expect(entriesData[0].player).to.equal( zeroAddress );
      expect(entriesData[1].player).to.equal(zeroAddress);
      expect(entriesData[3].player).to.equal(zeroAddress);
  
      for (let i = 0; i < 10; i++) {
        await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
      }
  
      for (let i = 0; i < 10; i++) {
        await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
      }
  
      // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2
      const rSetWinner = await manager.connect(operator).mockSetWinner(idRaffle, 0)
      receipt = await rSetWinner.wait();
  
      const raffle = await manager.raffles(idRaffle);
      expect(raffle.winner).to.equal(await player2.getAddress());
  
      ownerNFT = await token.ownerOf(tokenId);
      expect(ownerNFT).to.equal(await player2.getAddress());
      expect(seller).not.to.equal(await player2.getAddress());
  
      console.log("Gas used new setwinner: " + receipt.cumulativeGasUsed);
    });
  
    it("should allow a raffle with mixed entries at price 0 and with prices > 0", async function () {
      const result = await token.connect(owner).mint(seller.getAddress());
      const tokenId = getTokenId();
  
      const resultCreation = await manager.connect(operator).createRaffle(0, token.getAddress(), tokenId, 0,
        [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 0, ONLY_DIRECTLY);
      receipt = await resultCreation.wait();
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      // Player 2 and player 4 get 1 entry for free
      await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0', 'ether') });
      await manager.connect(player4).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0', 'ether') });
      // player 3 and player 5 get free entries via the operator
      await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
      // player 1 buys 10 entries for 0.005
      await manager.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.005', 'ether') });
  
      // The winner will be player2
      await manager.connect(operator).mockSetWinner(idRaffle, 0);
  
      // check the winner received the NFT
      const currentOwner = await token.ownerOf(tokenId);
      const winner = await manager.getWinnerAddressFromRandom(idRaffle, 1);
      //   console.log("current owner " + currentOwner + " winner " + winner)
      expect(currentOwner).to.equal(winner);
      expect(currentOwner).to.equal(await player2.getAddress());
    });
  
    it("should check winner by changing random number", async function () {
      const result = await token.connect(owner).mint(seller.getAddress());
      const tokenId = getTokenId();
      const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), token.getAddress(), tokenId, ethers.parseUnits('0.01', 'ether'),
        [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, 1);
      receipt = await resultCreation.wait();
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      await manager.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
      await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
      await manager.connect(player3).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
      await manager.connect(player4).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
      await manager.connect(player5).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
  
      // There are 5 items in the list + 1 created in the raffle and set to 0
      const entriesData = await manager.getEntriesBought(idRaffle);
      expect(entriesData.length).to.equal(6);
  
      await manager.connect(operator).mockSetWinner(idRaffle, 4);
      const raffle = await manager.raffles(idRaffle);
      expect(raffle.winner).to.equal(await player5.getAddress());
      expect(raffle.status).not.to.equal(1);
    });
  
    it("should ask a raffle for cancellation step 1", async function () {
      const result = await token.connect(owner).mint(seller.getAddress());
      const tokenId = getTokenId();
  
      const resultCreation = await manager.connect(operator).createRaffle(0, token.getAddress(), tokenId, 0,
        [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
      receipt = await resultCreation.wait();
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });
      await manager.connect(player4).buyEntry(idRaffle, 2, { value: ethers.parseUnits('2', 'ether') });
      await manager.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });
  
      await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
  
      const raffle = await manager.getRafflesEntryInfo(idRaffle);
      expect(raffle.amountRaised).to.equal(ethers.parseUnits("4", "ether"));
    });
  
    it("should fail transferring remaining funds if the raffle is not in cancellation_requested status", async function () {
      await expect(manager.connect(operator).transferRemainingFunds(idRaffle)).to.be.revertedWith("Wrong status");
    });
  
    it("should ask a raffle for cancellation step 2", async function () {
      await manager.connect(operator).cancelRaffle(idRaffle);
      const raffle = await manager.getRafflesEntryInfo(idRaffle);
      expect(raffle.status).to.equal(6);
    });
  
    it("should fail transferring remaining funds a non operator", async function () {
      await expect(manager.connect(player1).transferRemainingFunds(idRaffle)).to.be.reverted;
    });
  
    it("should transfer remaining funds", async function () {
      const destinationWallet = await manager.destinationWallet();
      const trackerBefore = await ethers.provider.getBalance(destinationWallet);
  
      const result = await manager.connect(operator).transferRemainingFunds(idRaffle);
      await expect(result).to.emit(manager, "RemainingFundsTransferred").withArgs(idRaffle, ethers.parseUnits('4', 'ether'));
      const raffle = await manager.getRafflesEntryInfo(idRaffle);
      expect(raffle.amountRaised).to.equal(ethers.parseUnits("0", "ether"));
  
      const trackerAfter = await ethers.provider.getBalance(destinationWallet);
      expect(trackerAfter - (trackerBefore)).to.equal(ethers.parseUnits("4", "ether")
      );
    });
  
    it("should allow transferring remaining funds two times", async function () {
      const result = await manager.connect(operator).transferRemainingFunds(idRaffle);
      await expect(result).to.emit(manager, "RemainingFundsTransferred").withArgs(idRaffle, ethers.parseUnits('0', 'ether'));
    });
  
    it("should fail if player wants to buy using an older priceId", async function () {
      const result = await token.connect(owner).mint(seller.getAddress());
      const tokenId = getTokenId();
      // the id used for the prices are 11 and 12
      const resultCreation = await manager.connect(operator).createRaffle(0, token.getAddress(), tokenId, 0,
        [{ id: 10, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 11, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
      receipt = await resultCreation.wait();
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      await manager.connect(player2).buyEntry(idRaffle, 10, { value: ethers.parseUnits('1', 'ether') });
      await manager.connect(player4).buyEntry(idRaffle, 11, { value: ethers.parseUnits('2', 'ether') });
      await manager.connect(player1).buyEntry(idRaffle, 10, { value: ethers.parseUnits('1', 'ether') });
      // Fails when using and id on the price struct that did not belong to this raffle
      await expect(manager.buyEntry(idRaffle, 1, { from: player1, value: 700 }), "EntryNotAllowed", ["Id not in raffleId"]);
      await expect(manager.connect(player1).buyEntry(idRaffle, 1, { value: 700 })).to.be.revertedWithCustomError(manager,"EntryNotAllowed", ["Id not in raffleId"]);
    });
  
    it("should fail giving free entries two times to the same player", async function () {
      let result = await token.connect(owner).mint(seller.getAddress());
      const tokenId = getTokenId();
      // the id used for the prices are 11 and 12
      const resultCreation = await manager.connect(operator).createRaffle(0, token.getAddress(), tokenId, 0,
        [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 5, price: ethers.parseUnits('0.005', 'ether') }], 2500, ONLY_DIRECTLY);
      receipt = await resultCreation.wait();
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      // free entry without problems for player 1
      result = await manager.connect(player1).buyEntry(idRaffle, 1, { value: 0 });
      // Revert if player 1 wants to get a free entry again
      //  await expect(manager.buyEntry(idRaffle, 1, { from: player1, value: 0 }), "EntryNotAllowed", ["Player already got free entry"]);
      await expect(manager.connect(player1).buyEntry(idRaffle, 1, { value: 0 })).to.be.revertedWithCustomError(manager,"EntryNotAllowed", ["Player already got free entry"]);
      // player1 can buy other packages (not the free one)
      await manager.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.005', 'ether') });
      await manager.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.005', 'ether') });
      // Player 2 and player 3 can buy free package, and paid packages
      await manager.connect(player2).buyEntry(idRaffle, 1, { value: 0 });
      const r1 = await manager.connect(player3).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.005', 'ether') });
      const r2 = await manager.connect(player3).buyEntry(idRaffle, 1, { value: 0 });
      receipt = await result.wait();
      console.log("Gas used buying free: " + receipt.cumulativeGasUsed);
      receipt = await r1.wait();
      console.log("Gas used buying paid: " + receipt.cumulativeGasUsed);
      receipt = await r2.wait();
      console.log("Gas used buying free 2nd time: " + receipt.cumulativeGasUsed);
    });
  
  
});
