const { expect } = require("chai");
const { ethers } = require("hardhat");

/// /// SEPOLIA //////////////

const SEPOLIA_VRF_COORDINATOR = "0x2bce784e69d2Ff36c71edcB9F88358dB0DfB55b4";
const SEPOLIA_LINK_TOKEN = "0x326C977E6efc84E512bB9C30f76E30c160eD06FB";
const SEPOLIA_KEYHASH =
  "0x0476f9a745b61ea5c0ab224d3a6e4c99f0b02fce4da01143a4f70aa80ae76e8a";
const ONLY_DIRECTLY = 0;

let token;
let manager;
let owner, player1, player2, player3, player4, player5, player6;
let seller, operator;
let idRaffle, receipt;
let requiredNFT, requiredNFT1, requiredNFT2, requiredNFT3, requiredTokenId, tokenId;
const zeroAddress = ethers.ZeroAddress;

async function getRaffleId() {
  filter = manager.filters.RaffleCreated;
  events = await manager.queryFilter(filter, -1)
  idRaffle = events[0].topics[1];
  return idRaffle;
}

async function getTokenId(tokenUsed) {
  filter = tokenUsed.filters.TokenCreated;
  events = await tokenUsed.queryFilter(filter, -1)
  returnedTokenId = events[0].topics[1];
  return returnedTokenId;
}

describe("NFT gating", function () {

  it("Should mint tokens and configure", async function () {
    [
      owner,
      player1,
      player2,
      player3,
      player4,
      player5,
      player6,
      player7,
      seller,
      operator,
    ] = await ethers.getSigners();

    const Basic721 = await ethers.getContractFactory("Basic721");
    token = await Basic721.deploy();
    await token.waitForDeployment();

    const RequiredNFT = await ethers.getContractFactory("Basic721");
    requiredNFT = await RequiredNFT.deploy();
    await requiredNFT.waitForDeployment();

    const RequiredNFT1 = await ethers.getContractFactory("Basic721");
    requiredNFT1 = await RequiredNFT1.deploy();
    await requiredNFT1.waitForDeployment();

    const RequiredNFT2 = await ethers.getContractFactory("Basic721");
    requiredNFT2 = await RequiredNFT2.deploy();
    await requiredNFT2.waitForDeployment();

    const RequiredNFT3 = await ethers.getContractFactory("Basic721");
    requiredNFT3 = await RequiredNFT3.deploy();
    await requiredNFT3.waitForDeployment();

    const MockNFTGatingComp = await ethers.getContractFactory(
      "MockNFTGatingComp"
    );
    manager = await MockNFTGatingComp.deploy(
      SEPOLIA_VRF_COORDINATOR,
      SEPOLIA_LINK_TOKEN,
      SEPOLIA_KEYHASH,
      false
    );
    await manager.waitForDeployment();

    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
  });

  it("Should add operator role to raffles", async function () {
    const operatorHash = ethers.id("OPERATOR");
    await manager.grantRole(operatorHash, operator.getAddress(), {
      from: owner.getAddress(),
    });
  });

  it("The contract balance should be cero ", async function () {
    const balance = await ethers.provider.getBalance(manager.getAddress());
    expect(balance).to.equal(0);
  });

  it("should create a raffle", async function () {
    const result = await manager.connect(operator).createRaffle(1000, 1, token.getAddress(), 0, 2000,
      [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, [], ONLY_DIRECTLY);
    await expect(result).to.emit(manager, "RaffleCreated").withArgs(0, await token.getAddress(), 0);
  });

  it("should stake asset in a raffle", async function () {
    await token.connect(seller).approve(manager.getAddress(), 0);
    const result = await manager.connect(seller).stakeNFT(0);
    await expect(result).to.emit(manager, "RaffleStarted").withArgs(0, await seller.getAddress());
    const currentOwner = await token.ownerOf(0);
    expect(currentOwner).to.equal(await manager.getAddress());
    expect(currentOwner).not.to.equal(await seller.getAddress());
  });

  it("should buy an entry on the raffle", async function () {
    const result = await manager.connect(player1).buyEntry(0, 1, ethers.ZeroAddress, 0, { value: 1000 });
    await expect(result).to.emit(manager, "EntrySold").withArgs(0, await player1.getAddress(), () => true, () => true);
  });

  it("should buy an entry on the raffle", async function () {
    const result = await manager.connect(player7).buyEntry(0, 1, ethers.ZeroAddress, 0, { value: 1000 });
    await expect(result).to.emit(manager, "EntrySold").withArgs(0, await player7.getAddress(), () => true, () => true);
  });

  it("should buy an entry on the raffle", async function () {
    const result = await manager.connect(player2).buyEntry(0, 1, ethers.ZeroAddress, 0, { value: 1000 });
    await expect(result).to.emit(manager, "EntrySold").withArgs(0, await player2.getAddress(), () => true, () => true);
  });

  it("should buy an entry on the raffle", async function () {
    const result = await manager.connect(player3).buyEntry(0, 1, ethers.ZeroAddress, 0, { value: 1000 });
    await expect(result).to.emit(manager, "EntrySold").withArgs(0, await player3.getAddress(), 4, 1);
    const receipt = await result.wait();
    console.log("Gas used buying: " + receipt.cumulativeGasUsed);
  });

  it("should give an entry for free", async function () {
    const result = await manager.connect(operator).giveBatchEntriesForFree(0, [player4.getAddress()]);
    const receipt = await result.wait();
    console.log("Gas used adding for free: " + receipt.cumulativeGasUsed);
  });

  it("should give a batch of free entries free", async function () {
    let entries = await manager.getEntriesBought(0);

    const result = await manager.connect(operator).giveBatchEntriesForFree(0, [player5.getAddress(), player6.getAddress()]);

    entries = await manager.getEntriesBought(0);

    const receipt = await result.wait();
    console.log("Gas used adding for free: " + receipt.cumulativeGasUsed);
  });

  it("should show the raffle info", async function () {
    const result = await manager.fundingList(0);
    expect(result.desiredFundsInWeis).to.equal(1000);
  });

  it("should close the raffle", async function () {
    const balanceBefore = await ethers.provider.getBalance(seller.getAddress());
    await manager.connect(operator).mockSetWinner(0, 2);

    // check the winner received the NFT
    const currentOwner = await token.ownerOf(0);
    expect(currentOwner).to.equal(await player2.getAddress());

    // check the seller received the money (75% of the raffle pool)
    const balanceAfter = await ethers.provider.getBalance(seller.getAddress());
    expect(balanceAfter).to.equal(balanceBefore + 3000n); // 75% of 4000 = 3000
  });

  it("should fail creating a raffle a non-operator", async function () {
    await expect(manager.connect(player2).createRaffle(1, 1, token.getAddress(), 0, 2000, [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, [], ONLY_DIRECTLY)).to.be.reverted;
  });

  it("should fail staking asset in a not created raffle", async function () {
    await expect(manager.connect(seller).stakeNFT(1)).to.be.reverted;
  });

  it("should fail staking asset in a raffle not in the right status", async function () {
    await expect(manager.connect(seller).stakeNFT(0)).to.be.revertedWith("Raffle not CREATED");
  });

  it("should create a new raffle", async function () {
    var result = await manager.connect(operator).createRaffle(1000, 5, token.getAddress(), 1, 2000000000,
      [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 25, price: 70000000000 }], 2500, [], ONLY_DIRECTLY);
    await expect(result).to.emit(manager, "RaffleCreated").withArgs(1, () => true, () => true);
  });

  it("should fail staking asset in a raffle by a non-owner", async function () {
    await expect(manager.connect(player4).stakeNFT(1)).to.be.revertedWith("NFT is not owned by caller");
  });

  it("should stake asset in a raffle", async function () {
    await token.connect(seller).approve(manager.getAddress(), 1);
    const result = await manager.connect(seller).stakeNFT(1);
    await expect(result).to.emit(manager, "RaffleStarted").withArgs(1, await seller.getAddress());
    const currentOwner = await token.ownerOf(1);
    expect(currentOwner).to.equal(await manager.getAddress());
    expect(currentOwner).not.to.equal(seller);
  });

  it("should fail buying an entry paying below price", async function () {
    await expect(manager.connect(player1).buyEntry(1, 1, ethers.ZeroAddress, 0, { value: 100 })).to.be.revertedWithCustomError(manager, "EntryNotAllowed", "msg.value not the price");
  });

  it("should fail  buying several entries for below price", async function () {
    await expect(manager.connect(player1).buyEntry(1, 3, ethers.ZeroAddress, 0, { value: 100 })).to.be.revertedWithCustomError(manager, "EntryNotAllowed", "msg.value not the price");
  });

  it("should fail buying a wrong amount of entries", async function () {
    await expect(manager.connect(player1).buyEntry(1, 6, ethers.ZeroAddress, 0, { value: 800 })).to.be.reverted;
  });

  it("should allow buy several entries", async function () {
    const balanceBefore = await ethers.provider.getBalance(manager.getAddress());
    await manager.connect(player1).buyEntry(1, 2, zeroAddress, 0, { value: 8000000000 });

    const balanceAfter = await ethers.provider.getBalance(manager.getAddress());
    // check the contract received the money
    expect(balanceAfter).to.equal(balanceBefore + 8000000000n);
  });

  it("should fail creating a raffle with more price categories than allowed", async function () {
    await expect(manager.createRaffle(1000, 1, token.getAddress(), 1, 200000000000, [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 25, price: 70000000000 }, { id: 4, numEntries: 25, price: 70000000000 }, { id: 5, numEntries: 25, price: 70000000000 }, { id: 6, numEntries: 25, price: 70000000000 }], 2500, [], ONLY_DIRECTLY))
      .to.be.reverted;
  });

  it("should change the destination address", async function () {
    await manager.setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3');
  });

  it("should fail changing the destination address by a non owner", async function () {
    await expect(manager.connect(operator).setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3')).to.be.reverted;
  });

  it("should add free entries", async function () {
    const result = await manager.connect(operator).createRaffle(ethers.parseUnits("1", "ether"), 1, token.getAddress(), 6, ethers.parseUnits("1", "ether"),
      [
        { id: 1, numEntries: 1, price: ethers.parseUnits("1", "ether") },
        { id: 2, numEntries: 5, price: ethers.parseUnits("2", "ether") },
      ], 2500, [], ONLY_DIRECTLY);
    receipt = await result.wait();
    idRaffle = await getRaffleId();

    await token.connect(seller).approve(manager.getAddress(), 6);
    await manager.connect(seller).stakeNFT(idRaffle);

    await manager.connect(player1).buyEntry(idRaffle, 1, zeroAddress, 0, { value: ethers.parseUnits('1', 'ether') });
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player4.getAddress(), player5.getAddress()]);
  });

  it("should transfer funds when the winner is from a free entry", async function () {
    const destinationWallet = await manager.destinationWallet();
    const balanceBefore = await ethers.provider.getBalance(destinationWallet);
    const balanceSellerBefore = await ethers.provider.getBalance(seller.getAddress());

    // the winner is from a free entry
    await manager.connect(operator).mockSetWinner(idRaffle, 2);

    const balanceAfter = await ethers.provider.getBalance(destinationWallet);
    const balanceSellerAfter = await ethers.provider.getBalance(seller.getAddress());
    //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    expect(balanceAfter).to.equal(balanceBefore + 250000000000000000n); // 25% of raised amount
    //check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    expect(balanceSellerAfter).to.equal(balanceSellerBefore + 750000000000000000n); // 75% of raised amount - gas fees
    // check the new owner of the token is the player (from the free entry) and not the previous owner (player2)
    const currentOwner = await token.ownerOf(6);
    expect(currentOwner).to.equal(await player4.getAddress());
    expect(currentOwner).not.to.equal(await player2.getAddress());
  });

  it("should transfer funds when the winner is from a Multi entry", async function () {
    let result = await manager.connect(operator).createRaffle(ethers.parseUnits('2', 'ether'), 5, token.getAddress(), 6,
      ethers.parseUnits('1', 'ether'),
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') },
      { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);
    receipt = await result.wait();
    idRaffle = await getRaffleId();
    await token.connect(player4).approve(manager.getAddress(), 6);
    await manager.connect(player4).stakeNFT(idRaffle);

    await manager.connect(player1).buyEntry(idRaffle, 2, zeroAddress, 0, { value: ethers.parseUnits('2', 'ether') });
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
    const destinationWallet = await manager.destinationWallet();
    const balanceBefore = await ethers.provider.getBalance(destinationWallet);
    const balanceSellerBefore = await ethers.provider.getBalance(player4.getAddress());

    // the winner is from a free entry
    result = await manager.connect(operator).mockSetWinner(idRaffle, 2);

    // check the event is emitted
    // expectEvent(result, 'RaffleEnded', { 'raffleId': idRaffle });
    await expect(result).to.emit(manager, "RaffleEnded").withArgs(idRaffle, () => true, () => true, () => true);
    // check the status is "ended"
    raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.equal(5);
    expect(raffle.entriesLength).to.equal(7);

    const balanceAfter = await ethers.provider.getBalance(destinationWallet);
    const balanceSellerAfter = await ethers.provider.getBalance(player4.getAddress());
    //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    expect(balanceAfter).to.equal(balanceBefore + 500000000000000000n); // 25% of raised amount
    //check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    expect(balanceSellerAfter).to.equal(balanceSellerBefore + 1500000000000000000n); // 75% of raised amount - gas fees

    // check the new owner of the token is the player (from the free entry) and not the previous owner (player2)
    const currentOwner = await token.ownerOf(6);
    expect(currentOwner).to.equal(await player1.getAddress());
    expect(currentOwner).not.to.equal(await player4.getAddress());
  });

  it("should fail adding free entries if the status is not accepted", async function () {
    // check adding free entries to a closed raffle
    await expect(manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()])).to.be.revertedWith("Raffle is not in accepted");
    // check adding free entries to a raffle that has not been accepted yet
    const result = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 1, token.getAddress(), 3, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);
    const receipt = await result.wait();
    idRaffle = await getRaffleId();
    expect(result).to.emit('RaffleCreated').withArgs(idRaffle);
    await expect(manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()])).to.be.revertedWith("Raffle is not in accepted");
  });

  it("should fail canceling a raffle already closed", async function () {
    await expect(manager.connect(operator).cancelRaffle(idRaffle - 1)).to.be.revertedWith("Wrong status");
  });

  it("should fail canceling a raffle a non-operator", async function () {
    await expect(manager.connect(player1).cancelRaffle(idRaffle)).to.be.reverted;
  });

  it("should cancel a raffle", async function () {
    let result = await manager.connect(operator).createRaffle(ethers.parseUnits('5', 'ether'), 1, token.getAddress(), 7, ethers.parseUnits('2', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);
    let receipt = await result.wait();
    idRaffle = await getRaffleId();
    result = await token.mint(seller.getAddress(), { from: owner.getAddress() });
    receipt = await result.wait();
    const tokenId = getTokenId(token);
    await token.connect(seller).approve(manager.getAddress(), 7);
    await manager.connect(seller).stakeNFT(idRaffle);

    await manager.connect(player1).buyEntry(idRaffle, 1, zeroAddress, 0, { value: ethers.parseUnits('1', 'ether') });

    let currentOwner = await token.ownerOf(7);
    expect(currentOwner).to.equal(await manager.getAddress());

    await manager.connect(operator).cancelRaffle(idRaffle);
    // check the seller has the nft back
    currentOwner = await token.ownerOf(7);
    expect(currentOwner).to.equal(await seller.getAddress());

    // check the raffle is canceled
    const raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.equal(6); // cancellation requested*/
  });

  it("should fail canceling a raffle already asked to cancel", async function () {
    await expect(manager.connect(player1).cancelRaffle(idRaffle)).to.be.reverted;
  });

  it("should allow to buy only those who stakes certain nft", async function () {
    result = await token.connect(owner).mint(seller.getAddress());
    receipt = await result.wait();
    tokenId = await getTokenId(token);

    result = await requiredNFT.connect(owner).mint(await player1.getAddress());
    receipt = await result.wait();
    const requiredTokenId1 = await getTokenId(requiredNFT);

    const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 10,
      token.getAddress(),
      tokenId,
      ethers.parseUnits("1", "ether"),
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') },
      { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500, [requiredNFT.getAddress()], ONLY_DIRECTLY);
    // console.log(JSON.stringify(resultCreation))

    idRaffle = await getRaffleId();

    await token.connect(seller).approve(manager.getAddress(), tokenId);
    await manager.connect(seller).stakeNFT(idRaffle);

    const destinationWallet = await manager.destinationWallet();
    const balanceBefore = await ethers.provider.getBalance(destinationWallet);

    // player1 has 1 nft of collection requiredToken
    result = await manager.connect(player1).buyEntry(idRaffle, 2, requiredNFT.getAddress(), requiredTokenId1, { value: ethers.parseUnits('2', 'ether') });
    receipt = await result.wait();
    console.log("Gas used buying with nft restriction: " + receipt.cumulativeGasUsed);

    await requiredNFT.mint(player2.getAddress());
    // player2 has 1 nft of collection requiredToken
    const r1 = await manager.connect(player2).buyEntry(idRaffle, 1, requiredNFT.getAddress(), 1, { value: ethers.parseUnits('1', 'ether') });
    receipt = await r1.wait();
    console.log("Gas used buying with nft restriction: " + receipt.cumulativeGasUsed);

    await expect(manager.connect(player4).buyEntry(idRaffle, 2, requiredNFT.getAddress(), 0, { value: ethers.parseUnits('2', 'ether') })).to.be.revertedWith("Not the owner of tokenId");
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    // the first call did generate a winner not in MW. So now lets do another call to setWinner
    result = await manager.connect(operator).mockSetWinner(idRaffle, 1);
    receipt = await result.wait();
    console.log("Gas used setting winner: " + receipt.cumulativeGasUsed);

    // check the winner received the NFT
    const currentOwner = await token.ownerOf(8);
    expect(currentOwner).to.equal(await player1.getAddress());

    // check the platform received the money (25% of the raffle pool)
    const balanceAfter = await ethers.provider.getBalance(destinationWallet);
    expect(balanceAfter).to.equal(balanceBefore + (ethers.parseUnits('0.75', 'ether'))); // 25% of 3 eth = 0.75 eth

  });

  it("should show the entries size", async function () {
    result = await token.connect(owner).mint(seller.getAddress());
    receipt = await result.wait();
    tokenId = getTokenId(token);

    const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 5, token.getAddress(), tokenId, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);

    idRaffle = await getRaffleId();

    await token.connect(seller).approve(manager.getAddress(), tokenId);
    await manager.connect(seller).stakeNFT(idRaffle);


    await manager.connect(player1).buyEntry(idRaffle, 2, zeroAddress, 0, { value: ethers.parseUnits('2', 'ether') });
    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.entriesLength).to.equal(5);

    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.entriesLength).to.equal(7);
  });

  it("should allow a free raffle", async function () {
    let result = await token.connect(owner).mint(seller.getAddress());
    let receipt = await result.wait();
    const tokenId = getTokenId(token);

    const resultCreation = await manager.connect(operator).createRaffle(0, 1, token.getAddress(), tokenId, 0,
      [{ id: 1, numEntries: 1, price: 0 }], 0, [], ONLY_DIRECTLY);

    idRaffle = await getRaffleId();

    await token.connect(seller).approve(manager.getAddress(), tokenId);
    await manager.connect(seller).stakeNFT(idRaffle);

    await manager.connect(player2).buyEntry(idRaffle, 1, zeroAddress, 0, { value: ethers.parseUnits('0', 'ether') });
    await manager.connect(player4).buyEntry(idRaffle, 1, zeroAddress, 0, { value: ethers.parseUnits('0', 'ether') });

    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    let currentOwner = await token.ownerOf(tokenId);
    // the first call did generate a winner not in MW. So now lets do another call to setWinner
    result = await manager.connect(operator).mockSetWinner(idRaffle, 0);

    // check the winner received the NFT
    currentOwner = await token.ownerOf(tokenId);

    const winner = await manager.getWinnerAddressFromRandom(idRaffle, 1);
    console.log("current owner " + currentOwner + " winner " + winner)
    expect(currentOwner).to.equal(winner);
  });

  it("should ask a raffle for cancellation step 1", async function () {
    let resultMint = await token.mint(seller.getAddress());
    const tokenId = getTokenId(token);

    let resultCreation = await manager.connect(operator).createRaffle(0, 5, token.getAddress(), tokenId, 0, [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);

    idRaffle = await getRaffleId();
    console.log("RaffleID = " + idRaffle)

    await token.connect(seller).approve(manager.getAddress(), tokenId);
    await manager.connect(seller).stakeNFT(idRaffle);

    await manager.connect(player2).buyEntry(idRaffle, 1, zeroAddress, 0, { value: ethers.parseUnits('1', 'ether') });
    await manager.connect(player4).buyEntry(idRaffle, 2, zeroAddress, 0, { value: ethers.parseUnits('2', 'ether') });
    await manager.connect(player1).buyEntry(idRaffle, 1, zeroAddress, 0, { value: ethers.parseUnits('1', 'ether') });
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.amountRaised).to.equal(ethers.parseUnits('4', 'ether'))
  });

  it("should fail transferring remaining funds if the raffle is not in cancellation_requested status", async function () {
    await expect(manager.connect(operator).transferRemainingFunds(idRaffle)).to.be.revertedWith("Wrong status");
  });

  it("should ask a raffle for cancellation step 2", async function () {
    await manager.connect(operator).cancelRaffle(idRaffle)
    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.equal(6)
  });

  it("should fail transferring remaining funds a non operator", async function () {
    await expect(manager.connect(player1).transferRemainingFunds(idRaffle)).to.be.reverted;
  });

  it("should transfer remaining funds", async function () {
    let result = await manager.connect(operator).transferRemainingFunds(idRaffle);
    expect(result).to.emit('RemainingFundsTransferred').withArgs(idRaffle, ethers.parseUnits('4', 'ether'));
  });

  it("should fail transferring remaining funds two times", async function () {
    let result = await manager.connect(operator).transferRemainingFunds(idRaffle);
    expect(result).to.emit('RemainingFundsTransferred').withArgs(idRaffle, ethers.parseUnits('0', 'ether'));
  });
  
    it("should allow to buy only those who stakes certain nft (only one required nft)", async function () {
      result = await requiredNFT.connect(owner).mint(player1.getAddress());
      receipt = await result.wait();
      requiredTokenId1 = getTokenId(requiredNFT);
  
      result = await token.connect(owner).mint(seller.getAddress());
      receipt = await result.wait();
      tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 10, token.getAddress(), tokenId, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500, [requiredNFT.getAddress()], ONLY_DIRECTLY);
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      // player1 has 1 nft (tokenId=0) of collection requiredToken. Buy 10 entries
      result = await manager.connect(player1).buyEntry(idRaffle, 2, requiredNFT.getAddress(), 0, { value: ethers.parseUnits('2', 'ether') });
      //  console.log("Gas used buying 10 entries with nft restriction: " + result.receipt.gasUsed);
  
      resultMint = await requiredNFT.mint(player2.getAddress());
      receipt = await result.wait();
      tokenId = getTokenId(requiredNFT);
      //  console.log("TokenId of player2 = " + tokenId);
      // player2 has 1 nft of collection requiredToken
  
      let r1 = await manager.connect(player2).buyEntry(idRaffle, 1, requiredNFT.getAddress(), 1, { value: ethers.parseUnits('1', 'ether') });
      //   console.log("Gas used buying 1 entry with nft restriction: " + r1.receipt.gasUsed);
  
      // should fail if the buyer did not have any nft
      await expect(manager.connect(player4).buyEntry(idRaffle, 2, requiredNFT.getAddress(), 0, { value: ethers.parseUnits('2', 'ether') }))
        .to.be.revertedWith("Not the owner of tokenId");
  
      // should fail if the buyer is not the owner of the tokenId
      await expect(manager.connect(player1).buyEntry(idRaffle, 1, requiredNFT.getAddress(), 1, { value: ethers.parseUnits('1', 'ether') }))
        .to.be.revertedWith("Not the owner of tokenId");
  
      // should fail if the tokenId was already used by another user (player2 receives tokenID=0 from player1)
      await requiredNFT.connect(player1).transferFrom(player1.getAddress(), player2.getAddress(), 0);
      await expect(manager.connect(player2).buyEntry(idRaffle, 2, requiredNFT.getAddress(), 0, { value: ethers.parseUnits('2', 'ether') }))
        .to.be.revertedWith("tokenId used");
  
      // should fail if the maxEntries is reached
      await requiredNFT.connect(player2).transferFrom(player2.getAddress(), player1.getAddress(), 0);
      // the current # of entries for player 1 is 20
      //   await manager.buyEntry(idRaffle, 2, requiredNFT1.getAddress(), 0, { from: player1, value: ethers.parseUnits('2', 'ether') });
  
      await expect(manager.connect(player1).buyEntry(idRaffle, 2, requiredNFT.getAddress(), 0, { value: ethers.parseUnits('2', 'ether') })
      ).to.revertedWithCustomError(manager,"EntryNotAllowed", "Wallet already used");
  
      await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
  
    });
  
    it("should allow to buy only those who stakes certain nft (several required nft)", async function () {
      result = await token.connect(owner).mint(seller.getAddress());
      receipt = await result.wait();
      tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 10, token.getAddress(), tokenId, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500, [requiredNFT1.getAddress(), requiredNFT2.getAddress(), requiredNFT3.getAddress()], ONLY_DIRECTLY);
  
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      resultMint = await requiredNFT1.mint(player1.getAddress());
  
      tokenId = getTokenId(requiredNFT1);
  
      // player1 has 1 nft (tokenId=0) of collection requiredToken. Buy 10 entries
      result = await manager.connect(player1).buyEntry(idRaffle, 2, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('2', 'ether') });
      //    console.log("Gas used buying 10 entries with nft restriction: " + result.receipt.gasUsed);
  
      resultMint = await requiredNFT1.mint(player2.getAddress());
      tokenId = getTokenId(requiredNFT1);
      // player2 has 1 nft of collection requiredToken  
      let r1 = await manager.connect(player2).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('1', 'ether') });
      //    console.log("Gas used buying 1 entry with nft restriction: " + r1.receipt.gasUsed);
  
      // should fail if the buyer did not have any nft
      await expect(manager.connect(player4).buyEntry(idRaffle, 2, requiredNFT1.getAddress(), 0, { value: ethers.parseUnits('2', 'ether') })).to.be.revertedWith("Not the owner of tokenId");
  
      // should fail if the buyer is not the owner of the tokenId
      await expect(manager.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), 1, {
        value: ethers.parseUnits('1', 'ether')
      })).to.be.revertedWith("Not the owner of tokenId");
  
      // should fail if the tokenId was already used by another user (player2 receives tokenID=0 from player1)
      await requiredNFT1.connect(player1).transferFrom(player1.getAddress(), player2.getAddress(), 0);
      await expect(manager.connect(player2).buyEntry(idRaffle, 2, requiredNFT1.getAddress(), 0, { value: ethers.parseUnits('2', 'ether') })).to.be.revertedWith("tokenId used");
  
      // should fail if the maxEntries is reached
      await requiredNFT1.connect(player2).transferFrom(player2.getAddress(), player1.getAddress(), 0);
  
      // mint token in requiredNFT2 and 3
      await requiredNFT2.mint(player5.getAddress());
      await requiredNFT3.mint(player2.getAddress());
  
      // buy an entry using required nfts #2 (player 5)
      await manager.connect(player5).buyEntry(idRaffle, 1, requiredNFT2.getAddress(), 0, { value: ethers.parseUnits('1', 'ether') });
      // should fail if the buyer is not the owner of the tokenId
      await expect(manager.connect(player3).buyEntry(idRaffle, 1, requiredNFT2.getAddress(), 0, { value: ethers.parseUnits('1', 'ether') })).to.be.revertedWith("Not the owner of tokenId");
      // buy an entry using required nfts #3 (player 2)
      //  await manager.buyEntry(idRaffle, 1, requiredNFT3.getAddress(), 0, { from: player2, value: ethers.parseUnits('1', 'ether') });
  
      await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
    });
  
    it("Gas cost when buying 100 entries (several required nft)", async function () {
      result = await token.connect(owner).mint(seller.getAddress());
      receipt = await result.wait();
      tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 200, token.getAddress(), tokenId, ethers.parseUnits('1', 'ether'),
        [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('3', 'ether') }], 2500, [requiredNFT1.getAddress(), requiredNFT2.getAddress(), requiredNFT3.getAddress()], ONLY_DIRECTLY);
  
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      resultMint = await requiredNFT1.mint(player1.getAddress());
      tokenId = getTokenId(requiredNFT1);
      // player1 has 1 nft (tokenId=0) of collection requiredToken. Buy 10 entries
      result = await manager.connect(player1).buyEntry(idRaffle, 2, requiredNFT1.getAddress(), 0, { value: ethers.parseUnits('2', 'ether') });
      //  console.log("Gas used buying 10 entries with nft restriction: " + result.receipt.gasUsed);
  
      resultMint = await requiredNFT1.mint(player2.getAddress());
      tokenId = getTokenId(requiredNFT1);
      // player2 has 1 nft of collection requiredToken
      // Buy 1 entry
      let r1 = await manager.connect(player2).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), 1, { value: ethers.parseUnits('1', 'ether') });
      //    console.log("Gas used buying 1 entry with nft restriction: " + r1.receipt.gasUsed);
  
      resultMint = await requiredNFT1.mint(player3.getAddress());
  
      tokenId = getTokenId(requiredNFT1);
      //  Buy 100 entries
      let r2 = await manager.connect(player3).buyEntry(idRaffle, 3, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('3', 'ether') });
      //    console.log("Gas used buying 100 entries with nft restriction: " + r2.receipt.gasUsed);
  
      resultMint = await requiredNFT1.mint(player4.getAddress());
  
      tokenId = getTokenId(requiredNFT1);
      //  Buy 100 entries
      let r3 = await manager.connect(player4).buyEntry(idRaffle, 3, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('3', 'ether') });
      //     console.log("Gas used buying 100 entries with nft restriction: " + r2.receipt.gasUsed);
  
      resultMint = await requiredNFT1.mint(player5.getAddress());
  
      tokenId = getTokenId(requiredNFT1);
      //  Buy 100 entries
      let r4 = await manager.connect(player5).buyEntry(idRaffle, 3, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('3', 'ether') });
      receipt = await r4.wait();
      console.log("Gas used buying 100 entries with nft restriction: " + receipt.cumulativeGasUsed);
  
      let rSetWinner = await manager.connect(operator).mockSetWinner(idRaffle, 11)
      let raffle = await manager.raffles(idRaffle);
      expect(raffle.winner).to.equal(await player3.getAddress());
      receipt = await rSetWinner.wait();
      console.log("Gas used setwinner: " + receipt.cumulativeGasUsed);
    });
  
    it("Gas cost when calling a lot of times getEntry (no required nft)", async function () {
      result = await token.connect(owner).mint(seller.getAddress());
      receipt = await result.wait();
      tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), 100, token.getAddress(), tokenId, ethers.parseUnits('0.01', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, [], ONLY_DIRECTLY);
  
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      // player2 buys 1 entry
      let r1 = await manager.connect(player2).buyEntry(idRaffle, 1, zeroAddress, 0, { value: ethers.parseUnits('0.01', 'ether') });
      receipt = await r1.wait();
      console.log("Gas used buying 1 entry no restriction: " + receipt.cumulativeGasUsed);
  
      // player1 buys 10 entries
      result = await manager.connect(player1).buyEntry(idRaffle, 2, zeroAddress, 0, { value: ethers.parseUnits('0.02', 'ether') });
      //  player3 buys 100 entries
      let r2 = await manager.connect(player3).buyEntry(idRaffle, 3, zeroAddress, 0, { value: ethers.parseUnits('0.03', 'ether') });
      //  player4 buys 100 entries
      let r3 = await manager.connect(player4).buyEntry(idRaffle, 3, zeroAddress, 0, { value: ethers.parseUnits('0.03', 'ether') });
  
      await expect(manager.connect(player6).buyEntry(idRaffle, 8, zeroAddress, 0, { value: ethers.parseUnits('0', 'ether') }))
        .to.be.revertedWithCustomError(manager,"EntryNotAllowed", "Id not in raffleId");
  
      //  player5 buys 100 entries
      let r4 = await manager.connect(player5).buyEntry(idRaffle, 3, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('0.03', 'ether') });
      receipt = await r4.wait();
      console.log("Gas used buying 100 entries no restriction: " + receipt.cumulativeGasUsed);
  
      let rSetWinner = await manager.connect(operator).mockSetWinner(idRaffle, 210)
      let raffle = await manager.raffles(idRaffle);
      //   console.log(JSON.stringify(raffle));
      expect(raffle.winner).to.equal(await player4.getAddress());
      receipt = await rSetWinner.wait();
      console.log("Gas used setwinner: " + receipt.cumulativeGasUsed);
    });
  
    it("should revert when player is blacklisted and allow if not", async function () {
      result = await token.connect(owner).mint(seller.getAddress());
      receipt = await result.wait();
      tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), 100, token.getAddress(), tokenId, ethers.parseUnits('0.01', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, [], ONLY_DIRECTLY);
  
      idRaffle = await getRaffleId();
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      // player1 allowed
      let r1 = await manager.connect(player1).buyEntry(idRaffle, 1, zeroAddress, 0, { value: ethers.parseUnits('0.01', 'ether') });
      //  player3 buys 100 entries
      let r2 = await manager.connect(player3).buyEntry(idRaffle, 3, zeroAddress, 0, { value: ethers.parseUnits('0.03', 'ether') });
      // player2 can buy again
      let r3 = await manager.connect(player2).buyEntry(idRaffle, 3, zeroAddress, 0, { value: ethers.parseUnits('0.03', 'ether') });
      let rSetWinner = await manager.connect(operator).mockSetWinner(idRaffle, 200)
  
      let raffle = await manager.raffles(idRaffle);
      //   console.log(JSON.stringify(raffle));
      expect(raffle.winner).to.equal(await player2.getAddress());
    });
  
    it("should manage blacklisted entries", async function () {
      result = await token.connect(owner).mint(seller.getAddress());
      receipt = await result.wait();
      tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), 100, token.getAddress(), tokenId, ethers.parseUnits('0.01', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, [], ONLY_DIRECTLY);
  
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      // player1 allowed
      let r1 = await manager.connect(player1).buyEntry(idRaffle, 1, zeroAddress, 0, { value: ethers.parseUnits('0.01', 'ether') });
      //  player3 buys 100 entries
      let r2 = await manager.connect(player2).buyEntry(idRaffle, 3, zeroAddress, 0, { value: ethers.parseUnits('0.03', 'ether') });
      let r3 = await manager.connect(player3).buyEntry(idRaffle, 3, zeroAddress, 0, { value: ethers.parseUnits('0.03', 'ether') });
  
      let rCancelEntry = await manager.connect(operator).cancelEntry(idRaffle, [2], player3.getAddress());
      receipt = await rCancelEntry.wait();
      console.log("Gas used cancelEntry: " + receipt.cumulativeGasUsed);
  
      // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2
      await manager.connect(operator).mockSetWinner(idRaffle, 200)
      let raffle = await manager.raffles(idRaffle);
      //   console.log(JSON.stringify(raffle));
      expect(raffle.winner).to.equal(await player2.getAddress());
    });
  
    it("should manage players as time passes", async function () {
      let result = await token.connect(owner).mint(seller.getAddress());
      receipt = await result.wait();
      tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), 100, token.getAddress(), tokenId, ethers.parseUnits('0.01', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, [], ONLY_DIRECTLY);
  
      idRaffle = await getRaffleId();
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
      // player1 allowed
      await manager.connect(player1).buyEntry(idRaffle, 1, zeroAddress, 0, { value: ethers.parseUnits('0.01', 'ether') });
      //  player3 buys 100 entries
      await manager.connect(player2).buyEntry(idRaffle, 3, zeroAddress, 0, { value: ethers.parseUnits('0.03', 'ether') });
      // player1 can buy again
      await manager.connect(player7).buyEntry(idRaffle, 3, zeroAddress, 0, { value: ethers.parseUnits('0.03', 'ether') });
      // player 4 buys 100 entries
      await manager.connect(player4).buyEntry(idRaffle, 3, zeroAddress, 0, { value: ethers.parseUnits('0.03', 'ether') });
      // player 5 buys 100 entries
      await manager.connect(player5).buyEntry(idRaffle, 3, zeroAddress, 0, { value: ethers.parseUnits('0.03', 'ether') });
  
      await expect(manager.connect(operator).cancelEntry(idRaffle, [2], player3.getAddress())).to.be.revertedWith("Entry did not belong to player");
      // test cancelEntry batch
      result = await manager.connect(operator).cancelEntry(idRaffle, [0], player1.getAddress());
      expect(result).to.emit('EntryCancelled').withArgs(idRaffle, '1', player1.getAddress());
  
      let entriesData = await manager.getEntriesBought(idRaffle);
      expect(entriesData[1].player).to.equal(zeroAddress);
      //  assert.equal(entriesData[3].player, zeroAddress);
  
      //for (let i = 0; i < 10; i++) {
      //    await manager.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player2, value: ethers.parseUnits('0.01', 'ether') });
      // }
  
      // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2
      let rSetWinner = await manager.connect(operator).mockSetWinner(idRaffle, 0)
      receipt = await rSetWinner.wait();
      console.log("Gas used setting winner: " + receipt.cumulativeGasUsed);
  
      let raffle = await manager.raffles(idRaffle);
      expect(raffle.winner).to.equal(await player5.getAddress());
    });
  
    it("should allow a raffle with mixed entries at price 0 and with prices > 0", async function () {
      result = await token.connect(owner).mint(seller.getAddress());
      receipt = await result.wait();
      tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(0, 10, token.getAddress(), tokenId, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 0, [], ONLY_DIRECTLY);
  
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      // Player 2 and player 4 get 1 entry for free
      await manager.connect(player2).buyEntry(idRaffle, 1, zeroAddress, 0, { value: ethers.parseUnits('0', 'ether') });
      await manager.connect(player4).buyEntry(idRaffle, 1, zeroAddress, 0, { value: ethers.parseUnits('0', 'ether') });
      // player 3 and player 5 get free entries via the operator
      await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
      // player 1 buys 10 entries for 0.005
      await manager.connect(player1).buyEntry(idRaffle, 2, zeroAddress, 0, { value: ethers.parseUnits('0.005', 'ether') });
  
      let currentOwner = await token.ownerOf(tokenId);
      // The winner will be player2
      result = await manager.connect(operator).mockSetWinner(idRaffle, 0);
  
      // check the winner received the NFT
      currentOwner = await token.ownerOf(tokenId);
  
      let winner = await manager.getWinnerAddressFromRandom(idRaffle, 1);
      expect(currentOwner).to.equal(winner);
      expect(currentOwner).to.equal(await player2.getAddress());
    });
  
    it("should avoid seller to buy entries (directly or hamburger)", async function () {
      let resultMint = await token.mint(seller.getAddress());
      let tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(0, 1, token.getAddress(), tokenId, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 0, [], 1);
  
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      // Seller cannot buy entries using buyEntry method
      //   await expectRevert(manager.buyEntry(idRaffle, 1, zeroAddress, 0, { from: seller, value: ethers.parseUnits('0', 'ether') }), "Seller cannot buy");
    });
  
    it("wallet cap == 0 means no wallet cap", async function () {
      let resultMint = await token.mint(seller.getAddress());
      let tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 0, token.getAddress(), tokenId, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500, [requiredNFT1.getAddress()], ONLY_DIRECTLY);
  
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      resultMint = await requiredNFT1.mint(player1.getAddress());
      tokenId = getTokenId(requiredNFT1);
      // player1 has 1 nft (tokenId=0) of collection requiredToken. Buy 10 entries
      result = await manager.connect(player1).buyEntry(idRaffle, 2, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('2', 'ether') });
  
      resultMint = await requiredNFT1.mint(player1.getAddress());
      tokenId = getTokenId(requiredNFT1);
      // player2 has 1 nft of collection requiredToken
      let r1 = await manager.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('1', 'ether') });
    });
  
    it("wallet cap == 2 means can call 2 times but fail a third time", async function () {
      let resultMint = await token.mint(seller.getAddress());
        tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 2, token.getAddress(), tokenId, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500, [requiredNFT1.getAddress()], ONLY_DIRECTLY);
  
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      resultMint = await requiredNFT1.mint(player1.getAddress());
      tokenId = getTokenId(requiredNFT1);
      // player1 has 1 nft (tokenId=0) of collection requiredToken. Buy 10 entries
      result = await manager.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('1', 'ether') });
  
      resultMint = await requiredNFT1.mint(player1.getAddress());
      tokenId = getTokenId(requiredNFT1);
      await manager.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('1', 'ether') });
  
      resultMint = await requiredNFT1.mint(player1.getAddress());
      tokenId = getTokenId(requiredNFT1);
  
      await expect(
        manager.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('1', 'ether') }),
      ).to.be.revertedWithCustomError(manager,"EntryNotAllowed", "Wallet already used");
    });
  
    it("param collection is in whitelist", async function () {
      let resultMint = await token.mint(seller.getAddress());
      let tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 2, token.getAddress(), tokenId, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500,
        [requiredNFT1.getAddress()], ONLY_DIRECTLY);
  
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      resultMint = await requiredNFT1.mint(player1.getAddress());
      tokenId = getTokenId(requiredNFT1);
      // player1 has 1 nft (tokenId=0) of collection requiredToken. Buy 10 entries
      result = await manager.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), 0, { value: ethers.parseUnits('1', 'ether') });
  
      resultMint = await requiredNFT1.mint(player1.getAddress());
      tokenId = getTokenId(requiredNFT1);
      await manager.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('1', 'ether') });
  
      resultMint = await requiredNFT3.mint(player1.getAddress());
      tokenId = getTokenId(requiredNFT3);
  
      await expect(manager.connect(player1).buyEntry(idRaffle, 1, requiredNFT3.getAddress(), tokenId, { value: ethers.parseUnits('1', 'ether') })
      ).to.be.revertedWith("Not in required collection");
  
      await expect(manager.connect(player1).buyEntry(idRaffle, 1, zeroAddress, tokenId, { value: ethers.parseUnits('1', 'ether') })
      ).to.be.revertedWith("Not in required collection");
    });
  
    it("should fail giving free entries two times to the same player", async function () {
      let resultMint = await token.mint(seller.getAddress());
      let tokenId = getTokenId(token);
  
      let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 100, token.getAddress(), tokenId, ethers.parseUnits('1', 'ether'),
        [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 2500,
        [requiredNFT1.getAddress()], ONLY_DIRECTLY);
  
      idRaffle = await getRaffleId();
  
      await token.connect(seller).approve(manager.getAddress(), tokenId);
      await manager.connect(seller).stakeNFT(idRaffle);
  
      resultMint = await requiredNFT1.mint(player1.getAddress());
  
      tokenId = getTokenId();
      // player1 has 1 nft (tokenId=0) of collection requiredToken. Buy 10 entries
      result = await manager.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), 0, { value: 0 });
  
      resultMint = await requiredNFT1.mint(player1.getAddress());
      tokenId = getTokenId(requiredNFT1);
      await expect(manager.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), tokenId, { value: 0 }))
        .to.be.revertedWithCustomError(manager,"EntryNotAllowed", "Player already got free entry");
  
      resultMint = await requiredNFT1.mint(player1.getAddress());
      tokenId = getTokenId(requiredNFT1);
      manager.connect(player1).buyEntry(idRaffle, 2, requiredNFT1.getAddress(), tokenId, { value: ethers.parseUnits('0.005', 'ether') })
    });
  
});
