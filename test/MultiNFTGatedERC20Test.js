const { expect } = require("chai");
const { ethers } = require("hardhat");

/// /// SEPOLIA //////////////

const SEPOLIA_VRF_COORDINATOR = "0x2bce784e69d2Ff36c71edcB9F88358dB0DfB55b4";
const SEPOLIA_LINK_TOKEN = "0x326C977E6efc84E512bB9C30f76E30c160eD06FB";
const SEPOLIA_KEYHASH =
  "0x0476f9a745b61ea5c0ab224d3a6e4c99f0b02fce4da01143a4f70aa80ae76e8a";
const ONLY_DIRECTLY = 0;

let token;
let manager;
let owner, player1, player2, player3, player4, player5, player6;
let seller, operator;
let idRaffle, receipt, result, resultMint, balance;
let requiredNFT,
  requiredNFT1,
  requiredNFT2,
  requiredNFT3,
  requiredTokenId,
  tokenId;
let basicERC20;
let tokenId1, tokenId2, tokenId3, tokenId4;
const zeroAddress = ethers.ZeroAddress;

async function getRaffleId() {
  filter = manager.filters.RaffleCreated;
  events = await manager.queryFilter(filter, -1)
  idRaffle = events[0].topics[1];
  return idRaffle;
}

async function getTokenId(tokenUsed) {
  filter = tokenUsed.filters.TokenCreated;
  events = await tokenUsed.queryFilter(filter, -1)
  returnedTokenId = events[0].topics[1];
  return returnedTokenId;
}

describe("ERC20 tests with 2 nft gating", function () {
  it("Should mint tokens and configure", async function () {
    [
      owner,
      player1,
      player2,
      player3,
      player4,
      player5,
      player6,
      player7,
      seller,
      operator,
    ] = await ethers.getSigners();

    const Basic721 = await ethers.getContractFactory("Basic721");
    token = await Basic721.deploy();
    await token.waitForDeployment();

    const BasicERC20 = await ethers.getContractFactory("BasicERC20");
    basicERC20 = await BasicERC20.deploy();
    await basicERC20.waitForDeployment();

    const RequiredNFT = await ethers.getContractFactory("Basic721");
    requiredNFT = await RequiredNFT.deploy();
    await requiredNFT.waitForDeployment();

    const RequiredNFT1 = await ethers.getContractFactory("Basic721");
    requiredNFT1 = await RequiredNFT1.deploy();
    await requiredNFT1.waitForDeployment();

    const RequiredNFT2 = await ethers.getContractFactory("Basic721");
    requiredNFT2 = await RequiredNFT2.deploy();
    await requiredNFT2.waitForDeployment();

    const RequiredNFT3 = await ethers.getContractFactory("Basic721");
    requiredNFT3 = await RequiredNFT3.deploy();
    await requiredNFT3.waitForDeployment();

    const MockMultiNFTGatedERC20 = await ethers.getContractFactory(
      "MockMultiNFTGatedERC20"
    );
    manager = await MockMultiNFTGatedERC20.deploy(
      SEPOLIA_VRF_COORDINATOR,
      SEPOLIA_LINK_TOKEN,
      SEPOLIA_KEYHASH,
      false
    );
    await manager.waitForDeployment();
  });

  it("Should add operator role to raffles", async function () {
    const operatorHash = ethers.id("OPERATOR");
    await manager.grantRole(operatorHash, operator.getAddress(), {
      from: owner.getAddress(),
    });
  });
  it("The contract balance should be cero ", async function () {
    const balance = await ethers.provider.getBalance(manager.getAddress());
    expect(balance).to.be.equal(0);
  });

  it("should allow to buy only those who stakes certain nft ", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(
      ethers.parseUnits("3", "ether"),
      11,
      basicERC20.getAddress(),
      100,
      ethers.parseUnits("1", "ether"),
      [
        { id: 1, numEntries: 1, price: ethers.parseUnits("1", "ether") },
        { id: 2, numEntries: 10, price: ethers.parseUnits("2", "ether") },
      ],
      2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress()],
      ONLY_DIRECTLY
    );
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();

    await basicERC20.approve(manager.getAddress(), 100);
    result = await manager.connect(owner).stake(0);
    expect(result).to.emit("RaffleStarted").withArgs("0", owner.getAddress());

    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);

    result = await manager.connect(player1).buyEntry(
      idRaffle,
      2,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("2", "ether") }
    );

    receipt = await result.wait();
    console.log(
      "Gas used buying 10 entries with 2 nft restriction: " +
      receipt.cumulativeGasUsed
    );

    // Player 1 buys another entry using other nfts he has.
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);

    await manager.connect(player1).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );

    await expect(
      manager.connect(player1).buyEntry(
        idRaffle,
        1,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        ],
        { value: ethers.parseUnits("1", "ether") }
      )
    ).to.be.revertedWithCustomError(manager, "EntryNotAllowed", "Wallet already used");

    resultMint = await requiredNFT1.mint(player2.getAddress());
    tokenId3 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player2.getAddress());
    tokenId4 = await getTokenId(requiredNFT2);
    const currentOwner = await requiredNFT2.ownerOf(tokenId4);
    expect(currentOwner).to.be.equal(await player2.getAddress());

    const tokenId5 = 2;
    await manager.connect(player2).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: 2 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId5 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );

    // should fail if the buyer is not the owner of the tokenId
    await expect(
      manager.connect(player4).buyEntry(
        idRaffle,
        2,
        [
          { collection: requiredNFT1.getAddress(), tokenId: 0 },
          { collection: requiredNFT2.getAddress(), tokenId: 0 },
        ],
        { value: ethers.parseUnits("2", "ether") }
      )
    ).to.be.revertedWith("Not the owner of tokenId");

    // should fail if the tokenId was already used by another user (player2 receives tokenID=0 from player1)
    await requiredNFT1
      .connect(player1)
      .transferFrom(player1.getAddress(), player2.getAddress(), 0);
    resultMint = await requiredNFT2.mint(player2.getAddress());
    tokenId1 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player2).buyEntry(
        idRaffle,
        2,
        [
          { collection: requiredNFT1.getAddress(), tokenId: 0 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId1 },
        ],
        { value: ethers.parseUnits("2", "ether") }
      ),
      "tokenId used"
    );

    // the current # of entries for player 1 is 20 (Wallets cap)
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player1).buyEntry(
        idRaffle,
        2,
        [
          { collection: requiredNFT1.getAddress(), tokenId: 3 },
          { collection: requiredNFT2.getAddress(), tokenId: 4 },
        ],
        { value: ethers.parseUnits("2", "ether") }
      )
    ).to.be.revertedWithCustomError(manager, "EntryNotAllowed", "Wallet already used");

    await manager.connect(operator).giveBatchEntriesForFree(0, [player3.getAddress(), player5.getAddress()]);
    // should fail if the player did not have an nft of each collection
    resultMint = await requiredNFT1.mint(player3.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player3.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player3).buyEntry(
        idRaffle,
        1,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT1.getAddress(), tokenId: tokenId2 },
        ],
        { value: ethers.parseUnits("1", "ether") }
      )
    ).to.be.revertedWith("Collection already used");
  });

  it("should close the raffle", async function () {
    const balanceBefore = await ethers.provider.getBalance(owner.getAddress());
    await manager.connect(operator).mockSetWinner(0, 2);

    result = await manager.raffles(0);
    expect(result.winner).to.be.equal(await player1.getAddress());

    // check the winner received the prize
    const winnerBalance = await basicERC20.balanceOf(player1.getAddress());
    expect(winnerBalance).to.be.equal(100);
    // check the seller received the money (75% of the raffle pool)
    const balanceAfter = await ethers.provider.getBalance(owner.getAddress());
    expect(balanceAfter).to.be.equal(
      balanceBefore + ethers.parseUnits("3", "ether")); // 75% of 3000 = 2250
  });

  it("should fail creating a raffle a non-operator", async function () {
    await expect(
      manager.connect(player2).createRaffle(
        1,
        1,
        basicERC20.getAddress(),
        100,
        2000,
        [
          { id: 1, numEntries: 1, price: 1000 },
          { id: 2, numEntries: 5, price: 800 },
          { id: 3, numEntries: 25, price: 700 },
        ],
        2500,
        [],
        ONLY_DIRECTLY
      )
    ).to.be.reverted;
  });

  it("should fail staking asset in a not created raffle", async function () {
    await basicERC20.approve(manager.getAddress(), 100);
    await expect(manager.connect(seller).stake(1)).to.be.reverted;
  });

  it("should fail staking asset in a raffle not in the right status", async function () {
    await expect(manager.connect(seller).stake(0)).to.be.revertedWith(
      "Raffle not CREATED"
    );
  });

  it("should create a new raffle", async function () {
    var result = await manager.connect(operator).createRaffle(
      1000,
      100,
      basicERC20.getAddress(),
      1,
      2000000000,
      [
        { id: 1, numEntries: 1, price: 1000000000 },
        { id: 2, numEntries: 5, price: 8000000000 },
        { id: 3, numEntries: 25, price: 70000000000 },
      ],
      2500,
      [],
      ONLY_DIRECTLY
    );
    expect(result).to.emit("RaffleCreated").withArgs("1");
  });

  it("should stake asset in a raffle", async function () {
    const result = await manager.stake(1);
    expect(result).to.emit("RaffleStarted").withArgs("1", owner.getAddress());
  });

  it("should create a new raffle with 3 collections in whitelist", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(
      ethers.parseUnits("3", "ether"),
      110,
      basicERC20.getAddress(),
      2,
      ethers.parseUnits("1", "ether"),
      [
        { id: 1, numEntries: 1, price: ethers.parseUnits("1", "ether") },
        { id: 2, numEntries: 10, price: ethers.parseUnits("2", "ether") },
      ],
      2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress(), requiredNFT3.getAddress()],
      ONLY_DIRECTLY
    );
    expect(resultCreation).to.emit("RaffleCreated").withArgs("2");
  });

  it("should stake asset in a raffle", async function () {
    const result = await manager.stake(2);
    expect(result).to.emit("RaffleStarted").withArgs("2", owner.getAddress());
  });

  it("should buy an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    resultMint = await requiredNFT3.mint(player1.getAddress());
    tokenId3 = await getTokenId(requiredNFT3);
    const r1 = await manager.connect(player1).buyEntry(
      2,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        { collection: requiredNFT3.getAddress(), tokenId: tokenId3 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );
    receipt = await r1.wait();
    console.log(
      "Gas used buying 1 entry with 3 nft restriction: " +
      receipt.cumulativeGasUsed
    );
  });

  it("should fail buying an entry using 3 nfts but 2 from same collection", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player1).buyEntry(
        2,
        1,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
          { collection: requiredNFT2.getAddress(), tokenId: 0 },
        ],
        { value: ethers.parseUnits("1", "ether") }
      )
    ).to.be.revertedWith("Collection already used");
  });

  it("should fail buying an entry using 3 nfts but 2 from same collection (1 and 2)", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId3 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player1).buyEntry(
        2,
        1,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT1.getAddress(), tokenId: tokenId3 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        ],
        { value: ethers.parseUnits("1", "ether") }
      )
    ).to.be.revertedWith("Collection already used");
  });

  it("should buy again an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    resultMint = await requiredNFT3.mint(player1.getAddress());
    tokenId3 = await getTokenId(requiredNFT3);
    const r1 = await manager.connect(player1).buyEntry(
      2,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        { collection: requiredNFT3.getAddress(), tokenId: tokenId3 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );
    receipt = await r1.wait();
    console.log(
      "Gas used buying 1 entry with 3 nft restriction: " +
      receipt.cumulativeGasUsed
    );
  });

  it("should buy again an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    resultMint = await requiredNFT3.mint(player1.getAddress());
    tokenId3 = await getTokenId(requiredNFT3);
    await manager.connect(player1).buyEntry(
      2,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        { collection: requiredNFT3.getAddress(), tokenId: tokenId3 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );
  });

  it("should fail  buying several entries for below price", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player1).buyEntry(
        2,
        2,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        ],
        { value: 700 }
      )
    ).to.be.revertedWithCustomError(manager, "EntryNotAllowed", "msg.value not the price");
  });

  it("should fail buying a wrong amount of entries", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player1).buyEntry(
        1,
        6,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        ],
        { value: 800 }
      )
    ).to.be.reverted;
  });

  it("should allow buy several entries", async function () {
    const balanceBefore = await ethers.provider.getBalance(manager.getAddress());

    resultMint = await requiredNFT1.mint(player2.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player2.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player2).buyEntry(
        2,
        2,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        ],
        { value: 8000000000 }
      )
    ).to.be.revertedWithCustomError(manager, "EntryNotAllowed", "msg.value not the price");
    await manager.connect(player2).buyEntry(
      2,
      2,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("2", "ether") }
    );

    const balanceAfter = await ethers.provider.getBalance(manager.getAddress());
    expect(balanceAfter).to.be.equal(
      balanceBefore + (ethers.parseUnits("2", "ether"))
    );
  });

  it("should change the destination address", async function () {
    await manager.setDestinationAddress(
      "0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3"
    );
  });

  it("should fail changing the destination address by a non owner", async function () {
    await expect(
      manager
        .connect(operator)
        .setDestinationAddress("0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3")
    ).to.be.reverted;
  });

  it("should fail when calling setwinner without having reached desired funds", async function () {
    await expect(
      manager.connect(operator).mockSetWinner(1, 1)
    ).to.be.revertedWith("Not enough funds raised");
  });

  it("should add free entries that won the raffle", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(
      ethers.parseUnits("1", "ether"),
      1,
      basicERC20.getAddress(),
      2,
      ethers.parseUnits("1", "ether"),
      [
        { id: 1, numEntries: 1, price: ethers.parseUnits("1", "ether") },
        { id: 2, numEntries: 5, price: ethers.parseUnits("2", "ether") },
      ],
      2500,
      [],
      ONLY_DIRECTLY
    );
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();
    await manager.stake(idRaffle);

    resultMint = await requiredNFT1.mint(player2.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player2.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player1).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );
    await manager
      .connect(operator)
      .giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    const destinationWallet = await manager.destinationWallet();
    const balanceBefore = await ethers.provider.getBalance(destinationWallet);
    const balanceSellerBefore = await ethers.provider.getBalance(owner.getAddress());

    // the winner is from a free entry
    await manager.connect(operator).mockSetWinner(idRaffle, 2);

    // check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    const balanceAfter = await ethers.provider.getBalance(destinationWallet);
    expect(balanceAfter).to.be.equal(balanceBefore + 250000000000000000n); // 25% of raised amount
    const balanceSellerAfter = await ethers.provider.getBalance(owner.getAddress());
    // check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    expect(balanceSellerAfter).to.be.equal(
      balanceSellerBefore + 750000000000000000n); // 75% of raised amount - gas fees
    // check the new owner of the tokens is the player (from the free entry)

    const winnerBalance = await basicERC20.balanceOf(player5.getAddress());
    console.log("Balance of winner = " + winnerBalance);
    expect(winnerBalance).to.be.equal(2);
  });

  it("should transfer funds when the winner is from a Multi entry", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(
      ethers.parseUnits("2", "ether"),
      100,
      basicERC20.getAddress(),
      2,
      ethers.parseUnits("1", "ether"),
      [
        { id: 1, numEntries: 1, price: ethers.parseUnits("1", "ether") },
        { id: 2, numEntries: 5, price: ethers.parseUnits("2", "ether") },
      ],
      2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress()],
      ONLY_DIRECTLY
    );
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();
    await manager.stake(idRaffle);

    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player1).buyEntry(
      idRaffle,
      2,
      [
        { collection: requiredNFT1.getAddress(), tokenId: 10 },
        { collection: requiredNFT2.getAddress(), tokenId: 9 },
      ],
      { value: ethers.parseUnits("2", "ether") }
    );
    await manager
      .connect(operator)
      .giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    const destinationWallet = await manager.destinationWallet();
    const balanceBefore = await ethers.provider.getBalance(destinationWallet);
    const balanceSellerBefore = await ethers.provider.getBalance(owner.getAddress());

    // the winner is from a free entry
    result = await manager.connect(operator).mockSetWinner(idRaffle, 2);

    // check the event is emitted
    expect(result).to.emit("RaffleEnded").withArgs(idRaffle);
    // check the status is "ended"
    raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(5);
    expect(raffle.entriesLength).to.be.equal(7);

    const balanceAfter = await ethers.provider.getBalance(destinationWallet);
    // check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    expect(balanceAfter).to.be.equal(balanceBefore + 500000000000000000n); // 25% of raised amount
    const balanceSellerAfter = await ethers.provider.getBalance(owner.getAddress());
    // check the funds sent to the seller are 75% of raised amount
    expect(balanceSellerAfter).to.be.equal(
      balanceSellerBefore + 1500000000000000000n
    ); // 75% of raised amount - gas fees
  });

  it("should fail adding free entries if the status is not accepted", async function () {
    // check adding free entries to a closed raffle
    await expect(
      manager
        .connect(operator)
        .giveBatchEntriesForFree(4, [player3.getAddress(), player5.getAddress()])
    ).to.be.revertedWith("Raffle is not in accepted");
    // check adding free entries to a raffle that has not been accepted yet
    let result = await manager.connect(operator).createRaffle(
      ethers.parseUnits("3", "ether"),
      1,
      basicERC20.getAddress(),
      2,
      ethers.parseUnits("1", "ether"),
      [
        { id: 1, numEntries: 1, price: ethers.parseUnits("1", "ether") },
        { id: 2, numEntries: 5, price: ethers.parseUnits("2", "ether") },
      ],
      2500,
      [],
      ONLY_DIRECTLY
    );
    expect(result).to.emit("RaffleCreated").withArgs("5");

    await expect(
      manager
        .connect(operator)
        .giveBatchEntriesForFree(5, [player3.getAddress(), player5.getAddress()])
    ).to.be.revertedWith("Raffle is not in accepted");
  });

  it("should fail canceling a raffle already closed", async function () {
    await expect(manager.connect(operator).cancelRaffle(4)).to.be.revertedWith(
      "Wrong status"
    );
  });

  it("should fail canceling a raffle a non-operator", async function () {
    await expect(manager.connect(player1).cancelRaffle(5)).to.be.reverted;
  });

  it("should cancel a raffle", async function () {
    let result = await manager.connect(operator).createRaffle(
      ethers.parseUnits("5", "ether"),
      1,
      basicERC20.getAddress(),
      7,
      ethers.parseUnits("2", "ether"),
      [
        { id: 1, numEntries: 1, price: ethers.parseUnits("1", "ether") },
        { id: 2, numEntries: 5, price: ethers.parseUnits("2", "ether") },
      ],
      2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress()],
      ONLY_DIRECTLY
    );
    expect(result, "RaffleCreated", { raffleId: "6" });
    receipt = await result.wait();
    idRaffle = await getRaffleId();
    await manager.stake(idRaffle);

    resultMint = await requiredNFT1.mint(player2.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player2.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player2).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );

    let sellerBalanceBefore = await basicERC20.balanceOf(owner.getAddress());

    await manager.connect(operator).cancelRaffle(idRaffle);

    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(6);

    // check the seller has the tokens back
    let sellerBalanceAfter = await basicERC20.balanceOf(owner.getAddress());
    let sellerBalance = sellerBalanceAfter - (sellerBalanceBefore);
    expect(sellerBalance).to.be.equal("7");
    // check the raised funds has been transferred to the destination wallet
    await manager.connect(operator).transferRemainingFunds(idRaffle);
  });

  it("should fail canceling a raffle already asked to cancel", async function () {
    await expect(manager.connect(operator).cancelRaffle(6)).to.be.reverted;
  });

  it("should allow a free raffle", async function () {
    let resultCreation = await manager
      .connect(operator)
      .createRaffle(
        0,
        1,
        basicERC20.getAddress(),
        1,
        0,
        [{ id: 1, numEntries: 1, price: 0 }],
        0,
        [requiredNFT1.getAddress(), requiredNFT2.getAddress()],
        ONLY_DIRECTLY
      );
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();

    await manager.stake(idRaffle);

    resultMint = await requiredNFT1.mint(player2.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player2.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player2).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("0", "ether") }
    );

    resultMint = await requiredNFT1.mint(player4.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player4.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player4).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("0", "ether") }
    );

    await manager
      .connect(operator)
      .giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    // the first call did generate a winner not in MW. So now lets do another call to setWinner
    result = await manager.connect(operator).mockSetWinner(idRaffle, 0);

    let winner = await manager.getWinnerAddressFromRandom(idRaffle, 1);
    expect(await player2.getAddress()).to.be.equal(winner);
  });

  it("should ask a raffle for cancellation step 1", async function () {
    let resultCreation = await manager.connect(operator).createRaffle(
      0,
      10,
      basicERC20.getAddress(),
      1,
      0,
      [
        { id: 1, numEntries: 1, price: ethers.parseUnits("1", "ether") },
        { id: 2, numEntries: 5, price: ethers.parseUnits("2", "ether") },
      ],
      2500,
      [],
      ONLY_DIRECTLY
    );
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();
    console.log("RaffleID = " + idRaffle);

    await manager.stake(idRaffle);

    resultMint = await requiredNFT1.mint(player2.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player2.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player2).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );
    resultMint = await requiredNFT1.mint(player4.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player4.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player4).buyEntry(
      idRaffle,
      2,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("2", "ether") }
    );
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player1).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );

    await manager
      .connect(operator)
      .giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    let raffle = await manager.getRafflesEntryInfo(idRaffle);

    expect(raffle.amountRaised).to.be.equal(
      ethers.parseUnits("4", "ether")
    );
  });

  it("should fail transferring remaining funds if the raffle is not in cancellation_requested status", async function () {
    await expect(
      manager.connect(operator).transferRemainingFunds(8)
    ).to.be.revertedWith("Wrong status");
  });

  it("should ask a raffle for cancellation step 2", async function () {
    await manager.connect(operator).cancelRaffle(8);
    let raffle = await manager.getRafflesEntryInfo(8);

    expect(raffle.status).to.be.equal(6);
  });

  it("should fail transferring remaining funds a non operator", async function () {
    await expect(manager.connect(player1).transferRemainingFunds(8)).to.be
      .reverted;
  });

  it("should transfer remaining funds", async function () {
    let result = await manager.connect(operator).transferRemainingFunds(8);
    expect(result)
      .to.emit("RemainingFundsTransferred")
      .withArgs("8", ethers.parseUnits("4", "ether"));
  });

  it("should fail transferring remaining funds two times", async function () {
    let result = await manager.connect(operator).transferRemainingFunds(8);

    expect(result)
      .to.emit("RemainingFundsTransferred")
      .withArgs("8", ethers.parseUnits("0", "ether"));
  });

  it("should manage blacklisted entries", async function () {
    let resultCreation = await manager.connect(operator).createRaffle(
      ethers.parseUnits("0.03", "ether"),
      1000,
      basicERC20.getAddress(),
      1,
      ethers.parseUnits("0.01", "ether"),
      [
        {
          id: 1,
          numEntries: 1,
          price: ethers.parseUnits("0.01", "ether"),
        },
        {
          id: 2,
          numEntries: 10,
          price: ethers.parseUnits("0.02", "ether"),
        },
        {
          id: 3,
          numEntries: 100,
          price: ethers.parseUnits("0.03", "ether"),
        },
      ],
      2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress()],
      ONLY_DIRECTLY
    );
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();

    await manager.stake(idRaffle);

    // player1 allowed
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player1).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("0.01", "ether") }
    );

    resultMint = await requiredNFT1.mint(player2.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player2.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player2).buyEntry(
      idRaffle,
      3,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("0.03", "ether") }
    );

    resultMint = await requiredNFT1.mint(player3.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player3.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player3).buyEntry(
      idRaffle,
      3,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("0.03", "ether") }
    );

    let rCancelEntry = await manager
      .connect(operator)
      .cancelEntry(idRaffle, [2], player3.getAddress());
    receipt = await rCancelEntry.wait();
    console.log("Gas used cancelEntry: " + receipt.cumulativeGasUsed);

    // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2

    let rSetWinner = await manager
      .connect(operator)
      .mockSetWinner(idRaffle, 200);
    let raffle = await manager.raffles(idRaffle);
    expect(raffle.winner, player2.getAddress());
  });

  it("should manage players as time passes", async function () {
    let resultCreation = await manager.connect(operator).createRaffle(
      ethers.parseUnits("0.03", "ether"),
      1000,
      basicERC20.getAddress(),
      1,
      ethers.parseUnits("0.01", "ether"),
      [
        {
          id: 1,
          numEntries: 1,
          price: ethers.parseUnits("0.01", "ether"),
        },
        {
          id: 2,
          numEntries: 10,
          price: ethers.parseUnits("0.02", "ether"),
        },
        {
          id: 3,
          numEntries: 100,
          price: ethers.parseUnits("0.03", "ether"),
        },
      ],
      2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress()],
      ONLY_DIRECTLY
    );
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();
    await manager.stake(idRaffle);
    // player1 allowed
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    let r1 = await manager.connect(player1).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("0.01", "ether") }
    );
    resultMint = await requiredNFT1.mint(player2.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player2.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    let r2 = await manager.connect(player2).buyEntry(
      idRaffle,
      3,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("0.03", "ether") }
    );
    // player1 can buy again
    resultMint = await requiredNFT1.mint(player7.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player7.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    let r3 = await manager.connect(player7).buyEntry(
      idRaffle,
      3,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("0.03", "ether") }
    );
    // player 4 buys 100 entries
    resultMint = await requiredNFT1.mint(player4.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player4.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player4).buyEntry(
      idRaffle,
      3,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("0.03", "ether") }
    );
    // player 5 buys 100 entries
    resultMint = await requiredNFT1.mint(player5.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player5.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player5).buyEntry(
      idRaffle,
      3,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("0.03", "ether") }
    );

    await expect(
      manager.connect(operator).cancelEntry(idRaffle, [2], player3.getAddress())
    ).to.be.revertedWith("Entry did not belong to player");
    // test cancelEntry batch
    let result = await manager
      .connect(operator)
      .cancelEntry(idRaffle, [0], player1.getAddress());
    expect(result)
      .to.emit("EntryCancelled")
      .withArgs(idRaffle, "1", player1.getAddress());

    let entriesData = await manager.getEntriesBought(idRaffle);
    expect(entriesData[1].player).to.be.equal(zeroAddress);

    // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2
    let rSetWinner = await manager.connect(operator).mockSetWinner(idRaffle, 0);

    let raffle = await manager.raffles(idRaffle);
    expect(raffle.winner).to.be.equal(await player5.getAddress());
  });

  it("should create a new raffle", async function () {
    var result = await manager.connect(operator).createRaffle(
      1000,
      100,
      basicERC20.getAddress(),
      1,
      2000000000,
      [
        { id: 1, numEntries: 1, price: 1000000000 },
        { id: 2, numEntries: 5, price: 8000000000 },
      ],
      2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress()],
      ONLY_DIRECTLY
    );
    expect(result).to.emit("RaffleCreated").withArgs("11");
  });
  it("should stake asset in a raffle", async function () {
    let result = await manager.stake(11);
    expect(result).to.emit("RaffleStarted").withArgs("11", owner.getAddress());
  });
  it("should fail buying with less nfts than required", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    await expect(
      manager
        .connect(player1)
        .buyEntry(
          11,
          1,
          [{ collection: requiredNFT1.getAddress(), tokenId: tokenId1 }],
          { value: 1000000000 }
        )
    ).to.be.revertedWith("Not enough tokens staked");
  });

  it("should fail giving free entries two times to the same player", async function () {
    let resultCreation = await manager.connect(operator).createRaffle(
      ethers.parseUnits("0.03", "ether"),
      1000,
      basicERC20.getAddress(),
      1,
      ethers.parseUnits("0.01", "ether"),
      [
        { id: 1, numEntries: 1, price: 0 },
        {
          id: 2,
          numEntries: 10,
          price: ethers.parseUnits("0.02", "ether"),
        },
      ],
      2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress()],
      ONLY_DIRECTLY
    );
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();
    await manager.stake(idRaffle);
    // player1 free entry
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player1).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: 0 }
    );
    // player 2 free entry
    resultMint = await requiredNFT1.mint(player2.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player2.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    let r2 = await manager.connect(player2).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: 0 }
    );
    // player1 can not get free entry again
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager,
      manager.connect(player1).buyEntry(
        idRaffle,
        1,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        ],
        { value: 0 }
      ),
      "EntryNotAllowed",
      ["Player already got free entry"]
    );
    // player 1 can buy paid entries
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player1).buyEntry(
      idRaffle,
      2,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("0.02", "ether") }
    );
  });
});
