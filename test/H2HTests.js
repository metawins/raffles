const { expect } = require("chai");
const { ethers } = require("hardhat");

/// /// SEPOLIA //////////////
const SEPOLIA_VRF_COORDINATOR = "0x2bce784e69d2Ff36c71edcB9F88358dB0DfB55b4";
const SEPOLIA_LINK_TOKEN = "0x326C977E6efc84E512bB9C30f76E30c160eD06FB";
const SEPOLIA_KEYHASH =
  "0x0476f9a745b61ea5c0ab224d3a6e4c99f0b02fce4da01143a4f70aa80ae76e8a";
const ONLY_DIRECTLY = 0;

let token;
let H2H;
let owner, player1, player2, player3, player4, player5, player6;
let seller, operator;
let idRaffle, receipt;
let requiredNFT,
  requiredNFT1,
  requiredNFT2,
  requiredNFT3,
  requiredTokenId,
  tokenId;
const zeroAddress = ethers.ZeroAddress;
const UNFULFILLED = 7;
const ENDED = 5;
const CANCELLED = 3;

async function getRaffleId() {
  filter = H2H.filters.RaffleCreated;
  events = await H2H.queryFilter(filter, -1)
  idRaffle = events[0].topics[1];
  return idRaffle;
}

describe("H2H", function () {
  it("Should mint tokens and configure", async function () {
    [
      owner,
      player1,
      player2,
      player3,
      player4,
      player5,
      player6,
      player7,
      seller,
      operator,
    ] = await ethers.getSigners();

    const H2HComp = await ethers.getContractFactory("MockH2HComp");
    H2H = await H2HComp.deploy(
      SEPOLIA_VRF_COORDINATOR,
      SEPOLIA_LINK_TOKEN,
      SEPOLIA_KEYHASH,
      false
    );
    await H2H.waitForDeployment();
  });

  it("Should add operator role to ETH-prize raffles", async function () {
    const operatorHash = ethers.id("OPERATOR");
    await H2H.grantRole(operatorHash, operator.getAddress());
  });

  it("should create an ETH-prized raffle", async function () {
    const result = await H2H.connect(operator).createH2HRaffle(
      ethers.parseUnits("1", "ether"),
      ethers.parseUnits("0.6", "ether"),
      ethers.parseUnits("0.55", "ether"),
      ONLY_DIRECTLY,
      2
    );
    expect(result).to.emit("RaffleCreated").withArgs("0");
  });

  it("should buy 2 entries on the raffle", async function () {
    let result = await H2H.connect(player3).buyEntry(0, {
      value: ethers.parseUnits("0.55", "ether"),
    });

    expect(result).to.emit("EntrySold").withArgs("0", player3, "1");
    receipt = await result.wait();
    console.log("Gas used buying 1st player: " + receipt.cumulativeGasUsed);

    result = await H2H.connect(player2).buyEntry(0, {
      value: ethers.parseUnits("0.55", "ether"),
    });
    expect(result).to.emit("EntrySold").withArgs("0", player2.getAddress(), "2");
    receipt = await result.wait();
    console.log("Gas used buying 2nd player: " + receipt.cumulativeGasUsed);
  });

  it("should fail paying less than set in the raffle", async function () {
    await expect(
      H2H.connect(player3).buyEntry(0, {
        value: ethers.parseUnits("0.15", "ether"),
      }),
      "EntryNotAllowed",
      ["Price not reached"]
    );
  });

  it("should close the raffle h2h with a winner", async function () {
    const balanceSellerBefore = await ethers.provider.getBalance(
      player1.getAddress()
    );
    const balanceWinnerBefore = await ethers.provider.getBalance(
      player2.getAddress()
    );
    const destinationWallet = await H2H.destinationWallet();
    const balanceDestinationBefore = await ethers.provider.getBalance(
      destinationWallet
    );

    const result = await H2H.connect(operator).mockSetWinner(0, 1);
    expect(result)
      .to.emit("RaffleEnded")
      .withArgs("0", ethers.parseUnits("1.1", "ether"));
    expect(result)
      .to.emit("FeeTransferredToPlatform")
      .withArgs(ethers.parseUnits("0.1", "ether"));

    receipt = await result.wait();
    console.log("Gas used closing with winner: " + receipt.cumulativeGasUsed);

    const balanceWinnerAfter = await ethers.provider.getBalance(
      player2.getAddress()
    );
    expect(balanceWinnerAfter).to.be.equal(
      balanceWinnerBefore+(ethers.parseUnits("1", "ether"))
    );

    const balanceDestinationAfter = await ethers.provider.getBalance(
      destinationWallet
    );
    expect(balanceDestinationAfter).to.equal(
      balanceDestinationBefore+(ethers.parseUnits("0.1", "ether"))
    );
  });

  it("should fail buying an entry after draw", async function () {
    await expect(
      H2H.connect(player1).buyEntry(0, {
        value: ethers.parseUnits("0.55", "ether"),
      })
    ).to.be.revertedWithCustomError(H2H,"EntryNotAllowed", ["Not in ACCEPTED"]);
  });

  it("should create another h2h. Only 1 buyer. Get failure amount fails because no balance, but do it well after being funded", async function () {
    const resultCreation = await H2H.connect(operator).createH2HRaffle(
      ethers.parseUnits("1", "ether"),
      ethers.parseUnits("0.6", "ether"),
      ethers.parseUnits("0.55", "ether"),
      ONLY_DIRECTLY,
      2
    );
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();

    const result = await H2H.connect(player3).buyEntry(idRaffle, {
      value: ethers.parseUnits("0.55", "ether"),
    });
    expect(result)
      .to.emit("EntrySold")
      .withArgs(idRaffle, player3.getAddress(), "1");
    receipt = await result.wait();
    console.log("Gas used buying: " + receipt.cumulativeGasUsed);

    const balanceWinnerBefore = await ethers.provider.getBalance(
      player3.getAddress()
    );

    // Fails because no balance enough
    await expect(
      H2H.connect(operator).mockSetWinner(idRaffle, 1)
    ).to.be.revertedWith("Not enough balance");
    let balanceH2H = await ethers.provider.getBalance(H2H.getAddress());

    console.log("Contract H2H balance Before = " + balanceH2H);
    await player1.sendTransaction({
      to: H2H.getAddress(),
      value: ethers.parseUnits("1", "ether"),
    });

    balanceH2H = await ethers.provider.getBalance(H2H.getAddress());
    console.log("Contract H2H balance after  = " + balanceH2H);
    // now the h2h contract is funded, setwinner can work
    const resultWin = await H2H.connect(operator).mockSetWinner(idRaffle, 1);
    receipt = await resultWin.wait();
    console.log(
      "Gas used closing with failure amount: " + receipt.cumulativeGasUsed
    );
    expect(resultWin).to.emit("RaffleUnfulfilled").withArgs(idRaffle);

    const balanceWinnerAfter = await ethers.provider.getBalance(
      player3.getAddress()
    );
    expect(balanceWinnerAfter).to.be.equal(
      balanceWinnerBefore+(ethers.parseUnits("0.6", "ether"))
    );
  });

  it("should create a 4 H2H raffle, and close it with a winner", async function () {
    const resultCreation = await H2H.connect(operator).createH2HRaffle(
      ethers.parseUnits("2", "ether"),
      ethers.parseUnits("0.6", "ether"),
      ethers.parseUnits("0.55", "ether"),
      ONLY_DIRECTLY,
      4
    );
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();

    expect(resultCreation).to.emit("RaffleCreated").withArgs(idRaffle);

    await H2H.connect(player1).buyEntry(idRaffle, {
      value: ethers.parseUnits("0.55", "ether"),
    });
    await H2H.connect(player2).buyEntry(idRaffle, {
      value: ethers.parseUnits("0.55", "ether"),
    });
    await H2H.connect(player3).buyEntry(idRaffle, {
      value: ethers.parseUnits("0.55", "ether"),
    });
    // assert only one entry per player is allowed
    //     await expectRevert(H2H.buyEntry(idRaffle, { from: player3, value: ethers.parseUnits('0.55', 'ether') }), "Bought too many entries");
    const result = await H2H.connect(player4).buyEntry(idRaffle, {
      value: ethers.parseUnits("0.55", "ether"),
    });
    receipt = result.wait();
    console.log("Gas used buying player #4: " + receipt.cumulativeGasUsed);
    // assert only 4 entries are sold in a 4 H2H competition
    await expect(
      H2H.connect(player3).buyEntry(idRaffle, {
        value: ethers.parseUnits("0.55", "ether"),
      })
    ).to.revertedWithCustomError(H2H, "EntryNotAllowed", "Total Cap Entries reached");

    const destinationWallet = await H2H.destinationWallet();
    const balanceDestinationBefore = await ethers.provider.getBalance(
      destinationWallet
    );
    const balanceWinnerBefore = await ethers.provider.getBalance(
      player3.getAddress()
    );

    const resultWin = await H2H.connect(operator).mockSetWinner(idRaffle, 2);
    expect(resultWin)
      .to.emit("RaffleEnded")
      .withArgs(idRaffle, ethers.parseUnits("2.2", "ether"));
    expect(resultWin)
      .to.emit("FeeTransferredToPlatform")
      .withArgs(ethers.parseUnits("0.2", "ether"));

    const balanceWinnerAfter = await ethers.provider.getBalance(
      player3.getAddress()
    );

    expect(balanceWinnerAfter).to.be.equal(
      balanceWinnerBefore+(ethers.parseUnits("2", "ether"))
    );

    const balanceDestinationAfter = await ethers.provider.getBalance(
      destinationWallet
    );
    expect(balanceDestinationAfter).to.be.equal(
      balanceDestinationBefore+(ethers.parseUnits("0.2", "ether"))
    );

    const raffle = await H2H.rafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(ENDED);
  });

  it("should create a 4 H2H raffle, and cancel it if only 3 players exists (SCENARIO 2)", async function () {
    const resultCreation = await H2H.connect(operator).createH2HRaffle(
      ethers.parseUnits("2", "ether"),
      ethers.parseUnits("0.6", "ether"),
      ethers.parseUnits("0.55", "ether"),
      ONLY_DIRECTLY,
      4
    );
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();

    expect(resultCreation).to.emit("RaffleCreated").withArgs(idRaffle);

    await H2H.connect(player1).buyEntry(idRaffle, {
      value: ethers.parseUnits("0.55", "ether"),
    });
    await H2H.connect(player2).buyEntry(idRaffle, {
      value: ethers.parseUnits("0.55", "ether"),
    });
    await H2H.connect(player3).buyEntry(idRaffle, {
      value: ethers.parseUnits("0.55", "ether"),
    });

    const destinationWallet = await H2H.destinationWallet();
    const balanceDestinationBefore = await ethers.provider.getBalance(
      destinationWallet
    );
    const balanceWinner1Before = await ethers.provider.getBalance(
      player1.getAddress()
    );
    const balanceWinner2Before = await ethers.provider.getBalance(
      player2.getAddress()
    );
    const balanceWinner3Before = await ethers.provider.getBalance(
      player3.getAddress()
    );

    const resultWin = await H2H.connect(operator).mockSetWinner(idRaffle, 2);
    expect(resultWin)
      .to.emit("RaffleUnfulfilled")
      .withArgs(
        idRaffle,
        player1,
        ethers.parseUnits("0.6", "ether"),
        ethers.parseUnits("1.65", "ether")
      );
    const entryInfo = await H2H.rafflesEntryInfo(idRaffle);
    const raffle = await H2H.raffles(idRaffle);
    expect(entryInfo.status).to.be.equal(UNFULFILLED);
    expect(raffle.winner).to.be.equal(await player1.getAddress());

    const balanceWinner1After = await ethers.provider.getBalance(
      player1.getAddress()
    );
    const balanceWinner2After = await ethers.provider.getBalance(
      player2.getAddress()
    );
    const balanceWinner3After = await ethers.provider.getBalance(
      player3.getAddress()
    );
    expect(balanceWinner1After).to.be.equal(
      balanceWinner1Before+(ethers.parseUnits("0.6", "ether"))
    );
    expect(balanceWinner2After).to.be.equal(
      balanceWinner2Before+(ethers.parseUnits("0.55", "ether"))
    );
    expect(balanceWinner3After).to.be.equal(
      balanceWinner3Before+(ethers.parseUnits("0.55", "ether"))
    );

    const balanceDestinationAfter = await ethers.provider.getBalance(
      destinationWallet
    );
    expect(balanceDestinationAfter).to.be.equal(
      balanceDestinationBefore+(ethers.parseUnits("0", "ether"))
    );
  });

  it("should create a 4 H2H raffle, but nobody boughts (SCENARIO 3)", async function () {
    const resultCreation = await H2H.connect(operator).createH2HRaffle(
      ethers.parseUnits("2", "ether"),
      ethers.parseUnits("0.6", "ether"),
      ethers.parseUnits("0.55", "ether"),
      ONLY_DIRECTLY,
      4
    );
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();

    expect(resultCreation).to.emit("RaffleCreated").withArgs(idRaffle);

    await expect(
      H2H.connect(operator).mockSetWinner(idRaffle, 2)
    ).to.be.revertedWith("No participants in the raffle");

    const resultCancel = await H2H.connect(operator).cancelRaffle(idRaffle);
    expect(resultCancel)
      .to.emit("RaffleCancelled")
      .withArgs(idRaffle, ethers.parseUnits("0", "ether"));
    const raffle = await H2H.rafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(CANCELLED);
  });

  it("should withdraw", async function () {
    const destinationWallet = await H2H.destinationWallet();
    const balanceDestinationBefore = await ethers.provider.getBalance(
      destinationWallet
    );

    await H2H.connect(operator).withdraw(
      ethers.parseUnits("0.01", "ether")
    );

    const balanceDestinationAfter = await ethers.provider.getBalance(
      destinationWallet
    );
    expect(balanceDestinationAfter).to.be.equal(
      balanceDestinationBefore+(ethers.parseUnits("0.01", "ether"))
    );
  });

  it("should create a 2 H2H raffle, and set a winner if only 1 player exists (SCENARIO 2)", async function () {
    const resultCreation = await H2H.connect(operator).createH2HRaffle(
      ethers.parseUnits("2", "ether"),
      ethers.parseUnits("0.6", "ether"),
      ethers.parseUnits("0.55", "ether"),
      ONLY_DIRECTLY,
      2
    );
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();

    expect(resultCreation).to.emit("RaffleCreated").withArgs(idRaffle);

    await H2H.connect(player1).buyEntry(idRaffle, {
      value: ethers.parseUnits("0.55", "ether"),
    });
    const destinationWallet = await H2H.destinationWallet();
    const balanceDestinationBefore = await ethers.provider.getBalance(
      destinationWallet
    );
    const balanceWinnerBefore = await ethers.provider.getBalance(
      player1.getAddress()
    );

    const resultWin = await H2H.connect(operator).mockSetWinner(idRaffle, 2);
    expect(resultWin)
      .to.emit("RaffleUnfulfilled")
      .withArgs(
        idRaffle,
        player1.getAddress(),
        ethers.parseUnits("0.6", "ether")
      );

    const entryInfo = await H2H.rafflesEntryInfo(idRaffle);
    const raffle = await H2H.raffles(idRaffle);
    expect(entryInfo.status).to.be.equal(UNFULFILLED);
    expect(raffle.winner).to.be.equal(await player1.getAddress());

    const balanceDestinationAfter = await ethers.provider.getBalance(
      destinationWallet
    );
    const balanceWinnerAfter = await ethers.provider.getBalance(
      player1.getAddress()
    );
    expect(balanceWinnerAfter).to.be.equal(
      balanceWinnerBefore+(ethers.parseUnits("0.6", "ether"))
    );
    expect(balanceDestinationAfter).to.be.equal(
      balanceDestinationBefore+(ethers.parseUnits("0", "ether"))
    );
  });

  it("should cancel a 2 H2H raffle (SCENARIO 2)", async function () {
    const resultCreation = await H2H.connect(operator).createH2HRaffle(
      ethers.parseUnits("2", "ether"),
      ethers.parseUnits("0.6", "ether"),
      ethers.parseUnits("0.55", "ether"),
      ONLY_DIRECTLY,
      2
    );
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();
    expect(resultCreation).to.emit("RaffleCreated").withArgs(idRaffle);

    await H2H.connect(player1).buyEntry(idRaffle, {
      value: ethers.parseUnits("0.55", "ether"),
    });
    const destinationWallet = await H2H.destinationWallet();
    const balanceDestinationBefore = await ethers.provider.getBalance(
      destinationWallet
    );
    const balanceWinnerBefore = await ethers.provider.getBalance(
      player1.getAddress()
    );

    const resultCancel = await H2H.connect(operator).cancelRaffle(idRaffle);
    expect(resultCancel)
      .to.emit("RaffleCancelled")
      .withArgs(idRaffle, ethers.parseUnits("0.55", "ether"));

    const raffle = await H2H.rafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(CANCELLED);

    // Player get funds back
    const balanceDestinationAfter = await ethers.provider.getBalance(
      destinationWallet
    );
    const balanceWinnerAfter = await ethers.provider.getBalance(
      player1.getAddress()
    );
    expect(balanceWinnerAfter).to.be.equal(
      balanceWinnerBefore+(ethers.parseUnits("0.55", "ether"))
    );
    expect(balanceDestinationAfter).to.be.equal(
      balanceDestinationBefore+(ethers.parseUnits("0", "ether"))
    );
  });

  it("should cancel a 2 H2H raffle without purchases (SCENARIO 3)", async function () {
    const resultCreation = await H2H.connect(operator).createH2HRaffle(
      ethers.parseUnits("2", "ether"),
      ethers.parseUnits("0.6", "ether"),
      ethers.parseUnits("0.55", "ether"),
      ONLY_DIRECTLY,
      2
    );
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();
    expect(resultCreation).to.emit("RaffleCreated").withArgs(idRaffle);

    const destinationWallet = await H2H.destinationWallet();
    const balanceDestinationBefore = await ethers.provider.getBalance(
      destinationWallet
    );

    const resultCancel = await H2H.connect(operator).cancelRaffle(idRaffle);
    expect(resultCancel)
      .to.emit("RaffleCancelled")
      .withArgs(idRaffle, ethers.parseUnits("0", "ether"));

    const raffle = await H2H.rafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(CANCELLED);

    const balanceDestinationAfter = await ethers.provider.getBalance(
      destinationWallet
    );
    expect(balanceDestinationAfter).to.be.equal(
      balanceDestinationBefore+(ethers.parseUnits("0", "ether"))
    );
  });

  it("Test: 1 H2H raffle, and close it with a winner", async function () {
    const resultCreation = await H2H.connect(operator).createH2HRaffle(
      ethers.parseUnits("0.5", "ether"),
      ethers.parseUnits("0.6", "ether"),
      ethers.parseUnits("0.55", "ether"),
      ONLY_DIRECTLY,
      1
    );
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();
    expect(resultCreation).to.emit("RaffleCreated").withArgs(idRaffle);

    await H2H.connect(player1).buyEntry(idRaffle, {
      value: ethers.parseUnits("0.55", "ether"),
    });

    const destinationWallet = await H2H.destinationWallet();
    const balanceDestinationBefore = await ethers.provider.getBalance(
      destinationWallet
    );
    const balanceWinnerBefore = await ethers.provider.getBalance(
      player1.getAddress()
    );

    const resultWin = await H2H.connect(operator).mockSetWinner(idRaffle, 2);
    expect(resultWin)
      .to.emit("RaffleEnded")
      .withArgs(idRaffle, ethers.parseUnits("0.55", "ether"));
    expect(resultWin)
      .to.emit("FeeTransferredToPlatform")
      .withArgs(ethers.parseUnits("0.05", "ether"));

    const balanceWinnerAfter = await ethers.provider.getBalance(
      player1.getAddress()
    );

    expect(balanceWinnerAfter).to.be.equal(
      balanceWinnerBefore+(ethers.parseUnits("0.5", "ether"))
    );

    const balanceDestinationAfter = await ethers.provider.getBalance(
        destinationWallet
      );
    expect(balanceDestinationAfter).to.be.equal(balanceDestinationBefore+(ethers.parseUnits("0.05", "ether")));
  });
  
    it("Test Harry issues", async function () {
        let resultCreation = await H2H.connect(operator).createH2HRaffle("2000000000000000",
            "2000000000000000",
            "1000000000000000",
            0,
            2);
            receipt = await resultCreation.wait();
            idRaffle = await getRaffleId();
    
            expect(resultCreation).to.emit("RaffleCreated").withArgs(idRaffle);
    
        await H2H.connect(player1).buyEntry(idRaffle, {  value: "1000000000000000" });
    
        let resultWin = await H2H.connect(operator).mockSetWinner(idRaffle, 3);
    
        expect(resultWin).to.emit('RaffleUnfulfilled').withArgs( idRaffle, player1.getAddress(),"2000000000000000");
        const entryInfo = await H2H.rafflesEntryInfo(idRaffle);
        const raffle = await H2H.raffles(idRaffle);
        expect(entryInfo.status).to.be.equal(UNFULFILLED)
        expect(raffle.winner).to.be.equal(await player1.getAddress())
        expect(raffle.randomNumber).to.be.equal(1)
    });
    
    it("should create a 2 H2H raffle, and close it with the second purchaser as winner", async function () {
        let resultCreation = await H2H.connect(operator).createH2HRaffle(ethers.parseUnits('1', 'ether'),
            ethers.parseUnits('0.6', 'ether'),
            ethers.parseUnits('0.55', 'ether'),
            ONLY_DIRECTLY, 2);
            idRaffle = await getRaffleId();
    
            expect(resultCreation).to.emit("RaffleCreated").withArgs(idRaffle);
    
        await H2H.connect(player1).buyEntry(idRaffle, { value: ethers.parseUnits('0.55', 'ether') });
        await H2H.connect(player2).buyEntry(idRaffle, { value: ethers.parseUnits('0.55', 'ether') });
    
        const destinationWallet = await H2H.destinationWallet();
    const balanceDestinationBefore = await ethers.provider.getBalance(destinationWallet);
    const balanceWinnerBefore = await ethers.provider.getBalance(player2.getAddress());

        let resultWin = await H2H.connect(operator).mockSetWinner(idRaffle, 3);
    
        expect(resultWin).to.emit('RaffleEnded').withArgs(idRaffle, ethers.parseUnits('1.1', 'ether'));
        expect(resultWin).to.emit('FeeTransferredToPlatform').withArgs(ethers.parseUnits('0.1', 'ether') );
    
        const entryInfo = await H2H.rafflesEntryInfo(idRaffle);
        const raffle = await H2H.raffles(idRaffle);
        expect(entryInfo.status).to.be.equal(ENDED);
        expect(raffle.winner).to.be.equal(await player2.getAddress());
    
        const balanceDestinationAfter = await ethers.provider.getBalance(destinationWallet);
    const balanceWinnerAfter = await ethers.provider.getBalance(player2.getAddress());
        expect(balanceWinnerAfter).to.be.equal(balanceWinnerBefore+(ethers.parseUnits('1', 'ether')))
        expect(balanceDestinationAfter).to.be.equal(balanceDestinationBefore+(ethers.parseUnits('0.1', 'ether')))
    });
     
});
