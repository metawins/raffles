import hre from "hardhat";
import { expect } from "chai";
import { loadFixture } from "@nomicfoundation/hardhat-toolbox-viem/network-helpers"; 
import { getVRFConfig, getConstructorArgs, IVRFConfig, zeroAddress } from "../constants/constants";
import { decodeEventLog, keccak256, toBytes, createTestClient, http, publicActions } from "viem";
import { GetContractReturnType, WalletClient } from "@nomicfoundation/hardhat-viem/types";
import { hardhat } from "viem/chains"

////////// SEPOLIA VRF CONSTANTS //////////////
const params: IVRFConfig = getVRFConfig(11155111); // SEPOLIA_CHAIN_ID
const constructorArguments = getConstructorArgs(params);
////////// SEPOLIA VRF CONSTANTS //////////////

const testClient = createTestClient({
  chain: hardhat,
  mode: 'hardhat',
  transport: http(),
})
  .extend(publicActions)

const ONLY_DIRECTLY = 0;

/// Deployment function to setup the initial state ///

const deploy = async () => {
  const basicNFT = await hre.viem.deployContract("Basic721");
  const basicToken = await hre.viem.deployContract("BasicERC20");
  const erc20Comp = await hre.viem.deployContract("MockERC20Comp" as string, constructorArguments);

  return [basicNFT, basicToken, erc20Comp];
};

let [basicNFT, basicToken, erc20Comp] : GetContractReturnType[] = [];
let [owner, player1, player2, player3, player4, player5, player6, seller, operator] : WalletClient[] = [];
let idRaffle: number, receipt, result;

async function getRaffleId() {


  const filter = await testClient.createContractEventFilter({
    abi: erc20Comp.abi,
    address: erc20Comp.address,
    eventName: "RaffleCreated",
  });
  const logs = await testClient.getFilterLogs({filter});
  const topic = logs[0].topics[1] as '0x${string}';
  //Decode the event log to get the raffle id from the event
  const id = decodeEventLog({ abi: erc20Comp.abi, eventName: 'RaffleCreated', topics: [topic], strict: false });
  idRaffle = id.args ? id.args[0] : 0; // get the raffle id from the first argument of the topic
  return idRaffle;
}