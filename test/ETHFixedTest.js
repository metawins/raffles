const { expect } = require("chai");
const { ethers } = require("hardhat");

/// /// SEPOLIA //////////////
const SEPOLIA_VRF_COORDINATOR = "0x2bce784e69d2Ff36c71edcB9F88358dB0DfB55b4";
const SEPOLIA_LINK_TOKEN = "0x326C977E6efc84E512bB9C30f76E30c160eD06FB";
const SEPOLIA_KEYHASH =
  "0x0476f9a745b61ea5c0ab224d3a6e4c99f0b02fce4da01143a4f70aa80ae76e8a";
const ONLY_DIRECTLY = 0;

let token;
let competitionETHAsPrize;
let owner, player1, player2, player3, player4, player5, player6;
let seller, operator;
let idRaffle, receipt;
let requiredNFT, requiredNFT1, requiredNFT2, requiredNFT3, requiredTokenId, tokenId;
const zeroAddress = ethers.ZeroAddress;

async function getRaffleId() {
  filter = competitionETHAsPrize.filters.RaffleCreated;
  events = await competitionETHAsPrize.queryFilter(filter, -1)
  idRaffle = events[0].topics[1];
  return idRaffle;
}

async function getTokenId(tokenUsed) {
  filter = tokenUsed.filters.TokenCreated;
  events = await tokenUsed.queryFilter(filter, -1)
  returnedTokenId = events[0].topics[1];
  return returnedTokenId;
}

describe("ETH Fixed", function () {

  it("Should mint tokens and configure", async function () {
    [
      owner,
      player1,
      player2,
      player3,
      player4,
      player5,
      player6,
      player7,
      seller,
      operator,
    ] = await ethers.getSigners();


    const MockETHComp = await ethers.getContractFactory(
      "MockETHComp"
    );
    competitionETHAsPrize = await MockETHComp.deploy(
      SEPOLIA_VRF_COORDINATOR,
      SEPOLIA_LINK_TOKEN,
      SEPOLIA_KEYHASH,
      false
    );
    await competitionETHAsPrize.waitForDeployment();
  });

  it("Should add operator role to ETH-prize raffles", async function () {
    const operatorHash = ethers.id("OPERATOR");
    await competitionETHAsPrize.grantRole(operatorHash, operator.address);
  });

  it("should create an ETH-prized raffle", async function () {
    const result = await competitionETHAsPrize.connect(operator).createRaffle(1000, ethers.parseUnits('4', 'ether'), 0, [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, ONLY_DIRECTLY);
    expect(result).to.emit("RaffleCreated").withArgs(0);
  });

  it("should stake ETH in an ETH raffle", async function () {
    const result = await competitionETHAsPrize.connect(player1).stakeETH(0, { value: ethers.parseUnits('4', 'ether') });
    expect(result).to.emit("RaffleStarted").withArgs("0", player1.address);
  });

  it("should buy an entry on the raffle", async function () {
    let result = await competitionETHAsPrize.connect(player3).buyEntry(0, 1, { value: ethers.parseUnits('1', 'ether') });
    expect(result).to.emit("EntrySold").withArgs("0", player3, "1", "1");
    receipt = await result.wait();
    console.log("Gas used buying: " + receipt.cumulativeGasUsed);

    result = await competitionETHAsPrize.connect(player2).buyEntry(0, 1, { value: ethers.parseUnits('1', 'ether') });
    expect(result).to.emit("EntrySold").withArgs("0", player2, "2", "1");
    receipt = await result.wait();
    console.log("Gas used buying: " + receipt.cumulativeGasUsed);
  });

  it("should fail buying by less than set in the prices", async function () {
    await expect(competitionETHAsPrize.connect(player1).buyEntry(0, 1, { value: 700 })).
      to.be.revertedWithCustomError(competitionETHAsPrize, "EntryNotAllowed", "msg.value not the price");
  });

  it("should give an entry for free", async function () {
    const result = await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(0, [player4.address]);
    receipt = await result.wait();
    console.log("Gas used adding for free: " + receipt.cumulativeGasUsedsUsed);
  });

  it("should give a batch of free entries free", async function () {
    const result = await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(0, [player5.address, player6.address]);
    expect(result).to.emit('FreeEntry').withArgs('0', [player5.address, player6.address], '2', '5');
    receipt = await result.wait();
    console.log("Gas used adding for free 2 players: " + receipt.cumulativeGasUsedsUsed);
  });

  it("should show the raffle info", async function () {
    const result = await competitionETHAsPrize.fundingList(0);
    //  console.log(JSON.stringify(result));
    expect(result.desiredFundsInWeis).to.equal('1000');
  });

  it("should close the raffle", async function () {
    const balanceSellerBefore = await ethers.provider.getBalance(player1.address);
    const balanceWinnerBefore = await ethers.provider.getBalance(player2.address);
    const destinationWallet = await competitionETHAsPrize.destinationWallet();
    const balanceDestinationBefore = await ethers.provider.getBalance(destinationWallet);
    const result = await competitionETHAsPrize.connect(operator).mockSetWinner(0,1);
    expect(result)
      .to.emit("RaffleEnded")
      .withArgs("0", ethers.parseUnits("2", "ether"));
    expect(result).to.emit('FeeTransferredToPlatform').withArgs(ethers.parseUnits('0.5', 'ether'));
    const balanceSellerAfter = await ethers.provider.getBalance(player1.address)
    expect(balanceSellerAfter).to.equal(balanceSellerBefore +(ethers.parseUnits('1.5', 'ether')))

    const balanceWinnerAfter = await ethers.provider.getBalance(player2.address);
    expect(balanceWinnerAfter).to.equal(balanceWinnerBefore +(ethers.parseUnits('4', 'ether')))

    const balanceDestinationAfter = await ethers.provider.getBalance(destinationWallet);
    expect(balanceDestinationAfter).to.equal(balanceDestinationBefore + (ethers.parseUnits('0.5', 'ether')))
    
  });

  it("should fail buying an entry after draw", async function () {
    await expect(
      competitionETHAsPrize.connect(player1).buyEntry(0, 1, { value: ethers.parseUnits('0.05', 'ether') }))
      .to.revertedWithCustomError(competitionETHAsPrize, "EntryNotAllowed", "Not in ACCEPTED");
  });

  it("should fail canceling a raffle already closed", async function () {
    await expect(competitionETHAsPrize.connect(operator).cancelRaffle(0)).to.be.revertedWith("Wrong status");
  });

  it("should fail canceling a raffle a non-operator", async function () {
    await expect(competitionETHAsPrize.connect(player1).cancelRaffle(0)).to.be.reverted;
  });

  it("should cancel a raffle", async function () {
    const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(1000, ethers.parseUnits('4', 'ether'), 0, [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, ONLY_DIRECTLY);
    idRaffle = getRaffleId()

    await competitionETHAsPrize.connect(player5).stakeETH(idRaffle, { value: ethers.parseUnits('4', 'ether') });
    await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });

    const destinationWallet = await competitionETHAsPrize.destinationWallet();
    const balanceSellerBefore = await ethers.provider.getBalance(player5.address);

    await competitionETHAsPrize.connect(operator).cancelRaffle(idRaffle);

    // check the raffle is canceled
    const raffle = await competitionETHAsPrize.rafflesEntryInfo(idRaffle);
    expect(raffle.status).to.equal(6); // cancellation requested

    // check the seller receives the money back
    const balanceSellerAfter = await ethers.provider.getBalance(player5.address)
    expect(balanceSellerAfter).to.equal(balanceSellerBefore + ethers.parseUnits('4', 'ether'));
});

it("should fail canceling a raffle already asked to cancel", async function () {
  await expect(competitionETHAsPrize.connect(operator).cancelRaffle(1)).to.be.reverted;
});

it("should fail staking asset in a not created raffle", async function () {
  await expect(competitionETHAsPrize.connect(operator).stakeETH(111)).to.be.reverted;
});

it("should fail staking asset in a raffle not in the right status", async function () {
  await expect(competitionETHAsPrize.connect(operator).stakeETH(0)).to.be.revertedWith("Raffle not CREATED");
});

it("should create a new ruffle", async function () {
  var result = await competitionETHAsPrize.connect(operator).createRaffle(1000, 80, 2000000000,
    [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 26, price: 70000000000 }], 2500, ONLY_DIRECTLY);
  expect(result).to.emit('RaffleCreated').withArgs('2');
});

it("should stake a raffle", async function () {
  const result = await competitionETHAsPrize.connect(operator).stakeETH(2, { value: 80 });
  expect(result).to.emit('RaffleStarted').withArgs('2', operator.address);
});

it("should fail buying an entry paying below price", async function () {
  await expect(competitionETHAsPrize.connect(player1).buyEntry(2, 1, { value: 100 }))
    .to.revertedWithCustomError(competitionETHAsPrize, "EntryNotAllowed", "msg.value not the price");
});

it("should fail buying several entries for below price", async function () {
  await expect(competitionETHAsPrize.connect(player1).buyEntry(2, 2, { value: 700 }))
    .to.be.revertedWithCustomError(competitionETHAsPrize, "EntryNotAllowed", "msg.value not the price");
});

it("should fail buying a wrong amount of entries", async function () {
  await expect(competitionETHAsPrize.connect(player1).buyEntry(2, 6, { value: 800 })).to.be.reverted;
});

it("should allow buy several entries", async function () {
  const balanceBefore = await ethers.provider.getBalance(competitionETHAsPrize.getAddress());

  await competitionETHAsPrize.connect(player1).buyEntry(2, 2, { value: 8000000000 });

  const balanceAfter = await ethers.provider.getBalance(competitionETHAsPrize.getAddress());
  // check the contract received the money
  expect(balanceAfter).to.be.equal(balanceBefore + 8000000000n);
});

it("should fail creating a raffle with more price categories than allowed", async function () {
  await expect(competitionETHAsPrize.createRaffle(1000, 1, 200000000000, [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 25, price: 70000000000 }, { id: 4, numEntries: 25, price: 70000000000 }, { id: 5, numEntries: 25, price: 70000000000 }, { id: 6, numEntries: 25, price: 70000000000 }], 2500, ONLY_DIRECTLY))
    .to.be.reverted;
});

it("should change the destination address", async function () {
  await competitionETHAsPrize.setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3');
});

it("should fail changing the destination address by a non owner", async function () {
  await expect(competitionETHAsPrize.connect(operator).setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3'));
});

it("should add free entries", async function () {
  const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(ethers.parseUnits('1', 'ether'), ethers.parseUnits('0.75', 'ether'), ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
  idRaffle = getRaffleId()

  await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: ethers.parseUnits('0.75', 'ether') });

  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });
  await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.address, player4.address, player5.address]);
});

it("should transfer funds when the winner is from a free entry", async function () {
  const destinationWallet = await competitionETHAsPrize.destinationWallet();
  const balanceDestinationBefore = await ethers.provider.getBalance(destinationWallet);
  const balanceSellerBefore = await ethers.provider.getBalance(player4.address);

  // the winner is from a free entry
  await competitionETHAsPrize.connect(operator).mockSetWinner(idRaffle, 2);

  const balanceDestinationAfter = await ethers.provider.getBalance(destinationWallet);

  // check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
  expect(balanceDestinationAfter).to.equal(balanceDestinationBefore + 250000000000000000n); // 25% of raised amount
  const balanceSellerAfter = await ethers.provider.getBalance(player4.address);
  // check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
  expect(balanceSellerAfter).to.be.equal(balanceSellerBefore + 750000000000000000n); // 75% of raised amount - gas fees
});

it("should transfer funds when the winner is from a Multi entry", async function () {
  const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(
    ethers.parseUnits('2', 'ether'), ethers.parseUnits('2', 'ether'), ethers.parseUnits('1', 'ether'),
     [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, 
     { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);

  idRaffle = getRaffleId()
  await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: ethers.parseUnits('2', 'ether') });

  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('2', 'ether') });
  await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.address, player5.address]);
  const destinationWallet = await competitionETHAsPrize.destinationWallet();
  const balanceDestinationBefore = await ethers.provider.getBalance(destinationWallet);
  const balanceWinnerBefore = await ethers.provider.getBalance(player1.address);

  // the winner is from a free entry
  result = await competitionETHAsPrize.connect(operator).mockSetWinner(idRaffle, 2);

  // check the event is emitted
  expect(result).to.emit('RaffleEnded').withArgs(idRaffle);
  // check the status is "ended"
  raffle = await competitionETHAsPrize.getRafflesEntryInfo(idRaffle);
  expect(raffle.status).to.be.equal(5);
  expect(raffle.entriesLength).to.be.equal(7);

  const balanceDestinationAfter = await ethers.provider.getBalance(destinationWallet);
  //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
  expect(balanceDestinationAfter).to.be.equal(balanceDestinationBefore + 500000000000000000n); // 25% of raised amount
  const balanceWinnerAfter = await ethers.provider.getBalance(player1.address); // winner gets the prize
  expect(balanceWinnerAfter).to.be.equal(balanceWinnerBefore + (ethers.parseUnits('2', 'ether')));
  
});

it("should fail adding free entries if the status is not accepted", async function () {
  // check adding free entries to a closed raffle
  await expect(competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.address, player5.address]))
    .to.revertedWith("Raffle is not in accepted");
  // check adding free entries to a raffle that has not been accepted yet
  const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 3, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);

  idRaffle = getRaffleId()

  expect(result).to.emit('RaffleCreated').withArgs(idRaffle);
  await expect(competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.address, player5.address]))
    .to.be.revertedWith("Raffle is not in accepted");
});

it("should cancel a raffle", async function () {
  const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(ethers.parseUnits('5', 'ether'), 0, ethers.parseUnits('2', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);

  idRaffle = getRaffleId()
  await competitionETHAsPrize.connect(operator).stakeETH(idRaffle);
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });

  await competitionETHAsPrize.connect(operator).cancelRaffle(idRaffle);

  // check the raffle is canceled
  const raffle = await competitionETHAsPrize.getRafflesEntryInfo(idRaffle);
  expect(raffle.status).to.be.equal(6); // cancellation requested   
});

it("should fail canceling a raffle already asked to cancel", async function () {
  await expect(competitionETHAsPrize.connect(player1).cancelRaffle(idRaffle)).to.be.reverted;
});


it("should ask a raffle for cancellation step 1", async function () {
  const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(0, 5, 0, [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);

  idRaffle = getRaffleId()

  await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: 5 });

  await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });
  await competitionETHAsPrize.connect(player4).buyEntry(idRaffle, 2, { value: ethers.parseUnits('2', 'ether') });
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });

  await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.address, player5.address]);

  const raffle = await competitionETHAsPrize.getRafflesEntryInfo(idRaffle);
  expect(raffle.amountRaised).to.be.equal(ethers.parseUnits("4", "ether"));
});

it("should fail transferring remaining funds if the raffle is not in cancellation_requested status", async function () {
  await expect(competitionETHAsPrize.connect(operator).transferRemainingFunds(idRaffle))
    .to.be.revertedWith("Wrong status");
});

it("should ask a raffle for cancellation step 2", async function () {
  await competitionETHAsPrize.connect(operator).cancelRaffle(idRaffle)
  const raffle = await competitionETHAsPrize.getRafflesEntryInfo(idRaffle);

  expect(raffle.status).to.be.eq(6)
});

it("should fail transferring remaining funds a non operator", async function () {
  await expect(competitionETHAsPrize.connect(player1).transferRemainingFunds(idRaffle))
    .to.be.reverted;
});

it("should transfer remaining funds", async function () {
  const result = await competitionETHAsPrize.connect(operator).transferRemainingFunds(idRaffle);
  expect(result).to.emit('RemainingFundsTransferred').withArgs(idRaffle, ethers.parseUnits('4', 'ether'));
});

it("should allow to buy and not revert if too many entries bought", async function () {
  const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 0, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);

  idRaffle = getRaffleId()

  await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: 0 });

  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('2', 'ether') });
  await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });
  // the current # of entries for player 1 is 20
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('2', 'ether') });
  await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.address, player5.address]);
});

it("Gas cost when buying 100 entries (no required nft)", async function () {
  const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 1, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('3', 'ether') }], 2500, ONLY_DIRECTLY);

  idRaffle = getRaffleId()

  await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: 1 });

  // player1 has 1 nft (tokenId=0) of collection requiredToken. Buy 10 entries
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('2', 'ether') });
  // Buy 1 entry
  await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });
  //  Buy 100 entries
  await competitionETHAsPrize.connect(player3).buyEntry(idRaffle, 3, { value: ethers.parseUnits('3', 'ether') });
  //  Buy 100 entries
  await competitionETHAsPrize.connect(player4).buyEntry(idRaffle, 3, { value: ethers.parseUnits('3', 'ether') });
  //  Buy 100 entries
  await competitionETHAsPrize.connect(player5).buyEntry(idRaffle, 3, { value: ethers.parseUnits('3', 'ether') });

  await competitionETHAsPrize.connect(operator).mockSetWinner(idRaffle, 11)
  const raffle = await competitionETHAsPrize.raffles(idRaffle);
  expect(raffle.winner).to.be.eq(player3.address);
});

it("Gas cost when calling a lot of times getEntry (no required nft)", async function () {
  const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), 1, ethers.parseUnits('0.01', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, ONLY_DIRECTLY);

  idRaffle = getRaffleId()

  await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: 1 });

  // player2 buys 1 entry
  await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
  // player1 buys 10 entries
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.02', 'ether') });
  //  player3 buys 100 entries
  await competitionETHAsPrize.connect(player3).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
  //  player4 buys 100 entries
  await competitionETHAsPrize.connect(player4).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });

  await expect(competitionETHAsPrize.connect(player6).buyEntry(idRaffle, 8, { value: ethers.parseUnits('0', 'ether') }))
    .to.be.revertedWithCustomError(competitionETHAsPrize, "EntryNotAllowed", "Id not in raffleId");
  //  player5 buys 100 entries
  await competitionETHAsPrize.connect(player5).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });

  await competitionETHAsPrize.connect(operator).mockSetWinner(idRaffle, 210)
  const raffle = await competitionETHAsPrize.raffles(idRaffle);
  expect(raffle.winner).to.be.equal(player4.address);
});

it("should manage canceled entries", async function () {
  const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), 1, ethers.parseUnits('0.01', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, ONLY_DIRECTLY);

  idRaffle = getRaffleId()

  await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: 1 });

  // player1 allowed
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
  //  player3 buys 100 entries
  await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
  await competitionETHAsPrize.connect(player3).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });

  const rCancelEntry = await competitionETHAsPrize.connect(operator).cancelEntry(idRaffle, [2], player3.address);
  receipt = await rCancelEntry.wait();
  console.log("Gas used cancelEntry: " + receipt.cumulativeGasUsed);

  //player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2
  await competitionETHAsPrize.connect(operator).mockSetWinner(idRaffle, 200)

  const raffle = await competitionETHAsPrize.raffles(idRaffle);
  expect(raffle.winner).to.be.equal(await player2.getAddress());
});


it("should manage canceled players as time passes", async function () {
  const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), 0, ethers.parseUnits('0.01', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, ONLY_DIRECTLY);

  idRaffle = getRaffleId()
  await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: 0 });
  // player1 allowed
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
  //  player3 buys 100 entries
  await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
  // player1 can buy again
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
  // player 4 buys 100 entries
  await competitionETHAsPrize.connect(player4).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
  // player 5 buys 100 entries
  await competitionETHAsPrize.connect(player5).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });

  await expect(competitionETHAsPrize.connect(operator).cancelEntry(idRaffle, [2], player3.address))
    .to.be.revertedWith("Entry did not belong to player");
  // test cancelEntry batch
  const result = await competitionETHAsPrize.connect(operator).cancelEntry(idRaffle, [0, 2], player1.address);
  expect(result).to.emit('EntryCancelled').withArgs(idRaffle, '101', player1.address);

  const entriesData = await competitionETHAsPrize.getEntriesBought(idRaffle);
  expect(entriesData[1].player).to.be.equal(zeroAddress);
  expect(entriesData[3].player).to.be.equal(zeroAddress);

  for (let i = 0; i < 10; i++) {
    await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
  }

  for (let i = 0; i < 10; i++) {
    await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
  }

  // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2
  const rSetWinner = await competitionETHAsPrize.connect(operator).mockSetWinner(idRaffle, 0)

  const raffle = await competitionETHAsPrize.raffles(idRaffle);
  expect(raffle.winner).to.be.equal(player2.address);
  receipt = await rSetWinner.wait();
  console.log("Gas used new setwinner: " + receipt.cumulativeGasUsed);
});

it("should allow a raffle with mixed entries at price 0 and with prices > 0", async function () {
  let resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(0, 1, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 0, ONLY_DIRECTLY);

  idRaffle = getRaffleId()

  await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: 1 });

  // Player 2 and player 4 get 1 entry for free
  await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0', 'ether') });
  await competitionETHAsPrize.connect(player4).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0', 'ether') });
  // player 3 and player 5 get free entries via the operator
  await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.address, player5.address]);
  // player 1 buys 10 entries for 0.005
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.005', 'ether') });

  // The winner will be player2
  result = await competitionETHAsPrize.connect(operator).mockSetWinner(idRaffle, 0);

  let raffle = await competitionETHAsPrize.raffles(idRaffle);
  expect(raffle.winner).to.be.equal(player2.address);
});

it("should fail if player wants to buy using an older priceId", async function () {
  // the id used for the prices are 11 and 12
  let resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(0, 1, 0, [{ id: 10, numEntries: 1, price: 0 }, { id: 11, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 0, ONLY_DIRECTLY);

  idRaffle = getRaffleId()

  await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: 1 });

  // Player 2 and player 4 get 1 entry for free
  await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 10, { value: ethers.parseUnits('0', 'ether') });
  await competitionETHAsPrize.connect(player4).buyEntry(idRaffle, 10, { value: ethers.parseUnits('0', 'ether') });
  // player 3 and player 5 get free entries via the operator
  await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.address, player5.address]);
  // player 1 buys 10 entries for 0.005
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 11, { value: ethers.parseUnits('0.005', 'ether') });
  // Fails when using and id on the price struct that did not belong to this raffle
  await expect(competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, { value: 700 }))
    .to.revertedWithCustomError(competitionETHAsPrize, "EntryNotAllowed", "Id not in raffleId");
});

it("should fail giving free entries two times to the same player", async function () {
  // the id used for the prices are 11 and 12
  let resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(0, 1, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 0, ONLY_DIRECTLY);
  idRaffle = getRaffleId();

  await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: 1 });
  // free entry without problems for player 1
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, { value: 0 });
  // Revert if player 1 wants to get a free entry again
  await expect(competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, { value: 0 }))
    .to.revertedWithCustomError(competitionETHAsPrize, "EntryNotAllowed", "Player already got free entry");
  // player1 can buy other packages (not the free one)
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.005', 'ether') });
  await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.005', 'ether') });
  // Player 2 and player 3 can buy free package, and paid packages
  await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 1, { value: 0 });
  let r1 = await competitionETHAsPrize.connect(player3).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.005', 'ether') });
  let r2 = await competitionETHAsPrize.connect(player3).buyEntry(idRaffle, 1, { value: 0 });
});
 
});
