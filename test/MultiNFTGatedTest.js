const { expect } = require("chai");
const { ethers } = require("hardhat");
const { loadFixture } = require("@nomicfoundation/hardhat-network-helpers");
const { BigNumber } = require("ethers");

/// /// SEPOLIA //////////////

const SEPOLIA_VRF_COORDINATOR = "0x2bce784e69d2Ff36c71edcB9F88358dB0DfB55b4";
const SEPOLIA_LINK_TOKEN = "0x326C977E6efc84E512bB9C30f76E30c160eD06FB";
const SEPOLIA_KEYHASH =
  "0x0476f9a745b61ea5c0ab224d3a6e4c99f0b02fce4da01143a4f70aa80ae76e8a";
const ONLY_DIRECTLY = 0;

let token;
let manager;
let owner, player1, player2, player3, player4, player5, player6;
let seller, operator;
let idRaffle, receipt, result, resultMint, balance;
let requiredNFT, requiredNFT1, requiredNFT2, requiredNFT3, requiredTokenId, tokenId;
let basicERC20;
let tokenId1, tokenId2, tokenId3, tokenId4
const zeroAddress = ethers.ZeroAddress;

async function getRaffleId() {
  filter = manager.filters.RaffleCreated;
  events = await manager.queryFilter(filter, -1)
  idRaffle = events[0].topics[1];
  return idRaffle;
}

async function getTokenId(tokenUsed) {
  filter = tokenUsed.filters.TokenCreated;
  events = await tokenUsed.queryFilter(filter, -1)
  returnedTokenId = events[0].topics[1];
  return returnedTokenId;
}

describe("ERC20 tests with 2 nft gating (phase 2)", function () {

  it("Should mint tokens and configure", async function () {
    [
      owner,
      player1,
      player2,
      player3,
      player4,
      player5,
      player6,
      player7,
      seller,
      operator,
    ] = await ethers.getSigners();

    const Basic721 = await ethers.getContractFactory("Basic721");
    token = await Basic721.deploy();
    await token.waitForDeployment();

    const BasicERC20 = await ethers.getContractFactory("BasicERC20");
    basicERC20 = await BasicERC20.deploy();
    await basicERC20.waitForDeployment();

    const RequiredNFT = await ethers.getContractFactory("Basic721");
    requiredNFT = await RequiredNFT.deploy();
    await requiredNFT.waitForDeployment();

    const RequiredNFT1 = await ethers.getContractFactory("Basic721");
    requiredNFT1 = await RequiredNFT1.deploy();
    await requiredNFT1.waitForDeployment();

    const RequiredNFT2 = await ethers.getContractFactory("Basic721");
    requiredNFT2 = await RequiredNFT2.deploy();
    await requiredNFT2.waitForDeployment();

    const RequiredNFT3 = await ethers.getContractFactory("Basic721");
    requiredNFT3 = await RequiredNFT3.deploy();
    await requiredNFT3.waitForDeployment();

    const MockMultiNFTGatedComp = await ethers.getContractFactory(
      "MockMultiNFTGatedComp"
    );
    manager = await MockMultiNFTGatedComp.deploy(
      SEPOLIA_VRF_COORDINATOR,
      SEPOLIA_LINK_TOKEN,
      SEPOLIA_KEYHASH,
      false
    );
    await manager.waitForDeployment();

    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
  });

  it("Should add operator role to raffles", async function () {
    const operatorHash = ethers.id("OPERATOR");
    await manager.grantRole(operatorHash, operator.getAddress(), {
      from: owner.getAddress(),
    });
  });
  it("The contract balance should be cero ", async function () {
    const balance = await ethers.provider.getBalance(manager.getAddress());
    expect(balance).to.be.equal(0);
  });
  it("should allow to buy only those who stakes certain nft ", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'),
      11, token.getAddress(), 0, ethers.parseUnits('1', 'ether'),
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress()], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();

    await token.connect(seller).approve(manager.getAddress(), 0);
    await manager.connect(seller).stakeNFT(idRaffle);

    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = getTokenId(requiredNFT2)

    result = await manager.connect(player1).buyEntry(idRaffle, 2, [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 }, { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 }], { value: ethers.parseUnits('2', 'ether') });
    receipt = await result.wait();
    console.log("Gas used buying 10 entries with 2 nft restriction: " + receipt.cumulativeGasUsed);

    // Player 1 buys another entry using other nfts he has.
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = getTokenId(requiredNFT2)

    let r1 = await manager.connect(player1).buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 }],
      { value: ethers.parseUnits('1', 'ether') });
    receipt = await r1.wait();
    console.log("Gas used buying 1 entry with nft restriction: " + receipt.cumulativeGasUsed);

    await expect(manager.connect(player1).buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 }],
      { value: ethers.parseUnits('1', 'ether') }))
      .to.be.revertedWithCustomError(manager,"EntryNotAllowed", "Wallet already used");

    resultMint = await requiredNFT1.mint(player2.getAddress());    
    tokenId3 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT2.mint(player2.getAddress());    
    tokenId4 = getTokenId(requiredNFT2)
    const currentOwner = await requiredNFT2.ownerOf(tokenId4);
    expect(currentOwner).to.be.equal(await player2.getAddress());

    const tokenId5 = 2;
    r1 = await manager.connect(player2).buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': 2 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId5 }],
      { value: ethers.parseUnits('1', 'ether') });
    receipt = await r1.wait();
    console.log("Gas used buying 1 entry with nft restriction: " + receipt.cumulativeGasUsed);

    // should fail if the buyer is not the owner of the tokenId
    await expect(manager.connect(player4).buyEntry(idRaffle, 2, [{ 'collection': requiredNFT1.getAddress(), 'tokenId': 0 },
    { 'collection': requiredNFT2.getAddress(), 'tokenId': 0 }], { value: ethers.parseUnits('2', 'ether') }))
      .to.be.revertedWith("Not the owner of tokenId");

    // should fail if the tokenId was already used by another user (player2 receives tokenID=0 from player1)
    await requiredNFT1.connect(player1).transferFrom(player1.getAddress(), player2.getAddress(), 0);
    resultMint = await requiredNFT2.mint(player2.getAddress());    
    tokenId1 = getTokenId(requiredNFT2)
    await expect(manager.connect(player2).buyEntry(idRaffle, 2,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': 0 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId1 }],
      { value: ethers.parseUnits('2', 'ether') })).to.be.revertedWith("tokenId used");

    // the current # of entries for player 1 is 20
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = getTokenId(requiredNFT2)
    await expect(manager.connect(player1).buyEntry(idRaffle, 2,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': 3 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': 4 }],
      { value: ethers.parseUnits('2', 'ether') }))
      .to.be.revertedWithCustomError(manager,"EntryNotAllowed", "Wallet already used");

    await manager.connect(operator).giveBatchEntriesForFree(0, [player3.getAddress(), player5.getAddress()]);
    // should fail if the player did not have an nft of each collection
    resultMint = await requiredNFT1.mint(player3.getAddress());    
    tokenId1 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT1.mint(player3.getAddress());    
    tokenId2 = getTokenId(requiredNFT1)
    await expect(manager.connect(player3).buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId2 }],
      { value: ethers.parseUnits('1', 'ether') })).to.be.revertedWith("Collection already used");

  });

  it("should close the raffle", async function () {
    const balanceSellerBefore = await ethers.provider.getBalance(seller.getAddress());

    await manager.connect(operator).mockSetWinner(idRaffle, 2);

    // check the winner received the NFT
    const currentOwner = await token.ownerOf(0);
    console.log("winner = " + currentOwner);
    expect(currentOwner).to.be.equal(await player1.getAddress());

    // check the seller received the money (75% of the raffle pool)
    const balanceSellerAfter = await ethers.provider.getBalance(seller.getAddress());
    expect(balanceSellerAfter).to.be.equal(balanceSellerBefore +(ethers.parseUnits('3', 'ether'))); // 75% of 3000 = 2250
  });

  it("should fail creating a raffle a non-operator", async function () {
    await expect(manager.connect(player2).createRaffle(1, 1, token.getAddress(), 0, 2000, [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, [], ONLY_DIRECTLY))
      .to.be.reverted;
  });

  it("should fail staking asset in a not created raffle", async function () {
    await expect(manager.connect(seller).stakeNFT(1)).to.be.reverted;
  });

  it("should fail staking asset in a raffle not in the right status", async function () {
    await expect(manager.connect(seller).stakeNFT(0)).to.be.revertedWith("Raffle not CREATED");
  });

  it("should create a new raffle", async function () {
    const result = await manager.connect(operator).createRaffle(
      1000, 1, token.getAddress(), 1, 2000000000,
      [
        { id: 1, numEntries: 1, price: 1000000000 },
        { id: 2, numEntries: 5, price: 8000000000 },
        { id: 3, numEntries: 25, price: 70000000000 },
      ],
      2500, [], ONLY_DIRECTLY
    );
    expect(result).to.emit("RaffleCreated").withArgs("1");
  });

  it("should fail staking asset in a raffle by a non-owner", async function () {
    await expect(manager.connect(player4).stakeNFT(1)).to.be.revertedWith(
      "NFT is not owned by caller"
    );
  });

  it("should stake asset in a raffle", async function () {
    await token.connect(seller).approve(manager.getAddress(), 1);
    const result = await manager.connect(seller).stakeNFT(1);
    expect(result).to.emit('RaffleStarted').withArgs('1', seller.getAddress());
    const currentOwner = await token.ownerOf(1);
    expect(currentOwner).to.be.equal(await manager.getAddress());
    expect(currentOwner).not.to.be.equal(await seller.getAddress());
  });

  it("should create a new raffle with 3 collections in whitelist", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 11,
      token.getAddress(), 2, ethers.parseUnits('1', 'ether'),
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress(), requiredNFT3.getAddress()], ONLY_DIRECTLY);
    expect(resultCreation).to.emit('RaffleCreated').withArgs('2');
  });

  it("should stake asset in a raffle", async function () {
    await token.connect(seller).approve(manager.getAddress(), 2);
    const result = await manager.connect(seller).stakeNFT(2);
    expect(result).to.emit('RaffleStarted').withArgs('2', seller.getAddress());
    const currentOwner = await token.ownerOf(2);
    expect(currentOwner).to.be.equal(await manager.getAddress());
    expect(currentOwner).not.to.be.equal(await seller.getAddress());
  });

  it("should buy an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = getTokenId(requiredNFT2)
    resultMint = await requiredNFT3.mint(player1.getAddress());    
    tokenId3 = getTokenId(requiredNFT3)
    const r1 = await manager.connect(player1).buyEntry(2, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 },
      { 'collection': requiredNFT3.getAddress(), 'tokenId': tokenId3 }],
      { value: ethers.parseUnits('1', 'ether') });
    receipt = r1.wait();
    console.log("Gas used buying 1 entry with 3 nft restriction: " + receipt.cumulativeGasUsed);
  });

  it("should fail buying an entry using 3 nfts but 2 from same collection", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = getTokenId(requiredNFT2)
    await expect(manager.connect(player1).buyEntry(2, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': 0 }],
      { value: ethers.parseUnits('1', 'ether') }))
      .to.be.revertedWith("Collection already used");
  });

  it("should fail buying an entry using 3 nfts but 2 from same collection (1 and 2)", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = getTokenId(requiredNFT2)
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId3 = getTokenId(requiredNFT1)
    await expect(manager.connect(player1).buyEntry(2, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId3 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 }],
      { value: ethers.parseUnits('1', 'ether') }))
      .to.be.revertedWith("Collection already used");

  });

  it("should buy again an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());
    tokenId1 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = getTokenId(requiredNFT2)
    resultMint = await requiredNFT3.mint(player1.getAddress());    
    tokenId3 = getTokenId(requiredNFT3)
    const r1 = await manager.connect(player1).buyEntry(2, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 },
      { 'collection': requiredNFT3.getAddress(), 'tokenId': tokenId3 }],
      { value: ethers.parseUnits('1', 'ether') });
    receipt = r1.wait();
    console.log("Gas used buying 1 entry with 3 nft restriction: " + receipt.cumulativeGasUsed);
  });

  it("should fail  buying several entries for below price", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = getTokenId(requiredNFT2)
    resultMint = await requiredNFT3.mint(player1.getAddress());    
    tokenId3 = getTokenId(requiredNFT3)
    await expect(manager.connect(player1).buyEntry(2, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 },
      { 'collection': requiredNFT3.getAddress(), 'tokenId': tokenId3 }],
      { value: 700 }))
      .to.be.revertedWithCustomError(manager,"EntryNotAllowed", "msg.value not the price");
  });

  it("should fail buying a wrong amount of entries", async function () {
    await expect(manager.connect(player1).buyEntry(1, 6,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 },
      { 'collection': requiredNFT3.getAddress(), 'tokenId': tokenId3 }],
      { value: 800 })).to.be.reverted;
  });

  it("should change the destination address", async function () {
    await manager.setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3');
  });

  it("should fail changing the destination address by a non owner", async function () {
    await expect(manager.connect(operator).setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3')).to.be.reverted;
  });

  it("should add free entries", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('1', 'ether'), 1, token.getAddress(), 6, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();
    await token.connect(seller).approve(manager.getAddress(), 6);
    await manager.connect(seller).stakeNFT(idRaffle);

    await manager.connect(player1).buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 }], { value: ethers.parseUnits('1', 'ether') });
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
  });

  it("should transfer funds when the winner is from a free entry", async function () {
    const destinationWallet = await manager.destinationWallet();
    const balanceBefore = await ethers.provider.getBalance(destinationWallet);
    const balanceSellerBefore = await ethers.provider.getBalance(seller.getAddress());
    // the winner is from a free entry
    await manager.connect(operator).mockSetWinner(idRaffle, 2);

    const balanceAfter = await ethers.provider.getBalance(destinationWallet);
    expect(balanceAfter).to.be.equal(balanceBefore+250000000000000000n); // 25% of raised amount
    const balanceSellerAfter = await ethers.provider.getBalance(seller.getAddress());
    // check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    expect(balanceSellerAfter).to.be.equal(balanceSellerBefore+750000000000000000n); // 75% of raised amount - gas fees
    // check the new owner of the token is the player (from the free entry) and not the previous owner (player2)
    const currentOwner = await token.ownerOf(6);
    expect(currentOwner).not.to.be.equal(await player4.getAddress());
    expect(currentOwner).to.be.equal(await player5.getAddress());
  });

  it("should transfer funds when the winner is from a Multi entry", async function () {
    let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('2', 'ether'), 21,
      token.getAddress(), 6, ethers.parseUnits('1', 'ether'),
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [requiredNFT1.getAddress(), requiredNFT2.getAddress()], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();
    await token.connect(player5).approve(manager.getAddress(), 6);
    await manager.connect(player5).stakeNFT(idRaffle);

    await manager.connect(player1).buyEntry(idRaffle, 2, [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
    { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 }], { value: ethers.parseUnits('2', 'ether') })
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
    const destinationWallet = await manager.destinationWallet();
    const balanceBefore = await ethers.provider.getBalance(destinationWallet);
    const balanceSellerBefore = await ethers.provider.getBalance(player5.getAddress());

    // the winner is from a free entry
    result = await manager.connect(operator).mockSetWinner(idRaffle, 2);

    // check the event is emitted
    expect(result).to.emit('RaffleEnded').withArgs(idRaffle);
    // check the status is "ended"
    raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(5);

    // check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    const balanceAfter = await ethers.provider.getBalance(destinationWallet);
    expect(balanceAfter).to.be.equal(balanceBefore+500000000000000000n); // 25% of raised amount
    const balanceSellerAfter = await ethers.provider.getBalance(player5.getAddress());
    // check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    expect(balanceSellerAfter).to.be.equal(
      balanceSellerBefore+1500000000000000000n); // 75% of raised amount - gas fees
    // check the new owner of the token is the player (from the free entry) and not the previous owner (player2)
    let currentOwner = await token.ownerOf(6);
    expect(currentOwner).to.be.equal(await player1.getAddress());
    expect(currentOwner).not.to.be.equal(await player5.getAddress());
  });

  it("should fail adding free entries if the status is not accepted", async function () {
    // check adding free entries to a closed raffle
    await expect(manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()])).to.be.revertedWith("Raffle is not in accepted");
    // check adding free entries to a raffle that has not been accepted yet
    let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 1, token.getAddress(), 3, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();
    expect(result).to.emit('RaffleCreated').withArgs(idRaffle);

    await expect(manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]))
      .to.be.revertedWith("Raffle is not in accepted");

  });

  it("should fail canceling a raffle already closed", async function () {
    await expect(manager.connect(operator).cancelRaffle(idRaffle - 1)).to.be.revertedWith("Wrong status");
  });

  it("should fail canceling a raffle a non-operator", async function () {
    await expect(manager.connect(player1).cancelRaffle(idRaffle)).to.be.reverted;
  });

  it("should cancel a raffle", async function () {
    let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('5', 'ether'), 1, token.getAddress(), 7, ethers.parseUnits('2', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();
    await token.mint(seller.getAddress());
    await token.connect(seller).approve(manager.getAddress(), 7);
    await manager.connect(seller).stakeNFT(idRaffle);
    await manager.connect(player1).buyEntry(idRaffle, 1, [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
    { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 }], { value: ethers.parseUnits('1', 'ether') });

    let currentOwner = await token.ownerOf(7);
    expect(currentOwner).to.be.equal(await manager.getAddress());

    await manager.connect(operator).cancelRaffle(idRaffle);
    // check the seller has the nft back
    currentOwner = await token.ownerOf(7);
    expect(currentOwner).to.be.equal(await seller.getAddress());

    // check the raffle is canceled
    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(6); // cancellation requested
  });

  it("should fail canceling a raffle already asked to cancel", async function () {
    await expect(manager.connect(player1).cancelRaffle(idRaffle)).to.be.reverted;
  });

  it("wallet cap == 2 means can call 2 times but fail a third time", async function () {
    let resultMint = await token.mint(seller.getAddress());    
    tokenId = getTokenId(token)

    let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 21,
      token.getAddress(), tokenId, ethers.parseUnits('1', 'ether'),
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress()], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();

    await token.connect(seller).approve(manager.getAddress(), tokenId);
    await manager.connect(seller).stakeNFT(idRaffle);

    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = getTokenId(requiredNFT2)
    // player1 has 1 nft (tokenId=0) of collection requiredToken. Buy 10 entries
    result = await manager.connect(player1).buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 }], { value: ethers.parseUnits('1', 'ether') });
    receipt = result.wait();
    console.log("Gas used buying 10 entries with nft restriction: " + receipt.cumulativeGasUsed);

    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId3 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId4 = getTokenId(requiredNFT2)
    await manager.connect(player1).buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId3 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId4 }], { value: ethers.parseUnits('1', 'ether') });
  
  });
  
    it("should fail giving free entries two times to the same player", async function () {
      let resultMint = await token.mint(seller.getAddress());    
    tokenId = getTokenId(token)

      resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 21,
        token.getAddress(), tokenId, ethers.parseUnits('1', 'ether'),
        [{ id: 1, numEntries: 1, price: 0 },
        { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500,
        [requiredNFT1.getAddress(), requiredNFT2.getAddress()], ONLY_DIRECTLY);
      receipt = await resultCreation.wait();
      idRaffle = await getRaffleId();  
      
    await token.connect(seller).approve(manager.getAddress(), tokenId);
    await manager.connect(seller).stakeNFT(idRaffle);
  
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = getTokenId(requiredNFT1)
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = getTokenId(requiredNFT2)
      await manager.connect(player1).buyEntry(idRaffle, 1,
        [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
        { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 }], { value: 0 })
  
        resultMint = await requiredNFT1.mint(player1.getAddress());        
        tokenId3 = getTokenId(requiredNFT1)
        resultMint = await requiredNFT2.mint(player1.getAddress());        
        tokenId4 = getTokenId(requiredNFT2)
      await expect(manager.connect(player1).buyEntry(idRaffle, 1,
          [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId3 },
          { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId4 }], {  value: 0 })
     ).to.be.revertedWithCustomError(manager,"EntryNotAllowed", "Player already got free entry");
  
     resultMint = await requiredNFT1.mint(player1.getAddress());     
     tokenId5 = getTokenId(requiredNFT1)
     resultMint = await requiredNFT2.mint(player1.getAddress());     
     tokenId6 = getTokenId(requiredNFT2)
      await manager.connect(player1).buyEntry(idRaffle, 2,
        [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId5 },
        { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId6 }], { value: ethers.parseUnits('2', 'ether') })
  
    });
 
});