const { expect } = require("chai");
const { ethers } = require("hardhat");

/// /// MAINNET //////////////
const MAINNET_VRF_COORDINATOR = "0xf0d54349aDdcf704F77AE15b96510dEA15cb7952";
const MAINNET_LINK_TOKEN = "0x514910771AF9Ca656af840dff83E8264EcF986CA";
const MAINNET_KEYHASH =
  "0xAA77729D3466CA35AE8D28B3BBAC7CC36A5031EFDC430821C02BC31A238AF445";
const ONLY_DIRECTLY = 0;

// Get deployer address
describe("erc20Comp", function () {
  let erc20Comp;
  let usdcTokenContract;
  let raffleId = 0;
  const normalizedRandomNumber = 40;

  before(async function () {
    const [deployer, blacklisted, nonBlacklisted] = await ethers.getSigners();


    // Impersonate the USDC token contract minter
    const usdcMinter = await ethers.getImpersonatedSigner("0x5B6122C109B78C6755486966148C1D70a50A47D7");

    const usdcBlacklister = await ethers.getImpersonatedSigner("0x10DF6B6fe66dd319B1f82BaB2d054cbb61cdAD2e");
    // Deploy the USDC token contract mock
    usdcTokenContract = await ethers.getContractAt("FiatTokenV2", "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48");

    // Transfer some eth to blacklister
    await deployer.sendTransaction({
      to: usdcBlacklister.address,
      value: ethers.parseEther("100")
    });

    // Blacklist an address in the USDC token contract mock - impersonate the USDC token contract owner
    await usdcTokenContract.connect(usdcBlacklister).blacklist(blacklisted.address);

    // First, deploy the BaseERC20Extension contract. 
    // Then, assign this deployed contract to the variable baseERC20Extension.
    const erc20CompFactory = await ethers.getContractFactory("ERC20Comp");
    erc20Comp = await erc20CompFactory.deploy(
      MAINNET_VRF_COORDINATOR,
      MAINNET_LINK_TOKEN,
      MAINNET_KEYHASH,
      true
    );

    // Grant role to erc20Comp
    await erc20Comp.grantRole(erc20Comp.DEFAULT_ADMIN_ROLE(), deployer.address);
    await erc20Comp.grantRole(erc20Comp.OPERATOR_ROLE(), deployer.address);

    await erc20Comp.createRaffle(1, usdcTokenContract.getAddress(), 100, 2000, [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }, { id: 4, numEntries: 80, price: 500 }], 2500, ONLY_DIRECTLY);

    // Mint usdc token //we have got the usdc token, but need to mint it, then add into the raffle, without it, it cant be used as a prize 
    await usdcTokenContract.connect(usdcMinter).mint(deployer.address, ethers.parseUnits("10000", 6));
    // Add minted token to the raffle
    await usdcTokenContract.approve(erc20Comp.getAddress(), ethers.parseUnits("10000", 6));

    // Take asset to start raffle - mint USDC to to and transfer to erc20Comp for the prize using stake function
    await erc20Comp.stake(raffleId);
  });

  describe("getWinnerAddressFromRandom", function () {
    it("should return a non-blacklisted address as winner and skip blacklisted", async function () {
      const [deployer, blacklisted, nonBlacklisted, blacklisted2] = await ethers.getSigners();

      // Buy entries with non-blacklisted address
      // Params: raffleId, price structure
      await erc20Comp.connect(blacklisted2).buyEntry(0, 3, { value: "700" }); //buy 25 entries for 700 gwei

      // Buy entries with blacklisted address
      await erc20Comp.connect(blacklisted).buyEntry(0, 3, { value: "700" }); //buy 25 entries for 700 gwei
      
      await erc20Comp.connect(blacklisted).buyEntry(0, 4, { value: "500" }); // Buy 80 entries for 500 gwei

      const winner = await erc20Comp.getWinnerAddressFromRandom(raffleId, normalizedRandomNumber);
      expect(winner).to.equal(blacklisted2.address);
    });

    it("should revert if all addresses are blacklisted", async function () {
      const [deployer, blacklisted, nonBlacklisted, blacklisted2] = await ethers.getSigners();
      const usdcBlacklister = await ethers.getImpersonatedSigner("0x10DF6B6fe66dd319B1f82BaB2d054cbb61cdAD2e");

      // Blacklist an address in the USDC token contract mock - impersonate the USDC token contract owner
      await usdcTokenContract.connect(usdcBlacklister).blacklist(blacklisted2.address);

      // Buy entries vwith non-blacklisted address
      // Params: raffleId, price structure
      await erc20Comp.connect(blacklisted2).buyEntry(0, 3, { value: "700" }); //buy 25 entries for 700 gwei

      // Buy entries with blacklisted address
      await erc20Comp.connect(blacklisted).buyEntry(0, 3, { value: "700" }); //buy 25 entries for 700 gwei

      await expect(erc20Comp.getWinnerAddressFromRandom(raffleId, normalizedRandomNumber)).to.be.revertedWith("All users blacklisted");
    });

    it("can trigger a redraw if gas limit is reached and successfuly draw a winner", async function () {
      const [deployer, blacklisted, nonBlacklisted] = await ethers.getSigners();
    
      const linkHolder = await ethers.getImpersonatedSigner("0xee5B5B923fFcE93A870B3104b7CA09c3db80047A")
      const linkTokenContract = await ethers.getContractAt("ERC20", MAINNET_LINK_TOKEN);

      // Transfer 1 LINK to the contract
      await linkTokenContract.connect(linkHolder).transfer(erc20Comp.getAddress(), ethers.parseEther("10"));

      // Scenario with many blacklisted addresses entering the raffle buying entries
      // Assuming you have a way to get or create Multi blacklisted addresses
      for (let i = 0; i < 80; i++) {
        await erc20Comp.connect(blacklisted).buyEntry(0, 4, { value: "500" }); // Buy 80 entries for 500 gwei each
      }

      await erc20Comp.setWinner(raffleId);

      let entryInfo = await erc20Comp.getRafflesEntryInfo(raffleId);

      expect(entryInfo.status).to.equal(4); // Status 4 = CLOSING_REQUESTED

      // Act & Assert: Attempt to select a winner and expect the process to fail due to gas limit
      await expect(erc20Comp.getWinnerAddressFromRandom(raffleId, 600, { gasLimit: "200000" }))
        .to.be.revertedWithoutReason(); //ran out of gas
      
      // Can't redraw because the raffle is in the wrong state
      await expect(erc20Comp.setWinner(raffleId))
        .to.be.revertedWith("Raffle in wrong status");

      // Reverts raffle to change state from CLOSE_REQUESTED to ACCEPTED
      await erc20Comp.revertCloseRequested(raffleId); 

      entryInfo = await erc20Comp.getRafflesEntryInfo(raffleId);
      expect(entryInfo.status).to.equal(1); // Status 1 = ACCEPTED

      // Buy entries with non-blacklisted address
      await erc20Comp.connect(nonBlacklisted).buyEntry(0, 3, { value: "700" }); //buy 25 entries for 700 gwei 

      // Redraw the raffle
      await erc20Comp.setWinner(raffleId);
      
      // Action: check status of raffle
      const winner = await erc20Comp.getWinnerAddressFromRandom(raffleId, 2);
      expect(winner).to.equal(nonBlacklisted.address);
    });
  });
});