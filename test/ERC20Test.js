const { expect } = require("chai");
const { ethers } = require("hardhat");

/// /// SEPOLIA //////////////
const SEPOLIA_VRF_COORDINATOR = "0x2bce784e69d2Ff36c71edcB9F88358dB0DfB55b4";
const SEPOLIA_LINK_TOKEN = "0x326C977E6efc84E512bB9C30f76E30c160eD06FB";
const SEPOLIA_KEYHASH =
  "0x0476f9a745b61ea5c0ab224d3a6e4c99f0b02fce4da01143a4f70aa80ae76e8a";
const ONLY_DIRECTLY = 0;

let token, manager, basicERC20;
let competitionETHAsPrize;
let owner, player1, player2, player3, player4, player5, player6;
let seller, operator;
let idRaffle, receipt, result;
let requiredNFT, requiredNFT1, requiredNFT2, requiredNFT3, requiredTokenId, tokenId;
const zeroAddress = ethers.ZeroAddress;

async function getRaffleId() {
  filter = manager.filters.RaffleCreated;
  events = await manager.queryFilter(filter, -1)
  idRaffle = events[0].topics[1];
  return idRaffle;
}

describe("ERC20", function () {

  it("Should mint tokens and configure", async function () {
    [
      owner,
      player1,
      player2,
      player3,
      player4,
      player5,
      player6,
      player7,
      seller,
      operator,
    ] = await ethers.getSigners();

    const Basic721 = await ethers.getContractFactory("Basic721");
    token = await Basic721.deploy();
    await token.waitForDeployment();

    const BasicERC20 = await ethers.getContractFactory("BasicERC20");
    basicERC20 = await BasicERC20.deploy();
    await basicERC20.waitForDeployment();

    const MockERC20Comp = await ethers.getContractFactory(
      "MockERC20Comp"
    );
    manager = await MockERC20Comp.deploy(
      SEPOLIA_VRF_COORDINATOR,
      SEPOLIA_LINK_TOKEN,
      SEPOLIA_KEYHASH,
      false
    );
    await manager.waitForDeployment();

    await token.connect(owner).mint(seller.getAddress());
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
    await token.mint(seller.getAddress(), { from: owner.getAddress() });
  });

  it("Should add operator role directly", async function () {
    const operatorHash = ethers.id("OPERATOR");
    await manager.connect(owner).grantRole(operatorHash, operator.getAddress());
  });

  it("should create a raffle", async function () {
    const result = await manager.connect(operator).createRaffle(1000, basicERC20.getAddress(), 100, 2000,
      [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, ONLY_DIRECTLY);
    expect(result).to.emit('RaffleCreated').withArgs('0');
  });

  it("should stake asset in a raffle", async function () {
    await basicERC20.approve(manager.getAddress(), 100);
    const result = await manager.stake(0);
    expect(result).to.emit('RaffleStarted').withArgs('0', owner.getAddress());

  });

  it("should buy an entry on the raffle", async function () {
    const result = await manager.connect(player1).buyEntry(0, 1, { value: 1000 });
    expect(result).to.emit("EntrySold").withArgs("0", player1.getAddress());
  });

  it("should buy an entry on the raffle", async function () {
    const result = await manager.connect(player1).buyEntry(0, 1, { value: 1000 });
    expect(result).to.emit("EntrySold").withArgs("0", player1.getAddress());
  });

  it("should buy an entry on the raffle", async function () {
    const result = await manager.connect(player2).buyEntry(0, 1, { value: 1000 });
    expect(result).to.emit("EntrySold").withArgs("0", player2.getAddress());
  });

  it("should buy an entry on the raffle", async function () {
    const result = await manager.connect(player3).buyEntry(0, 1, { value: 1000 });
    expect(result).to.emit('EntrySold').withArgs('0', player3.getAddress(), '4', '1');
    receipt = await result.wait();
    console.log("Gas used buying: " + receipt.cumulativeGasUsed);
  });

  it("should give an entry for free", async function () {
    const result = await manager.connect(operator).giveBatchEntriesForFree(0, [player4.getAddress()]);
    receipt = await result.wait();
    console.log("Gas used adding for free: " + receipt.cumulativeGasUsed);
  });

  it("should give a batch of free entries free", async function () {
    const result = await manager.connect(operator).giveBatchEntriesForFree(0, [player5.getAddress(), player6.getAddress()]);
    receipt = await result.wait();
    console.log("Gas used adding for free: " + receipt.cumulativeGasUsed);
  });

  it("should show the raffle info", async function () {
    const result = await manager.fundingList(0);
    expect(result.desiredFundsInWeis).to.be.equal("1000");
  });

  it("should close the raffle", async function () {
    const balanceSellerBefore = await ethers.provider.getBalance(owner.getAddress());
    await manager.connect(operator).mockSetWinner(0, 2);

    const result = await manager.raffles(0);
    let player2Address = await player2.getAddress()
    expect(result.winner).to.be.equal(player2Address);

    // check the winner received the prize (100 tokens of basicERC20)
    const balanceWinnerAfter = await basicERC20.balanceOf(player2.getAddress());
    expect(balanceWinnerAfter).to.be.equal(100);
    // check the seller received the money (75% of the raffle pool)
    const balanceSellerAfter = await ethers.provider.getBalance(owner.getAddress());
    expect(balanceSellerAfter).to.be.equal(balanceSellerBefore + 3000n); // 75% of 4000 = 3000
  });

  it("should fail buying an entry after draw", async function () {
    await expect(manager.connect(player1).buyEntry(0, 1, { value: ethers.parseUnits('0.05', 'ether') }))
      .to.be.revertedWithCustomError(manager,"EntryNotAllowed", "Not in ACCEPTED");
  });

  it("should fail creating a raffle a non-operator", async function () {
    await expect(manager.connect(player2).createRaffle(1, basicERC20.getAddress(), 100, 2000, [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, ONLY_DIRECTLY))
      .to.be.reverted
  });

  it("should fail staking asset in a not created raffle", async function () {
    await expect(manager.connect(seller).stake(1)).to.be.reverted;
  });

  it("should fail staking asset in a raffle not in the right status", async function () {
    await expect(manager.connect(seller).stake(0)).to.be.revertedWith("Raffle not CREATED");
  });

  it("should create a new raffle", async function () {
    var result = await manager.connect(operator).createRaffle(1000, basicERC20.getAddress(), 100, 2000000000, [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 25, price: 70000000000 }], 2500, ONLY_DIRECTLY);
    expect(result).to.emit('RaffleCreated').withArgs('1');
  });

  it("should stake asset in a raffle", async function () {
    await basicERC20.connect(owner).transfer(seller.getAddress(), 100000);
    await basicERC20.connect(seller).approve(manager.getAddress(), 100);
    const result = await manager.connect(seller).stake(1);
    expect(result).to.emit('RaffleStarted').withArgs('1', seller.getAddress());
    const balance = await basicERC20.balanceOf(manager.getAddress());
    console.log("Balance of contract = " + balance);
    expect(balance).to.be.equal("100");
  });

  it("should fail buying an entry paying below price", async function () {
    await expect(manager.connect(player1).buyEntry(1, 1, { value: 100 }))
      .to.be.revertedWithCustomError(manager,"EntryNotAllowed", "msg.value not the price");
  });

  it("should fail  buying several entries for below price", async function () {
    await expect(manager.connect(player1).buyEntry(1, 2, { value: 700 }))
      .to.be.revertedWithCustomError(manager,"EntryNotAllowed", "msg.value not the price");
  });

  it("should fail buying a wrong amount of entries", async function () {
    await expect(manager.connect(player1).buyEntry(1, 6, { value: 800 }));
  });

  it("should allow buy several entries", async function () {
    const balanceBefore = await ethers.provider.getBalance(manager.getAddress());
    await manager.connect(player1).buyEntry(1, 2, { value: 8000000000 });
    const balanceAfter = await ethers.provider.getBalance(manager.getAddress());
    // check the contract received the money
    expect(balanceAfter).to.be.equal(balanceBefore + 8000000000n);
  });


  it("should change the destination address", async function () {
    await manager.connect(owner).setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3');
  });

  it("should fail changing the destination address by a non owner", async function () {
    await expect(manager.connect(operator).setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3'));
  });

  it("should add free entries", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('1', 'ether'), basicERC20.getAddress(), 6, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
    //
    filter = manager.filters.RaffleCreated;
    events = await manager.queryFilter(filter, -1)
    idRaffle = events[0].topics[1];
            console.log(JSON.stringify(idRaffle))
    //idRaffle = await getRaffleId();
    await basicERC20.connect(seller).approve(manager.getAddress(), 6);
    result = await manager.connect(seller).stake(idRaffle);
    expect(result).to.emit('RaffleStarted').withArgs(idRaffle, seller.getAddress());
    manager.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player4.getAddress(), player5.getAddress()]);
  });

  it("should transfer funds when the winner is from a free entry", async function () {
    const destinationWallet = await manager.destinationWallet();
    const balanceDestinationBefore = await ethers.provider.getBalance(destinationWallet);
    const balanceSellerBefore = await ethers.provider.getBalance(seller.getAddress());

    // the winner is from a free entry
    await manager.connect(operator).mockSetWinner(idRaffle, 2);

    const balanceDestinationAfter = await ethers.provider.getBalance(destinationWallet);
    // check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    expect(balanceDestinationAfter).to.equal(balanceDestinationBefore + BigInt('250000000000000000')); // 25% of raised amount
    const balanceSellerAfter = await ethers.provider.getBalance(await seller.getAddress());
    // check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    expect(balanceSellerAfter).to.be.equal(balanceSellerBefore + BigInt('750000000000000000'));
    const winnerBalance = await basicERC20.balanceOf(player4.getAddress());
    expect(winnerBalance).to.be.equal("6");
  });

  it("should transfer funds when the winner is from a Multi entry", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('2', 'ether'), basicERC20.getAddress(), 6, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
   /* 
    idRaffle = await getRaffleId();
*/
    idRaffle = await getRaffleId();

    await basicERC20.connect(player4).approve(manager.getAddress(), 6);
    await manager.connect(player4).stake(idRaffle);

    await manager.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('2', 'ether') });
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
    const destinationWallet = await manager.destinationWallet();
    const balanceDestinationBefore = await ethers.provider.getBalance(destinationWallet);
    const balanceSellerBefore = await ethers.provider.getBalance(player4.getAddress());

    // the winner is from a free entry
    result = await manager.connect(operator).mockSetWinner(idRaffle, 2);
    // check the event is emitted
    expect(result).to.emit('RaffleEnded').withArgs(idRaffle);
    // check the status is "ended"
    const raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(5);

    // check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    const balanceDestinationAfter = await ethers.provider.getBalance(destinationWallet);
    expect(balanceDestinationAfter).to.equal(balanceDestinationBefore + BigInt('500000000000000000')); // 25% of raised amount
    const balanceSellerAfter = await ethers.provider.getBalance(await player4.getAddress());
    // check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    expect(balanceSellerAfter).to.be.equal(balanceSellerBefore + BigInt('1500000000000000000')); // 75% of raised amount - gas fees
  });

  it("should fail adding free entries if the status is not accepted", async function () {
    // check adding free entries to a closed raffle
    await expect(manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]))
      .to.be.revertedWith("Raffle is not in accepted");
    // check adding free entries to a raffle that has not been accepted yet
    const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), basicERC20.getAddress(), 3, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
    
    idRaffle = await getRaffleId();
    expect(result).to.emit('RaffleCreated').withArgs(idRaffle);

    await expect(manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]))
      .to.be.revertedWith("Raffle is not in accepted");

  });

  it("should fail canceling a raffle already closed", async function () {
    await expect(manager.connect(operator).cancelRaffle(idRaffle - 1))
      .to.be.revertedWith("Wrong status");
  });

  it("should fail canceling a raffle a non-operator", async function () {
    await expect(manager.connect(player1).cancelRaffle(idRaffle)).to.be.reverted
  });

  it("should cancel a raffle", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('5', 'ether'), basicERC20.getAddress(), 7, ethers.parseUnits('2', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
    
    idRaffle = await getRaffleId();
    await basicERC20.connect(seller).approve(manager.getAddress(), 7);
    await manager.connect(seller).stake(idRaffle);
    await manager.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });


    const balanceSellerBefore = await basicERC20.balanceOf(seller.getAddress());

    await manager.connect(operator).cancelRaffle(idRaffle);

    // The seller received back the prize.
    sellerBalanceAfter = await basicERC20.balanceOf(seller.getAddress());
    expect(sellerBalanceAfter).to.be.equal(balanceSellerBefore + 7n);

    // check the raffle is canceled
    const raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(6); // cancellation requested   
  });

  it("should fail canceling a raffle already asked to cancel", async function () {
    await expect(manager.connect(player1).cancelRaffle(idRaffle));
  });

  it("should show the entries size", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), basicERC20.getAddress(), 10, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
    
    idRaffle = await getRaffleId();

    await basicERC20.connect(seller).approve(manager.getAddress(), 10);
    await manager.connect(seller).stake(idRaffle);

    await manager.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('2', 'ether') });
    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.entriesLength).to.be.equal(5);

    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.entriesLength).to.be.equal(7);
  });

  it("should allow a free raffle", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(0, basicERC20.getAddress(), 10, 0, [{ id: 1, numEntries: 1, price: 0 }], 0, ONLY_DIRECTLY);
    
    idRaffle = await getRaffleId();

    await basicERC20.connect(seller).approve(manager.getAddress(), 10);
    await manager.connect(seller).stake(idRaffle);

    await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0', 'ether') });
    await manager.connect(player4).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0', 'ether') });

    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    const winnerBalanceBefore = await basicERC20.balanceOf(player2.getAddress());

    // the first call did generate a winner not in MW. So now lets do another call to setWinner
    result = await manager.connect(operator).mockSetWinner(idRaffle, 0);

    // check the winner received the prize
    const winnerBalanceAfter = await basicERC20.balanceOf(player2.getAddress());
    expect(winnerBalanceAfter).to.be.equal(winnerBalanceBefore + 10n);

    const winner = await manager.getWinnerAddressFromRandom(idRaffle, 1);
    expect(await player2.getAddress()).to.be.equal(winner);
  });

  it("should ask a raffle for cancellation step 1", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(0, basicERC20.getAddress(), 5, 0, [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, ONLY_DIRECTLY);
    
    idRaffle = await getRaffleId();

    await basicERC20.connect(seller).approve(manager.getAddress(), 5);
    await manager.connect(seller).stake(idRaffle);

    await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });
    await manager.connect(player4).buyEntry(idRaffle, 2, { value: ethers.parseUnits('2', 'ether') });
    await manager.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });

    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    const raffle = await manager.getRafflesEntryInfo(idRaffle);

    expect(raffle.amountRaised).to.be.equal(ethers.parseUnits('4', 'ether'))
  });

  it("should fail transferring remaining funds if the raffle is not in cancellation_requested status", async function () {
    await expect(manager.connect(operator).transferRemainingFunds(idRaffle))
      .to.revertedWith("Wrong status");
  });

  it("should ask a raffle for cancellation step 2", async function () {
    await manager.connect(operator).cancelRaffle(idRaffle)
    const raffle = await manager.getRafflesEntryInfo(idRaffle);

    expect(raffle.status).to.be.equal(6)
  });

  it("should reduce the amount raised for the raffle after the refund", async function () {
    const raffle = await manager.getRafflesEntryInfo(idRaffle);
    expect(raffle.amountRaised).to.be.equal(ethers.parseUnits('4', 'ether'))
  });

  it("should fail transferring remaining funds a non operator", async function () {
    await expect(manager.connect(player1).transferRemainingFunds(idRaffle))
      .to.be.reverted;
  });

  it("should transfer remaining funds", async function () {
    const result = await manager.connect(operator).transferRemainingFunds(idRaffle);
    expect(result).to.emit('RemainingFundsTransferred').withArgs(idRaffle, ethers.parseUnits('4', 'ether'));
  });
  it("Gas cost when buying 100 entries (no required nft)", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), basicERC20.getAddress(), 1, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('3', 'ether') }], 2500, ONLY_DIRECTLY);
    
    idRaffle = await getRaffleId();

    await basicERC20.connect(seller).approve(manager.getAddress(), 1);
    await manager.connect(seller).stake(idRaffle);

    result = await manager.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('2', 'ether') });
    receipt = await result.wait();
    console.log("Gas used buying 10 entries with nft restriction: " + receipt.cumulativeGasUsed);

    // Buy 1 entry
    const r1 = await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('1', 'ether') });

    receipt = await r1.wait()
    console.log("Gas used buying 1 entry with nft restriction: " + receipt.cumulativeGasUsed);

    //  Buy 100 entries
    const r2 = await manager.connect(player3).buyEntry(idRaffle, 3, { value: ethers.parseUnits('3', 'ether') });
    receipt = await r2.wait()
    console.log("Gas used buying 100 entries with nft restriction: " + receipt.cumulativeGasUsed);

    //  Buy 100 entries
    const r3 = await manager.connect(player4).buyEntry(idRaffle, 3, { value: ethers.parseUnits('3', 'ether') });
    receipt = await r3.wait()
    console.log("Gas used buying 100 entries with nft restriction: " + receipt.cumulativeGasUsed);

    //  Buy 100 entries
    const r4 = await manager.connect(player5).buyEntry(idRaffle, 3, { value: ethers.parseUnits('3', 'ether') });
    receipt = await r4.wait()
    console.log("Gas used buying 100 entries with nft restriction: " + receipt.cumulativeGasUsed);

    const rSetWinner = await manager.connect(operator).mockSetWinner(idRaffle, 11)
    raffle = await manager.raffles(idRaffle);
    expect(raffle.winner).to.be.equal(await player3.getAddress());
    receipt = await rSetWinner.wait();
    console.log("Gas used new setwinner: " + receipt.cumulativeGasUsed);
  });

  it("Gas cost when calling a lot of times getEntry (no required nft)", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), basicERC20.getAddress(), 1, ethers.parseUnits('0.01', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, ONLY_DIRECTLY);
    
    idRaffle = await getRaffleId();

    await basicERC20.connect(seller).approve(manager.getAddress(), 1);
    await manager.connect(seller).stake(idRaffle);

    const r1 = await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
    receipt = await r1.wait()
    console.log("Gas used buying 1 entry no restriction: " + receipt.cumulativeGasUsed);

    // player1 buys 10 entries
    result = await manager.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.02', 'ether') });
    receipt = await r1.wait()
    console.log("Gas used buying 10 entries no restriction: " + receipt.cumulativeGasUsed);
    //  player3 buys 100 entries
    const r2 = await manager.connect(player3).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
    receipt = await r2.wait()
    console.log("Gas used buying 100 entries no restriction: " + receipt.cumulativeGasUsed);

    //  player4 buys 100 entries
    const r3 = await manager.connect(player4).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
    receipt = await r3.wait()
    console.log("Gas used buying 100 entries no restriction: " + receipt.cumulativeGasUsed);

    await expect(manager.connect(player6).buyEntry(idRaffle, 8, { value: ethers.parseUnits('0', 'ether') }))
      .to.be.revertedWithCustomError(manager,"EntryNotAllowed", "Id not in raffleId");

    //  player5 buys 100 entries
    const r4 = await manager.connect(player5).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
    receipt = await r4.wait()
    console.log("Gas used buying 100 entries no restriction: " + receipt.cumulativeGasUsed);

    const rSetWinner = await manager.connect(operator).mockSetWinner(idRaffle, 210)
    const raffle = await manager.raffles(idRaffle);
    expect(raffle.winner).to.be.equal(await player4.getAddress());
    receipt = await rSetWinner.wait()
    console.log("Gas used new setwinner: " + receipt.cumulativeGasUsed);

  });

  it("should cancel entries entries", async function () {
    let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), basicERC20.getAddress(), 1, ethers.parseUnits('0.01', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, ONLY_DIRECTLY);
    
    idRaffle = await getRaffleId();

    await basicERC20.connect(seller).approve(manager.getAddress(), 1);
    await manager.connect(seller).stake(idRaffle);

    // player1 allowed
    let r1 = await manager.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
    //  player3 buys 100 entries
    let r2 = await manager.connect(player2).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
    let r3 = await manager.connect(player3).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
    let rCancelEntry = await manager.connect(operator).cancelEntry(idRaffle, [2], player3.getAddress());

    // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2
    let rSetWinner = await manager.connect(operator).mockSetWinner(idRaffle, 200)

    let raffle = await manager.raffles(idRaffle);
    expect(raffle.winner).to.be.equal(await player2.getAddress());
  });

  it("should manage cancel entries as time passes", async function () {
    let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('0.03', 'ether'), basicERC20.getAddress(), 1, ethers.parseUnits('0.01', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('0.01', 'ether') }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.02', 'ether') }, { id: 3, numEntries: 100, price: ethers.parseUnits('0.03', 'ether') }], 2500, ONLY_DIRECTLY);
    
    idRaffle = await getRaffleId();
    await basicERC20.connect(seller).approve(manager.getAddress(), 1);
    await manager.connect(seller).stake(idRaffle);
    // player1 allowed
    let r1 = await manager.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
    //  player3 buys 100 entries
    let r2 = await manager.connect(player2).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
    // player1 can buy again
    let r3 = await manager.connect(player1).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
    // player 4 buys 100 entries
    await manager.connect(player4).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });
    // player 5 buys 100 entries
    await manager.connect(player5).buyEntry(idRaffle, 3, { value: ethers.parseUnits('0.03', 'ether') });

    await expect(manager.connect(operator).cancelEntry(idRaffle, [2], player3.getAddress()))
      .to.revertedWith("Entry did not belong to player");
    // test cancelEntry batch
    let result = await manager.connect(operator).cancelEntry(idRaffle, [0, 2], player1.getAddress());
    expect(result).to.emit('EntryCancelled').withArgs(idRaffle, '101', player1.getAddress());

    let entriesData = await manager.getEntriesBought(idRaffle);
    expect(entriesData[1].player).to.be.equal(zeroAddress);
    expect(entriesData[3].player).to.be.equal(zeroAddress);

    for (let i = 0; i < 10; i++) {
      await manager.connect(player1).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
    }

    for (let i = 0; i < 10; i++) {
      await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0.01', 'ether') });
    }

    // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2
    await manager.connect(operator).mockSetWinner(idRaffle, 0)

    let raffle = await manager.raffles(idRaffle);
    expect(raffle.winner).to.be.equal(await player2.getAddress());

  });


  it("should allow a raffle with mixed entries at price 0 and with prices > 0", async function () {
    let resultCreation = await manager.connect(operator).createRaffle(0, basicERC20.getAddress(), 1, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 0, ONLY_DIRECTLY);
    
    idRaffle = await getRaffleId();

    await basicERC20.connect(seller).approve(manager.getAddress(), 1);
    await manager.connect(seller).stake(idRaffle);

    // Player 2 and player 4 get 1 entry for free
    await manager.connect(player2).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0', 'ether') });
    await manager.connect(player4).buyEntry(idRaffle, 1, { value: ethers.parseUnits('0', 'ether') });
    // player 3 and player 5 get free entries via the operator
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
    // player 1 buys 10 entries for 0.005
    await manager.connect(player1).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.005', 'ether') });

    // The winner will be player2
    result = await manager.connect(operator).mockSetWinner(idRaffle, 0);


    let entriesData = await manager.getEntriesBought(idRaffle);
    //console.log(JSON.stringify(entriesData));

    let winner = await manager.getWinnerAddressFromRandom(idRaffle, 1);
    //   console.log("current owner " + currentOwner + " winner " + winner)
    expect(winner).to.be.equal(await player2.getAddress());
  });

  it("should fail if player wants to buy using an older priceId", async function () {
    // the id used for the prices are 11 and 12
    let resultCreation = await manager.connect(operator).createRaffle(0, basicERC20.getAddress(), 10, 0, [{ id: 10, numEntries: 1, price: 0 }, { id: 11, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 0, 1);
    
    idRaffle = await getRaffleId();
    await basicERC20.connect(seller).approve(manager.getAddress(), 10);
    await manager.connect(seller).stake(idRaffle);

    // Player 2 and player 4 get 1 entry for free
    await manager.connect(player2).buyEntry(idRaffle, 10, { value: ethers.parseUnits('0', 'ether') });
    await manager.connect(player4).buyEntry(idRaffle, 10, { value: ethers.parseUnits('0', 'ether') });
    // player 3 and player 5 get free entries via the operator
    await manager.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
    // player 1 buys 10 entries for 0.005
    await manager.connect(player1).buyEntry(idRaffle, 11, { value: ethers.parseUnits('0.005', 'ether') });
    // Fails when using and id on the price struct that did not belong to this raffle
    await expect(manager.connect(player1).buyEntry(idRaffle, 1, { value: 700 }))
      .to.be.revertedWithCustomError(manager,"EntryNotAllowed", "Id not in raffleId");
  });
  
  it("should fail giving free entries two times to the same player", async function () {
    let resultCreation = await manager.connect(operator).createRaffle(0, basicERC20.getAddress(), 10, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 0, ONLY_DIRECTLY);
    
    idRaffle = await getRaffleId();
    
    await basicERC20.connect(seller).approve(await manager.getAddress(), 10);
    await manager.connect(seller).stake(idRaffle);
 
    // free entry without problems for player 1
    let result = await manager.connect(player1).buyEntry(idRaffle, 1, {value: 0 });
    // Revert if player 1 wants to get a free entry again
    await expect(manager.connect(player1).buyEntry(idRaffle, 1, {  value: 0 }))
    .to.be.revertedWithCustomError(manager,"EntryNotAllowed", "Player already got free entry");
    // player1 can buy other packages (not the free one)
    await manager.connect(player1).buyEntry(idRaffle, 2, {  value: ethers.parseUnits('0.005', 'ether') });
    await manager.connect(player1).buyEntry(idRaffle, 2, {  value: ethers.parseUnits('0.005', 'ether') });
    // Player 2 and player 3 can buy free package, and paid packages
    await manager.connect(player2).buyEntry(idRaffle, 1, {  value: 0 });
    let r1 = await manager.connect(player3).buyEntry(idRaffle, 2, { value: ethers.parseUnits('0.005', 'ether') });
    let r2 = await manager.connect(player3).buyEntry(idRaffle, 1, { value: 0 });
    receipt = await result.wait();
    console.log("Gas used buying free: " + receipt.cumulativeGasUsed);
    receipt = await r1.wait();
    console.log("Gas used buying paid: " + receipt.cumulativeGasUsed);
  }); 
 
});
