const { expect } = require("chai");
const { ethers } = require("hardhat");

/// /// SEPOLIA //////////////

const SEPOLIA_VRF_COORDINATOR = "0x2bce784e69d2Ff36c71edcB9F88358dB0DfB55b4";
const SEPOLIA_LINK_TOKEN = "0x326C977E6efc84E512bB9C30f76E30c160eD06FB";
const SEPOLIA_KEYHASH =
  "0x0476f9a745b61ea5c0ab224d3a6e4c99f0b02fce4da01143a4f70aa80ae76e8a";
const ONLY_DIRECTLY = 0;

let token;
let competitionETHAsPrize;
let owner, player1, player2, player3, player4, player5, player6;
let seller, operator;
let idRaffle, receipt, result, resultMint;
let requiredNFT, requiredNFT1, requiredNFT2, requiredNFT3, requiredTokenId, tokenId;
const zeroAddress = ethers.ZeroAddress;

async function getRaffleId() {
  filter = competitionETHAsPrize.filters.RaffleCreated;
  events = await competitionETHAsPrize.queryFilter(filter, -1)
  idRaffle = events[0].topics[1];
  return idRaffle;
}

async function getTokenId(tokenUsed) {
  filter = tokenUsed.filters.TokenCreated;
  events = await tokenUsed.queryFilter(filter, -1)
  returnedTokenId = events[0].topics[1];
  return returnedTokenId;
}

describe("Multi NFT gating (ETH prize) Phase 1", function () {

  it("Should mint tokens and configure", async function () {
    [
      owner,
      player1,
      player2,
      player3,
      player4,
      player5,
      player6,
      player7,
      seller,
      operator,
    ] = await ethers.getSigners();

    const Basic721 = await ethers.getContractFactory("Basic721");
    token = await Basic721.deploy();
    await token.waitForDeployment();

    const RequiredNFT = await ethers.getContractFactory("Basic721");
    requiredNFT = await RequiredNFT.deploy();
    await requiredNFT.waitForDeployment();

    const RequiredNFT1 = await ethers.getContractFactory("Basic721");
    requiredNFT1 = await RequiredNFT1.deploy();
    await requiredNFT1.waitForDeployment();

    const RequiredNFT2 = await ethers.getContractFactory("Basic721");
    requiredNFT2 = await RequiredNFT2.deploy();
    await requiredNFT2.waitForDeployment();

    const RequiredNFT3 = await ethers.getContractFactory("Basic721");
    requiredNFT3 = await RequiredNFT3.deploy();
    await requiredNFT3.waitForDeployment();

    const MockMultiNFTGatedETHComp = await ethers.getContractFactory(
      "MockMultiNFTGatedETHComp"
    );
    competitionETHAsPrize = await MockMultiNFTGatedETHComp.deploy(
      SEPOLIA_VRF_COORDINATOR,
      SEPOLIA_LINK_TOKEN,
      SEPOLIA_KEYHASH,
      false
    );
    await competitionETHAsPrize.waitForDeployment();
  });


  it("Should add operator role to raffles", async function () {
    const operatorHash = ethers.id("OPERATOR");
    await competitionETHAsPrize.grantRole(operatorHash, operator.getAddress(), {
      from: owner.getAddress(),
    });
  });

  it("The contract balance should be cero ", async function () {
    const balance = await ethers.provider.getBalance(competitionETHAsPrize.getAddress());
    expect(balance).to.be.equal(0);
  });


  it("should create an ETH-prized raffle", async function () {
    const result = await competitionETHAsPrize.connect(operator).createRaffle(1000,
      10,
      ethers.parseUnits('4', 'ether'), 0,
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') },
      { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500,
      [requiredNFT1.getAddress()], ONLY_DIRECTLY);
    expect(result).to.emit('RaffleCreated').withArgs('0');
  });

  it("should stake ETH in an ETH raffle", async function () {
    const result = await competitionETHAsPrize.connect(player1).stakeETH(0, { value: ethers.parseUnits('4', 'ether') });
    expect(result).to.emit('RaffleStarted').withArgs('0', player1.getAddress());
  });

  it("should buy an entry on the raffle", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT1);

    result = await competitionETHAsPrize.connect(player1).buyEntry(0, 1, requiredNFT1.getAddress(), [tokenId1, tokenId2], { value: ethers.parseUnits('1', 'ether') });
    receipt = await result.wait();
    console.log("Gas used buying 1 entries with 2 nft restriction: " + receipt.cumulativeGasUsed);

    expect(result).to.emit('EntrySold').withArgs('0', player1.getAddress(), '1', '1');

    // should fail if the buyer did not have any nft
    await expect(competitionETHAsPrize.connect(player4).buyEntry(0, 2, requiredNFT1.getAddress(), [],
      { value: ethers.parseUnits('2', 'ether') }))
      .to.be.reverted;

    // should fail if the buyer is not the owner of the tokenId
    resultMint = await requiredNFT1.mint(player2.getAddress());
    
    tokenId1 = await getTokenId(requiredNFT1);
    await expect(competitionETHAsPrize.connect(player1).buyEntry(0, 2, requiredNFT1.getAddress(), [1, tokenId1], { value: ethers.parseUnits('2', 'ether') })
    ).to.be.revertedWith("Not the owner of tokenId");

    // should fail if the tokenId was already used by another user (player2 receives tokenID=0 from player1)
    await requiredNFT1.connect(player1).transferFrom(player1.getAddress(), player2.getAddress(), 0);
    resultMint = await requiredNFT1.mint(player2.getAddress());
    
    tokenId1 = await getTokenId(requiredNFT1);
    await expect(competitionETHAsPrize.connect(player2).buyEntry(0, 2, requiredNFT1.getAddress(), [0, tokenId1], { value: ethers.parseUnits('2', 'ether') })
    ).to.be.revertedWith("tokenId used");

    // Check wallets cap
    resultMint = await requiredNFT1.mint(player1.getAddress());
    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT1.mint(player1.getAddress());
    
    tokenId2 = await getTokenId(requiredNFT1);
    await expect(competitionETHAsPrize.connect(player1).buyEntry(0, 2, requiredNFT1.getAddress(), [tokenId1, tokenId2], { value: ethers.parseUnits('2', 'ether') })
    ).to.be.revertedWithCustomError(competitionETHAsPrize, "EntryNotAllowed", "Wallet already used");

    // should fail if not using the right collection
    await expect(competitionETHAsPrize.connect(player2).buyEntry(0, 2, requiredNFT2.getAddress(), [0, 1], { value: ethers.parseUnits('2', 'ether') }), "Not in required collection");

    await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(0, [player3.getAddress(), player5.getAddress()]);

    // Player 2 has only 1 nft
    resultMint = await requiredNFT1.mint(player2.getAddress());
    
    tokenId3 = await getTokenId(requiredNFT1);
    await expect(competitionETHAsPrize.connect(player2).buyEntry(0, 1, requiredNFT1.getAddress(), [tokenId3], { value: ethers.parseUnits('1', 'ether') }), "Panic: Index out of bounds");
    resultMint = await requiredNFT1.mint(player2.getAddress());
    
    tokenId4 = await getTokenId(requiredNFT1);
    result = await competitionETHAsPrize.connect(player2).buyEntry(0, 1, requiredNFT1.getAddress(), [tokenId3, tokenId4], { value: ethers.parseUnits('1', 'ether') });
    receipt = result.wait();
    console.log("Gas used buying 10 entries with 2 nft restriction: " + receipt.cumulativeGasUsed);

  });

  it("should fail buying by less than set in the prices", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());
    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT1.mint(player1.getAddress());
    
    tokenId2 = await getTokenId(requiredNFT1);
    await expect(competitionETHAsPrize.connect(player1).buyEntry(0, 1, requiredNFT1.getAddress(), [tokenId1, tokenId2], { value: 700 }))
      .to.be.revertedWithCustomError(competitionETHAsPrize, "EntryNotAllowed", "msg.value not the price");
  });

  it("should give an entry for free", async function () {
    result = await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(0, [player4.getAddress()]);
    receipt = result.wait();
    const gasUsed = receipt.cumulativeGasUsed;
    console.log("Gas used adding for free: " + gasUsed);
  });

  it("should give a batch of free entries free", async function () {
    result = await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(0, [player5.getAddress(), player6.getAddress()]);
    expect(result).to.emit('FreeEntry').withArgs('0', [player5.getAddress(), player6.getAddress()], '2');
    receipt = result.wait();
    const gasUsed = receipt.cumulativeGasUsed;
    console.log("Gas used adding for free 2 players: " + gasUsed);
  });

  it("should show the raffle info", async function () {
    const result = await competitionETHAsPrize.fundingList(0);
    expect(result.desiredFundsInWeis).to.be.equal('1000');
  });

  it("should close the raffle", async function () {
    const destinationWallet = await competitionETHAsPrize.destinationWallet();
    const balanceBefore = await ethers.provider.getBalance(destinationWallet);
    const balanceSellerBefore = await ethers.provider.getBalance(player1.getAddress());
    const balanceWinnerBefore = await ethers.provider.getBalance(player3.getAddress());

    const result = await competitionETHAsPrize.connect(operator).mockSetWinner(0, 1);
    expect(result).to.emit('RaffleEnded').withArgs('0', ethers.parseUnits('2', 'ether'));
    expect(result).to.emit('FeeTransferredToPlatform').withArgs(ethers.parseUnits('0.5', 'ether'));
    const balanceSellerAfter = await ethers.provider.getBalance(player1.getAddress());
    expect(balanceSellerAfter).to.be.equal(balanceSellerBefore+(ethers.parseUnits('1.5', 'ether')))

    const balanceWinnerAfter = await ethers.provider.getBalance(player3.getAddress());
    expect(balanceWinnerAfter).to.be.equal(balanceWinnerBefore+(ethers.parseUnits('4', 'ether')))

    const balanceAfter = await ethers.provider.getBalance(destinationWallet);
    expect(balanceAfter).to.be.equal(balanceBefore+(ethers.parseUnits('0.5', 'ether')))
  });

  it("should fail buying an entry after draw", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT1);

    await expect(competitionETHAsPrize.connect(player1).
      buyEntry(0, 1, requiredNFT1.getAddress(), [tokenId1, tokenId2],
        { value: ethers.parseUnits('1', 'ether') }))
      .to.be.revertedWithCustomError(competitionETHAsPrize, "EntryNotAllowed", "Not in ACCEPTED")
  });

  it("should fail canceling a raffle already closed", async function () {
    await expect(competitionETHAsPrize.connect(operator).cancelRaffle(0)).to.be.revertedWith("Wrong status");
  });

  it("should fail canceling a raffle a non-operator", async function () {
    await expect(competitionETHAsPrize.connect(player1).cancelRaffle(0)).to.be.reverted;
  });

  it("should cancel a raffle", async function () {
    const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(1000, 10, ethers.parseUnits('4', 'ether'), 0,
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, [], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();

    await competitionETHAsPrize.connect(player5).stakeETH(idRaffle, { value: ethers.parseUnits('4', 'ether') });

    await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), [], { value: ethers.parseUnits('1', 'ether') });

    const balanceBefore = await ethers.provider.getBalance(player5.getAddress());

    await competitionETHAsPrize.connect(operator).cancelRaffle(idRaffle);

    // check the raffle is canceled
    const raffle = await competitionETHAsPrize.rafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(6); // cancellation requested

    // check the seller receives the money back
    const balanceAfter = await ethers.provider.getBalance(player5.getAddress());
    expect(balanceAfter).to.be.equal(balanceBefore+(ethers.parseUnits('4', 'ether')));
  });

  it("should fail canceling a raffle already asked to cancel", async function () {
    await expect(competitionETHAsPrize.connect(operator).cancelRaffle(1)).to.be.reverted;
  });

  it("should fail staking asset in a not created raffle", async function () {
    await expect(competitionETHAsPrize.connect(operator).stakeETH(111)).to.be.reverted;
  });

  it("should fail staking asset in a raffle not in the right status", async function () {
    await expect(competitionETHAsPrize.connect(operator).stakeETH(0)).to.be.revertedWith("Raffle not CREATED");
  });

  it("should create a new raffle", async function () {
    var result = await competitionETHAsPrize.connect(operator).createRaffle(1000, 80, 80, 0,
      [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 26, price: 70000000000 }], 2500, [], ONLY_DIRECTLY);
    expect(result).to.emit('RaffleCreated').withArgs('2');
  });

  it("should stake a raffle", async function () {
    const result = await competitionETHAsPrize.connect(operator).stakeETH(2, { value: 80 });
    expect(result).to.emit('RaffleStarted').withArgs('2', operator);
  });

  it("should fail buying an entry paying below price", async function () {
    await expect(competitionETHAsPrize.connect(player1).buyEntry(2, 1, requiredNFT1.getAddress(), [], { value: 100 })
    ).to.be.revertedWithCustomError(competitionETHAsPrize, "EntryNotAllowed", "msg.value not the price");
  });

  it("should fail buying several entries for below price", async function () {
    await expect(competitionETHAsPrize.connect(player1).buyEntry(2, 2, requiredNFT1.getAddress(), [], { value: 700 })
    ).to.be.revertedWithCustomError(competitionETHAsPrize, "EntryNotAllowed", "msg.value not the price");
  });

  it("should fail buying a wrong amount of entries", async function () {
    await expect(competitionETHAsPrize.connect(player1).buyEntry(2, 6, requiredNFT1.getAddress(), [], { value: 800 })).to.be.reverted;
  });

  it("should allow buy several entries", async function () {
    const tracker = await ethers.provider.getBalance(competitionETHAsPrize.getAddress());

    await competitionETHAsPrize.connect(player1).buyEntry(2, 2, requiredNFT1.getAddress(), [], { value: 8000000000 });

    const currentDelta = await ethers.provider.getBalance(competitionETHAsPrize.getAddress());
    // check the contract received the money
    expect(currentDelta).to.be.equal(tracker+8000000000n);
  });

  it("should change the destination address", async function () {
    await competitionETHAsPrize.connect(owner).setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3');
  });

  it("should fail changing the destination address by a non owner", async function () {
    await expect(competitionETHAsPrize.connect(operator).setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3'))
      .to.be.reverted;
  });

  it("should add free entries", async function () {
    const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(ethers.parseUnits('1', 'ether'), 10, ethers.parseUnits('0.75', 'ether'), ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();
    await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: ethers.parseUnits('0.75', 'ether') });

    await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), [], { value: ethers.parseUnits('1', 'ether') });
    await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player4.getAddress(), player5.getAddress()]);
  });

  it("should transfer funds when the winner is from a free entry", async function () {
    const destinationWallet = await competitionETHAsPrize.destinationWallet();
    const balanceBefore = await ethers.provider.getBalance(destinationWallet);
    const balanceSellerBefore = await ethers.provider.getBalance(player4.getAddress());

    // the winner is from a free entry
    await competitionETHAsPrize.connect(operator).mockSetWinner(idRaffle, 2);

    const balanceAfter = await ethers.provider.getBalance(destinationWallet);
    //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    expect(balanceAfter).to.be.equal(balanceBefore+250000000000000000n); // 25% of raised amount
    const balanceSellerAfter = await ethers.provider.getBalance(player4.getAddress());
    //check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    expect(balanceSellerAfter).to.be.equal(balanceSellerBefore+750000000000000000n); // 75% of raised amount - gas fees
  });

  it("should transfer funds when the winner is from a Multi entry", async function () {
    const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(ethers.parseUnits('2', 'ether'), 10, ethers.parseUnits('2', 'ether'), ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();
    await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: ethers.parseUnits('2', 'ether') });

    await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 2, requiredNFT1.getAddress(), [], { value: ethers.parseUnits('2', 'ether') });
    await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
    const destinationWallet = await competitionETHAsPrize.destinationWallet();
    const balanceBefore = await ethers.provider.getBalance(destinationWallet);
    const balanceWinnerBefore = await ethers.provider.getBalance(player1.getAddress());

    // the winner is from a free entry
    result = await competitionETHAsPrize.connect(operator).mockSetWinner(idRaffle, 2);

    // check the event is emitted
    expect(result).to.emit('RaffleEnded').withArgs(idRaffle);
    // check the status is "ended"
    raffle = await competitionETHAsPrize.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(5);
    expect(raffle.entriesLength).to.be.equal(7);

    const balanceAfter = await ethers.provider.getBalance(destinationWallet);
    const balanceWinnerAfter = await ethers.provider.getBalance(player1.getAddress());
    //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    expect(balanceAfter).to.be.equal(balanceBefore+500000000000000000n); // 25% of raised amount
    expect(balanceWinnerAfter).to.be.equal(balanceWinnerBefore+(ethers.parseUnits('2', 'ether')));
  });

  it("should fail adding free entries if the status is not accepted", async function () {
    // check adding free entries to a closed raffle
    await expect(competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()])
    ).to.be.revertedWith("Raffle is not in accepted");
    // check adding free entries to a raffle that has not been accepted yet
    const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'), 10, 3, ethers.parseUnits('1', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();

    expect(result).to.emit("RaffleCreated").withArgs(idRaffle);
    await expect(competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()])
    ).to.be.revertedWith("Raffle is not in accepted");
  });

  it("should cancel a raffle", async function () {
    const resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(ethers.parseUnits('5', 'ether'), 10, 0, ethers.parseUnits('2', 'ether'), [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();
    await competitionETHAsPrize.connect(operator).stakeETH(idRaffle);
    await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), [], { value: ethers.parseUnits('1', 'ether') });

    await competitionETHAsPrize.connect(operator).cancelRaffle(idRaffle);

    // check the raffle is canceled
    const raffle = await competitionETHAsPrize.getRafflesEntryInfo(idRaffle);
    expect(raffle.status).to.be.equal(6); // cancellation requested   
  });

  it("should fail canceling a raffle already asked to cancel", async function () {
    await expect(competitionETHAsPrize.connect(player1).cancelRaffle(idRaffle)).to.be.reverted;
  });


  it("should ask a raffle for cancellation step 1", async function () {
    let resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(0, 5, 5, 0, [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') }, { id: 2, numEntries: 5, price: ethers.parseUnits('2', 'ether') }], 2500, [], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();

    await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: 5 });

    await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), [], { value: ethers.parseUnits('1', 'ether') });
    await competitionETHAsPrize.connect(player4).buyEntry(idRaffle, 2, requiredNFT1.getAddress(), [], { value: ethers.parseUnits('2', 'ether') });
    await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), [], { value: ethers.parseUnits('1', 'ether') });

    await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);

    let raffle = await competitionETHAsPrize.getRafflesEntryInfo(idRaffle);

    expect(raffle.amountRaised).to.be.equal(ethers.parseUnits('4', 'ether'))
  });

  it("should fail transferring remaining funds if the raffle is not in cancellation_requested status", async function () {
    await expect(competitionETHAsPrize.connect(operator).transferRemainingFunds(idRaffle)).to.be.revertedWith("Wrong status");
  });

  it("should ask a raffle for cancellation step 2", async function () {
    await competitionETHAsPrize.connect(operator).cancelRaffle(idRaffle)
    let raffle = await competitionETHAsPrize.getRafflesEntryInfo(idRaffle);

    expect(raffle.status).to.be.equal(6)
  });

  it("should fail transferring remaining funds a non operator", async function () {
    await expect(competitionETHAsPrize.connect(player1).transferRemainingFunds(idRaffle))
      .to.be.reverted;
  });

  it("should transfer remaining funds", async function () {
    let result = await competitionETHAsPrize.connect(operator).transferRemainingFunds(idRaffle);
    expect(result).to.emit('RemainingFundsTransferred').withArgs(idRaffle, ethers.parseUnits('4', 'ether'));
  });


  it("should allow a raffle with mixed entries at price 0 and with prices > 0", async function () {
    let resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(0, 20, 1, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 0, [], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();

    await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, {value: 1 });

    // Player 2 and player 4 get 1 entry for free
    await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), [], { value: ethers.parseUnits('0', 'ether') });
    await competitionETHAsPrize.connect(player4).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), [], { value: ethers.parseUnits('0', 'ether') });
    // player 3 and player 5 get free entries via the operator
    await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
    // player 1 buys 10 entries for 0.005
    await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 2, requiredNFT1.getAddress(), [], { value: ethers.parseUnits('0.005', 'ether') });

    // The winner will be player2
    result = await competitionETHAsPrize.connect(operator).mockSetWinner(idRaffle, 0);
      
    let raffle = await competitionETHAsPrize.raffles(idRaffle);
    expect(raffle.winner).to.be.equal(await player2.getAddress());
  });
  
  it("should fail if player wants to buy using an older priceId", async function () {
    // the id used for the prices are 11 and 12
    let resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(0, 21, 1, 0, [{ id: 10, numEntries: 1, price: 0 }, { id: 11, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 0, [], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();
  
    await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, { value: 1 });
  
    // Player 2 and player 4 get 1 entry for free
    await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 10, requiredNFT1.getAddress(), [],{ value: ethers.parseUnits('0', 'ether') });
    await competitionETHAsPrize.connect(player4).buyEntry(idRaffle, 10, requiredNFT1.getAddress(), [],{ value: ethers.parseUnits('0', 'ether') });
    // player 3 and player 5 get free entries via the operator
    await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
    // player 1 buys 10 entries for 0.005
    await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 11, requiredNFT1.getAddress(), [],{ value: ethers.parseUnits('0.005', 'ether') });
    // Fails when using and id on the price struct that did not belong to this raffle
    await expect(competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 1, requiredNFT1.getAddress(), [],{ value: 700 }), "EntryNotAllowed", ["Id not in raffleId"]);
  });
  
  it("should fail if player wants get two times a free entry", async function () {
    // the id used for the prices are 11 and 12
    let resultCreation = await competitionETHAsPrize.connect(operator).createRaffle(0, 21, 1, 0, [{ id: 10, numEntries: 1, price: 0 }, { id: 11, numEntries: 10, price: ethers.parseUnits('0.005', 'ether') }], 0, [], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = getRaffleId();
  
    await competitionETHAsPrize.connect(operator).stakeETH(idRaffle, {  value: 1 });
  
    // Player 2 and player 4 get 1 entry for free
    await competitionETHAsPrize.connect(player2).buyEntry(idRaffle, 10, requiredNFT1.getAddress(), [],{value: ethers.parseUnits('0', 'ether') });
    await competitionETHAsPrize.connect(player4).buyEntry(idRaffle, 10, requiredNFT1.getAddress(), [],{ value: ethers.parseUnits('0', 'ether') });
    // player 3 and player 5 get free entries via the operator
    await competitionETHAsPrize.connect(operator).giveBatchEntriesForFree(idRaffle, [player3.getAddress(), player5.getAddress()]);
    // player 1 buys 10 entries for 0.005
    await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 11, requiredNFT1.getAddress(), [],{ value: ethers.parseUnits('0.005', 'ether') });
    // Fails when using and id on the price struct that did not belong to this raffle
  // await expect(competitionETHAsPrize, 
     await competitionETHAsPrize.connect(player1).buyEntry(idRaffle, 10, requiredNFT1.getAddress(), [],{  value: 0 })
      //, "EntryNotAllowed", ["Id not in raffleId"]);
  });
  
 
});
