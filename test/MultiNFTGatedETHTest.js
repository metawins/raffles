const { expect } = require("chai");
const { ethers } = require("hardhat");

/// /// SEPOLIA //////////////

const SEPOLIA_VRF_COORDINATOR = "0x2bce784e69d2Ff36c71edcB9F88358dB0DfB55b4";
const SEPOLIA_LINK_TOKEN = "0x326C977E6efc84E512bB9C30f76E30c160eD06FB";
const SEPOLIA_KEYHASH =
  "0x0476f9a745b61ea5c0ab224d3a6e4c99f0b02fce4da01143a4f70aa80ae76e8a";
const ONLY_DIRECTLY = 0;

let token;
let manager;
let owner, player1, player2, player3, player4, player5, player6;
let seller, operator;
let idRaffle, receipt, result, resultMint, balance;
let requiredNFT,
  requiredNFT1,
  requiredNFT2,
  requiredNFT3,
  requiredTokenId,
  tokenId;
let tokenId1, tokenId2, tokenId3, tokenId4;
const zeroAddress = ethers.ZeroAddress;

async function getRaffleId() {
  filter = manager.filters.RaffleCreated;
  events = await manager.queryFilter(filter, -1)
  idRaffle = events[0].topics[1];
  return idRaffle;
}

async function getTokenId(tokenUsed) {
  filter = tokenUsed.filters.TokenCreated;
  events = await tokenUsed.queryFilter(filter, -1)
  returnedTokenId = events[0].topics[1];
  return returnedTokenId;
}

describe("ETH prize Multi nft gating phase 2", function () {
  it("Should mint tokens and configure", async function () {
    [
      owner,
      player1,
      player2,
      player3,
      player4,
      player5,
      player6,
      player7,
      seller,
      operator,
    ] = await ethers.getSigners();

    const Basic721 = await ethers.getContractFactory("Basic721");
    token = await Basic721.deploy();
    await token.waitForDeployment();

    const RequiredNFT = await ethers.getContractFactory("Basic721");
    requiredNFT = await RequiredNFT.deploy();
    await requiredNFT.waitForDeployment();

    const RequiredNFT1 = await ethers.getContractFactory("Basic721");
    requiredNFT1 = await RequiredNFT1.deploy();
    await requiredNFT1.waitForDeployment();

    const RequiredNFT2 = await ethers.getContractFactory("Basic721");
    requiredNFT2 = await RequiredNFT2.deploy();
    await requiredNFT2.waitForDeployment();

    const RequiredNFT3 = await ethers.getContractFactory("Basic721");
    requiredNFT3 = await RequiredNFT3.deploy();
    await requiredNFT3.waitForDeployment();

    const MockMultiNFTGatedETH = await ethers.getContractFactory(
      "MockMultiNFTGatedETH"
    );
    manager = await MockMultiNFTGatedETH.deploy(
      SEPOLIA_VRF_COORDINATOR,
      SEPOLIA_LINK_TOKEN,
      SEPOLIA_KEYHASH,
      false
    );
    await manager.waitForDeployment();
  });

  it("Should add operator role to raffles", async function () {
    const operatorHash = ethers.id("OPERATOR");
    await manager.grantRole(operatorHash, operator.getAddress(), {
      from: owner.getAddress(),
    });
  });
  it("The contract balance should be cero ", async function () {
    const balance = await ethers.provider.getBalance(manager.getAddress());
    expect(balance).to.be.equal(0);
  });

  it("should allow to buy only those who stakes certain nft ", async function () {
    const resultCreation = await manager.connect(operator).createRaffle(
      ethers.parseUnits("3", "ether"),
      11,
      ethers.parseUnits("2", "ether"),
      ethers.parseUnits("1", "ether"),
      [
        { id: 1, numEntries: 1, price: ethers.parseUnits("1", "ether") },
        { id: 2, numEntries: 10, price: ethers.parseUnits("2", "ether") },
      ],
      2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress()],
      ONLY_DIRECTLY
    );
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();

    await manager.connect(seller).stakeETH(idRaffle, { value: ethers.parseUnits("2", "ether") });

    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);

    result = await manager.connect(player1).buyEntry(idRaffle, 2,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("2", "ether") }
    );
    receipt = await result.wait();
    console.log("Gas used buying 10 entries with 2 nft restriction: " + receipt.cumulativeGasUsed);

    // Player 1 buys another entry using other nfts he has.
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);

    await manager.connect(player1).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );

    await expect(
      manager.connect(player1).buyEntry(
        idRaffle,
        1,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        ],
        { value: ethers.parseUnits("1", "ether") }
      )
    ).to.be.revertedWithCustomError(manager, "EntryNotAllowed", "Wallet already used");

    resultMint = await requiredNFT1.mint(player2.getAddress());    
    tokenId3 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player2.getAddress());    
    tokenId4 = await getTokenId(requiredNFT2);
    const currentOwner = await requiredNFT2.ownerOf(tokenId4);
    expect(currentOwner).to.be.equal(await player2.getAddress());

    const tokenId5 = 2;
    await manager.connect(player2).buyEntry(
      idRaffle,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: 2 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId5 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );

    // should fail if the buyer is not the owner of the tokenId
    await expect(
      manager.connect(player4).buyEntry(
        idRaffle,
        2,
        [
          { collection: requiredNFT1.getAddress(), tokenId: 0 },
          { collection: requiredNFT2.getAddress(), tokenId: 0 },
        ],
        { value: ethers.parseUnits("2", "ether") }
      )
    ).to.be.revertedWith("Not the owner of tokenId");

    // should fail if the tokenId was already used by another user (player2 receives tokenID=0 from player1)
    await requiredNFT1
      .connect(player1)
      .transferFrom(player1.getAddress(), player2.getAddress(), 0);
    resultMint = await requiredNFT2.mint(player2.getAddress());    
    tokenId1 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player2).buyEntry(idRaffle, 2,
        [
          { collection: requiredNFT1.getAddress(), tokenId: 0 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId1 },
        ],
        { value: ethers.parseUnits("2", "ether") }
      ),
      "tokenId used"
    );

    // the current # of entries for player 1 is 20 (Wallets cap)
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player1).buyEntry(
        idRaffle,
        2,
        [
          { collection: requiredNFT1.getAddress(), tokenId: 3 },
          { collection: requiredNFT2.getAddress(), tokenId: 4 },
        ],
        { value: ethers.parseUnits("2", "ether") }
      )
    ).to.be.revertedWithCustomError(manager, "EntryNotAllowed", "Wallet already used");

    await manager.connect(operator).giveBatchEntriesForFree(0, [player3.getAddress(), player5.getAddress()]);
    // should fail if the player did not have an nft of each collection
    resultMint = await requiredNFT1.mint(player3.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player3.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player3).buyEntry(
        idRaffle,
        1,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT1.getAddress(), tokenId: tokenId2 },
        ],
        { value: ethers.parseUnits("1", "ether") }
      )
    ).to.be.revertedWith("Collection already used");

  });

  it("should close the raffle", async function () {
    const balanceSellerBefore = await ethers.provider.getBalance(seller.getAddress());
    const balanceWinnerBefore = await ethers.provider.getBalance(player1.getAddress());

    await manager.connect(operator).mockSetWinner(0, 2);

    // check the winner received the prize
    const balanceWinnerAfter = await ethers.provider.getBalance(player1.getAddress());
    expect(balanceWinnerAfter).to.be.equal(
      balanceWinnerBefore+(ethers.parseUnits("2", "ether")))
    // check the seller received the money (75% of the raffle pool)
    const balanceSellerAfter = await ethers.provider.getBalance(seller.getAddress());
    expect(balanceSellerAfter).to.be.equal(
      balanceSellerBefore+(ethers.parseUnits("3", "ether"))) // 75% of 3000 = 2250
  });

  it("should fail creating a raffle a non-operator", async function () {
    await expect(manager.connect(player2).createRaffle(1, 1, 0, 2000, [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, [], ONLY_DIRECTLY))
      .to.be.reverted;
  });

  it("should fail staking asset in a not created raffle", async function () {
    await expect(manager.connect(seller).stakeETH(1, { value: 0 })).to.be.reverted;
  });

  it("should fail staking asset in a raffle not in the right status", async function () {
    await expect(manager.connect(seller).stakeETH(0, { value: 0 })
    ).to.be.revertedWith("Raffle not CREATED");
  });

  it("should create a new ruffle", async function () {
    var result = await manager.connect(operator).createRaffle(1000, 1, 1, 2000000000, [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 25, price: 70000000000 }], 2500, [], ONLY_DIRECTLY);
    expect(result).to.emit('RaffleCreated',).withArgs('1');
  });

  it("should stake asset in a raffle", async function () {
    let result = await manager.connect(seller).stakeETH(1, { value: 1 });
    expect(result).to.emit('RaffleStarted').withArgs('1', seller.getAddress());
  });

  it("should create a new raffle with 3 collections in whitelist", async function () {
    let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'),
      110,
      2, ethers.parseUnits('1', 'ether'),
      [{ id: 1, numEntries: 1, price: ethers.parseUnits('1', 'ether') },
      { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress(), requiredNFT3.getAddress()], ONLY_DIRECTLY);
    expect(resultCreation).to.emit('RaffleCreated').withArgs('2');
  });

  it("should stake asset in a raffle", async function () {
    let result = await manager.connect(seller).stakeETH(2, { value: 2 });
    expect(result).to.emit('RaffleStarted').withArgs('2', seller.getAddress());
  });

  it("should buy an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);
    resultMint = await requiredNFT3.mint(player1.getAddress());    
    tokenId3 = await getTokenId(requiredNFT3);
    const r1 = await manager.connect(player1).buyEntry(
      2,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        { collection: requiredNFT3.getAddress(), tokenId: tokenId3 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );
    receipt = await r1.wait();
    console.log(
      "Gas used buying 1 entry with 3 nft restriction: " +
      receipt.cumulativeGasUsed
    );
  });

  it("should fail buying an entry using 3 nfts but 2 from same collection", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player1).buyEntry(
        2,
        1,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
          { collection: requiredNFT2.getAddress(), tokenId: 0 },
        ],
        { value: ethers.parseUnits("1", "ether") }
      )
    ).to.be.revertedWith("Collection already used");
  });

  it("should fail buying an entry using 3 nfts but 2 from same collection (1 and 2)", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId3 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player1).buyEntry(
        2,
        1,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT1.getAddress(), tokenId: tokenId3 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        ],
        { value: ethers.parseUnits("1", "ether") }
      )
    ).to.be.revertedWith("Collection already used");
  });

  it("should buy again an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);
    resultMint = await requiredNFT3.mint(player1.getAddress());    
    tokenId3 = await getTokenId(requiredNFT3);
    const r1 = await manager.connect(player1).buyEntry(
      2,
      1,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        { collection: requiredNFT3.getAddress(), tokenId: tokenId3 },
      ],
      { value: ethers.parseUnits("1", "ether") }
    );
    receipt = await r1.wait();
    console.log(
      "Gas used buying 1 entry with 3 nft restriction: " +
      receipt.cumulativeGasUsed
    );
  });

  it("should fail  buying several entries for below price", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player1).buyEntry(2, 2,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        ],
        { value: 700 }
      )
    ).to.be.revertedWithCustomError(manager, "EntryNotAllowed", "msg.value not the price");
  });

  it("should fail buying a wrong amount of entries", async function () {
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player1).buyEntry(1, 6,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        ],
        { value: 800 }
      )
    ).to.be.reverted;
  });

  it("should allow buy several entries", async function () {
    const balanceBefore = await ethers.provider.getBalance(manager.getAddress());

    resultMint = await requiredNFT1.mint(player2.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player2.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(
      manager.connect(player2).buyEntry(2, 2,
        [
          { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
          { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
        ], { value: 8000000000 }
      )
    ).to.be.revertedWithCustomError(manager, "EntryNotAllowed", "msg.value not the price");
    await manager.connect(player2).buyEntry(2, 2,
      [
        { collection: requiredNFT1.getAddress(), tokenId: tokenId1 },
        { collection: requiredNFT2.getAddress(), tokenId: tokenId2 },
      ], { value: ethers.parseUnits("2", "ether") }
    );

    const balanceAfter = await ethers.provider.getBalance(manager.getAddress());
    expect(balanceAfter).to.be.equal(
      balanceBefore+(ethers.parseUnits("2", "ether"))
    );
  });

  it("should fail giving free entries two times to the same player", async function () {
    let resultCreation = await manager.connect(operator).createRaffle(ethers.parseUnits('3', 'ether'),
      110, 2, ethers.parseUnits('1', 'ether'),
      [{ id: 1, numEntries: 1, price: 0 },
      { id: 2, numEntries: 10, price: ethers.parseUnits('2', 'ether') }], 2500,
      [requiredNFT1.getAddress(), requiredNFT2.getAddress(), requiredNFT3.getAddress()], ONLY_DIRECTLY);
    receipt = await resultCreation.wait();
    idRaffle = await getRaffleId();
    expect(resultCreation).to.emit('RaffleCreated').withArgs(idRaffle);

    let result = await manager.connect(seller).stakeETH(idRaffle, { value: 2 });
    expect(result).to.emit('RaffleStarted').withArgs(idRaffle, seller.getAddress());
    // player 1 gets free entry
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player1).buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 }], { value: 0 })

    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);
    await expect(manager.connect(player1).buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 }],
      { value: 0 })).to.be.revertedWithCustomError(manager, 
        "EntryNotAllowed", "Player already got free entry");

    // player 1 can buy paid entries
    resultMint = await requiredNFT1.mint(player1.getAddress());    
    tokenId1 = await getTokenId(requiredNFT1);
    resultMint = await requiredNFT2.mint(player1.getAddress());    
    tokenId2 = await getTokenId(requiredNFT2);
    await manager.connect(player1).buyEntry(idRaffle, 2,
      [{ 'collection': requiredNFT1.getAddress(), 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.getAddress(), 'tokenId': tokenId2 }], { value: ethers.parseUnits('2', 'ether') });

  });
});