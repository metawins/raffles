const BlackListManager = artifacts.require("BlackListManager");
const MockH2H = artifacts.require("MockH2H");

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
    balance
} = require('@openzeppelin/test-helpers');

contract("H2H tests", accounts => {

    var owner = accounts[0];
    var player1 = accounts[1];
    var player2 = accounts[2];
    var player3 = accounts[3];
    var player4 = accounts[4];
    var player5 = accounts[5];
    var seller = accounts[8];
    var operator = accounts[9]
    var manager;
    var zeroAddress = '0x0000000000000000000000000000000000000000';
    var blacklist;
    var H2H;
    const ONLY_DIRECTLY = 0;
    const ONLY_EXTERNAL_CONTRACT = 1;
    const MIXED = 2;

    before(async function () {
        // set contract instance into a variable
        blacklist = await BlackListManager.deployed();
        H2H = await MockH2H.deployed();
    })

    it("Should add operator role to ETH-prize raffles", async function () {
        const operatorHash = web3.utils.soliditySha3('OPERATOR');
        await H2H.grantRole(operatorHash, operator, { from: owner });
    });

    it("should create an ETH-prized raffle", async function () {
        let result = await H2H.createH2HRaffle(web3.utils.toWei('1', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'), [],
            ONLY_DIRECTLY, 2, { from: operator });
        expectEvent(result, 'RaffleCreated', { 'raffleId': '0' });
    });
    it("should buy 2 entries on the raffle", async function () {
        let result = await H2H.buyEntry(0, 1, zeroAddress, 0, { from: player3, value: web3.utils.toWei('0.55', 'ether') });
        expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player3, 'currentSize': '1', 'priceStructureId': '1' });
        let gasUsed = result.receipt.gasUsed;
        console.log("Gas used buying: " + gasUsed);

        result = await H2H.buyEntry(0, 1, zeroAddress, 0, { from: player2, value: web3.utils.toWei('0.55', 'ether') });
        expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player2, 'currentSize': '2', 'priceStructureId': '1' });
        gasUsed = result.receipt.gasUsed;
        console.log("Gas used buying: " + gasUsed);
    });

    /*   it("should give an entry for free", async function () {
           let result = await H2H.giveBatchEntriesForFree(0, [player4], { from: operator });
           //expectEvent(result, 'FreeEntry', { 'raffleId': '0', 'buyer': player4 });
           let gasUsed = result.receipt.gasUsed;
           console.log("Gas used adding for free: " + gasUsed);
       });
   
    /*   it("should give a batch of free entries free", async function () {
           let result = await H2H.giveBatchEntriesForFree(0, [accounts[5], accounts[6]], { from: operator });
           expectEvent(result, 'FreeEntry', { 'raffleId': '0', 'buyer': [accounts[5], accounts[6]], 'amount': '2', 'currentSize': '5' });
           let gasUsed = result.receipt.gasUsed;
           console.log("Gas used adding for free 2 players: " + gasUsed);
       });
   */
    it("should close the raffle h2h with a winner", async function () {
        const trackerSeller = await balance.tracker(player1);
        const trackerWinner = await balance.tracker(player2);
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        trackerWinner.get();
        await trackerSeller.get();

        await time.increase(35);
        let result = await H2H.mockSetWinner(0, 1, { from: operator });
        expectEvent(result, 'RaffleEnded', { 'raffleId': '0', 'amountRaised': web3.utils.toWei('1.1', 'ether') });
        expectEvent(result, 'FeeTransferredToPlatform', { 'amountTransferred': web3.utils.toWei('0.1', 'ether') });

        let deltaWinner = await trackerWinner.delta();
        //    console.log("deltawinner " + deltaWinner);
        assert.equal(deltaWinner, web3.utils.toWei('1', 'ether'))

        let deltaDestinationWallet = await trackerDestinationWallet.delta();
        //     console.log("deltaDestinationWallet " + deltaDestinationWallet);
        assert.equal(deltaDestinationWallet, web3.utils.toWei('0.1', 'ether'))
    });

/*    it("should fail canceling a raffle already closed", async function () {
        await expectRevert(H2H.cancelRaffle(0, { from: operator }), "Wrong status");
    });

    it("should fail canceling a raffle a non-operator", async function () {
        await expectRevert.unspecified(H2H.cancelRaffle(0, { from: player1 }));
    });
*/
    it("should create another h2h. Only 1 buyer. Get failure amount fails because no balance, but do it well after being funded", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('1', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'), [],
            ONLY_DIRECTLY, 2, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        let result = await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player3, value: web3.utils.toWei('0.55', 'ether') });
        expectEvent(result, 'EntrySold', { 'raffleId': idRaffle, 'buyer': player3, 'currentSize': '1', 'priceStructureId': '1' });
        let gasUsed = result.receipt.gasUsed;
        console.log("Gas used buying: " + gasUsed);

        const trackerWinner = await balance.tracker(player3);
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        await trackerWinner.get();

        await time.increase(35);
        // Fails because no balance enough
        await expectRevert(H2H.mockSetWinner(idRaffle, 1, { from: operator }), "Not enough balance");
        let balanceH2H = await web3.eth.getBalance(H2H.address)
        console.log("Contract H2H balance Before = " + balanceH2H);
        await web3.eth.sendTransaction({ from: accounts[1], to: H2H.address, value: web3.utils.toWei("1", "ether") });

        balanceH2H = await web3.eth.getBalance(H2H.address)
        console.log("Contract H2H balance after  = " + balanceH2H);
        // now the h2h contract is funded, setwinner can work
        let resultWin = await H2H.mockSetWinner(idRaffle, 1, { from: operator });
        //  console.log(JSON.stringify(resultWin))

        let raffle = await H2H.raffles(idRaffle);
        ///console.log(JSON.stringify(raffle))
        expectEvent(resultWin, 'RaffleCancelled', { 'amountRaised': web3.utils.toWei('0.55', 'ether') });

        let deltaWinner = await trackerWinner.delta();
        //    console.log("deltawinner " + deltaWinner);
        assert.equal(deltaWinner, web3.utils.toWei('0.6', 'ether'))

    });

    it("should create a 4 H2H raffle, and close it with a winner", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('2', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'), [],
            ONLY_DIRECTLY, 4, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });

        await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player1, value: web3.utils.toWei('0.55', 'ether') });
        await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player2, value: web3.utils.toWei('0.55', 'ether') });
        await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player3, value: web3.utils.toWei('0.55', 'ether') });
        // assert only one entry per player is allowed
        await expectRevert(H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player3, value: web3.utils.toWei('0.55', 'ether') }), "Bought too many entries");
        await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player4, value: web3.utils.toWei('0.55', 'ether') });

        const trackerWinner = await balance.tracker(player3);
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        await trackerWinner.get();

        let resultWin = await H2H.mockSetWinner(idRaffle, 2, { from: operator });
        expectEvent(resultWin, 'RaffleEnded', { 'raffleId': idRaffle, 'amountRaised': web3.utils.toWei('2.2', 'ether') });
        expectEvent(resultWin, 'FeeTransferredToPlatform', { 'amountTransferred': web3.utils.toWei('0.2', 'ether') });

        let deltaWinner = await trackerWinner.delta();
        assert.equal(deltaWinner, web3.utils.toWei('2', 'ether'))

        let deltaDestinationWallet = await trackerDestinationWallet.delta();
        assert.equal(deltaDestinationWallet, web3.utils.toWei('0.2', 'ether'))

    });

    it("should create a 4 H2H raffle, and cancel it if only 3 players exists", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('2', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'), [],
            ONLY_DIRECTLY, 4, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });

        await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player1, value: web3.utils.toWei('0.55', 'ether') });
        await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player2, value: web3.utils.toWei('0.55', 'ether') });
        await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player3, value: web3.utils.toWei('0.55', 'ether') });

        const trackerWinner1 = await balance.tracker(player1);
        const trackerWinner2 = await balance.tracker(player2);
        const trackerWinner3 = await balance.tracker(player3);
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        await trackerWinner1.get();
        await trackerWinner2.get();
        await trackerWinner3.get();

        let resultWin = await H2H.mockSetWinner(idRaffle, 2, { from: operator });
        expectEvent(resultWin, 'RaffleCancelled', { 'raffleId': idRaffle, 'amountRaised': web3.utils.toWei('1.65', 'ether') });
       
        let deltaWinner1 = await trackerWinner1.delta();
        assert.equal(deltaWinner1, web3.utils.toWei('0.6', 'ether'));
        let deltaWinner2 = await trackerWinner2.delta();
        assert.equal(deltaWinner2, web3.utils.toWei('0.55', 'ether'));
        let deltaWinner3 = await trackerWinner3.delta();
        assert.equal(deltaWinner3, web3.utils.toWei('0.55', 'ether'));

        let deltaDestinationWallet = await trackerDestinationWallet.delta();
        assert.equal(deltaDestinationWallet, web3.utils.toWei('0', 'ether'))

    });

    it("should create a 4 H2H raffle, but nobody boughts", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('2', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'), [],
            ONLY_DIRECTLY, 4, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });
        let resultWin = await H2H.mockSetWinner(idRaffle, 2, { from: operator });
        expectEvent(resultWin, 'RaffleCancelled', { 'raffleId': idRaffle, 'amountRaised': web3.utils.toWei('0', 'ether') });

    });
    /*
        it("should cancel a raffle", async function () {
            let resultCreation = await H2H.createRaffle(1000, 25, web3.utils.toWei('4', 'ether'), 0, [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, [], ONLY_DIRECTLY, 1000, { from: operator });
            let idRaffle = resultCreation.logs[0].args.raffleId;
    
            await H2H.stakeETH(idRaffle, { from: player5, value: web3.utils.toWei('4', 'ether') });
    
            await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player1, value: web3.utils.toWei('1', 'ether') });
    
            let destinationWallet = await H2H.destinationWallet();
            const tracker = await balance.tracker(player5);
            await tracker.get();
    
            await H2H.cancelRaffle(idRaffle, { from: operator });
    
            // check the raffle is canceled
            let raffle = await H2H.raffles(idRaffle);
            assert.equal(raffle.status, 6); // cancellation requested
    
            // check the seller receives the money back
            let currentDelta = await tracker.delta()
            console.log("deltaOperator " + currentDelta);
            assert.equal(currentDelta, web3.utils.toWei('4', 'ether')); 
        });
    
        it("should fail canceling a raffle already asked to cancel", async function () {
            await expectRevert.unspecified(H2H.cancelRaffle(1, { from: operator }));
        });
    
        it("should fail if a raffle has totalEntriesCap of 2 and a third player wants to participate", async function () {
            let resultCreation = await H2H.createRaffle(1000, 1, web3.utils.toWei('4', 'ether'), 0, [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }], 2500, [], ONLY_DIRECTLY, 2, { from: operator });
            let idRaffle = resultCreation.logs[0].args.raffleId;
    
            await H2H.stakeETH(idRaffle, { from: player5, value: web3.utils.toWei('4', 'ether') });
    
            await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player1, value: web3.utils.toWei('1', 'ether') });
            await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player2, value: web3.utils.toWei('1', 'ether') });
            await expectRevert(H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player3, value: web3.utils.toWei('1', 'ether') }), "Total Cap Entries reached");
    
            await time.increase(35);
            let result = await H2H.mockSetWinner(idRaffle, 1, { from: operator });
            expectEvent(result, 'RaffleEnded', { 'raffleId': idRaffle, 'amountRaised': web3.utils.toWei('2', 'ether') });
            expectEvent(result, 'FeeTransferredToPlatform', { 'amountTransferred': web3.utils.toWei('0.5', 'ether') });
    
        });
    /*
        it("should work if totalEntriesCap == 0 because that means not checking entries cap", async function () {
            let resultCreation = await H2H.createRaffle(1000, 5, web3.utils.toWei('4', 'ether'), 0, [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }], 2500, [], ONLY_DIRECTLY, 0, { from: operator });
            let idRaffle = resultCreation.logs[0].args.raffleId;
    
            await H2H.stakeETH(idRaffle, { from: player5, value: web3.utils.toWei('4', 'ether') });
    
            await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player1, value: web3.utils.toWei('1', 'ether') });
            await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player2, value: web3.utils.toWei('1', 'ether') });
            await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player3, value: web3.utils.toWei('1', 'ether') });
            await H2H.buyEntry(idRaffle, 1, zeroAddress, 0, { from: player4, value: web3.utils.toWei('1', 'ether') });
            
            await time.increase(35);
            let result = await H2H.mockSetWinner(idRaffle, 1, { from: operator });
            expectEvent(result, 'RaffleEnded', { 'raffleId': idRaffle, 'amountRaised': web3.utils.toWei('4', 'ether') });
            expectEvent(result, 'FeeTransferredToPlatform', { 'amountTransferred': web3.utils.toWei('1', 'ether') });
    
        });
    
        */
});
