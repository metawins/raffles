// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import "./MockCompetitionsManager.sol";

/// @title Info (read-only data) manager
/// @author Luis Pando
/// @notice Shows read-only data of the raffles
/// @dev A player is blacklisted for all the raffles at once.
contract MockCompetitionsInfo {
    MockCompetitionsManager compsMgr;

    constructor(address _competitionsManagerAddress) {
        compsMgr = MockCompetitionsManager(_competitionsManagerAddress);
    }

    function raffleNotInAcceptedState(uint256 _raffleId)
        external
        view
        returns (bool)
    {
        MockCompetitionsManager.RaffleStruct memory raffle = compsMgr
            .getRaffleData(_raffleId);
     
        return (raffle.status != MockCompetitionsManager.STATUS.ACCEPTED);
    }

    function playerReachedMaxEntries(
        address _player,
        uint256 _raffleId,
        uint256 _amountOfEntries
    ) external view returns (bool) {
        MockCompetitionsManager.RaffleStruct memory raffle = compsMgr
            .getRaffleData(_raffleId);

        bytes32 hash = keccak256(abi.encode(_player, _raffleId));
        (uint48 numEntriesPerUser, , ) = compsMgr.claimsData(hash);

         return (numEntriesPerUser +
            _amountOfEntries >
            raffle.maxEntries);    
    }

    function playerHasRequiredNFTs(
        address _player,
        uint256 _raffleId,
        address _collection,
        uint256 _tokenIdUsed
    ) external view returns (bool canBuy, string memory cause) {
         MockCompetitionsManager.RaffleStruct memory raffle = compsMgr
            .getRaffleData(_raffleId);

        // if the raffle requires an nft
        if (raffle.collectionWhitelist.length > 0) {
            bool hasRequiredCollection = false;
            for (uint256 i = 0; i < raffle.collectionWhitelist.length; i++) {
                if (raffle.collectionWhitelist[i] == _collection) {
                    hasRequiredCollection = true;
                    break;
                }
            }
            if (hasRequiredCollection == false)
                return (false, "Not in required collection");

            IERC721 requiredNFT = IERC721(_collection);
            if (requiredNFT.ownerOf(_tokenIdUsed) != _player)
                return (false, "Not the owner of tokenId");
            bytes32 hashRequiredNFT = keccak256(
                abi.encode(_collection, _raffleId, _tokenIdUsed)
            );
            // check the tokenId has not been using yet in the raffle, to avoid abuse
            if (compsMgr.requiredNFTWallets(hashRequiredNFT) == _player)
                return (false, "tokenId used");
        }

        return (true, "");
    }

     function playerIsSeller(address _player, uint256 _raffleId)
        external
        view
        returns (bool)
    {
        (address seller, , , , , ) = compsMgr.prizesData(_raffleId,0);
        return (seller == _player);
    }

   
}
