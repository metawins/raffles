// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import "../H2HLean.sol";

contract MockH2HLean is H2HLean {
    constructor(
        address _vrfCoordinator,
        address _linkToken,
        bytes32 _keyHash,
        bool _mainnetFee
    ) H2HLean(_vrfCoordinator, _linkToken, _keyHash, _mainnetFee) {}

    function mockSetWinner(
        uint256 _raffleId,
        uint256 _fakeRandomNumber
    ) external onlyRole(OPERATOR_ROLE) {
        bool shouldDoCallback = setWinnerActions(_raffleId);
        if (shouldDoCallback) {
            bytes32 requestId = bytes32(uint256(1));
            uint256 entriesSize = entriesList[_raffleId].length;
            getRandomNumber(_raffleId, entriesSize, requestId);
            fulfillRandomness(requestId, _fakeRandomNumber);
        }
    }
}
