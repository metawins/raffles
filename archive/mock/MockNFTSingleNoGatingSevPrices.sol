// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.19;

import "../NFTSingleNoGatingSevPrices.sol";

contract MockNFTSingleNoGatingSevPrices is NFTSingleNoGatingSevPrices {
    constructor(
        address _vrfCoordinator,
        address _linkToken,
        bytes32 _keyHash,
        bool _mainnetFee
    )
        NFTSingleNoGatingSevPrices(
            _vrfCoordinator,
            _linkToken,
            _keyHash,
            _mainnetFee
        )
    {}

    function mockSetWinner(
        uint256 _raffleId,
        uint256 _fakeRandomNumber
    ) external onlyRole(OPERATOR_ROLE) {
        EntryInfoStruct memory raffle = setWinnerActions(_raffleId);

        bytes32 requestId = bytes32(uint256(1));
        getRandomNumber(_raffleId, raffle.entriesLength, requestId);
        fulfillRandomness(requestId, _fakeRandomNumber);
    }

}
