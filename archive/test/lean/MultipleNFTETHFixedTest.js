const CompetitionETHAsPrize = artifacts.require("./LowGasVRF1/lean/MockMultipleNFTGatedETHFixedLean");

const RequiredNFT1 = artifacts.require("Basic721");
const RequiredNFT2 = artifacts.require("Basic721");
const RequiredNFT3 = artifacts.require("Basic721");

const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
  time,
  balance
} = require('@openzeppelin/test-helpers');

const { expectRevertCustomError } = require("custom-error-test-helper");

contract("LEAN ETH fixed prizes tests multiple nft gating", accounts => {

  var owner = accounts[0];
  var player1 = accounts[1];
  var player2 = accounts[2];
  var player3 = accounts[3];
  var player4 = accounts[4];
  var player5 = accounts[5];
  var seller = accounts[8];
  var operator = accounts[9]
  var competitionETHAsPrize;
  var requiredNFT1;
  var requiredNFT2;
  var requiredNFT3;
  var idRaffle;
  var zeroAddress = '0x0000000000000000000000000000000000000000';
  const ONLY_DIRECTLY = 0;
  const ONLY_EXTERNAL_CONTRACT = 1;
  const MIXED = 2;

  before(async function () {
    // set contract instance into a variable
    competitionETHAsPrize = await CompetitionETHAsPrize.deployed();
  })

  it("Should add operator role to ETH-prize raffles", async function () {
    const operatorHash = web3.utils.soliditySha3('OPERATOR');
    await competitionETHAsPrize.grantRole(operatorHash, operator, { from: owner });
  });

  it("should create an ETH-prized raffle", async function () {
    requiredNFT1 = await RequiredNFT1.new();
    let result = await competitionETHAsPrize.createRaffle(1000,
      10,
      web3.utils.toWei('4', 'ether'), 0,
      [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') },
      { id: 2, numEntries: 10, price: web3.utils.toWei('2', 'ether') }], 2500,
      [requiredNFT1.address], ONLY_DIRECTLY, { from: operator });
    expectEvent(result, 'RaffleCreated', { 'raffleId': '0' });
  });

  it("should stake ETH in an ETH raffle", async function () {
    let result = await competitionETHAsPrize.stakeETH(0, { from: player1, value: web3.utils.toWei('4', 'ether') });
    expectEvent(result, 'RaffleStarted', { 'raffleId': '0', 'seller': player1 });
  });

  it("should buy an entry on the raffle", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;

    result = await competitionETHAsPrize.buyEntry(0, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 1 entries with 2 nft restriction: " + result.receipt.gasUsed);

    expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player1, 'currentSize': '1', 'priceStructureId': '1' });
    let gasUsed = result.receipt.gasUsed;
    console.log("Gas used buying: " + gasUsed);

    // should fail if the buyer did not have any nft
    await expectRevert(competitionETHAsPrize.buyEntry(0, 2, requiredNFT1.address, [], { from: player4, value: web3.utils.toWei('2', 'ether') }), "Panic: Index out of bounds");

    // should fail if the buyer is not the owner of the tokenId
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    await expectRevert(competitionETHAsPrize.buyEntry(0, 2, requiredNFT1.address, [1, tokenId1], { from: player1, value: web3.utils.toWei('2', 'ether') }), "Not the owner of tokenId");

    // should fail if the tokenId was already used by another user (player2 receives tokenID=0 from player1)
    await requiredNFT1.transferFrom(player1, player2, 0, { from: player1 });
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    await expectRevert(competitionETHAsPrize.buyEntry(0, 2, requiredNFT1.address, [0, tokenId1], { from: player2, value: web3.utils.toWei('2', 'ether') }), "tokenId used");

    // Check wallets cap
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevertCustomError(competitionETHAsPrize,
      competitionETHAsPrize.buyEntry(0, 2, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: web3.utils.toWei('2', 'ether') }),
      "EntryNotAllowed", ["Wallet already used"]);

    // should fail if not using the right collection
    requiredNFT2 = await RequiredNFT2.new();
    await expectRevert(competitionETHAsPrize.buyEntry(0, 2, requiredNFT2.address, [0, 1], { from: player2, value: web3.utils.toWei('2', 'ether') }), "Not in required collection");

    await competitionETHAsPrize.giveBatchEntriesForFree(0, [player3, player5], { from: operator });

    // Player 2 has only 1 nft
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    await expectRevert(competitionETHAsPrize.buyEntry(0, 1, requiredNFT1.address, [tokenId3], { from: player2, value: web3.utils.toWei('1', 'ether') }), "Panic: Index out of bounds");
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId4 = resultMint.logs[0].args.tokenId;
    result = await competitionETHAsPrize.buyEntry(0, 1, requiredNFT1.address, [tokenId3, tokenId4], { from: player2, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 10 entries with 2 nft restriction: " + result.receipt.gasUsed);

  });

  it("should fail buying by less than set in the prices", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevertCustomError(competitionETHAsPrize, competitionETHAsPrize.buyEntry(0, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: 700 }), "EntryNotAllowed", ["msg.value not the price"]);
  });

  it("should give an entry for free", async function () {
    let result = await competitionETHAsPrize.giveBatchEntriesForFree(0, [player4], { from: operator });
    let gasUsed = result.receipt.gasUsed;
    console.log("Gas used adding for free: " + gasUsed);
  });

  it("should give a batch of free entries free", async function () {
    let result = await competitionETHAsPrize.giveBatchEntriesForFree(0, [accounts[5], accounts[6]], { from: operator });
    expectEvent(result, 'FreeEntry', { 'raffleId': '0', 'buyer': [accounts[5], accounts[6]], 'amount': '2' });
    let gasUsed = result.receipt.gasUsed;
    console.log("Gas used adding for free 2 players: " + gasUsed);
  });

  it("should show the raffle info", async function () {
    let result = await competitionETHAsPrize.fundingList(0);
    //  console.log(JSON.stringify(result));
    assert.equal(result.desiredFundsInWeis, '1000');
  });

  it("should close the raffle", async function () {
    const trackerSeller = await balance.tracker(player1);
    const trackerWinner = await balance.tracker(player3);
    const destinationWallet = await competitionETHAsPrize.destinationWallet();
    const trackerDestinationWallet = await balance.tracker(destinationWallet);
    await trackerDestinationWallet.get();
    trackerWinner.get();
    await trackerSeller.get();

    await time.increase(35);
    let result = await competitionETHAsPrize.mockSetWinner(0, [1], { from: operator });
    expectEvent(result, 'RaffleEnded', { 'raffleId': '0', 'amountRaised': web3.utils.toWei('2', 'ether') });
    expectEvent(result, 'FeeTransferredToPlatform', { 'amountTransferred': web3.utils.toWei('0.5', 'ether') });
    let deltaSeller = await trackerSeller.delta()
    console.log("deltaSeller " + deltaSeller);
    assert.equal(deltaSeller, web3.utils.toWei('1.5', 'ether'))

    let deltaWinner = await trackerWinner.delta();
    console.log("deltawinner " + deltaWinner);
    assert.equal(deltaWinner, web3.utils.toWei('4', 'ether'))

    let deltaDestinationWallet = await trackerDestinationWallet.delta();
    //     console.log("deltaDestinationWallet " + deltaDestinationWallet);
    assert.equal(deltaDestinationWallet, web3.utils.toWei('0.5', 'ether'))
  });

  it("should fail buying an entry after draw", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;

    await expectRevertCustomError(competitionETHAsPrize,
      competitionETHAsPrize.buyEntry(0, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: web3.utils.toWei('1', 'ether') }),
      "EntryNotAllowed", ["Not in ACCEPTED"]);
  });

  it("should fail canceling a raffle already closed", async function () {
    await expectRevert(competitionETHAsPrize.cancelRaffle(0, { from: operator }), "Wrong status");
  });

  it("should fail canceling a raffle a non-operator", async function () {
    await expectRevert.unspecified(competitionETHAsPrize.cancelRaffle(0, { from: player1 }));
  });

  it("should cancel a raffle", async function () {
    let resultCreation = await competitionETHAsPrize.createRaffle(1000, 10, web3.utils.toWei('4', 'ether'), 0, 
    [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, [], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;

    await competitionETHAsPrize.stakeETH(idRaffle, { from: player5, value: web3.utils.toWei('4', 'ether') });

    await competitionETHAsPrize.buyEntry(idRaffle, 1, requiredNFT1.address ,[], { from: player1, value: web3.utils.toWei('1', 'ether') });

    let destinationWallet = await competitionETHAsPrize.destinationWallet();
    const tracker = await balance.tracker(player5);
    await tracker.get();

    await competitionETHAsPrize.cancelRaffle(idRaffle, { from: operator });

    // check the raffle is canceled
    let raffle = await competitionETHAsPrize.rafflesEntryInfo(idRaffle);
    assert.equal(raffle.status, 6); // cancellation requested

    // check the seller receives the money back
    let currentDelta = await tracker.delta()
    console.log("deltaOperator " + currentDelta);
    assert.equal(currentDelta, web3.utils.toWei('4', 'ether'));
  });
  
    it("should fail canceling a raffle already asked to cancel", async function () {
      await expectRevert.unspecified(competitionETHAsPrize.cancelRaffle(1, { from: operator }));
    });
  
    it("should fail staking asset in a not created raffle", async function () {
      await expectRevert.unspecified(competitionETHAsPrize.stakeETH(111, { from: operator }));
    });
  
    it("should fail staking asset in a raffle not in the right status", async function () {
      await expectRevert(competitionETHAsPrize.stakeETH(0, { from: operator }), "Raffle not CREATED");
    });
  
    it("should create a new ruffle", async function () {
      var result = await competitionETHAsPrize.createRaffle(1000, 80, 80, 0,
        [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 26, price: 70000000000 }], 2500, [], ONLY_DIRECTLY, { from: operator });
      expectEvent(result, 'RaffleCreated', { 'raffleId': '2' });
    });
  
    it("should stake a raffle", async function () {
      let result = await competitionETHAsPrize.stakeETH(2, { from: operator, value: 80 });
      expectEvent(result, 'RaffleStarted', { 'raffleId': '2', 'seller': operator });
    });
  
    it("should fail buying an entry paying below price", async function () {
      await expectRevertCustomError(competitionETHAsPrize,
        competitionETHAsPrize.buyEntry(2, 1, requiredNFT1.address, [], { from: player1, value: 100 }),
        "EntryNotAllowed", ["msg.value not the price"]);
    });
  
    it("should fail buying several entries for below price", async function () {
      await expectRevertCustomError(competitionETHAsPrize,
        competitionETHAsPrize.buyEntry(2, 2, requiredNFT1.address, [], { from: player1, value: 700 }),
        "EntryNotAllowed", ["msg.value not the price"]);
    });
  
    it("should fail buying a wrong amount of entries", async function () {
      await expectRevert.unspecified(competitionETHAsPrize.buyEntry(2, 6, requiredNFT1.address, [], { from: player1, value: 800 }));
    });
  
    it("should allow buy several entries", async function () {
      const tracker = await balance.tracker(competitionETHAsPrize.address);
      await tracker.get();
  
      await competitionETHAsPrize.buyEntry(2, 2,requiredNFT1.address, [], { from: player1, value: 8000000000 });
  
      let currentDelta = await tracker.delta();
      // check the contract received the money
      assert.equal(currentDelta, 8000000000);
    });
  
    it("should fail creating a raffle with more price categories than allowed", async function () {
      await expectRevert.unspecified(competitionETHAsPrize.createRaffle(1000, 1, 200000000000, 0, [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 25, price: 70000000000 }, { id: 4, numEntries: 25, price: 70000000000 }, { id: 5, numEntries: 25, price: 70000000000 }, { id: 6, numEntries: 25, price: 70000000000 }], 2500, [], ONLY_DIRECTLY, { from: owner }));
    });
  
    it("should change the destination address", async function () {
      await competitionETHAsPrize.setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3', { from: owner });
    });
  
    it("should fail changing the destination address by a non owner", async function () {
      await expectRevert.unspecified(competitionETHAsPrize.setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3', { from: operator }));
    });
  
    it("should add free entries", async function () {
      let result = await competitionETHAsPrize.createRaffle(web3.utils.toWei('1', 'ether'), 10, web3.utils.toWei('0.75', 'ether'), web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
      idRaffle = result.logs[0].args.raffleId;
       await competitionETHAsPrize.stakeETH(idRaffle, { from: operator, value: web3.utils.toWei('0.75', 'ether') });
  
      await competitionETHAsPrize.buyEntry(idRaffle, 1, requiredNFT1.address, [],{ from: player1, value: web3.utils.toWei('1', 'ether') });
      await competitionETHAsPrize.giveBatchEntriesForFree(idRaffle, [player3, player4, player5], { from: operator });
    });
  
    it("should transfer funds when the winner is from a free entry", async function () {
      let destinationWallet = await competitionETHAsPrize.destinationWallet();
      const tracker = await balance.tracker(destinationWallet);
      const currentBalance = await tracker.get();
      //  console.log("Detination wallet balance = " + currentBalance);
  
      const trackerSeller = await balance.tracker(player4);
      const b1 = await trackerSeller.get();
  
      const raffle = await competitionETHAsPrize.raffles(idRaffle);
      //   console.log("raffle amount raised =      " + raffle.amountRaised.toString());
      const funding = await competitionETHAsPrize.fundingList(idRaffle);
      //  console.log("funding =      " + JSON.stringify(funding));
  
      // the winner is from a free entry
      await competitionETHAsPrize.mockSetWinner(idRaffle, [2], { from: operator });
      
      const tracker2 = await balance.tracker(destinationWallet);
      const currentBalance2 = await tracker2.get();
      // console.log("Destination wallet balance = " + currentBalance2);
  
      let currentDelta = await tracker.delta()
      //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
      assert.equal(currentDelta.toString(), '250000000000000000'); // 25% of raised amount
      let sellerDelta = await trackerSeller.delta();
      const b2 = await trackerSeller.get();
      //check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
      assert.equal(sellerDelta.toString(), '750000000000000000'); // 75% of raised amount - gas fees
    });
  
    it("should transfer funds when the winner is from a multiple entry", async function () {
      let result = await competitionETHAsPrize.createRaffle(web3.utils.toWei('2', 'ether'), 10, web3.utils.toWei('2', 'ether'), web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500,[], ONLY_DIRECTLY, { from: operator });
      idRaffle = result.logs[0].args.raffleId;
      await competitionETHAsPrize.stakeETH(idRaffle, { from: operator, value: web3.utils.toWei('2', 'ether') });
  
      await competitionETHAsPrize.buyEntry(idRaffle, 2, requiredNFT1.address, [], { from: player1, value: web3.utils.toWei('2', 'ether') });
      await competitionETHAsPrize.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
      let destinationWallet = await competitionETHAsPrize.destinationWallet();
      const tracker = await balance.tracker(destinationWallet);
      const trackerWinner = await balance.tracker(player1);
  
      // the winner is from a free entry
      result = await competitionETHAsPrize.mockSetWinner(idRaffle, [2], { from: operator });
  
      // check the event is emitted
      expectEvent(result, 'RaffleEnded', { 'raffleId':idRaffle });
      // check the status is "ended"
      raffle = await competitionETHAsPrize.getRafflesEntryInfo(idRaffle);
      assert.equal(raffle.status, 5);
      assert.equal(raffle.entriesLength, 7);
  
      let currentDelta = await tracker.delta()
      //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
      assert.equal(currentDelta.toString(), '500000000000000000'); // 25% of raised amount
      let winnerDelta = await trackerWinner.delta(); // winner gets the prize
      assert.equal(winnerDelta.toString(), web3.utils.toWei('2', 'ether'));
    });
  
    it("should fail adding free entries if the status is not accepted", async function () {
      // check adding free entries to a closed raffle
      await expectRevert(competitionETHAsPrize.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator }), "Raffle is not in accepted");
      // check adding free entries to a raffle that has not been accepted yet
      let result = await competitionETHAsPrize.createRaffle(web3.utils.toWei('3', 'ether'), 10, 3, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
      idRaffle = result.logs[0].args.raffleId;
      expectEvent(result, 'RaffleCreated', { 'raffleId': idRaffle });
      await expectRevert(competitionETHAsPrize.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator }), "Raffle is not in accepted");
    });
  
    it("should cancel a raffle", async function () {
      let result = await competitionETHAsPrize.createRaffle(web3.utils.toWei('5', 'ether'),10, 0, web3.utils.toWei('2', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
      idRaffle = result.logs[0].args.raffleId;
      await competitionETHAsPrize.stakeETH(idRaffle, { from: operator });
      await competitionETHAsPrize.buyEntry(idRaffle, 1, requiredNFT1.address, [], { from: player1, value: web3.utils.toWei('1', 'ether') });
  
      await competitionETHAsPrize.cancelRaffle(idRaffle, { from: operator });
  
      // check the raffle is canceled
      let raffle = await competitionETHAsPrize.getRafflesEntryInfo(idRaffle);
      assert.equal(raffle.status, 6); // cancellation requested   
    });
  
    it("should fail canceling a raffle already asked to cancel", async function () {
      await expectRevert.unspecified(competitionETHAsPrize.cancelRaffle(idRaffle, { from: player1 }));
    });
  
  
    it("should ask a raffle for cancellation step 1", async function () {
      let resultCreation = await competitionETHAsPrize.createRaffle(0, 5, 5,0, [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
      idRaffle = resultCreation.logs[0].args.raffleId;
      console.log("IDRaffle " + idRaffle)
  
      await competitionETHAsPrize.stakeETH(idRaffle, { from: operator, value: 5 });
  
      await competitionETHAsPrize.buyEntry(idRaffle, 1, requiredNFT1.address, [], { from: player2, value: web3.utils.toWei('1', 'ether') });
      await competitionETHAsPrize.buyEntry(idRaffle, 2,requiredNFT1.address, [], { from: player4, value: web3.utils.toWei('2', 'ether') });
      await competitionETHAsPrize.buyEntry(idRaffle, 1, requiredNFT1.address, [],{ from: player1, value: web3.utils.toWei('1', 'ether') });
  
      await competitionETHAsPrize.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
  
      let raffle = await competitionETHAsPrize.getRafflesEntryInfo(idRaffle);
  
      assert.equal(raffle.amountRaised, web3.utils.toWei('4', 'ether'))
    });
  
    it("should fail transferring remaining funds if the raffle is not in cancellation_requested status", async function () {
      await expectRevert(competitionETHAsPrize.transferRemainingFunds(idRaffle, { from: operator }), "Wrong status");
    });
  
    it("should ask a raffle for cancellation step 2", async function () {
      await competitionETHAsPrize.cancelRaffle(idRaffle, { from: operator })
      let raffle = await competitionETHAsPrize.getRafflesEntryInfo(idRaffle);
  
      assert.equal(raffle.status, 6)
    });
  
    it("should fail transferring remaining funds a non operator", async function () {
      await expectRevert.unspecified(competitionETHAsPrize.transferRemainingFunds(idRaffle, { from: player1 }), "claim time expired");
    });
  
    it("should transfer remaining funds", async function () {
      let result = await competitionETHAsPrize.transferRemainingFunds(idRaffle, { from: operator });
      expectEvent(result, 'RemainingFundsTransferred', { 'raffleId': idRaffle, 'amountInWeis': web3.utils.toWei('4', 'ether') });
    });
      

    it("should allow a raffle with mixed entries at price 0 and with prices > 0", async function () {
      let resultCreation = await competitionETHAsPrize.createRaffle(0, 20, 1, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: web3.utils.toWei('0.005', 'ether') }], 0, [], ONLY_DIRECTLY, { from: operator });
      let idRaffle = resultCreation.logs[0].args.raffleId;
  
      await competitionETHAsPrize.stakeETH(idRaffle, { from: operator, value: 1 });
  
      // Player 2 and player 4 get 1 entry for free
      await competitionETHAsPrize.buyEntry(idRaffle, 1, requiredNFT1.address, [],{ from: player2, value: web3.utils.toWei('0', 'ether') });
      await competitionETHAsPrize.buyEntry(idRaffle, 1, requiredNFT1.address, [],{ from: player4, value: web3.utils.toWei('0', 'ether') });
      // player 3 and player 5 get free entries via the operator
      await competitionETHAsPrize.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
      // player 1 buys 10 entries for 0.005
      await competitionETHAsPrize.buyEntry(idRaffle, 2, requiredNFT1.address, [],{ from: player1, value: web3.utils.toWei('0.005', 'ether') });
  
      // The winner will be player2
      result = await competitionETHAsPrize.mockSetWinner(idRaffle, [0], { from: operator });
      let gasUsed = result.receipt.gasUsed;
      //    console.log("Gas used setting winner: " + gasUsed);
  
      let entriesData = await competitionETHAsPrize.getEntriesBought(idRaffle);
      //console.log(JSON.stringify(entriesData));
  
      let raffle = await competitionETHAsPrize.raffles(idRaffle);
      assert.equal(raffle.winner, player2);
    });
  
    it("should fail if player wants to buy using an older priceId", async function () {
      // the id used for the prices are 11 and 12
      let resultCreation = await competitionETHAsPrize.createRaffle(0, 21, 1, 0, [{ id: 10, numEntries: 1, price: 0 }, { id: 11, numEntries: 10, price: web3.utils.toWei('0.005', 'ether') }], 0, [], ONLY_DIRECTLY, { from: operator });
      idRaffle = resultCreation.logs[0].args.raffleId;
  
      await competitionETHAsPrize.stakeETH(idRaffle, { from: operator, value: 1 });
  
      // Player 2 and player 4 get 1 entry for free
      await competitionETHAsPrize.buyEntry(idRaffle, 10, requiredNFT1.address, [],{ from: player2, value: web3.utils.toWei('0', 'ether') });
      await competitionETHAsPrize.buyEntry(idRaffle, 10, requiredNFT1.address, [],{ from: player4, value: web3.utils.toWei('0', 'ether') });
      // player 3 and player 5 get free entries via the operator
      await competitionETHAsPrize.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
      // player 1 buys 10 entries for 0.005
      await competitionETHAsPrize.buyEntry(idRaffle, 11, requiredNFT1.address, [],{ from: player1, value: web3.utils.toWei('0.005', 'ether') });
      // Fails when using and id on the price struct that did not belong to this raffle
      await expectRevertCustomError(competitionETHAsPrize, 
        competitionETHAsPrize.buyEntry(idRaffle, 1, requiredNFT1.address, [],{ from: player1, value: 700 }), "EntryNotAllowed", ["Id not in raffleId"]);
    });
 
    it("should fail if player wants get two times a free entry", async function () {
      // the id used for the prices are 11 and 12
      let resultCreation = await competitionETHAsPrize.createRaffle(0, 21, 1, 0, [{ id: 10, numEntries: 1, price: 0 }, { id: 11, numEntries: 10, price: web3.utils.toWei('0.005', 'ether') }], 0, [], ONLY_DIRECTLY, { from: operator });
      idRaffle = resultCreation.logs[0].args.raffleId;
  
      await competitionETHAsPrize.stakeETH(idRaffle, { from: operator, value: 1 });
  
      // Player 2 and player 4 get 1 entry for free
      await competitionETHAsPrize.buyEntry(idRaffle, 10, requiredNFT1.address, [],{ from: player2, value: web3.utils.toWei('0', 'ether') });
      await competitionETHAsPrize.buyEntry(idRaffle, 10, requiredNFT1.address, [],{ from: player4, value: web3.utils.toWei('0', 'ether') });
      // player 3 and player 5 get free entries via the operator
      await competitionETHAsPrize.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
      // player 1 buys 10 entries for 0.005
      await competitionETHAsPrize.buyEntry(idRaffle, 11, requiredNFT1.address, [],{ from: player1, value: web3.utils.toWei('0.005', 'ether') });
      // Fails when using and id on the price struct that did not belong to this raffle
    // await expectRevertCustomError(competitionETHAsPrize, 
       await competitionETHAsPrize.buyEntry(idRaffle, 10, requiredNFT1.address, [],{ from: player1, value: 0 })
        //, "EntryNotAllowed", ["Id not in raffleId"]);
    });
 

});
