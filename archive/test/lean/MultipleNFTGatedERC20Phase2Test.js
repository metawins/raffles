const Manager = artifacts.require("MockMultipleNFTGatedERC20Phase2");
const RequiredNFT1 = artifacts.require("Basic721");
const RequiredNFT2 = artifacts.require("Basic721");
const RequiredNFT3 = artifacts.require("Basic721");
const BasicERC20 = artifacts.require("BasicERC20.sol");

const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
  time,
  balance
} = require('@openzeppelin/test-helpers');

const { expectRevertCustomError } = require("custom-error-test-helper");

contract("Raffle tests ERC20 prize phase 2 (2 nft-gating different collections)", accounts => {

  var owner = accounts[0];
  var player1 = accounts[1];
  var player2 = accounts[2];
  var player3 = accounts[3];
  var player4 = accounts[4];
  var player5 = accounts[5];
  var player6 = accounts[6];
  var player7 = accounts[7];
  var seller = accounts[8];
  var operator = accounts[9]
  var token;
  var manager;
  var requiredNFT1;
  var requiredNFT2;
  var requiredNFT3;
  var zeroAddress = '0x0000000000000000000000000000000000000000';
  const ONLY_DIRECTLY = 0;
  const ONLY_EXTERNAL_CONTRACT = 1;
  const MIXED = 2;

  before(async function () {
    manager = await Manager.deployed();
    basicERC20 = await BasicERC20.deployed();
  })

  it("Should be deployed", async () => {
    assert.notEqual(manager, null);
  });

  it("Should add operator role directly", async function () {
    const operatorHash = web3.utils.soliditySha3('OPERATOR');
    await manager.grantRole(operatorHash, operator, { from: owner });
  });

  it("The contract balance should be cero ", async function () {
    let balance = await web3.eth.getBalance(manager.address);
    //  console.log("Balance contract = " + web3.utils.fromWei(balance, 'ether'));
    assert.equal(balance, 0);
  });

  it("should allow to buy only those who stakes certain nft ", async function () {
    requiredNFT1 = await RequiredNFT1.new(); // not deployed as that is singleton. Using new() instead
    requiredNFT2 = await RequiredNFT2.new();

    let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'),
      11, basicERC20.address, 100, web3.utils.toWei('1', 'ether'),
      [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') },
      { id: 2, numEntries: 10, price: web3.utils.toWei('2', 'ether') }], 2500,
      [requiredNFT1.address, requiredNFT2.address], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;

    await basicERC20.approve(manager.address, 100, { from: owner });
    let result = await manager.stake(idRaffle, { from: owner });
    expectEvent(result, 'RaffleStarted', { 'raffleId': '0', 'seller': owner });

    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;

    result = await manager.buyEntry(idRaffle, 2, [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 }, { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }], { from: player1, value: web3.utils.toWei('2', 'ether') });

    console.log("Gas used buying 10 entries with 2 nft restriction: " + result.receipt.gasUsed);

    // Player 1 buys another entry using other nfts he has.
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("token1 " + tokenId1 + " token2 " + tokenId2)

    let r1 = await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 1 entry with nft restriction: " + r1.receipt.gasUsed);

    await expectRevertCustomError(manager,
      manager.buyEntry(idRaffle, 1,
        [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
        { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }],
        { from: player1, value: web3.utils.toWei('1', 'ether') }),
      "EntryNotAllowed", ["Wallet already used"]);
    //   console.log(JSON.stringify(r1));

    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    let tokenId4 = resultMint.logs[0].args.tokenId;
    console.log("token3 " + tokenId3 + " token4 " + tokenId4)
    let currentOwner = await requiredNFT2.ownerOf(tokenId4);
    assert.equal(currentOwner, player2);

    let tokenId5 = 2;
    r1 = await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': 2 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId5 }],
      { from: player2, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 1 entry with nft restriction: " + r1.receipt.gasUsed);

    // should fail if the buyer is not the owner of the tokenId
    await expectRevert(manager.buyEntry(idRaffle, 2, [{ 'collection': requiredNFT1.address, 'tokenId': 0 },
    { 'collection': requiredNFT2.address, 'tokenId': 0 }], { from: player4, value: web3.utils.toWei('2', 'ether') }), "Not the owner of tokenId");

    // should fail if the tokenId was already used by another user (player2 receives tokenID=0 from player1)
    await requiredNFT1.transferFrom(player1, player2, 0, { from: player1 });
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    await expectRevert(manager.buyEntry(idRaffle, 2,
      [{ 'collection': requiredNFT1.address, 'tokenId': 0 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId1 }],
      { from: player2, value: web3.utils.toWei('2', 'ether') }), "tokenId used");

    // the current # of entries for player 1 is 20 (Wallets cap)
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("token1 " + tokenId1 + " token2 " + tokenId2)
    await expectRevertCustomError(manager,
      manager.buyEntry(idRaffle, 2,
        [{ 'collection': requiredNFT1.address, 'tokenId': 3 },
        { 'collection': requiredNFT2.address, 'tokenId': 4 }],
        { from: player1, value: web3.utils.toWei('2', 'ether') })
      , "EntryNotAllowed", ["Wallet already used"]);

    await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
    // should fail if the player did not have an nft of each collection
    resultMint = await requiredNFT1.mint(player3, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player3, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevert(manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT1.address, 'tokenId': tokenId2 }],
      { from: player3, value: web3.utils.toWei('1', 'ether') }), "Collection already used");

  });

  it("should close the raffle", async function () {
    const tracker = await balance.tracker(owner);

    let currentBalance1 = await tracker.get();
    console.log("current balance1 " + currentBalance1);
    await time.increase(35);
    await manager.mockSetWinner(0, 2, { from: operator });

    let result = await manager.raffles(0);
    console.log(JSON.stringify(result));
    assert.equal(result.winner, player1);

    let entries = await manager.getEntriesBought(0);
    console.log(JSON.stringify(entries));

    // check the winner received the prize
    let winnerBalance = await basicERC20.balanceOf(player1);
    console.log("Balance of winner = " + winnerBalance);
    assert.equal(winnerBalance, 100);
    // check the seller received the money (75% of the raffle pool)
    let currentDelta = await tracker.delta()
    //   console.log("delta " + currentDelta);
    assert.equal(currentDelta, web3.utils.toWei('3', 'ether')); // 75% of 3000 = 2250
  });

  it("should fail creating a raffle a non-operator", async function () {
    await expectRevert.unspecified(manager.createRaffle(1, 1, basicERC20.address, 100, 2000, [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, [], ONLY_DIRECTLY, { from: player2 }));
  });

  it("should fail staking asset in a not created raffle", async function () {
    await basicERC20.approve(manager.address, 100, { from: owner });
    await expectRevert.unspecified(manager.stake(1, { from: seller }));
  });

  it("should fail staking asset in a raffle not in the right status", async function () {
    await expectRevert(manager.stake(0, { from: seller }), "Raffle not CREATED");
  });

  it("should create a new raffle", async function () {
    var result = await manager.createRaffle(1000, 100, basicERC20.address, 1, 2000000000,
      [{ id: 1, numEntries: 1, price: 1000000000 },
      { id: 2, numEntries: 5, price: 8000000000 },
      { id: 3, numEntries: 25, price: 70000000000 }], 2500, [], ONLY_DIRECTLY, { from: operator });
    expectEvent(result, 'RaffleCreated', { 'raffleId': '1' });
  });

  it("should stake asset in a raffle", async function () {
    let result = await manager.stake(1, { from: owner });
    expectEvent(result, 'RaffleStarted', { 'raffleId': '1', 'seller': owner });
  });

  it("should create a new raffle with 3 collections in whitelist", async function () {
    requiredNFT1 = await RequiredNFT1.new(); // not deployed as that is singleton. Using new() instead
    requiredNFT2 = await RequiredNFT2.new();
    requiredNFT3 = await RequiredNFT3.new();

    let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'),
      110, basicERC20.address, 2, web3.utils.toWei('1', 'ether'),
      [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') },
      { id: 2, numEntries: 10, price: web3.utils.toWei('2', 'ether') }], 2500,
      [requiredNFT1.address, requiredNFT2.address, requiredNFT3.address], ONLY_DIRECTLY, { from: operator });
    expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': '2' });
  });

  it("should stake asset in a raffle", async function () {
    let result = await manager.stake(2, { from: owner });
    expectEvent(result, 'RaffleStarted', { 'raffleId': '2', 'seller': owner });
  });

  it("should buy an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT3.mint(player1, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    console.log("tokenId3 = " + tokenId3)
    let r1 = await manager.buyEntry(2, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 },
      { 'collection': requiredNFT3.address, 'tokenId': tokenId3 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') });

    console.log("Gas used buying 1 entry with 3 nft restriction: " + r1.receipt.gasUsed);
  });

  it("should fail buying an entry using 3 nfts but 2 from same collection", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevert(manager.buyEntry(2, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 },
      { 'collection': requiredNFT2.address, 'tokenId': 0 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') })
      , "Collection already used");
  });

  it("should fail buying an entry using 3 nfts but 2 from same collection (1 and 2)", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    await expectRevert(manager.buyEntry(2, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT1.address, 'tokenId': tokenId3 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') })
      , "Collection already used");

  });

  it("should buy again an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT3.mint(player1, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    console.log("tokenId3 = " + tokenId3)
    let r1 = await manager.buyEntry(2, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 },
      { 'collection': requiredNFT3.address, 'tokenId': tokenId3 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') });

    console.log("Gas used buying 1 entry with 3 nft restriction: " + r1.receipt.gasUsed);
  });

  it("should buy again an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT3.mint(player1, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    console.log("tokenId3 = " + tokenId3)
    let r1 = await manager.buyEntry(2, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 },
      { 'collection': requiredNFT3.address, 'tokenId': tokenId3 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') });

    console.log("Gas used buying 1 entry with 3 nft restriction: " + r1.receipt.gasUsed);
  });

  it("should fail  buying several entries for below price", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    //   await expectRevert(manager.buyEntry(1, 2, zeroAddress, [0], { from: player1, value: 700 }), "msg.value not the price");
    await expectRevertCustomError(manager,
      manager.buyEntry(2, 2, [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }], { from: player1, value: 700 }),
      "EntryNotAllowed", ["msg.value not the price"]);
  });

  it("should fail buying a wrong amount of entries", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevert.unspecified(manager.buyEntry(1, 6,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }], { from: player1, value: 800 }));
  });

  it("should allow buy several entries", async function () {
    const tracker = await balance.tracker(manager.address);
    await tracker.get();
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    console.log("Token1 " + tokenId1)
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("Token2 " + tokenId2)
    await expectRevertCustomError(manager,
      manager.buyEntry(2, 2,
        [{ 'collection': requiredNFT1.address, 'tokenId': 8 },
        { 'collection': requiredNFT2.address, 'tokenId': 7 }], { from: player2, value: 8000000000 }),
      "EntryNotAllowed", ["msg.value not the price"]);
    await manager.buyEntry(2, 2,
      [{ 'collection': requiredNFT1.address, 'tokenId': 8 },
      { 'collection': requiredNFT2.address, 'tokenId': 7 }], { from: player2, value: web3.utils.toWei('2', 'ether') });

    let currentDelta = await tracker.delta()

    // check the contract received the money
    assert.equal(currentDelta, web3.utils.toWei('2', 'ether'));
  });

  it("should change the destination address", async function () {
    await manager.setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3', { from: owner });
  });

  it("should fail changing the destination address by a non owner", async function () {
    await expectRevert.unspecified(manager.setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3', { from: operator }));
  });

  it("should fail when calling setwinner without having reached desired funds", async function () {
    await expectRevert(manager.mockSetWinner(1, 1, { from: operator }), "Not enough funds raised");
  });

  it("should add free entries that won the raffle", async function () {
    let resultCreation = await manager.createRaffle(web3.utils.toWei('1', 'ether'), 1,
      basicERC20.address, 2, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;
    await manager.stake(idRaffle, { from: owner });

    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    console.log("Token1 " + tokenId1)
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("Token2 " + tokenId2)
    await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': 9 },
      { 'collection': requiredNFT2.address, 'tokenId': 9 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') });
    await manager.giveBatchEntriesForFree(idRaffle, [player3, player4, player5], { from: operator });

    let destinationWallet = await manager.destinationWallet();
    const tracker = await balance.tracker(destinationWallet);
    const currentBalance = await tracker.get();
    //  console.log("Detination wallet balance = " + currentBalance);

    const trackerSeller = await balance.tracker(owner);
    const b1 = await trackerSeller.get();

    // the winner is from a free entry
    await manager.mockSetWinner(idRaffle, 2, { from: operator });

    const tracker2 = await balance.tracker(destinationWallet);
    const currentBalance2 = await tracker2.get();
    // console.log("Destination wallet balance = " + currentBalance2);

    let currentDelta = await tracker.delta()
    //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    assert.equal(currentDelta.toString(), '250000000000000000'); // 25% of raised amount
    let sellerDelta = await trackerSeller.delta();
    const b2 = await trackerSeller.get();
    //check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    assert.equal(sellerDelta.toString(), '750000000000000000'); // 75% of raised amount - gas fees
    // check the new owner of the tokens is the player (from the free entry)

    let winnerBalance = await basicERC20.balanceOf(player4);
    console.log("Balance of winner = " + winnerBalance);
    assert.equal(winnerBalance, 2);
  });

  it("should transfer funds when the winner is from a multiple entry", async function () {
    let resultCreation = await manager.createRaffle(web3.utils.toWei('2', 'ether'), 100,
      basicERC20.address, 2, web3.utils.toWei('1', 'ether'),
      [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') },
      { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [requiredNFT1.address, requiredNFT2.address], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;
    await manager.stake(idRaffle, { from: owner });

    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    console.log("Token1 " + tokenId1)
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("Token2 " + tokenId2)
    await manager.buyEntry(idRaffle, 2,
      [{ 'collection': requiredNFT1.address, 'tokenId': 10 },
      { 'collection': requiredNFT2.address, 'tokenId': 9 }], { from: player1, value: web3.utils.toWei('2', 'ether') });
    await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
    let destinationWallet = await manager.destinationWallet();
    const tracker = await balance.tracker(destinationWallet);
    const currentBalance = await tracker.get();
    const trackerSeller = await balance.tracker(owner);
    const b1 = await trackerSeller.get();

    // the winner is from a free entry
    result = await manager.mockSetWinner(idRaffle, 2, { from: operator });

    // check the event is emitted
    expectEvent(result, 'RaffleEnded', { 'raffleId': idRaffle });
    // check the status is "ended"
    raffle = await manager.getRafflesEntryInfo(idRaffle);
    assert.equal(raffle.status, 5);
    assert.equal(raffle.entriesLength, 7);

    let currentDelta = await tracker.delta()
    //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    assert.equal(currentDelta.toString(), '500000000000000000'); // 25% of raised amount
    let sellerDelta = await trackerSeller.delta();
    const b2 = await trackerSeller.get();
    //check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    assert.equal(sellerDelta.toString(), '1500000000000000000'); // 75% of raised amount - gas fees

  });

  it("should fail adding free entries if the status is not accepted", async function () {
    // check adding free entries to a closed raffle
    await expectRevert(manager.giveBatchEntriesForFree(4, [player3, player5], { from: operator }), "Raffle is not in accepted");
    // check adding free entries to a raffle that has not been accepted yet
    let result = await manager.createRaffle(web3.utils.toWei('3', 'ether'), 1,
      basicERC20.address, 2, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
    expectEvent(result, 'RaffleCreated', { 'raffleId': '5' });

    await expectRevert(manager.giveBatchEntriesForFree(5, [player3, player5], { from: operator }), "Raffle is not in accepted");

  });

  it("should fail canceling a raffle already closed", async function () {
    await expectRevert(manager.cancelRaffle(4, { from: operator }), "Wrong status");
  });

  it("should fail canceling a raffle a non-operator", async function () {
    await expectRevert.unspecified(manager.cancelRaffle(5, { from: player1 }));
  });

  it("should cancel a raffle", async function () {
    let result = await manager.createRaffle(web3.utils.toWei('5', 'ether'), 1,
      basicERC20.address, 7, web3.utils.toWei('2', 'ether'),
      [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500,
      [requiredNFT1.address, requiredNFT2.address], ONLY_DIRECTLY, { from: operator });
    expectEvent(result, 'RaffleCreated', { 'raffleId': '6' });
    let idRaffle = result.logs[0].args.raffleId;
    await manager.stake(idRaffle, { from: owner });

    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    console.log("Token1 " + tokenId1)
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("Token2 " + tokenId2)
    await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': 11 },
      { 'collection': requiredNFT2.address, 'tokenId': 10 }],
      { from: player2, value: web3.utils.toWei('1', 'ether') });

    let destinationWallet = await manager.destinationWallet();
    const tracker = await balance.tracker(destinationWallet);
    const currentBalance = await tracker.get();

    let sellerBalanceBefore = await basicERC20.balanceOf(owner);
    await manager.cancelRaffle(idRaffle, { from: operator });

    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    assert.equal(raffle.status, 6);

    // check the seller has the tokens back
    let sellerBalanceAfter = await basicERC20.balanceOf(owner);
    let sellerBalance = BigInt(sellerBalanceAfter) - BigInt(sellerBalanceBefore)
    assert.equal(sellerBalance, '7')
    // check the raised funds has been transferred to the destination wallet
    await manager.transferRemainingFunds(idRaffle, { from: operator });
    let currentDelta = await tracker.delta()
    assert.equal(currentDelta.toString(), raffle.amountRaised); // 25% of raised amount
  });

  it("should fail canceling a raffle already asked to cancel", async function () {
    await expectRevert.unspecified(manager.cancelRaffle(6, { from: operator }));
  });


  it("should allow a free raffle", async function () {

    let resultCreation = await manager.createRaffle(0, 1, basicERC20.address, 1, 0, [{ id: 1, numEntries: 1, price: 0 }], 0,
      [requiredNFT1.address, requiredNFT2.address], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;

    await manager.stake(idRaffle, { from: owner });

    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    console.log("Token1 " + tokenId1)
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("Token2 " + tokenId2)
    await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': 12 },
      { 'collection': requiredNFT2.address, 'tokenId': 11 }], { from: player2, value: web3.utils.toWei('0', 'ether') });

    resultMint = await requiredNFT1.mint(player4, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    console.log("Token1 " + tokenId1)
    resultMint = await requiredNFT2.mint(player4, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("Token2 " + tokenId2)
    await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': 13 },
      { 'collection': requiredNFT2.address, 'tokenId': 12 }],
      { from: player4, value: web3.utils.toWei('0', 'ether') });

    await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });

    // the first call did generate a winner not in MW. So now lets do another call to setWinner
    result = await manager.mockSetWinner(idRaffle, 0, { from: operator });
    let gasUsed = result.receipt.gasUsed;
    console.log("Gas used setting winner: " + gasUsed);

    let entriesData = await manager.getEntriesBought(idRaffle);
    console.log(JSON.stringify(entriesData));

    let winner = await manager.getWinnerAddressFromRandom(idRaffle, 1);
    assert.equal(player2, winner);
  });

  it("should ask a raffle for cancellation step 1", async function () {

    let resultCreation = await manager.createRaffle(0, 10, basicERC20.address, 1, 0, [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;
    console.log("RaffleID = " + idRaffle)

    await manager.stake(idRaffle, { from: owner });

    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    console.log("Token1 " + tokenId1)
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("Token2 " + tokenId2)
    await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': 14 },
      { 'collection': requiredNFT2.address, 'tokenId': 13 }], { from: player2, value: web3.utils.toWei('1', 'ether') });
    resultMint = await requiredNFT1.mint(player4, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    console.log("Token1 " + tokenId1)
    resultMint = await requiredNFT2.mint(player4, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("Token2 " + tokenId2)
    await manager.buyEntry(idRaffle, 2,
      [{ 'collection': requiredNFT1.address, 'tokenId': 15 },
      { 'collection': requiredNFT2.address, 'tokenId': 14 }], { from: player4, value: web3.utils.toWei('2', 'ether') });
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    console.log("Token1 " + tokenId1)
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("Token2 " + tokenId2)
    await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': 16 },
      { 'collection': requiredNFT2.address, 'tokenId': 15 }], { from: player1, value: web3.utils.toWei('1', 'ether') });

    await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });

    let raffle = await manager.getRafflesEntryInfo(idRaffle);

    assert.equal(raffle.amountRaised, web3.utils.toWei('4', 'ether'))
  });

  it("should fail transferring remaining funds if the raffle is not in cancellation_requested status", async function () {
    await expectRevert(manager.transferRemainingFunds(8, { from: operator }), "Wrong status");
  });

  it("should ask a raffle for cancellation step 2", async function () {
    await manager.cancelRaffle(8, { from: operator })
    let raffle = await manager.getRafflesEntryInfo(8);

    assert.equal(raffle.status, 6)
  });

  it("should fail transferring remaining funds a non operator", async function () {
    await expectRevert.unspecified(manager.transferRemainingFunds(8, { from: player1 }), "claim time expired");
  });

  it("should transfer remaining funds", async function () {
    let result = await manager.transferRemainingFunds(8, { from: operator });
    expectEvent(result, 'RemainingFundsTransferred', { 'raffleId': '8', 'amountInWeis': web3.utils.toWei('4', 'ether') });
  });

  it("should fail transferring remaining funds two times", async function () {
    //    await expectRevert(manager.transferRemainingFunds(13, { from: operator }), "Wrong status");
    let result = await manager.transferRemainingFunds(8, { from: operator });

    expectEvent(result, 'RemainingFundsTransferred', { 'raffleId': '8', 'amountInWeis': web3.utils.toWei('0', 'ether') });

  });

  it("should manage blacklisted entries", async function () {

    let resultCreation = await manager.createRaffle(
      web3.utils.toWei('0.03', 'ether'), 1000, basicERC20.address, 1,
      web3.utils.toWei('0.01', 'ether'),
      [{ id: 1, numEntries: 1, price: web3.utils.toWei('0.01', 'ether') },
      { id: 2, numEntries: 10, price: web3.utils.toWei('0.02', 'ether') },
      { id: 3, numEntries: 100, price: web3.utils.toWei('0.03', 'ether') }], 2500, [requiredNFT1.address, requiredNFT2.address], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;

    await manager.stake(idRaffle, { from: owner });

    let destinationWallet = await manager.destinationWallet();
    const tracker = await balance.tracker(destinationWallet);
    const currentBalance = await tracker.get();

    // player1 allowed
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    console.log("token1 " + tokenId1)
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("Token2 " + tokenId2)
    let r1 = await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': 17 },
      { 'collection': requiredNFT2.address, 'tokenId': 16 }], { from: player1, value: web3.utils.toWei('0.01', 'ether') });
    console.log("Gas used buying 1 entry no restriction: " + r1.receipt.gasUsed);

    //  player3 buys 100 entries
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    console.log("token1 " + tokenId1)
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("Token2 " + tokenId2)
    let r2 = await manager.buyEntry(idRaffle, 3,
      [{ 'collection': requiredNFT1.address, 'tokenId': 18 },
      { 'collection': requiredNFT2.address, 'tokenId': 17 }], { from: player2, value: web3.utils.toWei('0.03', 'ether') });
    console.log("Gas used buying 100 entries no restriction: " + r2.receipt.gasUsed);

    resultMint = await requiredNFT1.mint(player3, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player3, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    let r3 = await manager.buyEntry(idRaffle, 3,
      [{ 'collection': requiredNFT1.address, 'tokenId': 19 },
      { 'collection': requiredNFT2.address, 'tokenId': 18 }], { from: player3, value: web3.utils.toWei('0.03', 'ether') });
    console.log("Gas used buying 100 entries no restriction: " + r3.receipt.gasUsed);

    let rCancelEntry = await manager.cancelEntry(idRaffle, [2], player3, { from: operator });
    console.log("Gas used cancelEntry: " + rCancelEntry.receipt.gasUsed);

    // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2

    let rSetWinner = await manager.mockSetWinner(idRaffle, 200, { from: operator })
    const tracker2 = await balance.tracker(destinationWallet);
    const currentBalance2 = await tracker2.get();
    let currentDelta = await tracker.delta()

    let entriesData = await manager.getEntriesBought(idRaffle);
    //   console.log(JSON.stringify(entriesData));

    let raffle = await manager.raffles(idRaffle);
    //   console.log(JSON.stringify(raffle));
    assert.equal(raffle.winner, player2);

    //    console.log("Gas used new setwinner: " + rSetWinner.receipt.gasUsed);   
  });

  it("should manage players as time passes", async function () {
    let resultCreation = await manager.createRaffle(web3.utils.toWei('0.03', 'ether'),
      1000, basicERC20.address, 1, web3.utils.toWei('0.01', 'ether'),
      [{ id: 1, numEntries: 1, price: web3.utils.toWei('0.01', 'ether') },
      { id: 2, numEntries: 10, price: web3.utils.toWei('0.02', 'ether') },
      { id: 3, numEntries: 100, price: web3.utils.toWei('0.03', 'ether') }], 2500,
      [requiredNFT1.address, requiredNFT2.address], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;
    await manager.stake(idRaffle, { from: owner });
    // player1 allowed
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    let r1 = await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': 20 },
      { 'collection': requiredNFT2.address, 'tokenId': 19 }], { from: player1, value: web3.utils.toWei('0.01', 'ether') });
    //  player3 buys 100 entries
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    let r2 = await manager.buyEntry(idRaffle, 3,
      [{ 'collection': requiredNFT1.address, 'tokenId': 21 },
      { 'collection': requiredNFT2.address, 'tokenId': 20 }], { from: player2, value: web3.utils.toWei('0.03', 'ether') });
    // player1 can buy again
    resultMint = await requiredNFT1.mint(player7, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player7, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    let r3 = await manager.buyEntry(idRaffle, 3,
      [{ 'collection': requiredNFT1.address, 'tokenId': 22 },
      { 'collection': requiredNFT2.address, 'tokenId': 21 }], { from: player7, value: web3.utils.toWei('0.03', 'ether') });
    // player 4 buys 100 entries
    resultMint = await requiredNFT1.mint(player4, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player4, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await manager.buyEntry(idRaffle, 3,
      [{ 'collection': requiredNFT1.address, 'tokenId': 23 },
      { 'collection': requiredNFT2.address, 'tokenId': 22 }], { from: player4, value: web3.utils.toWei('0.03', 'ether') });
    // player 5 buys 100 entries
    resultMint = await requiredNFT1.mint(player5, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player5, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await manager.buyEntry(idRaffle, 3,
      [{ 'collection': requiredNFT1.address, 'tokenId': 24 },
      { 'collection': requiredNFT2.address, 'tokenId': 23 }], { from: player5, value: web3.utils.toWei('0.03', 'ether') });

    await expectRevert(manager.cancelEntry(idRaffle, [2], player3, { from: operator }), "Entry did not belong to player");
    // test cancelEntry batch
    let result = await manager.cancelEntry(idRaffle, [0], player1, { from: operator });
    expectEvent(result, 'EntryCancelled', { 'raffleId': idRaffle, 'amountOfEntriesCanceled': '1', 'player': player1 });

    let entriesData = await manager.getEntriesBought(idRaffle);
    assert.equal(entriesData[1].player, zeroAddress);
    //  assert.equal(entriesData[3].player, zeroAddress);
    // 31 days passes
    await time.increase(time.duration.days(31));

    // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2
    let rSetWinner = await manager.mockSetWinner(idRaffle, 0, { from: operator })

    entriesData = await manager.getEntriesBought(idRaffle);
    // console.log(JSON.stringify(entriesData));

    let raffle = await manager.raffles(idRaffle);
    assert.equal(raffle.winner, player5);

    console.log("Gas used new setwinner: " + rSetWinner.receipt.gasUsed);
  });

  it("should create a new raffle", async function () {
    var result = await manager.createRaffle(1000, 100, basicERC20.address, 1, 2000000000,
      [{ id: 1, numEntries: 1, price: 1000000000 },
      { id: 2, numEntries: 5, price: 8000000000 }],
      2500, [requiredNFT1.address, requiredNFT2.address], ONLY_DIRECTLY, { from: operator });
    expectEvent(result, 'RaffleCreated', { 'raffleId': '11' });
  });
  it("should stake asset in a raffle", async function () {
    let result = await manager.stake(11, { from: owner });
    expectEvent(result, 'RaffleStarted', { 'raffleId': '11', 'seller': owner });
  });
  it("should fail buying with less nfts than required", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    await expectRevert(manager.buyEntry(11, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 }],
      { from: player1, value: 1000000000 }), "Not enough tokens staked");
  });


  it("should fail giving free entries two times to the same player", async function () {
    let resultCreation = await manager.createRaffle(web3.utils.toWei('0.03', 'ether'),
      1000, basicERC20.address, 1, web3.utils.toWei('0.01', 'ether'),
      [{ id: 1, numEntries: 1, price:0 },
      { id: 2, numEntries: 10, price: web3.utils.toWei('0.02', 'ether') }], 2500,
      [requiredNFT1.address, requiredNFT2.address], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;
    await manager.stake(idRaffle, { from: owner });
    // player1 free entry
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    let r1 = await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': 20 },
      { 'collection': requiredNFT2.address, 'tokenId': 19 }], { from: player1, value: 0});
    // player 2 free entry
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    let r2 = await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': 21 },
      { 'collection': requiredNFT2.address, 'tokenId': 20 }], { from: player2, value: 0});
    // player1 can not get free entry again
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevertCustomError(manager, manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }], { from: player1, value: 0 })
      ,"EntryNotAllowed", ["Player already got free entry"]);
    // player 1 can buy paid entries
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await manager.buyEntry(idRaffle, 2,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }], { from: player1, value: web3.utils.toWei('0.02', 'ether') });
   
  });
});