const Manager = artifacts.require("MockMultipleNFTGatedETHPhase2");
const RequiredNFT1 = artifacts.require("Basic721");
const RequiredNFT2 = artifacts.require("Basic721");
const RequiredNFT3 = artifacts.require("Basic721");

const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
  time,
  balance
} = require('@openzeppelin/test-helpers');

const { expectRevertCustomError } = require("custom-error-test-helper");

contract("Raffle tests ETH Prize phase 2 (2 nft-gating different collections)", accounts => {

  var owner = accounts[0];
  var player1 = accounts[1];
  var player2 = accounts[2];
  var player3 = accounts[3];
  var player4 = accounts[4];
  var player5 = accounts[5];
  var player6 = accounts[6];
  var player7 = accounts[7];
  var seller = accounts[8];
  var operator = accounts[9]
  var token;
  var manager;
  var requiredNFT1;
  var requiredNFT2;
  var requiredNFT3;
  var idRaffle;
  var zeroAddress = '0x0000000000000000000000000000000000000000';
  const ONLY_DIRECTLY = 0;
  const ONLY_EXTERNAL_CONTRACT = 1;
  const MIXED = 2;

  before(async function () {
    manager = await Manager.deployed();
  })

  it("Should be deployed", async () => {
    assert.notEqual(manager, null);
  });

  it("Should add operator role directly", async function () {
    const operatorHash = web3.utils.soliditySha3('OPERATOR');
    await manager.grantRole(operatorHash, operator, { from: owner });
  });

  it("The contract balance should be cero ", async function () {
    let balance = await web3.eth.getBalance(manager.address);
    //  console.log("Balance contract = " + web3.utils.fromWei(balance, 'ether'));
    assert.equal(balance, 0);
  });

  it("should allow to buy only those who stakes certain nft ", async function () {
    requiredNFT1 = await RequiredNFT1.new(); // not deployed as that is singleton. Using new() instead
    requiredNFT2 = await RequiredNFT2.new();

    let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'),
      11, web3.utils.toWei('2', 'ether'), web3.utils.toWei('1', 'ether'),
      [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') },
      { id: 2, numEntries: 10, price: web3.utils.toWei('2', 'ether') }], 2500,
      [requiredNFT1.address, requiredNFT2.address], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;

    await manager.stakeETH(idRaffle, { from: seller, value: web3.utils.toWei('2', 'ether') });

    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;

    result = await manager.buyEntry(idRaffle, 2, [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 }, { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }], { from: player1, value: web3.utils.toWei('2', 'ether') });

    console.log("Gas used buying 10 entries with 2 nft restriction: " + result.receipt.gasUsed);

    // Player 1 buys another entry using other nfts he has.
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("token1 " + tokenId1 + " token2 " + tokenId2)

    let r1 = await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 1 entry with nft restriction: " + r1.receipt.gasUsed);

    await expectRevertCustomError(manager,
      manager.buyEntry(idRaffle, 1,
        [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
        { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }],
        { from: player1, value: web3.utils.toWei('1', 'ether') }),
      "EntryNotAllowed", ["Wallet already used"]);
    //   console.log(JSON.stringify(r1));

    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    let tokenId4 = resultMint.logs[0].args.tokenId;
    console.log("token3 " + tokenId3 + " token4 " + tokenId4)
    let currentOwner = await requiredNFT2.ownerOf(tokenId4);
    assert.equal(currentOwner, player2);

    let tokenId5 = 2;
    r1 = await manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': 2 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId5 }],
      { from: player2, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 1 entry with nft restriction: " + r1.receipt.gasUsed);

    // should fail if the buyer is not the owner of the tokenId
    await expectRevert(manager.buyEntry(idRaffle, 2, [{ 'collection': requiredNFT1.address, 'tokenId': 0 },
    { 'collection': requiredNFT2.address, 'tokenId': 0 }], { from: player4, value: web3.utils.toWei('2', 'ether') }), "Not the owner of tokenId");

    // should fail if the tokenId was already used by another user (player2 receives tokenID=0 from player1)
    await requiredNFT1.transferFrom(player1, player2, 0, { from: player1 });
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    await expectRevert(manager.buyEntry(idRaffle, 2,
      [{ 'collection': requiredNFT1.address, 'tokenId': 0 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId1 }],
      { from: player2, value: web3.utils.toWei('2', 'ether') }), "tokenId used");

    // the current # of entries for player 1 is 20
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("token1 " + tokenId1 + " token2 " + tokenId2)
    await expectRevertCustomError(manager,
      manager.buyEntry(idRaffle, 2,
        [{ 'collection': requiredNFT1.address, 'tokenId': 3 },
        { 'collection': requiredNFT2.address, 'tokenId': 4 }],
        { from: player1, value: web3.utils.toWei('2', 'ether') })
      , "EntryNotAllowed", ["Wallet already used"]);

    await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
    // should fail if the player did not have an nft of each collection
    resultMint = await requiredNFT1.mint(player3, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player3, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevert(manager.buyEntry(idRaffle, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT1.address, 'tokenId': tokenId2 }],
      { from: player3, value: web3.utils.toWei('1', 'ether') }), "Collection already used");

  });

  it("should close the raffle", async function () {
    const tracker = await balance.tracker(seller);
    const winnerTracker = await balance.tracker(player1);
    winnerTracker.get();
    let currentBalance1 = await tracker.get();
    console.log("current balance1 " + currentBalance1);
    await time.increase(35);
    let result = await manager.mockSetWinner(0, 2, { from: operator });

    let entries = await manager.getEntriesBought(0);
    console.log(JSON.stringify(entries));

    // check the winner received the prize
    let winnerDelta = await winnerTracker.delta()
    // console.log("delta " + winnerDelta);
    assert.equal(winnerDelta, web3.utils.toWei('2', 'ether'));

    // check the seller received the money (75% of the raffle pool)
    let currentDelta = await tracker.delta()
    //   console.log("delta " + currentDelta);
    assert.equal(currentDelta, web3.utils.toWei('3', 'ether')); // 75% of 3000 = 2250
  });

  it("should fail creating a raffle a non-operator", async function () {
    await expectRevert.unspecified(manager.createRaffle(1, 1, 0, 2000, [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, [], ONLY_DIRECTLY, { from: player2 }));
  });

  it("should fail staking asset in a not created raffle", async function () {
    //await token.approve(manager.address, 0, { from: seller });
    await expectRevert.unspecified(manager.stakeETH(1, { from: seller, value: 0 }));
  });

  it("should fail staking asset in a raffle not in the right status", async function () {
    await expectRevert(manager.stakeETH(0, { from: seller, value: 0 }), "Raffle not CREATED");
  });

  it("should create a new ruffle", async function () {
    var result = await manager.createRaffle(1000, 1, 1, 2000000000, [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 25, price: 70000000000 }], 2500, [], ONLY_DIRECTLY, { from: operator });
    expectEvent(result, 'RaffleCreated', { 'raffleId': '1' });
  });

  it("should stake asset in a raffle", async function () {
    let result = await manager.stakeETH(1, { from: seller, value: 1 });
    expectEvent(result, 'RaffleStarted', { 'raffleId': '1', 'seller': seller });

  });

  it("should create a new raffle with 3 collections in whitelist", async function () {
    requiredNFT1 = await RequiredNFT1.new(); // not deployed as that is singleton. Using new() instead
    requiredNFT2 = await RequiredNFT2.new();
    requiredNFT3 = await RequiredNFT3.new();


    let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'),
      110,
      2, web3.utils.toWei('1', 'ether'),
      [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, 
      { id: 2, numEntries: 10, price: web3.utils.toWei('2', 'ether') }], 2500,
      [requiredNFT1.address, requiredNFT2.address, requiredNFT3.address], ONLY_DIRECTLY, { from: operator });
    expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': '2' });
  });

  it("should stake asset in a raffle", async function () {
    let result = await manager.stakeETH(2, { from: seller, value: 2 });
    expectEvent(result, 'RaffleStarted', { 'raffleId': '2', 'seller': seller });
  });

  it("should buy an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT3.mint(player1, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    console.log("tokenId3 = " + tokenId3)
    let r1 = await manager.buyEntry(2, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 },
      { 'collection': requiredNFT3.address, 'tokenId': tokenId3 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') });

    console.log("Gas used buying 1 entry with 3 nft restriction: " + r1.receipt.gasUsed);
  });

  it("should fail buying an entry using 3 nfts but 2 from same collection", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevert(manager.buyEntry(2, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 },
      { 'collection': requiredNFT2.address, 'tokenId': 0 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') })
      , "Collection already used");
  });

  it("should fail buying an entry using 3 nfts but 2 from same collection (1 and 2)", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    await expectRevert(manager.buyEntry(2, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT1.address, 'tokenId': tokenId3 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') })
      , "Collection already used");

  });

  it("should buy again an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT3.mint(player1, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    console.log("tokenId3 = " + tokenId3)
    let r1 = await manager.buyEntry(2, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 },
      { 'collection': requiredNFT3.address, 'tokenId': tokenId3 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') });

    console.log("Gas used buying 1 entry with 3 nft restriction: " + r1.receipt.gasUsed);
  });

  it("should buy again an entry using 3 nfts", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT3.mint(player1, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    console.log("tokenId3 = " + tokenId3)
    let r1 = await manager.buyEntry(2, 1,
      [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 },
      { 'collection': requiredNFT3.address, 'tokenId': tokenId3 }],
      { from: player1, value: web3.utils.toWei('1', 'ether') });

    console.log("Gas used buying 1 entry with 3 nft restriction: " + r1.receipt.gasUsed);
  });

  it("should fail  buying several entries for below price", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    //   await expectRevert(manager.buyEntry(1, 2, zeroAddress, [0], { from: player1, value: 700 }), "msg.value not the price");
    await expectRevertCustomError(manager,
      manager.buyEntry(2, 2, [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
      { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }], { from: player1, value: 700 }),
      "EntryNotAllowed", ["msg.value not the price"]);
  });

  it("should fail buying a wrong amount of entries", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevert.unspecified(manager.buyEntry(1, 6, [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
    { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }], { from: player1, value: 800 }));
  });

  it("should allow buy several entries", async function () {
    const tracker = await balance.tracker(manager.address);
    await tracker.get();
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    console.log("Token1 " + tokenId1)
    resultMint = await requiredNFT2.mint(player2, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    console.log("Token2 " + tokenId2)
    await expectRevertCustomError(manager,
      manager.buyEntry(2, 2,
        [{ 'collection': requiredNFT1.address, 'tokenId': 8 },
        { 'collection': requiredNFT2.address, 'tokenId': 7 }], { from: player2, value: 8000000000 }),
      "EntryNotAllowed", ["msg.value not the price"]);
      await manager.buyEntry(2, 2,
        [{ 'collection': requiredNFT1.address, 'tokenId': 8 },
        { 'collection': requiredNFT2.address, 'tokenId': 7 }], { from: player2, value: web3.utils.toWei('2', 'ether') });
      
    let currentDelta = await tracker.delta()

    // check the contract received the money
    assert.equal(currentDelta, web3.utils.toWei('2', 'ether'));
  });

  it("should fail giving free entries two times to the same player", async function () {
    let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'),
    110,
    2, web3.utils.toWei('1', 'ether'),
    [{ id: 1, numEntries: 1, price: 0 }, 
    { id: 2, numEntries: 10, price: web3.utils.toWei('2', 'ether') }], 2500,
    [requiredNFT1.address, requiredNFT2.address, requiredNFT3.address], ONLY_DIRECTLY, { from: operator });
    idRaffle = resultCreation.logs[0].args.raffleId;
    expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });
    
    let result = await manager.stakeETH(idRaffle, { from: seller, value: 2 });
    expectEvent(result, 'RaffleStarted', { 'raffleId': idRaffle, 'seller': seller });
      // player 1 gets free entry
      resultMint = await requiredNFT1.mint(player1, { from: owner });
      tokenId1 = resultMint.logs[0].args.tokenId;
      resultMint = await requiredNFT2.mint(player1, { from: owner });
      tokenId2 = resultMint.logs[0].args.tokenId;
      await  manager.buyEntry(idRaffle, 1,
        [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
        { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }], { from: player1, value:0 })
   
   resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevertCustomError(manager,
      manager.buyEntry(idRaffle, 1,
        [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
        { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }], 
        { from: player1, value: 0 }),
      "EntryNotAllowed", ["Player already got free entry"]);

      // player 1 can buy paid entries
      resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT2.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
      await manager.buyEntry(idRaffle, 2,
        [{ 'collection': requiredNFT1.address, 'tokenId': tokenId1 },
        { 'collection': requiredNFT2.address, 'tokenId': tokenId2 }], { from: player1, value: web3.utils.toWei('2', 'ether') });
      
  });
});