const Token = artifacts.require("Basic721");
const Manager = artifacts.require("MockMultipleNFTGatedERC20");
const RequiredNFT1 = artifacts.require("Basic721");
const RequiredNFT2 = artifacts.require("Basic721");
const RequiredNFT3 = artifacts.require("Basic721");
const BasicERC20 = artifacts.require("BasicERC20.sol");

const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
  time,
  balance
} = require('@openzeppelin/test-helpers');

const { expectRevertCustomError } = require("custom-error-test-helper");

contract("ERC20 tests with 2 nft gating", accounts => {

  var owner = accounts[0];
  var player1 = accounts[1];
  var player2 = accounts[2];
  var player3 = accounts[3];
  var player4 = accounts[4];
  var player5 = accounts[5];
  var seller = accounts[8];
  var operator = accounts[9]
  var token;
  var manager;
  var requiredNFT1;
  var requiredNFT2;
  var requiredNFT3;
  var zeroAddress = '0x0000000000000000000000000000000000000000';
  var idRaffle;
  var basicERC20;
  const ONLY_DIRECTLY = 0;
  const ONLY_EXTERNAL_CONTRACT = 1;
  const MIXED = 2;

  before(async function () {
    // set contract instance into a variable
    token = await Token.deployed();
    manager = await Manager.deployed();
    basicERC20 = await BasicERC20.deployed();
  })

  it("Should be deployed", async () => {
    assert.notEqual(token, null);
    assert.notEqual(manager, null);
  });

  it("Should mint tokens", async function () {
    await token.mint(seller, { from: owner });
    await token.mint(seller, { from: owner });
    await token.mint(seller, { from: owner });
    await token.mint(seller, { from: owner });
    await token.mint(seller, { from: owner });
    await token.mint(seller, { from: owner });
    await token.mint(seller, { from: owner });
  });

  it("Should add operator role directly", async function () {
    const operatorHash = web3.utils.soliditySha3('OPERATOR');
    await manager.grantRole(operatorHash, operator, { from: owner });
  });

  it("The contract balance should be cero ", async function () {
    let balance = await web3.eth.getBalance(manager.address);
    //  console.log("Balance contract = " + web3.utils.fromWei(balance, 'ether'));
    assert.equal(balance, 0);
  });

  it("should create a raffle", async function () {
    requiredNFT1 = await RequiredNFT1.new();
    let result = await manager.createRaffle(1000, 10, basicERC20.address, 100, 2000,
      [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') },
      { id: 2, numEntries: 10, price: web3.utils.toWei('2', 'ether') }], 2500,
      [requiredNFT1.address], ONLY_DIRECTLY, { from: operator });
    expectEvent(result, 'RaffleCreated', { 'raffleId': '0' });
  });

  it("should show the raffle info", async function () {
    let result = await manager.raffles(0);
    console.log(JSON.stringify(result));
  });

  it("should stake asset in a raffle", async function () {
    await basicERC20.approve(manager.address, 100, { from: owner });
    let result = await manager.stake(0, { from: owner });
    expectEvent(result, 'RaffleStarted', { 'raffleId': '0', 'seller': owner });
    let balance = await basicERC20.balanceOf(manager.address);
    console.log("Balance of contract = " + balance);
  });

  it("should buy an entry on the raffle", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;

    result = await manager.buyEntry(0, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 1 entries with 2 nft restriction: " + result.receipt.gasUsed);
    expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player1 });
  });

  it("should buy another entry on the raffle", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;

    result = await manager.buyEntry(0, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 1 entries with 2 nft restriction: " + result.receipt.gasUsed);
    expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player1 });
  });

  it("should fail buying without nfts", async function () {
    await expectRevert(manager.buyEntry(0, 2, requiredNFT1.address, [], { from: player4, value: web3.utils.toWei('2', 'ether') }), "Panic: Index out of bounds");
  });

  it("should fail buying if the buyer is not the owner of the tokenId", async function () {
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    await expectRevert(manager.buyEntry(0, 2, requiredNFT1.address, [1, tokenId1],
      { from: player1, value: web3.utils.toWei('2', 'ether') }), "Not the owner of tokenId");
  });

  it("should fail if the tokenId was already used by another user", async function () {
    await requiredNFT1.transferFrom(player1, player2, 0, { from: player1 });
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    await expectRevert(manager.buyEntry(0, 2, requiredNFT1.address, [0, tokenId1], { from: player2, value: web3.utils.toWei('2', 'ether') }), "tokenId used");
  });

  it("Check wallets cap", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevertCustomError(manager,
      manager.buyEntry(0, 2, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: web3.utils.toWei('2', 'ether') }),
      "EntryNotAllowed", ["Wallet already used"]);
  });

  it("should fail if not using the right collection", async function () {
    requiredNFT2 = await RequiredNFT2.new();
    await expectRevert(manager.buyEntry(0, 2, requiredNFT2.address, [0, 1], { from: player2, value: web3.utils.toWei('2', 'ether') }), "Not in required collection");
  });

  it("should fail if player 2 has only 1 nft", async function () {
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    await expectRevert(manager.buyEntry(0, 1, requiredNFT1.address, [tokenId3], { from: player2, value: web3.utils.toWei('1', 'ether') }), "Panic: Index out of bounds");
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId4 = resultMint.logs[0].args.tokenId;
    result = await manager.buyEntry(0, 1, requiredNFT1.address, [tokenId3, tokenId4], { from: player2, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 10 entries with 2 nft restriction: " + result.receipt.gasUsed);
  });

  it("should give an entry for free", async function () {
    let result = await manager.giveBatchEntriesForFree(0, [player4], { from: operator });
    //expectEvent(result, 'FreeEntry', { 'raffleId': '0', 'buyer': player4 });
    let gasUsed = result.receipt.gasUsed;
    console.log("Gas used adding for free: " + gasUsed);
  });

  it("should give a batch of free entries free", async function () {
    let entries = await manager.getEntriesBought(0);
    let result = await manager.giveBatchEntriesForFree(0, [accounts[5], accounts[6]], { from: operator });
    entries = await manager.getEntriesBought(0);
    let gasUsed = result.receipt.gasUsed;
    console.log("Gas used adding for free: " + gasUsed);
  });

  it("should buy an entry on the raffle (player 2)", async function () {
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player2, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;

    result = await manager.buyEntry(0, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player2, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 1 entries with 2 nft restriction: " + result.receipt.gasUsed);
    expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player2 });
  });

  it("should buy 10 entry on the raffle (player 3)", async function () {
    resultMint = await requiredNFT1.mint(player3, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player3, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;

    result = await manager.buyEntry(0, 2, requiredNFT1.address, [tokenId1, tokenId2], { from: player3, value: web3.utils.toWei('2', 'ether') });
    console.log("Gas used buying 10 entries with 2 nft restriction: " + result.receipt.gasUsed);
    expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player3 });
  });

  it("should close the raffle", async function () {
    const tracker = await balance.tracker(owner);
    await time.increase(35);
    await manager.mockSetWinner(0, 2, { from: operator });

    let result = await manager.raffles(0);
    console.log(JSON.stringify(result));
    assert.equal(result.winner, player2);

    let currentDelta = await tracker.delta()
    console.log("delta " + currentDelta);
    assert.equal(currentDelta, web3.utils.toWei('4.5', 'ether'))

    // check the winner received the prize
    let winnerBalance = await basicERC20.balanceOf(player2);
    console.log("Balance of winner = " + winnerBalance);
    assert.equal(winnerBalance, 100);
  });

  it("should fail creating a raffle a non-operator", async function () {
    await expectRevert.unspecified(manager.createRaffle(1, 25, basicERC20.address, 100, 2000, [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, [], ONLY_DIRECTLY, { from: player2 }));
  });

  it("should fail staking asset in a not created raffle", async function () {
    await expectRevert.unspecified(manager.stake(1, { from: seller }));
  });

  it("should fail staking asset in a raffle not in the right status", async function () {
    await expectRevert(manager.stake(0, { from: seller }), "Raffle not CREATED");
  });

  it("should create a new raffle", async function () {
    var result = await manager.createRaffle(1000, 25, basicERC20.address, 100, 2000000000,
      [{ id: 1, numEntries: 1, price: 1000000000 },
      { id: 2, numEntries: 5, price: 8000000000 },
      { id: 3, numEntries: 25, price: 70000000000 }], 2500,
      [requiredNFT1.address], ONLY_DIRECTLY,
      { from: operator });
    expectEvent(result, 'RaffleCreated', { 'raffleId': '1' });
  });

  it("should fail buying an entry in not started raffle", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevertCustomError(manager,
      manager.buyEntry(0, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: 1000 }), "EntryNotAllowed", ["Not in ACCEPTED"]);
  });

  it("should stake asset in a raffle", async function () {
    await basicERC20.transfer(seller, 100000, { from: owner });
    await basicERC20.approve(manager.address, 100, { from: seller });
    let result = await manager.stake(1, { from: seller });
    expectEvent(result, 'RaffleStarted', { 'raffleId': '1', 'seller': seller });
    let balance = await basicERC20.balanceOf(manager.address);
    console.log("Balance of contract = " + balance);
    assert.equal(balance, "100");
  });

  it("should fail buying an entry paying below price", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevertCustomError(manager,
      manager.buyEntry(1, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: 100 }),
      "EntryNotAllowed", ["msg.value not the price"]);
  });

  it("should fail buying an entry in an ended raffle", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevertCustomError(manager,
      manager.buyEntry(0, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: 1000 }),
      "EntryNotAllowed", ["Not in ACCEPTED"]);
  });

  it("should fail buying several entries for below price", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId; await expectRevertCustomError(manager,
      manager.buyEntry(1, 2, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: 700 }),
      "EntryNotAllowed", ["msg.value not the price"]);
  });

  it("should fail buying a wrong amount of entries", async function () {
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await expectRevert.unspecified(manager.buyEntry(1, 6, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: 800 }));
  });

  it("should allow buy several entries", async function () {
    const tracker = await balance.tracker(manager.address);
    await tracker.get();
    resultMint = await requiredNFT1.mint(player4, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player4, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    await manager.buyEntry(1, 2, requiredNFT1.address, [tokenId1, tokenId2], { from: player4, value: 8000000000 });

    let currentDelta = await tracker.delta()

    // check the contract received the money
    assert.equal(currentDelta, 8000000000);
  });

  it("should fail creating a raffle with more price categories than allowed", async function () {
    await expectRevert.unspecified(manager.createRaffle(1000, 25, basicERC20.address, 1, 200000000000, [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 25, price: 70000000000 }, { id: 4, numEntries: 25, price: 70000000000 }, { id: 5, numEntries: 25, price: 70000000000 }, { id: 6, numEntries: 25, price: 70000000000 }], 2500, [], ONLY_DIRECTLY, { from: owner }));
  });

  it("should change the destination address", async function () {
    await manager.setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3', { from: owner });
  });

  it("should fail changing the destination address by a non owner", async function () {
    await expectRevert.unspecified(manager.setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3', { from: operator }));
  });

  it("should add free entries", async function () {
    let result = await manager.createRaffle(web3.utils.toWei('1', 'ether'), 25, basicERC20.address, 6, web3.utils.toWei('1', 'ether'), 
    [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
    idRaffle = result.logs[0].args.raffleId;
    await basicERC20.approve(manager.address, 6, { from: seller });
     await manager.stake(idRaffle, { from: seller });

    await manager.buyEntry(idRaffle, 1, zeroAddress, [], { from: player1, value: web3.utils.toWei('1', 'ether') });
    await manager.giveBatchEntriesForFree(idRaffle, [player3, player4, player5], { from: operator });
  });

  it("should transfer funds when the winner is from a free entry", async function () {
    let destinationWallet = await manager.destinationWallet();
    const tracker = await balance.tracker(destinationWallet);
    const currentBalance = await tracker.get();
    //  console.log("Detination wallet balance = " + currentBalance);

    const trackerSeller = await balance.tracker(seller);
    const b1 = await trackerSeller.get();

    const raffle = await manager.raffles(idRaffle);
    //   console.log("raffle amount raised =      " + raffle.amountRaised.toString());
    const funding = await manager.fundingList(6);
    //  console.log("funding =      " + JSON.stringify(funding));

    // the winner is from a free entry
    await manager.mockSetWinner(idRaffle, 2, { from: operator });
    //await manager.transferNFTAndFunds(6, 2, { from: operator })

    const tracker2 = await balance.tracker(destinationWallet);
    const currentBalance2 = await tracker2.get();
    // console.log("Destination wallet balance = " + currentBalance2);

    let currentDelta = await tracker.delta()
    //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    assert.equal(currentDelta.toString(), '250000000000000000'); // 25% of raised amount
    let sellerDelta = await trackerSeller.delta();
    const b2 = await trackerSeller.get();
    //check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    assert.equal(sellerDelta.toString(), '750000000000000000'); // 75% of raised amount - gas fees
    winnerBalance = await basicERC20.balanceOf(player4);
    console.log("Balance of winner after = " + winnerBalance);
    assert.equal(winnerBalance, "6");

  });

  it("should transfer funds when the winner is from a multiple entry", async function () {
    let result = await manager.createRaffle(web3.utils.toWei('2', 'ether'), 25, basicERC20.address, 6, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
    idRaffle = result.logs[0].args.raffleId;
    await basicERC20.approve(manager.address, 6, { from: player4 });
    await manager.stake(idRaffle, { from: player4 });

    await manager.buyEntry(idRaffle, 2, zeroAddress, [], { from: player1, value: web3.utils.toWei('2', 'ether') });
    await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
    let destinationWallet = await manager.destinationWallet();
    const tracker = await balance.tracker(destinationWallet);
    const currentBalance = await tracker.get();
    const trackerSeller = await balance.tracker(player4);
    const b1 = await trackerSeller.get();

    // the winner is from a free entry
    result = await manager.mockSetWinner(idRaffle, 2, { from: operator });

    // check the status is "CLOSING_REQUESTED"
    //  let raffle = await manager.raffles(7);
    //  assert.equal(raffle.status, 4);

    // result = await manager.transferNFTAndFunds(7, 2, { from: operator });
    // check the event is emitted
    expectEvent(result, 'RaffleEnded', { 'raffleId': idRaffle });
    // check the status is "ended"
    raffle = await manager.getRafflesEntryInfo(idRaffle);
    assert.equal(raffle.status, 5);
    
    let currentDelta = await tracker.delta()
    //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
    assert.equal(currentDelta.toString(), '500000000000000000'); // 25% of raised amount
    let sellerDelta = await trackerSeller.delta();
    const b2 = await trackerSeller.get();
    //check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
    assert.equal(sellerDelta.toString(), '1500000000000000000'); // 75% of raised amount - gas fees

  });

  it("should fail adding free entries if the status is not accepted", async function () {
    // check adding free entries to a closed raffle
    await expectRevert(manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator }), "Raffle is not in accepted");
    // check adding free entries to a raffle that has not been accepted yet
    let result = await manager.createRaffle(web3.utils.toWei('3', 'ether'), 25, basicERC20.address, 3, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
    idRaffle = result.logs[0].args.raffleId;
    expectEvent(result, 'RaffleCreated', { 'raffleId': idRaffle });

    await expectRevert(manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator }), "Raffle is not in accepted");

  });

  it("should fail canceling a raffle already closed", async function () {
    await expectRevert(manager.cancelRaffle(idRaffle -1, { from: operator }), "Wrong status");
  });

  it("should fail canceling a raffle a non-operator", async function () {
    await expectRevert.unspecified(manager.cancelRaffle(idRaffle, { from: player1 }));
  });

  it("should cancel a raffle", async function () {
    let result = await manager.createRaffle(web3.utils.toWei('5', 'ether'), 25, basicERC20.address, 7, web3.utils.toWei('2', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
    idRaffle = result.logs[0].args.raffleId;
    await basicERC20.approve(manager.address, 7, { from: seller });
    await manager.stake(idRaffle, { from: seller });
    await manager.buyEntry(idRaffle, 1, zeroAddress, [], { from: player1, value: web3.utils.toWei('1', 'ether') });

    let destinationWallet = await manager.destinationWallet();
    const tracker = await balance.tracker(destinationWallet);
    const currentBalance = await tracker.get();

    sellerBalanceBefore = await basicERC20.balanceOf(seller);

    await manager.cancelRaffle(idRaffle, { from: operator });

    // The seller received back the prize.
    sellerBalanceAfter = await basicERC20.balanceOf(seller);
    assert.equal(sellerBalanceAfter, sellerBalanceBefore.toNumber() + 7);

    // check the raffle is canceled
    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    assert.equal(raffle.status, 6); // cancellation requested   
  });

  it("should fail canceling a raffle already asked to cancel", async function () {
    await expectRevert.unspecified(manager.cancelRaffle(idRaffle, { from: player1 }));
  });

  it("should allow to buy only those who stakes certain nft", async function () {
    requiredNFT = await RequiredNFT1.new(); // not deployed as that is singleton. Using new() instead

    let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'), 25, basicERC20.address, 10, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 10, price: web3.utils.toWei('2', 'ether') }], 2500, [requiredNFT.address], ONLY_DIRECTLY, { from: operator });
    idRaffle = resultCreation.logs[0].args.raffleId;

    await basicERC20.approve(manager.address, 10, { from: seller });
    await manager.stake(idRaffle, { from: seller });

    let destinationWallet = await manager.destinationWallet();
    const tracker = await balance.tracker(destinationWallet);
    await tracker.get();

    let resultMint = await requiredNFT.mint(player1, { from: owner });
    let tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT.mint(player1, { from: owner });
    let tokenId2 = resultMint.logs[0].args.tokenId;
    // player1 has 1 nft of collection requiredToken
    result = await manager.buyEntry(idRaffle, 2, requiredNFT.address, [tokenId1, tokenId2], { from: player1, value: web3.utils.toWei('2', 'ether') });
    console.log("Gas used buying with nft restriction: " + result.receipt.gasUsed);

    await requiredNFT.mint(player2, { from: owner });
    // player2 has 1 nft of collection requiredToken

    resultMint = await requiredNFT.mint(player2, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT.mint(player2, { from: owner });
    tokenId4 = resultMint.logs[0].args.tokenId;
    let r1 = await manager.buyEntry(idRaffle, 1, requiredNFT.address, [tokenId3, tokenId4], { from: player2, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying with nft restriction: " + r1.receipt.gasUsed);

    await expectRevert(manager.buyEntry(idRaffle, 2, requiredNFT.address, [tokenId1, tokenId4], { from: player4, value: web3.utils.toWei('2', 'ether') }), "Not the owner of tokenId");

    await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });

    winnerBalanceBefore = await basicERC20.balanceOf(player1);

    // the first call did generate a winner not in MW. So now lets do another call to setWinner
    result = await manager.mockSetWinner(idRaffle, 1, { from: operator });
    let gasUsed = result.receipt.gasUsed;
    console.log("Gas used setting winner: " + gasUsed);

    // check the winner received the prize
    winnerBalanceAfter = await basicERC20.balanceOf(player1);
    assert.equal(winnerBalanceAfter, winnerBalanceBefore.toNumber() + 10);

    // check the platform received the money (25% of the raffle pool)
    let currentDelta = await tracker.delta()
    assert.equal(currentDelta.toString(), web3.utils.toWei('0.75', 'ether')); // 25% of 3 eth = 0.75 eth
  });

  it("should show the entries size", async function () {
    let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'), 25, basicERC20.address, 10, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;

    await basicERC20.approve(manager.address, 10, { from: seller });
    let result = await manager.stake(idRaffle, { from: seller });

    await manager.buyEntry(idRaffle, 2, zeroAddress, [], { from: player1, value: web3.utils.toWei('2', 'ether') });
    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    assert.equal(raffle.entriesLength, '5');

    await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });

    raffle = await manager.getRafflesEntryInfo(idRaffle);
    assert.equal(raffle.entriesLength, '7');
  });

  it("should allow a free raffle", async function () {
    let resultCreation = await manager.createRaffle(0, 25, basicERC20.address, 10, 0, [{ id: 1, numEntries: 1, price: 0 }], 0, [], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;

    await basicERC20.approve(manager.address, 10, { from: seller });
    await manager.stake(idRaffle, { from: seller });

    let r2 = await manager.buyEntry(idRaffle, 1, zeroAddress, [], { from: player2, value: web3.utils.toWei('0', 'ether') });
    await manager.buyEntry(idRaffle, 1, zeroAddress, [], { from: player4, value: web3.utils.toWei('0', 'ether') });

    await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });

    let winnerBalanceBefore = await basicERC20.balanceOf(player2);

    // the first call did generate a winner not in MW. So now lets do another call to setWinner
    result = await manager.mockSetWinner(idRaffle, 0, { from: operator });
    let gasUsed = result.receipt.gasUsed;
    // console.log("Gas used setting winner: " + gasUsed);

    let entriesData = await manager.getEntriesBought(idRaffle);
    //   console.log(JSON.stringify(entriesData));

    // check the winner received the prize
    winnerBalanceAfter = await basicERC20.balanceOf(player2);
    assert.equal(winnerBalanceAfter, winnerBalanceBefore.toNumber() + 10);

    let winner = await manager.getWinnerAddressFromRandom(idRaffle, 1);
    assert.equal(player2, winner);
  });

  it("should ask a raffle for cancellation step 1", async function () {
    let resultCreation = await manager.createRaffle(0, 25, basicERC20.address, 5, 0, [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, [], ONLY_DIRECTLY, { from: operator });
    idRaffle = resultCreation.logs[0].args.raffleId;
    console.log("RaffleID = " + idRaffle)

    await basicERC20.approve(manager.address, 5, { from: seller });
    await manager.stake(idRaffle, { from: seller });

    await manager.buyEntry(idRaffle, 1, zeroAddress, [], { from: player2, value: web3.utils.toWei('1', 'ether') });
    await manager.buyEntry(idRaffle, 2, zeroAddress, [], { from: player4, value: web3.utils.toWei('2', 'ether') });
    await manager.buyEntry(idRaffle, 1, zeroAddress, [], { from: player1, value: web3.utils.toWei('1', 'ether') });

    await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });

    let raffle = await manager.getRafflesEntryInfo(idRaffle);

    assert.equal(raffle.amountRaised, web3.utils.toWei('4', 'ether'))
   
  });

  it("should fail transferring remaining funds if the raffle is not in cancellation_requested status", async function () {
    await expectRevert(manager.transferRemainingFunds(idRaffle,{ from: operator }), "Wrong status");
  });

  it("should ask a raffle for cancellation step 2", async function () {
    await manager.cancelRaffle(idRaffle,{ from: operator })
    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    assert.equal(raffle.status, 6)
  });

  it("should reduce the amount raised for the raffle after the refund", async function () {
    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    assert.equal(raffle.amountRaised, web3.utils.toWei('4', 'ether'))
  });

  it("should fail transferring remaining funds a non operator", async function () {
    await expectRevert.unspecified(manager.transferRemainingFunds(idRaffle,{ from: player1 }), "claim time expired");
  });

  it("should transfer remaining funds (get all funds)", async function () {
    let raffle = await manager.getRafflesEntryInfo(idRaffle);
    assert.equal(raffle.status, 6)
    let result = await manager.transferRemainingFunds(idRaffle,{ from: operator });
    expectEvent(result, 'RemainingFundsTransferred', { 'raffleId': idRaffle, 'amountInWeis': web3.utils.toWei('4', 'ether') });
  });
  
  it("should allow a raffle with mixed entries at price 0 and with prices > 0", async function () {
    let resultCreation = await manager.createRaffle(0, 25, basicERC20.address, 1, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: web3.utils.toWei('0.005', 'ether') }], 0, [], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;

    await basicERC20.approve(manager.address, 1, { from: seller });
    await manager.stake(idRaffle, { from: seller });

    // Player 2 and player 4 get 1 entry for free
    await manager.buyEntry(idRaffle, 1, zeroAddress, [], { from: player2, value: web3.utils.toWei('0', 'ether') });
    await manager.buyEntry(idRaffle, 1, zeroAddress, [], { from: player4, value: web3.utils.toWei('0', 'ether') });
    // player 3 and player 5 get free entries via the operator
    await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
    // player 1 buys 10 entries for 0.005
    await manager.buyEntry(idRaffle, 2, zeroAddress, [], { from: player1, value: web3.utils.toWei('0.005', 'ether') });

    // The winner will be player2
    result = await manager.mockSetWinner(idRaffle, 0, { from: operator });
    let gasUsed = result.receipt.gasUsed;
    //    console.log("Gas used setting winner: " + gasUsed);

    let entriesData = await manager.getEntriesBought(idRaffle);
    //console.log(JSON.stringify(entriesData));

    // check the winner received the NFT
    currentOwner = await token.ownerOf(1);

    let winner = await manager.getWinnerAddressFromRandom(idRaffle, 1);
    //   console.log("current owner " + currentOwner + " winner " + winner)
    assert.equal(winner, player2);
  });

  it("should allow buy directly only if raffle allows that", async function () {
    let resultMint = await token.mint(seller, { from: owner });
    let tokenId = resultMint.logs[0].args.tokenId;

    let resultCreation = await manager.createRaffle(0, 25, basicERC20.address, tokenId, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: web3.utils.toWei('0.005', 'ether') }], 0, [], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;

    await basicERC20.approve(manager.address, tokenId, { from: seller });
    await manager.stake(idRaffle, { from: seller });

    // Player 2 and player 4 can buy without problems
    await manager.buyEntry(idRaffle, 1, zeroAddress, [], { from: player2, value: web3.utils.toWei('0', 'ether') });
    await manager.buyEntry(idRaffle, 2, zeroAddress, [], { from: player4, value: web3.utils.toWei('0.005', 'ether') });
    // player 3 and player 5 get free entries via the operator without problems
    await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
    // using external contract fails
  });

  
  it("wallet cap == 0 means no wallet cap", async function () {
    requiredNFT1 = await RequiredNFT1.new(); // not deployed as that is singleton. Using new() instead

    let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'), 25, basicERC20.address, 1, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 10, price: web3.utils.toWei('2', 'ether') }], 2500, [requiredNFT1.address], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;

    await basicERC20.approve(manager.address, 1, { from: seller });
    await manager.stake(idRaffle, { from: seller });

    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    // player1 has 1 nft (tokenId=0) of collection requiredToken. Buy 10 entries
    result = await manager.buyEntry(idRaffle, 2, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: web3.utils.toWei('2', 'ether') });
    console.log("Gas used buying 10 entries with nft restriction: " + result.receipt.gasUsed);

    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId4 = resultMint.logs[0].args.tokenId;

    let r1 = await manager.buyEntry(idRaffle, 1, requiredNFT1.address, [tokenId3, tokenId4], { from: player1, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 1 entry with nft restriction: " + r1.receipt.gasUsed);
  });

  it("wallet cap == 2 means can call 2 times but fail a third time", async function () {
    requiredNFT1 = await RequiredNFT1.new(); // not deployed as that is singleton. Using new() instead

    let resultMint = await token.mint(seller, { from: owner });
    let tokenId = resultMint.logs[0].args.tokenId;

    let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'), 2, basicERC20.address, tokenId, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 10, price: web3.utils.toWei('2', 'ether') }], 2500, [requiredNFT1.address], ONLY_DIRECTLY, { from: operator });
     idRaffle = resultCreation.logs[0].args.raffleId;

    await basicERC20.approve(manager.address, tokenId, { from: seller });
    await manager.stake(idRaffle, { from: seller });

    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    // player1 has 1 nft (tokenId=0) of collection requiredToken. Buy 10 entries
    result = await manager.buyEntry(idRaffle, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 10 entries with nft restriction: " + result.receipt.gasUsed);

    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId4 = resultMint.logs[0].args.tokenId;
    await manager.buyEntry(idRaffle, 1, requiredNFT1.address, [tokenId3, tokenId4], { from: player1, value: web3.utils.toWei('1', 'ether') });

    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId = resultMint.logs[0].args.tokenId;

    await expectRevertCustomError(manager,
      manager.buyEntry(idRaffle, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: web3.utils.toWei('1', 'ether') }),
      "EntryNotAllowed", ["Wallet already used"]);
  });

  it("param collection is in whitelist", async function () {
    requiredNFT1 = await RequiredNFT1.new(); // not deployed as that is singleton. Using new() instead

    let resultMint = await token.mint(seller, { from: owner });
    let tokenId = resultMint.logs[0].args.tokenId;

    let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'), 25, basicERC20.address, 5, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 10, price: web3.utils.toWei('2', 'ether') }], 2500,
      [requiredNFT1.address], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;

    await basicERC20.approve(manager.address, 5, { from: seller });
    await manager.stake(idRaffle, { from: seller });

    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    // player1 has 1 nft (tokenId=0) of collection requiredToken. Buy 10 entries
    result = await manager.buyEntry(idRaffle, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: web3.utils.toWei('1', 'ether') });
    console.log("Gas used buying 10 entries with nft restriction: " + result.receipt.gasUsed);

    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId4 = resultMint.logs[0].args.tokenId;
    await manager.buyEntry(idRaffle, 1, requiredNFT1.address, [tokenId3, tokenId4], { from: player1, value: web3.utils.toWei('1', 'ether') });

    requiredNFT3 = await RequiredNFT3.new();
    resultMint = await requiredNFT3.mint(player1, { from: owner });
    tokenId5 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT3.mint(player1, { from: owner });
    tokenId6 = resultMint.logs[0].args.tokenId;

    await expectRevert(manager.buyEntry(idRaffle, 1, requiredNFT3.address, [tokenId5, tokenId6], { from: player1, value: web3.utils.toWei('1', 'ether') }),
      "Not in required collection");

    await expectRevert(manager.buyEntry(idRaffle, 1, zeroAddress, [tokenId], { from: player1, value: web3.utils.toWei('1', 'ether') }),
      "Not in required collection");
  });

  it("should fail giving free entries two times to the same player", async function () {
    requiredNFT1 = await RequiredNFT1.new(); // not deployed as that is singleton. Using new() instead
    let resultMint = await token.mint(seller, { from: owner });
    let tokenId = resultMint.logs[0].args.tokenId;

    let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'), 12, basicERC20.address, tokenId, web3.utils.toWei('1', 'ether'),
      [{ id: 1, numEntries: 1, price: 0 },
      { id: 2, numEntries: 10, price: web3.utils.toWei('0.005', 'ether') }]
      , 2500, [requiredNFT1.address], ONLY_DIRECTLY, { from: operator });
    let idRaffle = resultCreation.logs[0].args.raffleId;

    await basicERC20.approve(manager.address, 12, { from: seller });
    await manager.stake(idRaffle, { from: seller });

    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId1 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId2 = resultMint.logs[0].args.tokenId;
    // free entry without problems for player 1
    await manager.buyEntry(idRaffle, 1, requiredNFT1.address, [tokenId1, tokenId2], { from: player1, value: 0 });

    // Player 1 can buy entries (paying)
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId3 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId4 = resultMint.logs[0].args.tokenId;
    manager.buyEntry(idRaffle, 2, requiredNFT1.address, [tokenId3, tokenId4],
      { from: player1, value: web3.utils.toWei('0.005', 'ether') });

    // player 1 cannot claim more free entries
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId5 = resultMint.logs[0].args.tokenId;
    resultMint = await requiredNFT1.mint(player1, { from: owner });
    tokenId6 = resultMint.logs[0].args.tokenId;
    await expectRevertCustomError(manager,
      manager.buyEntry(idRaffle, 1, requiredNFT1.address, [tokenId5, tokenId6], { from: player1, value: 0 })
      , "EntryNotAllowed", ["Player already got free entry"]);
      
  });


});