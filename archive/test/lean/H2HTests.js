const MockH2HLean = artifacts.require("MockH2HLean");

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
    balance
} = require('@openzeppelin/test-helpers');

const { expectRevertCustomError } = require("custom-error-test-helper");

contract("H2H lean contract tests", accounts => {

    var owner = accounts[0];
    var player1 = accounts[1];
    var player2 = accounts[2];
    var player3 = accounts[3];
    var player4 = accounts[4];
    var player5 = accounts[5];
    var seller = accounts[8];
    var operator = accounts[9]
    var manager;
    var zeroAddress = '0x0000000000000000000000000000000000000000';
    var H2H;
    const ONLY_DIRECTLY = 0;
    const ONLY_EXTERNAL_CONTRACT = 1;
    const MIXED = 2;
    const UNFULFILLED = 7;
    const ENDED = 5;
    const CANCELLED = 3;

    before(async function () {
        // set contract instance into a variable        
        H2H = await MockH2HLean.deployed();
    })

    it("Should add operator role to ETH-prize raffles", async function () {
        const operatorHash = web3.utils.soliditySha3('OPERATOR');
        await H2H.grantRole(operatorHash, operator, { from: owner });
    });

    it("should create an ETH-prized raffle", async function () {
        let result = await H2H.createH2HRaffle(web3.utils.toWei('1', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'),
            ONLY_DIRECTLY, 2, { from: operator });
        expectEvent(result, 'RaffleCreated', { 'raffleId': '0' });
    });
    it("should buy 2 entries on the raffle", async function () {
        let result = await H2H.buyEntry(0, { from: player3, value: web3.utils.toWei('0.55', 'ether') });

        expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player3, 'currentSize': '1'});
        let gasUsed = result.receipt.gasUsed;
        console.log("Gas used buying 1st player: " + gasUsed);

        result = await H2H.buyEntry(0, { from: player2, value: web3.utils.toWei('0.55', 'ether') });
        expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player2, 'currentSize': '2'});
        gasUsed = result.receipt.gasUsed;
        console.log("Gas used buying 2nd player: " + gasUsed);
    });

    it("should fail paying less than set in the raffle", async function () {
        await expectRevertCustomError(H2H, H2H.buyEntry(0, { from: player3, value: web3.utils.toWei('0.15', 'ether') }), "EntryNotAllowed", ["Price not reached"]);
    });

    it("should close the raffle h2h with a winner", async function () {
        const trackerSeller = await balance.tracker(player1);
        const trackerWinner = await balance.tracker(player2);
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        trackerWinner.get();
        await trackerSeller.get();

        await time.increase(35);
        let result = await H2H.mockSetWinner(0, 1, { from: operator });
        expectEvent(result, 'RaffleEnded', { 'raffleId': '0', 'amountRaised': web3.utils.toWei('1.1', 'ether') });
        expectEvent(result, 'FeeTransferredToPlatform', { 'amountTransferred': web3.utils.toWei('0.1', 'ether') });

        let gasUsed = result.receipt.gasUsed;
        console.log("Gas used closing with winner: " + gasUsed);

        let deltaWinner = await trackerWinner.delta();
        //    console.log("deltawinner " + deltaWinner);
        assert.equal(deltaWinner, web3.utils.toWei('1', 'ether'))

        let deltaDestinationWallet = await trackerDestinationWallet.delta();
        //     console.log("deltaDestinationWallet " + deltaDestinationWallet);
        assert.equal(deltaDestinationWallet, web3.utils.toWei('0.1', 'ether'))        
    });
   
    it("should fail buying an entry after draw", async function () {
        await expectRevertCustomError(H2H,
            H2H.buyEntry(0, { from: player1, value: web3.utils.toWei('0.55', 'ether') }),
          "EntryNotAllowed", ["Not in ACCEPTED"]);
      });

    it("should create another h2h. Only 1 buyer. Get failure amount fails because no balance, but do it well after being funded", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('1', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'),
            ONLY_DIRECTLY, 2, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        let result = await H2H.buyEntry(idRaffle, { from: player3, value: web3.utils.toWei('0.55', 'ether') });
        expectEvent(result, 'EntrySold', { 'raffleId': idRaffle, 'buyer': player3, 'currentSize': '1' });
        let gasUsed = result.receipt.gasUsed;
        console.log("Gas used buying: " + gasUsed);

        const trackerWinner = await balance.tracker(player3);
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        await trackerWinner.get();

        await time.increase(35);
        // Fails because no balance enough
        await expectRevert(H2H.mockSetWinner(idRaffle, [1], { from: operator }), "Not enough balance");
        let balanceH2H = await web3.eth.getBalance(H2H.address)
        console.log("Contract H2H balance Before = " + balanceH2H);
        await web3.eth.sendTransaction({ from: accounts[1], to: H2H.address, value: web3.utils.toWei("1", "ether") });

        balanceH2H = await web3.eth.getBalance(H2H.address)
        console.log("Contract H2H balance after  = " + balanceH2H);
        // now the h2h contract is funded, setwinner can work
        let resultWin = await H2H.mockSetWinner(idRaffle, [1], { from: operator });
        gasUsed = resultWin.receipt.gasUsed;
        console.log("Gas used closing with failure amount: " + gasUsed);

        let raffle = await H2H.rafflesEntryInfo(idRaffle);
        ///console.log(JSON.stringify(raffle))
        expectEvent(resultWin, 'RaffleUnfulfilled', { 'raffleId': idRaffle });

        let deltaWinner = await trackerWinner.delta();
        //    console.log("deltawinner " + deltaWinner);
        assert.equal(deltaWinner, web3.utils.toWei('0.6', 'ether'))

    });

    it("should create a 4 H2H raffle, and close it with a winner", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('2', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'),
            ONLY_DIRECTLY, 4, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });

        await H2H.buyEntry(idRaffle, { from: player1, value: web3.utils.toWei('0.55', 'ether') });
        await H2H.buyEntry(idRaffle, { from: player2, value: web3.utils.toWei('0.55', 'ether') });
        await H2H.buyEntry(idRaffle, { from: player3, value: web3.utils.toWei('0.55', 'ether') });
        // assert only one entry per player is allowed
   //     await expectRevert(H2H.buyEntry(idRaffle, { from: player3, value: web3.utils.toWei('0.55', 'ether') }), "Bought too many entries");
        let result = await H2H.buyEntry(idRaffle, { from: player4, value: web3.utils.toWei('0.55', 'ether') });
        let gasUsed = result.receipt.gasUsed;
        console.log("Gas used buying player #4: " + gasUsed);
        // assert only 4 entries are sold in a 4 H2H competition 
        await expectRevertCustomError(H2H, H2H.buyEntry(idRaffle, { from: player3, value: web3.utils.toWei('0.55', 'ether') }), "EntryNotAllowed", ["Total Cap Entries reached"]);

        const trackerWinner = await balance.tracker(player3);
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        await trackerWinner.get();

        let resultWin = await H2H.mockSetWinner(idRaffle, [2], { from: operator });
        expectEvent(resultWin, 'RaffleEnded', { 'raffleId': idRaffle, 'amountRaised': web3.utils.toWei('2.2', 'ether') });
        expectEvent(resultWin, 'FeeTransferredToPlatform', { 'amountTransferred': web3.utils.toWei('0.2', 'ether') });

        let deltaWinner = await trackerWinner.delta();
        assert.equal(deltaWinner, web3.utils.toWei('2', 'ether'))

        let deltaDestinationWallet = await trackerDestinationWallet.delta();
        assert.equal(deltaDestinationWallet, web3.utils.toWei('0.2', 'ether'))

        const raffle = await H2H.rafflesEntryInfo(idRaffle);
        assert.equal(raffle.status, ENDED);
    });

    it("should create a 4 H2H raffle, and cancel it if only 3 players exists (SCENARIO 2)", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('2', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'),
            ONLY_DIRECTLY, 4, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });

        await H2H.buyEntry(idRaffle, { from: player1, value: web3.utils.toWei('0.55', 'ether') });
        await H2H.buyEntry(idRaffle, { from: player2, value: web3.utils.toWei('0.55', 'ether') });
        await H2H.buyEntry(idRaffle, { from: player3, value: web3.utils.toWei('0.55', 'ether') });

        const trackerWinner1 = await balance.tracker(player1);
        const trackerWinner2 = await balance.tracker(player2);
        const trackerWinner3 = await balance.tracker(player3);
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        await trackerWinner1.get();
        await trackerWinner2.get();
        await trackerWinner3.get();

        let resultWin = await H2H.mockSetWinner(idRaffle, [2], { from: operator });
        expectEvent(resultWin, 'RaffleUnfulfilled', {
            'raffleId': idRaffle,
            'winner': player1, 'unfunfilledAmount': web3.utils.toWei('0.6', 'ether'),
            'amountRaised': web3.utils.toWei('1.65', 'ether')
        });
        const entryInfo = await H2H.rafflesEntryInfo(idRaffle);
        const raffle = await H2H.raffles(idRaffle);
        assert.equal(entryInfo.status, UNFULFILLED)
        assert.equal(raffle.winner, player1)

        let deltaWinner1 = await trackerWinner1.delta();
        assert.equal(deltaWinner1, web3.utils.toWei('0.6', 'ether'));
        let deltaWinner2 = await trackerWinner2.delta();
        assert.equal(deltaWinner2, web3.utils.toWei('0.55', 'ether'));
        let deltaWinner3 = await trackerWinner3.delta();
        assert.equal(deltaWinner3, web3.utils.toWei('0.55', 'ether'));

        let deltaDestinationWallet = await trackerDestinationWallet.delta();
        assert.equal(deltaDestinationWallet, web3.utils.toWei('0', 'ether'))

    });

    it("should create a 4 H2H raffle, but nobody boughts (SCENARIO 3)", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('2', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'),
            ONLY_DIRECTLY, 4, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });
        
        await expectRevert(H2H.mockSetWinner(idRaffle, 2, { from: operator }), "No participants in the raffle");

        let resultCancel = await H2H.cancelRaffle(idRaffle, { from: operator });
        expectEvent(resultCancel, 'RaffleCancelled', {
            'raffleId': idRaffle,
            'amountRaised': web3.utils.toWei('0', 'ether')
        });
        const raffle = await H2H.rafflesEntryInfo(idRaffle);
        assert.equal(raffle.status, CANCELLED)
    });

    it("should withdraw", async function () {
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        const trackerContract = await balance.tracker(H2H.address);
        await trackerContract.get();

        await H2H.withdraw(web3.utils.toWei('0.01', 'ether'), { from: operator });

        let deltaDestinationWallet = await trackerDestinationWallet.delta();
        assert.equal(deltaDestinationWallet, web3.utils.toWei('0.01', 'ether'))
    });

    it("should create a 2 H2H raffle, and set a winner if only 1 player exists (SCENARIO 2)", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('2', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'),
            ONLY_DIRECTLY, 2, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });

        await H2H.buyEntry(idRaffle, { from: player1, value: web3.utils.toWei('0.55', 'ether') });

        const trackerWinner1 = await balance.tracker(player1);
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        await trackerWinner1.get();

        let resultWin = await H2H.mockSetWinner(idRaffle, [2], { from: operator });
        expectEvent(resultWin, 'RaffleUnfulfilled', {
            'raffleId': idRaffle,
            'winner': player1, 'unfunfilledAmount': web3.utils.toWei('0.6', 'ether')
        });
        const entryInfo = await H2H.rafflesEntryInfo(idRaffle);
        const raffle = await H2H.raffles(idRaffle);
        assert.equal(entryInfo.status, UNFULFILLED)
        assert.equal(raffle.winner, player1)

        let deltaWinner1 = await trackerWinner1.delta();
        assert.equal(deltaWinner1, web3.utils.toWei('0.6', 'ether'));

        let deltaDestinationWallet = await trackerDestinationWallet.delta();
        assert.equal(deltaDestinationWallet, web3.utils.toWei('0', 'ether'))

    });

    it("should cancel a 2 H2H raffle (SCENARIO 2)", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('2', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'),
            ONLY_DIRECTLY, 2, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });

        await H2H.buyEntry(idRaffle, { from: player1, value: web3.utils.toWei('0.55', 'ether') });

        const trackerWinner1 = await balance.tracker(player1);
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        await trackerWinner1.get();

        let resultCancel = await H2H.cancelRaffle(idRaffle, { from: operator });
        expectEvent(resultCancel, 'RaffleCancelled', {
            'raffleId': idRaffle,
            'amountRaised': web3.utils.toWei('0.55', 'ether')
        });
        const raffle = await H2H.rafflesEntryInfo(idRaffle);
        assert.equal(raffle.status, CANCELLED)

        // Player get funds back
        let deltaWinner1 = await trackerWinner1.delta();
        assert.equal(deltaWinner1, web3.utils.toWei('0.55', 'ether'));

        let deltaDestinationWallet = await trackerDestinationWallet.delta();
        assert.equal(deltaDestinationWallet, web3.utils.toWei('0', 'ether'))
    });

    it("should cancel a 2 H2H raffle without purchases (SCENARIO 3)", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('2', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'),
            ONLY_DIRECTLY, 2, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });

        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();

        let resultCancel = await H2H.cancelRaffle(idRaffle, { from: operator });
        expectEvent(resultCancel, 'RaffleCancelled', {
            'raffleId': idRaffle,
            'amountRaised': web3.utils.toWei('0', 'ether')
        });
        const raffle = await H2H.rafflesEntryInfo(idRaffle);
        assert.equal(raffle.status, CANCELLED)

        let deltaDestinationWallet = await trackerDestinationWallet.delta();
        assert.equal(deltaDestinationWallet, web3.utils.toWei('0', 'ether'))
    });

    it("Test: 1 H2H raffle, and close it with a winner", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('0.5', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'),
            ONLY_DIRECTLY, 1, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });

        await H2H.buyEntry(idRaffle, { from: player1, value: web3.utils.toWei('0.55', 'ether') });

        const trackerWinner = await balance.tracker(player1);
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        await trackerWinner.get();

        let resultWin = await H2H.mockSetWinner(idRaffle, [2], { from: operator });
        expectEvent(resultWin, 'RaffleEnded', { 'raffleId': idRaffle, 'amountRaised': web3.utils.toWei('0.55', 'ether') });
        expectEvent(resultWin, 'FeeTransferredToPlatform', { 'amountTransferred': web3.utils.toWei('0.05', 'ether') });

        let deltaWinner = await trackerWinner.delta();
        assert.equal(deltaWinner, web3.utils.toWei('0.5', 'ether'))

        let deltaDestinationWallet = await trackerDestinationWallet.delta();
        assert.equal(deltaDestinationWallet, web3.utils.toWei('0.05', 'ether'))

    });

    it("Test Harry issues", async function () {
        let resultCreation = await H2H.createH2HRaffle("2000000000000000",
            "2000000000000000",
            "1000000000000000",           
            0,
            2, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });

        await H2H.buyEntry(idRaffle, { from: player1, value: "1000000000000000" });

        let resultWin = await H2H.mockSetWinner(idRaffle, [3], { from: operator });

        expectEvent(resultWin, 'RaffleUnfulfilled', {
            'raffleId': idRaffle,
            'winner': player1, 'unfunfilledAmount': "2000000000000000"
        });
        const entryInfo = await H2H.rafflesEntryInfo(idRaffle);
        const raffle = await H2H.raffles(idRaffle);
        assert.equal(entryInfo.status, UNFULFILLED)
        assert.equal(raffle.winner, player1)
        assert.equal(raffle.randomNumber, 1)
    });
    
    it("should create a 2 H2H raffle, and close it with the second purchaser as winner", async function () {
        let resultCreation = await H2H.createH2HRaffle(web3.utils.toWei('1', 'ether'),
            web3.utils.toWei('0.6', 'ether'),
            web3.utils.toWei('0.55', 'ether'),
            ONLY_DIRECTLY, 2, { from: operator });
        let idRaffle = resultCreation.logs[0].args.raffleId;

        expectEvent(resultCreation, 'RaffleCreated', { 'raffleId': idRaffle });

        await H2H.buyEntry(idRaffle, { from: player1, value: web3.utils.toWei('0.55', 'ether') });
        await H2H.buyEntry(idRaffle, { from: player2, value: web3.utils.toWei('0.55', 'ether') });
   
        const trackerWinner = await balance.tracker(player2);
        const destinationWallet = await H2H.destinationWallet();
        const trackerDestinationWallet = await balance.tracker(destinationWallet);
        await trackerDestinationWallet.get();
        await trackerWinner.get();

        let resultWin = await H2H.mockSetWinner(idRaffle, 3, { from: operator });
      
        expectEvent(resultWin, 'RaffleEnded', { 'raffleId': idRaffle, 'amountRaised': web3.utils.toWei('1.1', 'ether') });
        expectEvent(resultWin, 'FeeTransferredToPlatform', { 'amountTransferred': web3.utils.toWei('0.1', 'ether') });

        const entryInfo = await H2H.rafflesEntryInfo(idRaffle);
        const raffle = await H2H.raffles(idRaffle); 
        assert.equal(entryInfo.status, ENDED);
        assert.equal(raffle.winner, player2);

        let deltaWinner = await trackerWinner.delta();
        assert.equal(deltaWinner, web3.utils.toWei('1', 'ether'))

        let deltaDestinationWallet = await trackerDestinationWallet.delta();
        assert.equal(deltaDestinationWallet, web3.utils.toWei('0.1', 'ether'))        
    });

});
