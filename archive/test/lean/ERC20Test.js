const Manager = artifacts.require("MockERC20Lean");
const BasicERC20 = artifacts.require("BasicERC20.sol");
const ReentrancyAttacker = artifacts.require("ReentrancyAttacker");

const {
  BN,           // Big Number support
  constants,    // Common constants, like the zero address and largest integers
  expectEvent,  // Assertions for emitted events
  expectRevert, // Assertions for transactions that should fail
  time,
  balance
} = require('@openzeppelin/test-helpers');

const { expectRevertCustomError } = require("custom-error-test-helper");

contract("ERC20 tests with gating", accounts => {

  var owner = accounts[0];
  var player1 = accounts[1];
  var player2 = accounts[2];
  var player3 = accounts[3];
  var player4 = accounts[4];
  var player5 = accounts[5];
  var seller = accounts[8];
  var operator = accounts[9]
  var manager;
  var zeroAddress = '0x0000000000000000000000000000000000000000';
  var basicERC20;
  var reentrancyAttacker;
  const ONLY_DIRECTLY = 0;
  const ONLY_EXTERNAL_CONTRACT = 1;
  const MIXED = 2;
  var currentIdRaffle;

  before(async function () {
    manager = await Manager.deployed();
    basicERC20 = await BasicERC20.deployed();
    reentrancyAttacker = await ReentrancyAttacker.deployed();
  })

  it("Should be deployed", async () => {
    assert.notEqual(manager, null);
  });

  it("Should add operator role directly", async function () {
    const operatorHash = web3.utils.soliditySha3('OPERATOR');
    await manager.grantRole(operatorHash, operator, { from: owner });
  });

  it("The contract balance should be cero ", async function () {
    let balance = await web3.eth.getBalance(manager.address);
    //  console.log("Balance contract = " + web3.utils.fromWei(balance, 'ether'));
    assert.equal(balance, 0);
  });

  it("should create a raffle", async function () {
    let result = await manager.createRaffle(1000, basicERC20.address, 100, 2000,
      [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, ONLY_DIRECTLY, { from: operator });
    expectEvent(result, 'RaffleCreated', { 'raffleId': '0' });
  });

  it("should show the raffle info", async function () {
    let result = await manager.raffles(0);
    console.log(JSON.stringify(result));
  });

  it("should stake asset in a raffle", async function () {
    await basicERC20.approve(manager.address, 100, { from: owner });
    let result = await manager.stake(0, { from: owner });
    expectEvent(result, 'RaffleStarted', { 'raffleId': '0', 'seller': owner });
    let balance = await basicERC20.balanceOf(manager.address);
    console.log("Balance of contract = " + balance);
  });

  it("should buy an entry on the raffle", async function () {
    let result = await manager.buyEntry(0, 1, { from: player1, value: 1000 });
    expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player1 });
  });

  it("should buy an entry on the raffle", async function () {
    let result = await manager.buyEntry(0, 1, { from: player1, value: 1000 });
    expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player1 });
  });

  it("should buy an entry on the raffle", async function () {
    let result = await manager.buyEntry(0, 1, { from: player2, value: 1000 });
    expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player2 });
  });

  it("should buy an entry on the raffle", async function () {
    let result = await manager.buyEntry(0, 1, { from: player3, value: 1000 });
    expectEvent(result, 'EntrySold', { 'raffleId': '0', 'buyer': player3, 'currentSize': '4', 'priceStructureId': '1' });
    let gasUsed = result.receipt.gasUsed;
    console.log("Gas used buying: " + gasUsed);
  });

  it("should give an entry for free", async function () {
    let result = await manager.giveBatchEntriesForFree(0, [player4], { from: operator });
    //expectEvent(result, 'FreeEntry', { 'raffleId': '0', 'buyer': player4 });
    let gasUsed = result.receipt.gasUsed;
    console.log("Gas used adding for free: " + gasUsed);
  });

  it("should give a batch of free entries free", async function () {
    let entries = await manager.getEntriesBought(0);
    //   console.log(JSON.stringify(entries));

    let result = await manager.giveBatchEntriesForFree(0, [accounts[5], accounts[6]], { from: operator });

    entries = await manager.getEntriesBought(0);
    //    console.log(JSON.stringify(entries));

    let gasUsed = result.receipt.gasUsed;
    console.log("Gas used adding for free: " + gasUsed);
  });

  it("should show the raffle info", async function () {
    let result = await manager.fundingList(0);
    //  console.log(JSON.stringify(result));
    assert.equal(result.desiredFundsInWeis, '1000');
  });

  it("should close the raffle", async function () {
    const tracker = await balance.tracker(owner);
    let winnerBalanceBalance = await basicERC20.balanceOf(player2);
    //  let currentBalance1 = await tracker.get();
    //  console.log ("current balance1 " + currentBalance1);
    await time.increase(35);
    await manager.mockSetWinner(0, 2, { from: operator });

    let result = await manager.raffles(0);
    console.log(JSON.stringify(result));
    assert(result.winner, player2);

    // check the winner received the prize
    let winnerBalance = await basicERC20.balanceOf(player2);
    console.log("Balance of winner = " + winnerBalance);
    assert.equal(winnerBalance, "100");
    assert.equal(winnerBalance - winnerBalanceBalance, "100");
    // check the seller received the money (75% of the raffle pool)
    let currentDelta = await tracker.delta()
    console.log("delta " + currentDelta);
    assert.equal(currentDelta, 3000); // 75% of 4000 = 3000
  });

  it("should fail buying an entry after draw", async function () {
    await expectRevertCustomError(manager,
      manager.buyEntry(0, 1, { from: player1, value: web3.utils.toWei('0.05', 'ether') }),
      "EntryNotAllowed", ["Not in ACCEPTED"]);
  });

  it("should fail creating a raffle a non-operator", async function () {
    await expectRevert.unspecified(manager.createRaffle(1, basicERC20.address, 100, 2000, [{ id: 1, numEntries: 1, price: 1000 }, { id: 2, numEntries: 5, price: 800 }, { id: 3, numEntries: 25, price: 700 }], 2500, ONLY_DIRECTLY, { from: player2 }));
  });

  it("should fail staking asset in a not created raffle", async function () {
    await expectRevert.unspecified(manager.stake(1, { from: seller }));
  });

  it("should fail staking asset in a raffle not in the right status", async function () {
    await expectRevert(manager.stake(0, { from: seller }), "Raffle not CREATED");
  });

  it("should create a new ruffle", async function () {
    var result = await manager.createRaffle(1000, basicERC20.address, 100, 2000000000, [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 25, price: 70000000000 }], 2500, ONLY_DIRECTLY, { from: operator });
    expectEvent(result, 'RaffleCreated', { 'raffleId': '1' });
  });

  it("should stake asset in a raffle", async function () {
    await basicERC20.transfer(seller, 100000, { from: owner });
    await basicERC20.approve(manager.address, 100, { from: seller });
    let result = await manager.stake(1, { from: seller });
    expectEvent(result, 'RaffleStarted', { 'raffleId': '1', 'seller': seller });
    let balance = await basicERC20.balanceOf(manager.address);
    console.log("Balance of contract = " + balance);
    assert.equal(balance, "100");
  });

  it("should fail buying an entry paying below price", async function () {
    await expectRevertCustomError(manager,
      manager.buyEntry(1, 1, { from: player1, value: 100 }),
      "EntryNotAllowed", ["msg.value not the price"]);
  });

  it("should fail  buying several entries for below price", async function () {
    await expectRevertCustomError(manager,
      manager.buyEntry(1, 2, { from: player1, value: 700 }),
      "EntryNotAllowed", ["msg.value not the price"]);
  });

  it("should fail buying a wrong amount of entries", async function () {
    await expectRevert.unspecified(manager.buyEntry(1, 6, { from: player1, value: 800 }));
  });

  it("should allow buy several entries", async function () {
    const tracker = await balance.tracker(manager.address);
    await tracker.get();

    await manager.buyEntry(1, 2, { from: player1, value: 8000000000 });

    let currentDelta = await tracker.delta()

    // check the contract received the money
    assert.equal(currentDelta, 8000000000);
  });
  /*
    it("should fail when calling setwinner without having reached desired funds", async function () {
      await expectRevert(manager.mockSetWinner(1, 1, { from: operator }), "Desired funds not raised");
    });
   */
  it("should fail creating a raffle with more price categories than allowed", async function () {
    await expectRevert.unspecified(manager.createRaffle(1000, basicERC20.address, 1, 200000000000, [{ id: 1, numEntries: 1, price: 1000000000 }, { id: 2, numEntries: 5, price: 8000000000 }, { id: 3, numEntries: 25, price: 70000000000 }, { id: 4, numEntries: 25, price: 70000000000 }, { id: 5, numEntries: 25, price: 70000000000 }, { id: 6, numEntries: 25, price: 70000000000 }], 2500, ONLY_DIRECTLY, { from: owner }));
  });

  it("should change the destination address", async function () {
    await manager.setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3', { from: owner });
  });

  it("should fail changing the destination address by a non owner", async function () {
    await expectRevert.unspecified(manager.setDestinationAddress('0x1544D2de126e3A4b194Cfad2a5C6966b3460ebE3', { from: operator }));
  });

  it("should add free entries", async function () {
    let result = await manager.createRaffle(web3.utils.toWei('1', 'ether'), basicERC20.address, 6, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, ONLY_DIRECTLY, { from: operator });
    let idRaffle = result.logs[0].args.raffleId;
    currentIdRaffle = idRaffle;

    await basicERC20.approve(manager.address, 6, { from: seller });
    result = await manager.stake(idRaffle, { from: seller });
    expectEvent(result, 'RaffleStarted', { 'raffleId': idRaffle, 'seller': seller });

    manager.buyEntry(idRaffle, 1, { from: player1, value: web3.utils.toWei('1', 'ether') });

    await manager.giveBatchEntriesForFree(idRaffle, [player3, player4, player5], { from: operator });
  });
  
    it("should transfer funds when the winner is from a free entry", async function () {
      let destinationWallet = await manager.destinationWallet();
      const tracker = await balance.tracker(destinationWallet);
      const currentBalance = await tracker.get();
      //  console.log("Detination wallet balance = " + currentBalance);
  
      const trackerSeller = await balance.tracker(seller);
      const b1 = await trackerSeller.get();
  
      const raffle = await manager.raffles(currentIdRaffle);
      //   console.log("raffle amount raised =      " + raffle.amountRaised.toString());
      const funding = await manager.fundingList(currentIdRaffle);
      //  console.log("funding =      " + JSON.stringify(funding));
  
      // the winner is from a free entry
      await manager.mockSetWinner(currentIdRaffle, 2, { from: operator });
     
      const tracker2 = await balance.tracker(destinationWallet);
      const currentBalance2 = await tracker2.get();
      // console.log("Destination wallet balance = " + currentBalance2);
  
      let currentDelta = await tracker.delta()
      //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
      assert.equal(currentDelta.toString(), '250000000000000000'); // 25% of raised amount
      let sellerDelta = await trackerSeller.delta();
      const b2 = await trackerSeller.get();
      //check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
      assert.equal(sellerDelta.toString(), '750000000000000000'); // 75% of raised amount - gas fees
      winnerBalance = await basicERC20.balanceOf(player4);
      console.log("Balance of winner after = " + winnerBalance);
      assert.equal(winnerBalance, "6");  
    });
  
    it("should transfer funds when the winner is from a multiple entry", async function () {
      let result = await manager.createRaffle(web3.utils.toWei('2', 'ether'), basicERC20.address, 6, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, ONLY_DIRECTLY, { from: operator });
      let idRaffle = result.logs[0].args.raffleId;
      currentIdRaffle = idRaffle;

      await basicERC20.approve(manager.address, 6, { from: player4 });
      await manager.stake(idRaffle, { from: player4 });
  
      await manager.buyEntry(idRaffle, 2, { from: player1, value: web3.utils.toWei('2', 'ether') });
      await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
      let destinationWallet = await manager.destinationWallet();
      const tracker = await balance.tracker(destinationWallet);
      const currentBalance = await tracker.get();
      const trackerSeller = await balance.tracker(player4);
      const b1 = await trackerSeller.get();
  
      // the winner is from a free entry
      result = await manager.mockSetWinner(idRaffle, 2, { from: operator });
  
      // check the status is "CLOSING_REQUESTED"
      //  let raffle = await manager.raffles(7);
      //  assert.equal(raffle.status, 4);
  
      // result = await manager.transferNFTAndFunds(7, 2, { from: operator });
      // check the event is emitted
      expectEvent(result, 'RaffleEnded', { 'raffleId': idRaffle });
      // check the status is "ended"
      raffle = await manager.getRafflesEntryInfo(idRaffle);
      assert.equal(raffle.status, 5);
      
      let currentDelta = await tracker.delta()
      //check the funds sent to the destination wallet are 25% of raised amount - the profits previously withdrawn
      assert.equal(currentDelta.toString(), '500000000000000000'); // 25% of raised amount
      let sellerDelta = await trackerSeller.delta();
      const b2 = await trackerSeller.get();
      //check the funds sent to the seller are 75% of raised amount (75% of 210000001000)
      assert.equal(sellerDelta.toString(), '1500000000000000000'); // 75% of raised amount - gas fees
    });
  
    it("should fail adding free entries if the status is not accepted", async function () {
      // check adding free entries to a closed raffle
      await expectRevert(manager.giveBatchEntriesForFree(currentIdRaffle, [player3, player5], { from: operator }), "Raffle is not in accepted");
      // check adding free entries to a raffle that has not been accepted yet
      let result = await manager.createRaffle(web3.utils.toWei('3', 'ether'), basicERC20.address, 3, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, ONLY_DIRECTLY, { from: operator });
      let idRaffle = result.logs[0].args.raffleId;
      currentIdRaffle = idRaffle;
      expectEvent(result, 'RaffleCreated', { 'raffleId': idRaffle });
  
      await expectRevert(manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator }), "Raffle is not in accepted");
  
    });
  
    it("should fail canceling a raffle already closed", async function () {
      await expectRevert(manager.cancelRaffle(currentIdRaffle - 1, { from: operator }), "Wrong status");
    });
  
    it("should fail canceling a raffle a non-operator", async function () {
      await expectRevert.unspecified(manager.cancelRaffle(currentIdRaffle, { from: player1 }));
    });
  
    it("should cancel a raffle", async function () {
      let result = await manager.createRaffle(web3.utils.toWei('5', 'ether'), basicERC20.address, 7, web3.utils.toWei('2', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, ONLY_DIRECTLY, { from: operator });
      let idRaffle = result.logs[0].args.raffleId;
      currentIdRaffle = idRaffle;
      await basicERC20.approve(manager.address, 7, { from: seller });
      await manager.stake(idRaffle, { from: seller });
      await manager.buyEntry(idRaffle, 1, { from: player1, value: web3.utils.toWei('1', 'ether') });
  
      let destinationWallet = await manager.destinationWallet();
      const tracker = await balance.tracker(destinationWallet);
      const currentBalance = await tracker.get();
  
      sellerBalanceBefore = await basicERC20.balanceOf(seller);
  
      await manager.cancelRaffle(idRaffle, { from: operator });
  
      // The seller received back the prize.
      sellerBalanceAfter = await basicERC20.balanceOf(seller);
      assert.equal(sellerBalanceAfter, sellerBalanceBefore.toNumber() + 7);
  
      // check the raffle is canceled
      let raffle = await manager.getRafflesEntryInfo(idRaffle);
      assert.equal(raffle.status, 6); // cancellation requested   
    });
  
    it("should fail canceling a raffle already asked to cancel", async function () {
      await expectRevert.unspecified(manager.cancelRaffle(currentIdRaffle, { from: player1 }));
    });
  
    it("should show the entries size", async function () {
      let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'), basicERC20.address, 10, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, ONLY_DIRECTLY, { from: operator });
      let idRaffle = resultCreation.logs[0].args.raffleId;
  
      await basicERC20.approve(manager.address, 10, { from: seller });
      let result = await manager.stake(idRaffle, { from: seller });
  
      await manager.buyEntry(idRaffle, 2, { from: player1, value: web3.utils.toWei('2', 'ether') });
      let raffle = await manager.getRafflesEntryInfo(idRaffle);
      assert.equal(raffle.entriesLength, '5');
  
      await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
  
      raffle = await manager.getRafflesEntryInfo(idRaffle);
      assert.equal(raffle.entriesLength, '7');
    });
  
    it("should allow a free raffle", async function () {
      let resultCreation = await manager.createRaffle(0, basicERC20.address, 10, 0, [{ id: 1, numEntries: 1, price: 0 }], 0, ONLY_DIRECTLY, { from: operator });
      let idRaffle = resultCreation.logs[0].args.raffleId;
  
      await basicERC20.approve(manager.address, 10, { from: seller });
      await manager.stake(idRaffle, { from: seller });
  
      let r2 = await manager.buyEntry(idRaffle, 1, { from: player2, value: web3.utils.toWei('0', 'ether') });
      await manager.buyEntry(idRaffle, 1, { from: player4, value: web3.utils.toWei('0', 'ether') });
  
      await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
  
      let winnerBalanceBefore = await basicERC20.balanceOf(player2);
  
      // the first call did generate a winner not in MW. So now lets do another call to setWinner
      result = await manager.mockSetWinner(idRaffle, 0, { from: operator });
      let gasUsed = result.receipt.gasUsed;
      // console.log("Gas used setting winner: " + gasUsed);
  
      let entriesData = await manager.getEntriesBought(idRaffle);
      //   console.log(JSON.stringify(entriesData));
  
      // check the winner received the prize
      winnerBalanceAfter = await basicERC20.balanceOf(player2);
      assert.equal(winnerBalanceAfter, winnerBalanceBefore.toNumber() + 10);
  
      let winner = await manager.getWinnerAddressFromRandom(idRaffle, 1);
      assert.equal(player2, winner);
    });
  
    it("should ask a raffle for cancellation step 1", async function () {
      let resultCreation = await manager.createRaffle(0, basicERC20.address, 5, 0, [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 5, price: web3.utils.toWei('2', 'ether') }], 2500, ONLY_DIRECTLY, { from: operator });
      let idRaffle = resultCreation.logs[0].args.raffleId;
      currentIdRaffle = idRaffle
      console.log("RaffleID = " + idRaffle)
  
      await basicERC20.approve(manager.address, 5, { from: seller });
      await manager.stake(idRaffle, { from: seller });
  
      await manager.buyEntry(idRaffle, 1, { from: player2, value: web3.utils.toWei('1', 'ether') });
      await manager.buyEntry(idRaffle, 2, { from: player4, value: web3.utils.toWei('2', 'ether') });
      await manager.buyEntry(idRaffle, 1, { from: player1, value: web3.utils.toWei('1', 'ether') });
  
      await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
  
      let raffle = await manager.getRafflesEntryInfo(idRaffle);
  
      assert.equal(raffle.amountRaised, web3.utils.toWei('4', 'ether')) 
    });
  
    it("should fail transferring remaining funds if the raffle is not in cancellation_requested status", async function () {
      await expectRevert(manager.transferRemainingFunds(currentIdRaffle, { from: operator }), "Wrong status");
    });
  
    it("should ask a raffle for cancellation step 2", async function () {
      await manager.cancelRaffle(currentIdRaffle, { from: operator })
      let raffle = await manager.getRafflesEntryInfo(currentIdRaffle);
  
      assert.equal(raffle.status, 6)
    });
   
    it("should reduce the amount raised for the raffle after the refund", async function () {
      let raffle = await manager.getRafflesEntryInfo(currentIdRaffle);
      assert.equal(raffle.amountRaised, web3.utils.toWei('4', 'ether'))
    });
  
    it("should fail transferring remaining funds a non operator", async function () {
      await expectRevert.unspecified(manager.transferRemainingFunds(currentIdRaffle, { from: player1 }), "claim time expired");
    });
  
    it("should transfer remaining funds", async function () {   
      let result = await manager.transferRemainingFunds(currentIdRaffle, { from: operator });
      expectEvent(result, 'RemainingFundsTransferred', { 'raffleId': currentIdRaffle, 'amountInWeis': web3.utils.toWei('4', 'ether') });
    });
     it("Gas cost when buying 100 entries (no required nft)", async function () {
      let resultCreation = await manager.createRaffle(web3.utils.toWei('3', 'ether'), basicERC20.address, 1, web3.utils.toWei('1', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('1', 'ether') }, { id: 2, numEntries: 10, price: web3.utils.toWei('2', 'ether') }, { id: 3, numEntries: 100, price: web3.utils.toWei('3', 'ether') }], 2500, ONLY_DIRECTLY, { from: operator });
      let idRaffle = resultCreation.logs[0].args.raffleId;
  
      await basicERC20.approve(manager.address, 1, { from: seller });
      await manager.stake(idRaffle, { from: seller });
  
      result = await manager.buyEntry(idRaffle, 2, { from: player1, value: web3.utils.toWei('2', 'ether') });
      console.log("Gas used buying 10 entries with nft restriction: " + result.receipt.gasUsed);
  
      // Buy 1 entry
      let r1 = await manager.buyEntry(idRaffle, 1, { from: player2, value: web3.utils.toWei('1', 'ether') });
      console.log("Gas used buying 1 entry with nft restriction: " + r1.receipt.gasUsed);
  
      //  Buy 100 entries
      let r2 = await manager.buyEntry(idRaffle, 3, { from: player3, value: web3.utils.toWei('3', 'ether') });
      console.log("Gas used buying 100 entries with nft restriction: " + r2.receipt.gasUsed);
  
      //  Buy 100 entries
      let r3 = await manager.buyEntry(idRaffle, 3, { from: player4, value: web3.utils.toWei('3', 'ether') });
      console.log("Gas used buying 100 entries with nft restriction: " + r2.receipt.gasUsed);
  
      //  Buy 100 entries
      let r4 = await manager.buyEntry(idRaffle, 3, { from: player5, value: web3.utils.toWei('3', 'ether') });
      console.log("Gas used buying 100 entries with nft restriction: " + r2.receipt.gasUsed);
  
      let rSetWinner = await manager.mockSetWinner(idRaffle, 11, { from: operator })
      let raffle = await manager.raffles(idRaffle);
      console.log(JSON.stringify(raffle));
      assert.equal(raffle.winner, player3);
  
      console.log("Gas used new setwinner: " + rSetWinner.receipt.gasUsed);
    });
  
    it("Gas cost when calling a lot of times getEntry (no required nft)", async function () {
      let resultCreation = await manager.createRaffle(web3.utils.toWei('0.03', 'ether'), basicERC20.address, 1, web3.utils.toWei('0.01', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('0.01', 'ether') }, { id: 2, numEntries: 10, price: web3.utils.toWei('0.02', 'ether') }, { id: 3, numEntries: 100, price: web3.utils.toWei('0.03', 'ether') }], 2500, ONLY_DIRECTLY, { from: operator });
      let idRaffle = resultCreation.logs[0].args.raffleId;
  
      await basicERC20.approve(manager.address, 1, { from: seller });
      await manager.stake(idRaffle, { from: seller });
  
      let r1 = await manager.buyEntry(idRaffle, 1, { from: player2, value: web3.utils.toWei('0.01', 'ether') });
      console.log("Gas used buying 1 entry no restriction: " + r1.receipt.gasUsed);
  
      // player1 buys 10 entries
      result = await manager.buyEntry(idRaffle, 2, { from: player1, value: web3.utils.toWei('0.02', 'ether') });
      console.log("Gas used buying 10 entries no restriction: " + result.receipt.gasUsed);
  
      // player2 buys 1 entry
  
      //  player3 buys 100 entries
      let r2 = await manager.buyEntry(idRaffle, 3, { from: player3, value: web3.utils.toWei('0.03', 'ether') });
      console.log("Gas used buying 100 entries no restriction: " + r2.receipt.gasUsed);
  
      //  player4 buys 100 entries
      let r3 = await manager.buyEntry(idRaffle, 3, { from: player4, value: web3.utils.toWei('0.03', 'ether') });
      console.log("Gas used buying 100 entries no restriction: " + r3.receipt.gasUsed);
  
      await expectRevertCustomError(manager,
        manager.buyEntry(idRaffle, 8, { from: accounts[6], value: web3.utils.toWei('0', 'ether') }),
        "EntryNotAllowed", ["Id not in raffleId"]);
  
      //  player5 buys 100 entries
      let r4 = await manager.buyEntry(idRaffle, 3, { from: player5, value: web3.utils.toWei('0.03', 'ether') });
      console.log("Gas used buying 100 entries no restriction: " + r4.receipt.gasUsed);
  
      let rSetWinner = await manager.mockSetWinner(idRaffle, 210, { from: operator })
      let raffle = await manager.raffles(idRaffle);
      //   console.log(JSON.stringify(raffle));
      assert.equal(raffle.winner, player4);
  
      console.log("Gas used new setwinner: " + rSetWinner.receipt.gasUsed);
  
      let entriesData = await manager.getEntriesBought(idRaffle);
      //  console.log(JSON.stringify(entriesData));
    });
  
    it("should cancel entries entries", async function () {
      let resultCreation = await manager.createRaffle(web3.utils.toWei('0.03', 'ether'), basicERC20.address, 1, web3.utils.toWei('0.01', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('0.01', 'ether') }, { id: 2, numEntries: 10, price: web3.utils.toWei('0.02', 'ether') }, { id: 3, numEntries: 100, price: web3.utils.toWei('0.03', 'ether') }], 2500, ONLY_DIRECTLY, { from: operator });
      let idRaffle = resultCreation.logs[0].args.raffleId;
  
      await basicERC20.approve(manager.address, 1, { from: seller });
      await manager.stake(idRaffle, { from: seller });
  
      let destinationWallet = await manager.destinationWallet();
      const tracker = await balance.tracker(destinationWallet);
      const currentBalance = await tracker.get();
  
      // player1 allowed
      let r1 = await manager.buyEntry(idRaffle, 1, { from: player1, value: web3.utils.toWei('0.01', 'ether') });
      console.log("Gas used buying 1 entry no restriction: " + r1.receipt.gasUsed);
  
      //  player3 buys 100 entries
      let r2 = await manager.buyEntry(idRaffle, 3, { from: player2, value: web3.utils.toWei('0.03', 'ether') });
      console.log("Gas used buying 100 entries no restriction: " + r2.receipt.gasUsed);
  
      let r3 = await manager.buyEntry(idRaffle, 3, { from: player3, value: web3.utils.toWei('0.03', 'ether') });
      console.log("Gas used buying 100 entries no restriction: " + r3.receipt.gasUsed);
  
      let rCancelEntry = await manager.cancelEntry(idRaffle, [2], player3, { from: operator });
      console.log("Gas used cancelEntry: " + rCancelEntry.receipt.gasUsed);
  
      // amount stored in raffle before cheater claims funds back
      let raffleData = await manager.raffles(idRaffle);
      let amountRaised1 = raffleData.amountRaised;
    
      // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2
  
      let rSetWinner = await manager.mockSetWinner(idRaffle, 200, { from: operator })
      const tracker2 = await balance.tracker(destinationWallet);
      const currentBalance2 = await tracker2.get();
      let currentDelta = await tracker.delta()
  
      raffleData = await manager.raffles(idRaffle);
      let amountRaised2 = raffleData.amountRaised;
  
      let entriesData = await manager.getEntriesBought(idRaffle);
      //   console.log(JSON.stringify(entriesData));
  
      let raffle = await manager.raffles(idRaffle);
      //   console.log(JSON.stringify(raffle));
      assert.equal(raffle.winner, player2);
  
      //    console.log("Gas used new setwinner: " + rSetWinner.receipt.gasUsed);   
    });
  
    it("should manage cancel entries as time passes", async function () {
      let resultCreation = await manager.createRaffle(web3.utils.toWei('0.03', 'ether'), basicERC20.address, 1, web3.utils.toWei('0.01', 'ether'), [{ id: 1, numEntries: 1, price: web3.utils.toWei('0.01', 'ether') }, { id: 2, numEntries: 10, price: web3.utils.toWei('0.02', 'ether') }, { id: 3, numEntries: 100, price: web3.utils.toWei('0.03', 'ether') }], 2500, ONLY_DIRECTLY, { from: operator });
      let idRaffle = resultCreation.logs[0].args.raffleId;
      await basicERC20.approve(manager.address, 1, { from: seller });
      await manager.stake(idRaffle, { from: seller });
      // player1 allowed
      let r1 = await manager.buyEntry(idRaffle, 1, { from: player1, value: web3.utils.toWei('0.01', 'ether') });
      //  player3 buys 100 entries
      let r2 = await manager.buyEntry(idRaffle, 3, { from: player2, value: web3.utils.toWei('0.03', 'ether') });
      // player1 can buy again
      let r3 = await manager.buyEntry(idRaffle, 3, { from: player1, value: web3.utils.toWei('0.03', 'ether') });
      // player 4 buys 100 entries
      await manager.buyEntry(idRaffle, 3, { from: player4, value: web3.utils.toWei('0.03', 'ether') });
      // player 5 buys 100 entries
      await manager.buyEntry(idRaffle, 3, { from: player5, value: web3.utils.toWei('0.03', 'ether') });
  
      await expectRevert(manager.cancelEntry(idRaffle, [2], player3, { from: operator }), "Entry did not belong to player");
      // test cancelEntry batch
      let result = await manager.cancelEntry(idRaffle, [0, 2], player1, { from: operator });
      expectEvent(result, 'EntryCancelled', { 'raffleId': idRaffle, 'amountOfEntriesCanceled': '101', 'player': player1 });
  
      let entriesData = await manager.getEntriesBought(idRaffle);
      assert.equal(entriesData[1].player, zeroAddress);
      assert.equal(entriesData[3].player, zeroAddress);
  
      for (let i = 0; i < 10; i++) {
        await manager.buyEntry(idRaffle, 1, { from: player2, value: web3.utils.toWei('0.01', 'ether') });
      }
  
      for (let i = 0; i < 10; i++) {
        await manager.buyEntry(idRaffle, 1, { from: player2, value: web3.utils.toWei('0.01', 'ether') });
      }
  
      // 31 days passes
      await time.increase(time.duration.days(31));
  
      // player3 is the winner, but is blacklisted, therefore the winner is the one on the left, player2
      let rSetWinner = await manager.mockSetWinner(idRaffle, 0, { from: operator })
  
      entriesData = await manager.getEntriesBought(idRaffle);
      // console.log(JSON.stringify(entriesData));
  
      let raffle = await manager.raffles(idRaffle);
      assert.equal(raffle.winner, player2);
  
    });
  
   
    it("should allow a raffle with mixed entries at price 0 and with prices > 0", async function () {
      let resultCreation = await manager.createRaffle(0, basicERC20.address, 1, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: web3.utils.toWei('0.005', 'ether') }], 0, ONLY_DIRECTLY, { from: operator });
      let idRaffle = resultCreation.logs[0].args.raffleId;
  
      await basicERC20.approve(manager.address, 1, { from: seller });
      await manager.stake(idRaffle, { from: seller });
  
      // Player 2 and player 4 get 1 entry for free
      await manager.buyEntry(idRaffle, 1, { from: player2, value: web3.utils.toWei('0', 'ether') });
      await manager.buyEntry(idRaffle, 1, { from: player4, value: web3.utils.toWei('0', 'ether') });
      // player 3 and player 5 get free entries via the operator
      await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
      // player 1 buys 10 entries for 0.005
      await manager.buyEntry(idRaffle, 2, { from: player1, value: web3.utils.toWei('0.005', 'ether') });
  
      // The winner will be player2
      result = await manager.mockSetWinner(idRaffle, 0, { from: operator });
      let gasUsed = result.receipt.gasUsed;
      //    console.log("Gas used setting winner: " + gasUsed);
  
      let entriesData = await manager.getEntriesBought(idRaffle);
      //console.log(JSON.stringify(entriesData));
  
      let winner = await manager.getWinnerAddressFromRandom(idRaffle, 1);
      //   console.log("current owner " + currentOwner + " winner " + winner)
      assert.equal(winner, player2);
    });
 
    it("should fail if player wants to buy using an older priceId", async function () {
      // the id used for the prices are 11 and 12
      let resultCreation = await manager.createRaffle(0, basicERC20.address, 10, 0, [{ id: 10, numEntries: 1, price: 0 }, { id: 11, numEntries: 10, price: web3.utils.toWei('0.005', 'ether') }], 0, MIXED, { from: operator });
      let idRaffle = resultCreation.logs[0].args.raffleId;
      await basicERC20.approve(manager.address, 10, { from: seller });
      await manager.stake(idRaffle, { from: seller });
  
      // Player 2 and player 4 get 1 entry for free
      await manager.buyEntry(idRaffle, 10, { from: player2, value: web3.utils.toWei('0', 'ether') });
      await manager.buyEntry(idRaffle, 10, { from: player4, value: web3.utils.toWei('0', 'ether') });
      // player 3 and player 5 get free entries via the operator
      await manager.giveBatchEntriesForFree(idRaffle, [player3, player5], { from: operator });
      // player 1 buys 10 entries for 0.005
      await manager.buyEntry(idRaffle, 11, { from: player1, value: web3.utils.toWei('0.005', 'ether') });
      // Fails when using and id on the price struct that did not belong to this raffle
      await expectRevertCustomError(manager, 
        manager.buyEntry(idRaffle, 1, { from: player1, value: 700 }), "EntryNotAllowed", ["Id not in raffleId"]);
    });
  
    
    it("should fail giving free entries two times to the same player", async function () {
      let resultCreation = await manager.createRaffle(0, basicERC20.address, 10, 0, [{ id: 1, numEntries: 1, price: 0 }, { id: 2, numEntries: 10, price: web3.utils.toWei('0.005', 'ether') }], 0, ONLY_DIRECTLY, { from: operator });
      let idRaffle = resultCreation.logs[0].args.raffleId;
      
      await basicERC20.approve(manager.address, 10, { from: seller });
      await manager.stake(idRaffle, { from: seller });
  
      // free entry without problems for player 1
      let result = await manager.buyEntry(idRaffle, 1, { from: player1, value: 0 });
      // Revert if player 1 wants to get a free entry again
      await expectRevertCustomError(manager, manager.buyEntry(idRaffle, 1, { from: player1, value: 0 }), "EntryNotAllowed", ["Player already got free entry"]);
      // player1 can buy other packages (not the free one)
      await manager.buyEntry(idRaffle, 2, { from: player1, value: web3.utils.toWei('0.005', 'ether') });
      await manager.buyEntry(idRaffle, 2, { from: player1, value: web3.utils.toWei('0.005', 'ether') });
      // Player 2 and player 3 can buy free package, and paid packages
      await manager.buyEntry(idRaffle, 1, { from: player2, value: 0 });
      let r1 = await manager.buyEntry(idRaffle, 2, { from: player3, value: web3.utils.toWei('0.005', 'ether') });
      let r2 = await manager.buyEntry(idRaffle, 1, { from: player3, value: 0 });
      console.log("Gas used buying free: " + result.receipt.gasUsed);
      console.log("Gas used buying paid: " + r1.receipt.gasUsed);
    });
  
 /*   it("should fail a reentrancy attack", async function () {
     // await reentrancyAttacker.attack(25, 1);
      //await expectRevertCustomError(reentrancyAttacker, reentrancyAttacker.attack(25, 1), "EntryNotAllowed", ["No contracts allowed"]);
    });
   */ 
});