// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import "../lean_dont_remove_yet/MultipleNFTGatedETHPhase2.sol";

contract MockMultipleNFTGatedETHPhase2 is MultipleNFTGatedETHPhase2 {
    constructor(
        address _vrfCoordinator,
        address _linkToken,
        bytes32 _keyHash,
        bool _mainnetFee
    )
        MultipleNFTGatedETHPhase2(
            _vrfCoordinator,
            _linkToken,
            _keyHash,
            _mainnetFee
        )
    {}

    function mockSetWinner(
        uint256 _raffleId,
        uint256 _fakeRandomNumber
    ) external onlyRole(OPERATOR_ROLE) {
        EntryInfoStruct memory raffle = setWinnerActions(_raffleId);

        bytes32 requestId = bytes32(uint256(1));
        getRandomNumber(_raffleId, raffle.entriesLength, requestId);
        fulfillRandomness(requestId, _fakeRandomNumber);
    }

     function mockEarlyCashOut(
        uint256 _raffleId,
        uint256 _fakeRandomNumber
    ) external {
        EntryInfoStruct memory raffle = setCashOutActions(_raffleId);

        bytes32 requestId = bytes32(uint256(1));
        getRandomNumber(_raffleId, raffle.entriesLength, requestId);
        fulfillRandomness(requestId, _fakeRandomNumber);
    }
}
