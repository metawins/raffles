// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import "./ERC20SingleVRF2.sol";

contract MockERC20SingleVRF2 is ERC20SingleVRF2 {
    constructor(
        uint64 subscriptionId,
        address _vrfCoordinator,
        bytes32 _keyHash
    ) ERC20SingleVRF2(subscriptionId, _vrfCoordinator, _keyHash) {}

    function mockSetWinner(
        uint256 _raffleId,
        uint256[] calldata _fakeRandomNumbers
    ) external onlyRole(OPERATOR_ROLE) {
        EntryInfoStruct memory raffle = setWinnerActions(_raffleId);

        uint256 requestId = 1; //bytes32(uint256(1));
        getRandomNumber(_raffleId, raffle.entriesLength, requestId);
        fulfillRandomWords(requestId, _fakeRandomNumbers);
    }

    function mockEarlyCashOut(
        uint256 _raffleId,
        uint256[] calldata _fakeRandomNumbers
    ) external {
        EntryInfoStruct memory raffle = setCashOutActions(_raffleId);

        uint256 requestId = 1;//bytes32(uint256(1));
        getRandomNumber(_raffleId, raffle.entriesLength, requestId);
        fulfillRandomWords(requestId, _fakeRandomNumbers);
    }
}
