// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import "../core/ERC20Comp.sol";

contract MockERC20Comp is ERC20Comp {
    constructor(
        address _vrfCoordinator,
        bytes32 _keyHash,
        uint64 _subId,
        uint16 _minimumRequestConfirmations,
        uint32 _callbackGasLimit,
        uint32 _numWords)
        payable ERC20Comp(_vrfCoordinator,
        _keyHash,
         _subId,
         _minimumRequestConfirmations,
         _callbackGasLimit,
         _numWords) {}

    function mockSetWinner(
        uint256 _raffleId,
        uint256 _fakeRandomNumber
    ) external onlyRole(OPERATOR_ROLE) {
        EntryInfoStruct memory raffle = _setWinnerActions(_raffleId);

        uint256[] memory _fakeRandomNumbers = new uint256[](_fakeRandomNumber);
         _getRandomNumber(_raffleId, raffle.entriesLength, 1);
        _fulfillRandomWords(1, _fakeRandomNumbers);
    }
}
