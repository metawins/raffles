// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.23;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract Basic721 is ERC721 {
    event TokenCreated(
        uint256 indexed tokenId,
        address indexed to
    );

    uint256 private _nextTokenId;
    constructor() ERC721("Basic721", "BAS") {}

     function mint(address to) public {
        uint256 tokenId = _nextTokenId++;
        _safeMint(to, tokenId);
        emit TokenCreated(tokenId, to);
    }
}