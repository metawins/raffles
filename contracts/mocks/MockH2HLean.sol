// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import "../core/H2HComp.sol";

contract MockH2HComp is H2HComp {
    constructor(
        address _vrfCoordinator,
        bytes32 _keyHash,
        uint64 _subId,
        uint16 _minimumRequestConfirmations,
        uint32 _callbackGasLimit,
        uint32 _numWords)
        payable H2HComp(_vrfCoordinator,
        _keyHash,
         _subId,
         _minimumRequestConfirmations,
         _callbackGasLimit,
         _numWords) {}

    function mockSetWinner(
        uint256 _raffleId,
        uint256 _fakeRandomNumber
    ) external onlyRole(OPERATOR_ROLE) {
        bool shouldDoCallback = _setWinnerActions(_raffleId);
        if (shouldDoCallback) {
            uint256 entriesSize = entriesList[_raffleId].length;
            
            uint256[] memory _fakeRandomNumbers = new uint256[](_fakeRandomNumber);
            _getRandomNumber(_raffleId, entriesSize, 1);
            _fulfillRandomWords(1, _fakeRandomNumbers);
        }
    }
}
