// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.23;

import "../core/NFTComp.sol";

contract MockNFTComp is NFTComp {
    constructor(
        address _vrfCoordinator,
        bytes32 _keyHash,
        uint64 _subId,
        uint16 _minimumRequestConfirmations,
        uint32 _callbackGasLimit,
        uint32 _numWords)
        payable NFTComp(_vrfCoordinator,
        _keyHash,
         _subId,
         _minimumRequestConfirmations,
         _callbackGasLimit,
         _numWords) {}

    function mockSetWinner(
        uint256 _raffleId,
        uint256 _fakeRandomNumber
    ) external onlyRole(OPERATOR_ROLE) {
        EntryInfoStruct memory raffle = _setWinnerActions(_raffleId);

        uint256[] memory _fakeRandomNumbers = new uint256[](_fakeRandomNumber);
         _getRandomNumber(_raffleId, raffle.entriesLength, 1);
        _fulfillRandomWords(1, _fakeRandomNumbers);
    }
}
