// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.4;

import "./H2HVRF2.sol";

contract MockH2HVRF2_1 is H2HVRF2 {
    constructor(
        address _blacklistManager,
        uint64 subscriptionId,
        address _vrfCoordinator,
        bytes32 _keyHash
    ) H2HVRF2(_blacklistManager, subscriptionId, _vrfCoordinator, _keyHash) {}

    function mockSetWinner(
        uint256 _raffleId,
        uint256[] calldata _fakeRandomNumber
    ) external onlyRole(OPERATOR_ROLE) {
        (RaffleStruct memory raffle, bool shouldDoCallback) = setWinnerActions(_raffleId);
        if (shouldDoCallback){ 
            uint256 requestId = 1;
            getRandomNumber(_raffleId, raffle.entriesLength, requestId);
            fulfillRandomWords(requestId, _fakeRandomNumber);
        }
    }
}
