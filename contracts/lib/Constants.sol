//SPDX-License-Identifier: MIT
pragma solidity ^0.8.22;

import "@openzeppelin/contracts/access/AccessControl.sol";

//import "@chainlink/contracts/src/v0.8/VRFConsumerBase.sol";

import "../interfaces/IDelegationRegistry.sol";
import "./MinimalConstants.sol";

abstract contract Constants is AccessControl, MinimalConstants {
    ////////// DELEGATE:CASH ///////////////
    IDelegationRegistry reg;

    // Main raffle data struct
    struct RaffleStruct {
        uint256 prizeNumber; // number (can be a percentage, an id, an amount, etc. depending on the competition)
        uint48 platformPercentage; // percentage of the funds raised that goes to the platform
        address prizeAddress; // address of the prize
        address winner; // address of thed winner of the raffle. Address(0) if no winner yet
        address seller; // address of the seller of the NFT
        uint256 randomNumber; // normalized (0-Entries array size) random number generated by the VRF
    }
    // The main structure is an array of raffles
    RaffleStruct[] public raffles;

    struct EntryInfoStruct {
        bool requireWhitelisting;
        STATUS status; // status of the raffle. Can be created, accepted, ended, etc
        uint48 walletsCap;
        uint48 entriesLength; // to easy frontend, the length of the entries array is saved here
        uint128 amountRaised; // funds raised so far in wei
    }
    // The main structure is an array of raffles
    EntryInfoStruct[] public rafflesEntryInfo;

    // All the different status a rafVRFCoordinatorfle can have
    enum STATUS {
        CREATED, // the operator creates the raffle
        ACCEPTED, // the seller stakes the nft for the raffle
        EARLY_CASHOUT, // the seller wants to cashout early
        CANCELLED, // the operator cancels the raffle and transfer the remaining funds after 30 days passes
        CLOSING_REQUESTED, // the operator requests to close the raffle and start the VRF
        ENDED, // the raffle is finished, and NFT and funds were transferred
        CANCEL_REQUESTED, // operator asks to cancel the raffle. Players has 30 days to ask for a refund
        WINNER_DETERMINED // the vrf sets a winner
    }

    // Event sent when the raffle is created by the operator
    event RaffleCreated(
        uint256 indexed raffleId,
        address indexed nftAddress,
        uint256 indexed nftId
    );
    // Event sent when one or more entries are sold (info from the price structure)
    event EntrySold(
        uint256 indexed raffleId,
        address indexed buyer,
        uint256 currentSize,
        uint256 priceStructureId
    );

    constructor() {
        _grantRole(OPERATOR_ROLE, 0x13503B622abC0bD30A7e9687057DF6E8c42Fb928);
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);

        // the delegation registry addess is the same in all networks:
        // https://github.com/delegatecash/delegate-registry
        reg = IDelegationRegistry(0x00000000000076A84feF008CDAbe6409d2FE638B);
    }
}
