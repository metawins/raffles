// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/**
 * @title IBlacklistable
 * @dev Interface for a contract that allows accounts to be blacklisted by a "blacklister" role.
 */
interface IBlacklistable {
    
    // Events
    event Blacklisted(address indexed account);
    event UnBlacklisted(address indexed account);
    event BlacklisterChanged(address indexed newBlacklister);

    // Functions
    /**
     * @dev Checks if account is blacklisted
     * @param _account The address to check
     * @return bool True if the account is blacklisted
     */
    function isBlacklisted(address _account) external view returns (bool);

    /**
     * @dev Adds account to blacklist
     * @param _account The address to blacklist
     */
    function blacklist(address _account) external;

    /**
     * @dev Removes account from blacklist
     * @param _account The address to remove from the blacklist
     */
    function unBlacklist(address _account) external;

    /**
     * @dev Updates the blacklister address
     * @param _newBlacklister The address of the new blacklister
     */
    function updateBlacklister(address _newBlacklister) external;
}
