// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/**
 * @title LinkTokenInterface
 * @dev Interface for the standard functions of a Link token, compliant with ERC-20.
 */
interface LinkTokenInterface {

    /**
     * @dev Returns the amount of tokens that an owner allowed to a spender.
     * @param owner The address of the token owner.
     * @param spender The address of the token spender.
     * @return remaining The remaining amount of tokens that spender is allowed to spend from owner's account.
     */
    function allowance(address owner, address spender) external view returns (uint256 remaining);

    /**
     * @dev Approves a spender to withdraw from the caller's account up to the specified amount.
     * @param spender The address of the spender.
     * @param value The amount of tokens to be approved for spending.
     * @return success True if the operation was successful.
     */
    function approve(address spender, uint256 value) external returns (bool success);

    /**
     * @dev Returns the balance of a given account.
     * @param owner The address of the account whose balance is to be queried.
     * @return balance The token balance of the specified account.
     */
    function balanceOf(address owner) external view returns (uint256 balance);

    /**
     * @dev Returns the number of decimal places the token uses.
     * @return decimalPlaces The number of decimal places for the token.
     */
    function decimals() external view returns (uint8 decimalPlaces);

    /**
     * @dev Decreases the amount of tokens that an owner allowed to a spender.
     * @param spender The address of the spender.
     * @param subtractedValue The amount by which the allowance will be decreased.
     * @return success True if the operation was successful.
     */
    function decreaseApproval(address spender, uint256 subtractedValue) external returns (bool success);

    /**
     * @dev Increases the amount of tokens that an owner allowed to a spender.
     * @param spender The address of the spender.
     * @param addedValue The amount by which the allowance will be increased.
     */
    function increaseApproval(address spender, uint256 addedValue) external;

    /**
     * @dev Returns the name of the token.
     * @return tokenName The name of the token.
     */
    function name() external view returns (string memory tokenName);

    /**
     * @dev Returns the symbol of the token.
     * @return tokenSymbol The symbol of the token.
     */
    function symbol() external view returns (string memory tokenSymbol);

    /**
     * @dev Returns the total token supply.
     * @return totalTokensIssued The total amount of tokens issued.
     */
    function totalSupply() external view returns (uint256 totalTokensIssued);

    /**
     * @dev Transfers tokens to a specified address.
     * @param to The address to transfer tokens to.
     * @param value The amount of tokens to be transferred.
     * @return success True if the transfer was successful.
     */
    function transfer(address to, uint256 value) external returns (bool success);

    /**
     * @dev Transfers tokens to a specified address and then calls a function on the recipient's address.
     * @param to The address to transfer tokens to.
     * @param value The amount of tokens to be transferred.
     * @param data Additional data with no specified format, sent in call to `to`.
     * @return success True if the transfer was successful.
     */
    function transferAndCall(
        address to,
        uint256 value,
        bytes calldata data
    ) external returns (bool success);

    /**
     * @dev Transfers tokens from one address to another.
     * @param from The address which you want to send tokens from.
     * @param to The address which you want to transfer to.
     * @param value The amount of tokens to be transferred.
     * @return success True if the transfer was successful.
     */
    function transferFrom(
        address from,
        address to,
        uint256 value
    ) external returns (bool success);
}

