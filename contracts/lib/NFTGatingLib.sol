//SPDX-License-Identifier: MIT
pragma solidity ^0.8.22;

import "../lib/MinimalConstants.sol";

abstract contract NFTGatingLib is MinimalConstants {
    function isInWhiteListCollection(
        address _collection,
        uint256 _raffleId
    ) public view {
        bytes32 collectionHash = keccak256(abi.encode(_collection, _raffleId));
        require(
            whitelistCollections[collectionHash] == true,
            "Not in required collection"
        );
    }

    function updateRequiredNFTWallets(
        address _collection,
        uint256 _raffleId,
        uint256 _tokenIdUsed,
        address _sender
    ) internal {
        bytes32 hashRequiredNFT = keccak256(
            abi.encode(_collection, _raffleId, _tokenIdUsed)
        );
        // check the tokenId has not been using yet in the raffle, to avoid abuse
        if (requiredNFTWallets[hashRequiredNFT] == address(0)) {
            requiredNFTWallets[hashRequiredNFT] = _sender;
        } else
            require(
                requiredNFTWallets[hashRequiredNFT] == _sender,
                "tokenId used"
            );
    }

    function checkCollectionsNotUsedIn( NFTIdUsed[] calldata _tokensUsed) public pure {
        require(_tokensUsed.length >= 2, "Not enough tokens staked");
            if (_tokensUsed.length == 2)
                require(
                    _tokensUsed[0].collection != _tokensUsed[1].collection,
                    "Collection already used"
                );
            else {
                for (uint256 i; i < _tokensUsed.length; ++i) {
                    // check the nft collection has not been used yet
                    bool exists = false;
                    for (uint256 j; j < i; ++j) {
                        if (
                            _tokensUsed[j].collection ==
                            _tokensUsed[i].collection
                        ) exists = true;
                    }
                    require(exists == false, "Collection already used");
                }
            }
    }
}
