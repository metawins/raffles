export interface IVRFConfig {
    vrfCoordinator: string;
    keyHash: string;
    numWords: number;
    numberOfConfirmations: number;
    gasLimit: number;
    subId: number; // Added subscription ID
}

export const zeroAddress = "0x0000000000000000000000000000000000000000";

// Constants
const SEPOLIA_VRF_COORDINATOR = "0x8103B0A8A00be2DDC778e6e7eaa21791Cd364625";
const SEPOLIA_KEYHASH = "0x474e34a077df58807dbe9c96d3c009b23b3c6d0cce433e59bbf5b34f823bc56c";
// Add SEPOLIA_SUB_ID here

const MAINNET_VRF_COORDINATOR = "0x8103B0A8A00be2DDC778e6e7eaa21791Cd364625";
const MAINNET_KEYHASH = "0xAA77729D3466CA35AE8D28B3BBAC7CC36A5031EFDC430821C02BC31A238AF445";
// Add MAINNET_SUB_ID here

const SEPOLIA_ARBITRUM_VRF_COORDINATOR = "0x50d47e4142598E3411aA864e08a44284e471AC6f";
const SEPOLIA_ARBITRUM_KEYHASH = "0x027f94ff1465b3525f9fc03e9ff7d6d2c0953482246dd6ae07570c45d6631414";
// Add SEPOLIA_ARBITRUM_SUB_ID here

const ARBITRUM_VRF_COORDINATOR = "0x41034678D6C633D8a95c75e1138A360a28bA15d1";
const ARBITRUM_KEYHASH = "0x08ba8f62ff6c40a58877a106147661db43bc58dabfb814793847a839aa03367f";
// Add ARBITRUM_SUB_ID here

// Chain IDs
const SEPOLIA_CHAIN_ID = 11155111;
const MAINNET_CHAIN_ID = 1;
const SEPOLIA_ARBITRUM_CHAIN_ID = 421614;
const ARBITRUM_CHAIN_ID = 42161;

// Common VRF settings
const NUM_WORDS = 5;
const NUMBER_OF_CONFIRMATIONS = 3;
const GAS_LIMIT = 500000;

// Define the subscription IDs for each network
const SEPOLIA_SUB_ID = Number(process.env.SUBSCRIPTION_ID)||8112;
const MAINNET_SUB_ID = 456; // Replace with actual subscription ID
const SEPOLIA_ARBITRUM_SUB_ID = 789; // Replace with actual subscription ID
const ARBITRUM_SUB_ID = 101112; // Replace with actual subscription ID

// Mapping of chain IDs to their respective VRF configurations
const chainIdToVRFConfig: { [chainId: number]: IVRFConfig } = {
    [SEPOLIA_CHAIN_ID]: {
        vrfCoordinator: SEPOLIA_VRF_COORDINATOR,
        keyHash: SEPOLIA_KEYHASH,
        numWords: NUM_WORDS,
        numberOfConfirmations: NUMBER_OF_CONFIRMATIONS,
        gasLimit: GAS_LIMIT,
        subId: SEPOLIA_SUB_ID,
    },
    [MAINNET_CHAIN_ID]: {
        vrfCoordinator: MAINNET_VRF_COORDINATOR,
        keyHash: MAINNET_KEYHASH,
        numWords: NUM_WORDS,
        numberOfConfirmations: NUMBER_OF_CONFIRMATIONS,
        gasLimit: GAS_LIMIT,
        subId: MAINNET_SUB_ID,
    },
    [SEPOLIA_ARBITRUM_CHAIN_ID]: {
        vrfCoordinator: SEPOLIA_ARBITRUM_VRF_COORDINATOR,
        keyHash: SEPOLIA_ARBITRUM_KEYHASH,
        numWords: NUM_WORDS,
        numberOfConfirmations: NUMBER_OF_CONFIRMATIONS,
        gasLimit: GAS_LIMIT,
        subId: SEPOLIA_ARBITRUM_SUB_ID,
    },
    [ARBITRUM_CHAIN_ID]: {
        vrfCoordinator: ARBITRUM_VRF_COORDINATOR,
        keyHash: ARBITRUM_KEYHASH,
        numWords: NUM_WORDS,
        numberOfConfirmations: NUMBER_OF_CONFIRMATIONS,
        gasLimit: GAS_LIMIT,
        subId: ARBITRUM_SUB_ID,
    },
};

// Function to get VRF configuration based on chain ID
export function getVRFConfig(chainId: number): IVRFConfig {
    if (chainIdToVRFConfig[chainId] === undefined) {
        throw new Error(`No VRF configuration found for chain ID ${chainId}`);
    }
    return chainIdToVRFConfig[chainId];
}
export function getConstructorArgs (_params: IVRFConfig): any[] {
    return [ _params.vrfCoordinator, _params.keyHash, _params.subId, _params.numberOfConfirmations, _params.gasLimit, _params.numWords];
}
